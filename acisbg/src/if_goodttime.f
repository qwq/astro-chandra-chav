c Maxim Markevitch <maxim % head.cfa.harvard.edu>, last change 24 Mar 07

c 24.3.7: av added data and save declarations for f95;
c         in write_gti, in_gti to be set by ftgiou instead of newunit
c 30.1.7: string lengths from 128 to 512 (also in get_param.c)
c emandel removed commas from read()/write() for compatib.
c 11.12.1: changed maxgti to 16384 (from 1024), changed printout order.
c 31.1.1: changed ftopen to ftnopn to allow filename[gti] or filename+7
c 7.7.00: this function is made from if_goodtime (file if_goodtime.f) by
c changing name; arg to real*8 tt; also, changed GTI array from integer igti
c to real*8 ggti.
c Also, separated old (int) and new (real*8) versions in different files:
c if_goodtime.f and if_goodttime.f

c 28.1.00: sub read_gti now accepts param name gti_file and gtifile.
c 15.10.95 previous change

      logical function if_goodttime (tt)

      implicit none
      integer maxgti
      parameter (maxgti=16384)
      real*8 tt, tt0, ggti(maxgti,2)
      logical ajunk, if_goodttime0, first_time
      integer ngti,i
      data tt0, if_goodttime0, first_time /-123456d0, .false., .true./
      save ggti, ngti, first_time


      if (first_time) then
        call read_ggti (ggti, maxgti, ngti, ajunk)

*        print*,' r_g ok, ngti', ngti
*        do i=1,ngti
*          print*,ggti(i,1),ggti(i,2)
*        end do 
        first_time=.false.
      end if 


c check this photon:

      if (tt.ne.tt0) then
c if the time is new, do real check:        
      
        if_goodttime=.false.
        do i=1,ngti 
          if (tt.gt.ggti(i,1) .and. tt.lt.ggti(i,2)) then
            if_goodttime=.true.
            goto 1
          end if 
        end do 
 1      continue 

        tt0=tt
        if_goodttime0=if_goodttime

      else 
c else return old value of if_goodttime:
        if_goodttime=if_goodttime0
      end if 

      end
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
c Maxim Markevitch <maxim % head-cfa.harvard.edu>, 07 Jul 00

c 7.7.00: changed read_gti (file if_goodtime.f) throughout to read GTI into a
c real*8 array instead of integer, and renamed the function to read_ggti.

      subroutine read_ggti (ggti, maxgti, ngti, was_fits)

c read fits file with gti, whose name specified as gti_file in param_file.

      implicit none
      integer maxgti, ngti
      character*1 pfile*512, pname*512, val*512, a80*80, a30*30
      real*8 ggti(maxgti,2), tt0, tt1
      logical anyf, was_fits
      integer in_gti,newunit,iret,junk,istat,ncol,lnblnk,i,nhdu
      common /parf/ pfile

c get gti filename from pfile:
      pname="gti_file "
      call get_param (iret, pfile, pname, val)

      if (iret.ne.0) then
        pname="gtifile "
        call get_param (iret, pfile, pname, val)

        if (iret.ne.0) then
          print'(" no gti_file provided, assuming all times are good")'
        
          ngti=1
          ggti(1,1)=0d0
          ggti(1,2)=999999999d0
        
          return
        end if 
      end if 
      
c open it:
      istat = 0
      call ftgiou (in_gti,istat)
      call ftnopn (in_gti, val, 0, istat)
      
      if (istat.eq.0) then
c this is fits:
        
c get the current ext number and see if need to move (if the filename did 
c not contain the ext already):
        call ftghdn (in_gti, nhdu)

        if (nhdu.eq.1) then
c did not specify ext in the filename, so need to move to 2nd hdu (1st ext
c after primary array):
          call ftmahd (in_gti, 2, junk, istat)
        end if 

        call ftgkyj (in_gti, "NAXIS2", ngti, a80, istat)
        
        call ftgcno (in_gti, .false.,"START", ncol, istat)
        
c check if have column "START" (this will check if we are in a GTI ext.):
        if (istat.ne.0) then
          call ftgerr (istat, a30)
          print'(" read_ggti: error <",a,"> on file")',a30(1:lnblnk(a30))
          print'(" ",a65)',val
          print'(" exiting.")'
          
          call exit(1)
        end if
   
c check maxgti:     
        if (istat.eq.0.and.ngti.gt.maxgti) then
          print'(" read_ggti: increase maxgti=",i5," in the code. exiting.")',
     ~        maxgti
          call exit(1)
        end if 

c read the rest:

c read values in START:
        call ftgcvd (in_gti, ncol, 1,1, ngti, -1, ggti(1,1), anyf, istat)
        
        call ftgcno (in_gti, .false.,"STOP", ncol, istat)
        call ftgcvd (in_gti, ncol, 1,1, ngti, -1, ggti(1,2), anyf, istat)
        
        if (istat.ne.0) then
          call ftgerr (istat, a30)
          print'(" read_ggti: error <",a,"> on file")',a30(1:lnblnk(a30))
          print'(" ",a65)',val
          print'(" exiting.")'
          
          call exit(1)
        end if
        
        call ftclos (in_gti, istat)
        call ftfiou (in_gti, istat)
        was_fits=.true.

      else
c maybe gti file is ascii:
        istat=0
        call ftclos (in_gti, istat)
        istat = 0
        call ftfiou (in_gti, istat)
        istat = 0
        
        in_gti=newunit()
        open(in_gti, file=val, status="old", iostat=iret)

        if (iret.eq.0) then
          ngti=0
          do i=1,maxgti
            read(in_gti,*,iostat=iret) tt0, tt1
            if (iret.eq.0) then
              ggti(i,1)=tt0
              ggti(i,2)=tt1
              ngti=i
            end if
          end do
          if (ngti.eq.0) then
            print'(" read_ggti: gti_file is empty? exiting.")'
            call exit(1)
          end if 
          close(in_gti)
        else
          print'(" read_ggti: cannot open gti file, tried fits and ascii:")'
          print'(" ",a70)',val
          print'(" exiting.")'
          call exit(1)
        end if 

        was_fits=.false.
      end if 
      
*      do i=1,ngti
*        print*,i,ggti(i,1),ggti(i,2)
*      end do 

      end
