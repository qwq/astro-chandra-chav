c 24.3.7: av added save statement

      FUNCTION URAND(IY)
      DOUBLE PRECISION HALFM
      DATA ITWO/2/,M2/0/
      save HALFM, ITWO, M2, M, IC, IA, MIC, S

      IF(M2.NE.0) GO TO 20
      M=1
 10   M2=M
      M=ITWO*M2
      IF(M.GT.M2) GO TO 10
      HALFM=M2
      IC=912703751
      IA=660258821
      MIC=(M2-IC)+M2
      S=0.5/HALFM
 20   IY=IY*IA
      IF(IY.GT.MIC) IY=(IY-M2)-M2
      IY=IY+IC
      IF(IY.LT.0) IY=(IY+M2)+M2
      URAND=FLOAT(IY)*S
      RETURN
      END
