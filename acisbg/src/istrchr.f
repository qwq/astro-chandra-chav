c Returns position of first occurence of substring c in string cs. 0 if not
c present.
c (from A.Vikhlinin)

      integer function istrchr(cs,c)

      implicit none
      character*(*) cs,c
      integer lnblnk,n,n1,i
      
      n=lnblnk(cs)
      n1=lnblnk(c)-1

      do i=1,n-n1
        if(cs(i:i+n1).eq.c)then
          istrchr=i
          return
        endif
      enddo
        
      istrchr=0

      end
