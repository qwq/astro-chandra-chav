c outputs unopened fortran file unit
c (from A. Vikhlinin)

      integer function newunit ()
      
      logical opened
      integer i
      
      opened=.true.
      i=6
      do while (opened)
        i=i+1
        inquire(unit=i,opened=opened)
      enddo
      
      newunit=i
      
      end
