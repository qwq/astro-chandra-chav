c Maxim Markevitch <maxim % hea.iki.rssi.ru>, Wed Aug 30 23:32:38 1995
c 15.3.00: changed acosd to acos

      subroutine theta_phi (x0, y0, x, y, theta, phi)

c x0,y0 and x,y - coords of 2 points,
c theta (pix) and phi (deg) - pos. angle from x axis to vector
c (x0,y0)->(x,y), counterclockwise.

      implicit none
      real x0,y0,x,y,theta,phi,cosphi

      theta=sqrt((x-x0)**2+(y-y0)**2)

      if (theta.gt.0.) then
        cosphi=(x-x0)/theta
        if (y.gt.y0) then
          phi=acos(cosphi)/3.1415926*180
        else
          phi=360.-acos(cosphi)/3.1415926*180
        end if
      else
        phi=0.
      end if
      if (phi.ge.360.) phi=phi-360.


      end

