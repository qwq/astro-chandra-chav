c Maxim Markevitch <maxim % head-cfa.harvard.edu>, last change 30 Jan 7

c 30.1.7: changed string dim. from 128 to 512, removed commas from write()
c 30.8.95: orig.
c Gets numbered parameter in either of the 3 forms: pbase00N, pbase0N, pbaseN

      subroutine get_numbered_param (iret,pfile,pbase,n,val)


      implicit none
      integer iret,n
      character*512 pfile, pname, val
      character*(*) pbase
      integer len,lnblnk


      len=lnblnk(pbase)

      write(pname,'(a,i3.3," ")') pbase(1:len),n
      call get_param (iret,pfile,pname,val)
      if (iret.ne.0) then

        write(pname,'(a,i2.2," ")') pbase(1:len),n
        call get_param (iret,pfile,pname,val)
        if (iret.ne.0) then

          write(pname,'(a,i1," ")') pbase(1:len),n
          call get_param (iret,pfile,pname,val)
        end if 
      end if 
      
      end
      
