/* Maxim Markevitch <maxim % head.cfa.harvard.edu>, last change 30 Jan 7 */
/* 30.1.7: changed MAXLEN from 128 to 512, removed old asca functions */
/* 14.8.95 orig. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXLEN 512 /* should be <= provided size of the string */

void get_param_();
void pad_blank_();
char* RemoveBlanks();

/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

void  get_param_ (iret, param_file, param_name, param_value, l1,l2,l3)

/* strips leading * and stops at ENDPARAMETERS */


char param_file[MAXLEN], param_name[MAXLEN], param_value[MAXLEN];
int *iret;
long *l1, *l2, *l3; /* junk for fortran */
{
  FILE *parfile;
  char readstr[MAXLEN], *str;
  char *pin, *fm;

  /* open file and try to read from it: */

  param_file=RemoveBlanks(param_file);
  param_name=RemoveBlanks(param_name);

  if ((parfile=fopen(param_file,"r")) != NULL) {
    if (fgets(readstr, MAXLEN, parfile) == NULL) {
      /* printf(" getparam: file %s is empty?\n", param_file); */
      *iret=255;
      return;
    }
    str=readstr;
    if ((strstr(str,"endparameters")!=NULL) || 
	(strstr(str,"ENDPARAMETERS")!=NULL)) {
      *iret=254;
      return;
    }
    if (str[0]=='*') str++;
    
    if (str[0]!='#' && str[0]!=' ' && str[0]!='\n'
	&& ((pin=strchr(str,'=')) != NULL)) {       /* a meaningful line */
      *pin='\0';
      str=RemoveBlanks(str);
    }


    /* look for the parameter: */
    
    while(strcmp(str,param_name) != 0) {
      if (fgets(readstr,MAXLEN,parfile) == NULL) {
	*iret=254;
	return;
      }
      str=readstr;
      if ((strstr(str,"endparameters")!=NULL) || 
	  (strstr(str,"ENDPARAMETERS")!=NULL)) {
	*iret=254;
	return;
      }
      if (str[0]=='*') str++;

      if (str[0]!='#' && str[0]!=' ' && str[0]!='\n'
	  && ((pin=strchr(str,'=')) != NULL)) {
	*pin='\0';
	str=RemoveBlanks(str);
      }
    }
    fclose(parfile);


    /* found the parameter. now read its value: */

    str=RemoveBlanks(pin+1);

    if ((fm=strchr(str,'"')) == NULL) {
      if ((pin=strchr(str,'\n')) != NULL) *pin='\0';

      sprintf(param_value,"%s",str);
    }
    else {
      if ((pin=strchr(fm+1,'"')) != NULL) *pin='\0';
      if ((pin=strchr(fm+1,'\n')) != NULL) *pin='\0';

      sprintf(param_value,"%s",fm+1);
    }

    if (strcmp(param_value,"") != 0) *iret=0;
    else *iret=253; /* empty string */

    /* fill the rest of the line with blanks: */
    pad_blank_ (param_name,l1);
    pad_blank_ (param_value,l1);
    pad_blank_ (param_file,l1);

    /* printf("_%s_\n",param_value); */
  }
  else{ 
    /* printf(" getparam: cannot open %s\n", param_file); */

    *iret=255;
    return;
  }
}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

char*                RemoveBlanks(string)
char *string;

/* removes trialing and leading blanks from string 
   (if a blank is found within string, the rest of string is truncated) */

{
  char *pin;
  
  /* remove leading blanks and tabs: */
  while(string[0]==' '||string[0]=='\t') string++;

  /* remove trialing blanks: */
  if ((pin=strchr(string,' ')) != NULL) *pin='\0';
  if ((pin=strchr(string,'\t')) != NULL) *pin='\0';

  return string;
}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

void    pad_blank_ (a512, l1)

char a512[MAXLEN];
long *l1; /* junk for fortran */
{
  int i;

  for (i=strlen(a512); i<MAXLEN; i++) *(a512+i)=' ';
}
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
