#!/bin/sh
set -x
rm -rf ./config.cache

if [ x"$1" = x ]; then
  name=`uname -n`
else
  if [ x"$1" = x"--" ]; then
    name=`uname -n`
  else
    name="$1"
  fi
  shift
fi

GCC64="-m64 -mcpu=v9"

# handle saord builds specially
if [ x$name = xsaord ]; then
  SAORDPREFIX=/soft/saord
  SAOCC=gcc
  SAOFC=g77
  SAOCFLAGS="-m32"
  SAOFFLAGS="-m32"
  platform=`uname`
  case $platform in
  SunOS)
    SAORDPREFIX=/net/saord
    SAOFC=f95
    SAOCC=cc
  ;;
  esac
fi

echo "configure for: " $name

case $name in

# maxim's own installation, Solaris example:
  maxim-solaris)
  ./configure   --with-ciao=/soft/ciao				\
      		--with-cfitsio=/iraf/irafext/lheasoft/sparc-sun-solaris2.8 \
		--prefix=/home/maxim/BIN/SOL			\
		FC=g77 CC=gcc CFLAGS="-O" FFLAGS=" " LDFLAGS="-lsocket -lnsl"\
      		$*
  ;;

# maxim's installation, Linux example (64-bit but making 32-bit binaries;
# also ok to use --with-cfitsio=/data/alexey3l/zhtools/zhext):
  maxim-linux)
  ./configure   --with-ciao=/soft/ciao				\
      		--with-cfitsio=/data/maxim1/soft/lheasoft/i686-pc-linux-gnu-libc2.1 	\
		--prefix=/home/maxim/BIN/LIN			\
		FC=g77 CC=gc CFLAGS="-m32" FFLAGS="-m32"	\
      		$*
  ;;

# alexey's 64-bit opteron linux:
  alexey-ciao4)
  ./configure   --with-ciao=/soft/ciao				\
      		--with-cfitsio=/data/alexey3l/zhtools/zhext 	\
		--prefix=/data/alexey3l/CHAV			\
		FC=g77 CC=gcc CFLAGS="-m32" FFLAGS="-m32"	\
      		$*
  ;;
# alexey's 64-bit opteron linux:
  alexey-ciao43-32)
  ./configure   --with-ciao=/proj/xena/ciao4.3/Linux/ciao-4.3   \
      		--with-cfitsio=/data/alexey3l/zhtools/zhext 	\
		--prefix=/data/alexey3l/CHAV			\
		FC=g77 CC=gcc CFLAGS="-m32" FFLAGS="-m32"	\
      		$*
  ;;

  alexey-ciao43-mac)
  ./configure   --with-ciao=/soft/ciao-4.3   \
                --with-cfitsio=/soft/zhext   \
                --prefix=/soft/chav          \
                --with-mekal=/soft/chav/CAL/mekal.mod  \
                FC=gfortran CC=gcc CFLAGS="-m32 -O" FFLAGS="-m32 -O -fbackslash"\
                $*
  ;;
# alexey's 64-bit opteron linux:
  alexey-kiev)
  ./configure   --with-ciao=/soft/ciao3.4				\
      		--with-cfitsio=/data/alexey3l/zhtools/zhext 	\
		--prefix=/data/alexey3l/CHAV			\
		FC=g77 CC=gcc CFLAGS="-m32" FFLAGS="-m32"	\
      		$*
  ;;
  alexey-mac*)
  ./configure   --with-ciao=/soft/ciao                      \
                --with-cfitsio=/soft/zhext                  \
                --prefix=/soft/chav                         \
                FC=gfortran CC=gcc CFLAGS="-m32" FFLAGS="-m32" \
                $*
  ;;

# HEAD install
  saord*)
  ./configure	--with-ciao=/soft/ciao				\
      		--with-cfitsio=/soft/saord/zhext		\
		--prefix=$SAORDPREFIX 				\
      		CC=$SAOCC FC=$SAOFC				\
		CFLAGS="$SAOCFLAGS" FFLAGS="$SAOFFLAGS"		\
		$*
  ;;


  bokhara|bokhara32*)
  ./configure	--with-ciao=/soft/ciao				\
      		--with-cfitsio=${HOME}/zhext			\
		--prefix=${HOME}/pool				\
		FC=g77 CC=gcc CFLAGS="-m32" FFLAGS="-m32"	\
  ;;

# will not work until ciao supports 64-bit progs
# Linux64 example
  bokhara64)
  ./configure   --with-ciao=/soft/ciao				\
      		--with-cfitsio=${HOME}/zhext			\
		--prefix=${HOME}/pool				\
		FC=gfortran CC=gcc				\
      		$*
  ;;

# Linux32 example g77
  archer77)
  ./configure   --with-ciao=/soft/ciao				\
      		--with-cfitsio=${HOME}/zhext			\
		--prefix=${HOME}/pool				\
                FC=g77 CC=gcc32					\
      		$*
  ;;

# OSX intel example
  duet.local)
  ./configure 	--with-ciao=/soft/ciao				\
    	      	--with-cfitsio=${HOME}/zhext			\
	      	--prefix=${HOME}/pool  				\
      		FC=g95 CC=gcc					\
      	      	$*
  ;;

# OSX PPC example
  g5.local)
  ./configure 	--with-ciao=/chakram/osx_ciaox			\
    	    	--with-cfitsio=${HOME}/zhext			\
	      	--prefix=${HOME}/pool  				\
      		FC=g95 CC=gcc					\
      	      	$*
  ;;


# Solaris example
  cfa208)
  ./configure	--with-ciao=/soft/ciao				\
      		--with-cfitsio=${HOME}/zhext			\
		--prefix=${HOME}/pool				\
                CC=cc  FC=f95 					\
		$*
  ;;

  kiev*)
  ./configure   --with-ciao=/soft/ciao				\
      		--with-cfitsio=/soft/saord/zhext		\
		--prefix=${HOME}/pool				\
		FC=g77 CC=gcc CFLAGS="-m32" FFLAGS="-m32"	\
      		$*
  ;;

# Linux example using intel compilers
  saddam*)
  ./configure	--with-ciao=/soft/ciao				\
      		--with-cfitsio=/soft/saord/zhext		\
		--prefix=${HOME}/chandra3			\
		FC=ifort CC=icc					\
      		$*
  ;;

# OSX example
  mocha*|Mocha*|luwak*|*Luwak*|decaf*|Decaf*|barista*|Barista*|knomad*|Knomad*)
  ./configure	--with-ciao=/usr/local/ciao			\
      		--with-cfitsio=/usr/local/lib/zhext		\
		--prefix=/usr/local				\
		FC=g95 CC=gcc
  ;;

# Intel Mac
  roma*|Roma*)
  ./configure --with-ciao=/soft/ciao/			\
    	      --with-cfitsio=/soft/zhext		\
	      --prefix=/soft/chav/  			\
      	      FC=gfortran CC=gcc $*
  ;;

  *)
  ./configure 	$*
  ;;


esac
