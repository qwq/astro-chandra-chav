\documentclass{acisnote}

\begin{document}

\title{Procedures for co-adding spectral files}
\author{A.~Vikhlinin}
\maketitle

\begin{abstract}
  This memos reviews the issues relevant for co-adding spectral files and
  related spectral calibration. We consider the case when two or more
  observations of (spectrally) the same region are to be co-added but the
  observations have different exposure or cover different area.
\end{abstract}

\section{XSPEC spectral files and related calibration}

\texttt{XSPEC}-type spectral files record the observed source spectrum as
the number of photons (or counts rate) per channel, and also contain in the
header pointers to the relevant calibration data. Here is an example of the
header file (only relevant keywords are shown):
\begin{verbatim}
HDUCLAS2= 'TOTAL   '           / Gross PHA Spectrum (source + bkgd)
HDUCLAS3= 'COUNT   '           / PHA data stored as Counts (not count/s)
EXPOSURE=         15149.700195 / exposure (in seconds)
AREASCAL=             1.000000 / area scaling factor
BACKFILE= 'tprof_5289_reg6_sfi.bg'
BACKSCAL=     1.07365076257837
CORRFILE= 'tprof_5289_reg6_sfi.rdt'
CORRSCAL=                    1
RESPFILE= 'tprof_5289_reg6_sfi.rmf' / response file
ANCRFILE= 'tprof_5289_reg6_sfi.arf' / ARF
POISSERR=                    T / Poissonian errors to be assumed
STAT_ERR=                    0 / no statistical error specified
\end{verbatim}

\begin{enumerate}
\item Since \verb+HDUCLAS3='COUNT'+, the observed flux in each channel
  will be divided by \texttt{EXPOSURE}
\item \textbf{Background.} \verb+HDUCLAS2='TOTAL'+ signals that the spectrum
  needs to be background-subtraced. The background file is pointed to by the
  \texttt{BACKFILE} keyword; its structure is identical to the source
  spectrum.

  The background is subtracted using the following procedure. Let
  \begin{equation}
    \begin{split}\label{eq:def:1}
      c_{s,i}&= \text{Observed flux [cnt] in channel $i$ in source file} \\
      t_s&= \text{Source exposure given by the \texttt{EXPOSURE} keywords}\\
      B_s&= \text{The value of the \texttt{BACKSCAL} in the source file},
    \end{split}
  \end{equation}
  and $c_{b,i}$, $t_b$, and $B_b$ are corresponding quantities from the
  background file. \texttt{XSPEC} calculates the source counts per channel
  as 
  \begin{equation}\label{eq:bg:subtr}
    c_i = c_{s,i} - \frac{t_s \, B_s}{t_b\, B_b}\times c_{b,i}.
  \end{equation}
  If $\mathtt{BACKSCAL}=1$ in the background file, then \texttt{BACKSCAL} in
  the source file gives the factor by which the background spectrum needs to
  be multiplied. For example, if the ACIS count rate in the 10--12~keV band
  in your data is 4\% higher than that in the blank-field dataset, you need
  $\mathtt{BACKSCAL}=1.04$ in the source spectrum.

\item \textbf{Correction file.} In addition to the background subtraction, a
  ``correction file'' pointed to by the \texttt{CORRFILE} keyword can be
  applied to the data. If $C$ is the value of the \texttt{CORRSCAL} in the
  source file and its exposure is $t_c$, the source spectrum is
  \begin{equation}\label{eq:corr:subtr}
    c_i = c_i - C\times\frac{t_s}{t_c}\times c_{c,i},
  \end{equation}
  where $c_{c,i}$ is the correction spectrum [cnt/chan].

\item \texttt{\ttfamily\bfseries AREASCAL} --- I do not use this keyword and
  always set it to one.

\item \textbf{Calculation of statistical uncertainties}. If
  $\mathtt{POISSERR}=\mathtt{T}$, XSPEC calculates statistical errorbars
  (after grouping) as
  \begin{equation}
    e_{s,i} = \sqrt{\max(c_{s,i},1)},
  \end{equation}
  (and identical relations for the uncertainties in the background and
  correction files), which are then propagated in
  equations~\ref{eq:bg:subtr}--\ref{eq:corr:subtr}. 

If $\mathtt{POISSERR}=\mathtt{F}$, statistical uncertainties must be
provided as the values of the \verb+STAT_ERR+ column.

\item \textbf{Response calibration.} Finally, the effective area and
  spectral response files are pointed to by the \texttt{ANCRFILE} and
  \texttt{RESPFILE} keywords, respectively. These files tabulate the
  effective area of the telescope, $A(E)$, and redistribution function of
  the detector, $R(i,E)$. If the model spectrum is $f(E)$, the expected
  observed spectrum is
  \begin{equation}
    m_i = \int f(E)\,A(E)\,R(i,E)\,dE
  \end{equation}

\end{enumerate}



\section{Adding two spectra}


\subsection{Mathematics}

When two spectra are to be added, the most optimal procedure from the
statistical point of view is to add counts (not count rates),
\begin{equation}\label{eq:add:src:spec}
  c_{s,i} = c_{s,i}^{(1)} + c_{s,i}^{(2)}
\end{equation}

\paragraph{Adding exposures} If we add two observations of exactly the same
region, clearly we should add exposure times,
\begin{equation}
  t_s = t_s^{(1)} + t_s^{(2)}.
\end{equation}
If we have two regions from the same observations, exposure obviously must
be kept identical to either first or second spectrum,
\begin{equation}
  t_s = t_s^{(1)}.
\end{equation}
If we have different regions in different observations, it is unclear what
to do with the exposure but it has little effect on the final spectrum
except for the overall normalization. Below, we just assume that the
exposure assigned to the co-added spectrum is $t_s$.


\subsubsection{Adding background and correction files}

It is clear from eq.~(\ref{eq:add:src:spec}) that the background must be
subtraced from co-added spectrum as
\begin{equation}\label{eq:add:bg:1}
  c_i = c_{s,i} - 
  \frac{t^{(1)}_s \, B^{(1)}_s}{t^{(1)}_b\, B^{(1)}_b}\times c_{b,i}^{(1)} -
  \frac{t^{(2)}_s \, B^{(2)}_s}{t^{(2)}_b\, B^{(2)}_b}\times c_{b,i}^{(2)}
\end{equation}
but we would like to produce a co-added background spectrum so that we can
use the usual expression (eq.~\ref{eq:bg:subtr}),
\begin{equation}\label{eq:add:bg:2}
  c_i = c_{s,i} - \frac{t_s \, B_s}{t_b\, B_b}\times c_{b,i}.
\end{equation}
From (\ref{eq:add:bg:1}) and (\ref{eq:add:bg:2}) we have
\begin{equation}
  \frac{t_s \, B_s}{t_b\, B_b}\times c_{b,i} =   
  \frac{t^{(1)}_s \, B^{(1)}_s}{t^{(1)}_b\, B^{(1)}_b}\times c_{b,i}^{(1)} +
  \frac{t^{(2)}_s \, B^{(2)}_s}{t^{(2)}_b\, B^{(2)}_b}\times c_{b,i}^{(2)}
\end{equation}
We have some freedom in assigning the \texttt{EXPOSURE} and
\texttt{BACKSCAL} keywords in the coadded background spectrum. A natural
choice seems to be to assign
\begin{equation}\label{eq:add:bg:exp_and_backscal}
  B_b=B_s=1 \quad \text{and} \quad t_b=1.
\end{equation}
With these definitions, the co-added background spectrum is 
\begin{equation}\label{eq:add:bg}
  c_{b,i} =   \frac{1}{t_s} \times \left[
  \frac{t^{(1)}_s \, B^{(1)}_s}{t^{(1)}_b\, B^{(1)}_b}\times c_{b,i}^{(1)} +
  \frac{t^{(2)}_s \, B^{(2)}_s}{t^{(2)}_b\, B^{(2)}_b}\times c_{b,i}^{(2)}
  \right]
\end{equation}

\bigskip

\noindent Identical considerations lead to the following procedure for
adding correction files:

\begin{equation}\label{eq:corr:bg:exp_and_corrscal}
  t_c = 1 \quad \text{and} \quad C=1,
\end{equation}

\begin{equation}\label{eq:add:corr}
  c_{c,i} = \frac{1}{t_s} \left[
    C^{(1)}\times\frac{t_s^{(1)}}{t_c^{(1)}}\times c_{c,i}^{(1)}
    + C^{(2)}\times\frac{t_s^{(2)}}{t_c^{(2)}}\times c_{c,i}^{(2)}\right].
\end{equation}


\paragraph{Error calculations} While procedure for co-adding the source
spectra (eq.~\ref{eq:add:src:spec}) obviously preserves Poisson errors,
those for background or correction (eq.~\ref{eq:add:bg}
and~\ref{eq:add:corr}) do not. The best solution is to set
$\mathtt{POISSERR}=\mathtt{F}$ in the background and correction files and
compute the \verb+STAT_ERR+ column as
\begin{equation}
  e_i = \sqrt{c_i}
\end{equation}
For some channels, $e_i$ will be zero but this approach allows to
essentially keep the Poisson error calculations when the channels are
groupped. For the background or correction files $e_i=0$ usually will not
lead to bad consequences because all $e_i$ are anyway much smaller than the
uncertainties in the source spectrum.


\subsubsection{Adding response data}
\label{sec:adding-response-data}

Even if the observed counts are simply added (eq.~\ref{eq:add:src:spec}),
the response data must be correctly weighted,
\begin{equation}\label{eq:arf}
  A(E) = w_1\, A_1(E) + w_2\, A_2(E),
\end{equation}
\begin{equation}\label{eq:rmf}
  R(i,E) =  w_1\, R_1(i,E) + w_2\, R_2(i,E).
\end{equation}
\begin{quote}
\begin{small}  
Strictly speaking, the weights $w_1$ and $w_2$ should be different for
adding the effective area and redistribution files. The predicted co-added
spectrum is 
\begin{equation}
  m_i = m_i^{(1)} + m_i^{(2)} = 
  N_1 \int f(E)\,A_1(E)\,R_1(i,E)\,dE +
  N_2 \int f(E)\,A_2(E)\,R_2(i,E)\,dE,
\end{equation}
where $N_{1,2}$ are normalization factor reflecting exposure and area
coverage of each observation. For the co-added response, we want
\begin{equation}
  m_i = (N_1+N_2)\times\int f(E)\,A(E)\,R(i,E)\,dE,
\end{equation}
therefore
\begin{equation}
  \int f(E)\,A(E)\,R(i,E)\,dE = \frac{1}{N_1+N_2}
  \int f(E) \Big(N_1 A_1(E) R_1(i,E) + N_2 A_1(E) R_1(i,E)\Big)\,dE,
\end{equation}
\begin{equation}
  A(E)\,R(i,E) = 
  \frac{N_1}{N_1 + N_2} A_1(E)\,R_1(i,E) + 
  \frac{N_2}{N_1 + N_2} A_2(E)\,R_2(i,E).
\end{equation}
When the redistribution functions are identical, we want
$R(i,E)=R_1(i,E)=R_2(i,E)$, and therefore the weights for the area average
are
\begin{equation}
  w_1 = \frac{N_1}{N_1+N_2}, \qquad w_2=\frac{N_2}{N_1+N_2}.
\end{equation}
If we use this ``natural'' choice of weights for both effective areas and
redistribution functions, we have
\begin{equation}
  A(E)\,R(i,E) = (w_1 A_1 + w_2 A_2) (w_1 R_1 + w_2 R_2) = 
  w_1 A_1 (w_1 R_1 + w_2 R_2) + w_2 A_2 (w_1 R_1 + w_2 R_2)
\end{equation}
instead of 
\begin{equation}
  A(E)\,R(i,E) = w_1 A_1 R_1 + w_2 A_2 R_2
\end{equation}
The error introduced vanishes in two cases,
\smallskip
\begin{enumerate}
\item Same redistribution functions, $R_1=R_2$, because
  \begin{equation}
    w_1 A_1 (w_1 R_1 + w_2 R_2) + w_2 A_2 (w_1 R_1 + w_2 R_2) = w_1 A_1
    R_1 +  w_2 A_2 R_2
  \end{equation}
\item Same effective area, $A_1=A_2$, because
  \begin{equation}
    \begin{split}
      w_1 A_1 (w_1 R_1 + w_2 R_2) + w_2 A_2 (w_1 R_1 + w_2 R_2) &= 
      w_1 R_1 (w_1 A_1 + w_2 A_2) + w_2 R_2 (w_1 A_1 + w_2 A_2) \\
      &=w_1 A_1 R_1 +  w_2 A_2 R_2
    \end{split}
  \end{equation}
\end{enumerate}
Both extremes are valid when adding spectra from the single-type (either BI
or FI) \emph{Chandra} CCDs. If the spectral from different-type CCDs are
added, they must be binned so that the response becomes diagonal for each
type.
\end{small}
\end{quote}
The above discussion shows that the natural choice for the weights in
eq.~(\ref{eq:arf}) and~(\ref{eq:rmf}) is
\begin{equation}
  \label{eq:resp:weight}
  w_j = \frac{\text{area}_j\times\text{exposure}_j}
  {\sum_j \text{area}_j\times\text{exposure}_j}
\end{equation}
The spectral files do not contain enough information to correctly assign the
weights unless it can be assumed that the all areas are the same (in this
case the weights are proportional to exposures). In the general case, the
weights must be computed externally using, e.g., the exposure maps or/and
the model distribution of the source surface brightness.


\subsection{Software}

All the necessary computations can be performed using \texttt{FTOOLS} tasks
\texttt{mathpha}, \texttt{addrmf}, and \texttt{addarf}. Below, we provide
the necessary procedures and discuss the issues.



\subsubsection{Adding source spectra with {\normalfont \texttt{mathpha}}}

\begin{verbatim}
 mathpha expr=spec1.pha+spec2.pha outfil=result.pha \
       units=COUNTS ncomments=0 \
       properr=no ERRMETH=POISS-0\
       areascal=NULL\
       exposure=CALC
\end{verbatim}
When \texttt{exposure=CALC}, the exposures in the input spectra are
added. If this is not what you want, it can be changed to e.g.\
\texttt{exposure=spec1.pha} (to copy the value from \texttt{spec1.pha}) or
\texttt{exposure=10000} (to set it to 10~ksec).

\subsubsection{Adding background and correction files}

The weights in eq.~(\ref{eq:add:bg}) and~(\ref{eq:add:corr}) are best to
calculate externally using the values of exposure and \texttt{*SCAL}
keywords in the input file. In the following examples, we assume that the
weights 0.01 and 0.003
\begin{verbatim}
 mathpha expr="0.01*spec1.bg+0.003*spec2.bg" outfil=result.bg \
         units=RATE ncomments=0 \
         properr=yes errmeth=Gauss \
         areascal=NULL\
         exposure=1.0
\end{verbatim}
This creates the correct \verb+STAT_ERR+ column and we do not want to
restore the Poisson errors. Identical computation (with different weights)
is applied to correction files. 

\subsubsection{Issues for {\normalfont \texttt{mathpha}}}

Sadly, \texttt{mathpha} allows division, '\texttt{/}', as a valid
operand. This means one cannot have input spectra in different
directories. Either copy them or make soft links to the current directory.


\subsubsection{Adding response files}

THe strategies for computing the weights for the spectral response files are
discussed in \S\ref{sec:adding-response-data}. Let us assume that the
correct weights are 0.75 and 0.25 (they should total to 1).
\begin{verbatim}
  addrmf spec1.rmf,spec2.rmf 0.75,0.25 rmffile=result.rmf
  addrmf spec1.arf,spec2.arf 0.75,0.25 result.arf
\end{verbatim}

\subsubsection{Updating headers}

As a final step, we want to update the keywords in the resulting spectrum:
\begin{verbatim}
 fparkey result.bg result.pha"[spectrum]" BACKFILE
 fparkey 1.0       result.pha"[spectrum]" BACKSCAL
 fparkey result.arf result.pha"[spectrum]" ANCRFILE
 fparkey result.rmf result.pha"[spectrum]" RESPFILE
 fparkey result.cor result.pha"[spectrum]" CORRFILE
 fparkey 1.0        result.pha"[spectrum]" CORRSCAL
\end{verbatim}


\section{Adding more than two spectra}

Mathematically, equations for adding the spectra, background, correction,
and response files can be trivially generalized to the case of more than two
input files. Software (\texttt{mathpha}, \texttt{addrmf}, \texttt{addarf})
also allows for several input files. However, if there are too many spectra
to add, this can be done consecutively.

\subsection{Adding many spectra ``in turn''}

The source and background/correction files can be easily added ``in turn''
using the procedure for two spectra outlined above:
\begin{verbatim}
 spec.pha = spec1.pha + spec2.pha
 spec.pha = spec.pha + spec3.pha
 ...
\end{verbatim}
and identically for the background and correction files. 

The procedure is only slightly more complicated for the response data. Let
${w_1, \ldots, w_n}$ are correct weights for adding response in $n$ input
spectra ($\sum w_i = 1$). The ratio of first two responses in the result
should be $r_1:r_2=w_1:w_2$, therefore on the first step,
\begin{equation}
  R_2 = \frac{w_1}{w_1+w_2}\,r_1+\frac{w_2}{w_1+w_2}\, r_2
\end{equation}
On the second step,
\begin{equation}
  R_3 = \frac{w_1+w_2}{w_1+w_2+w_3}\, R_2 + \frac{w_3}{w_1+w_2+w_3}\, r_3
\end{equation}
and so on. This is the formal rule:
\begin{equation}
 R_1 = r_1, \qquad 
 R_i = \frac{\sum_{j=1,i-1} w_j}{\sum_{j=1,i} w_j}\, R_{i-1} +
  \frac{w_i}{\sum_{j=1,i} w_j}\, r_i
\end{equation}


\appendix


\end{document}
