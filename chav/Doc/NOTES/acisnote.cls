\LoadClassWithOptions{article}
\ProvidesClass{acisnote}

% Fonts
\RequirePackage{amsmath}
\usepackage{mathpazo}

% Page stup
\usepackage[letterpaper,twoside,twosideshift=-0.05in,textwidth=7in,textheight=52\baselineskip,top=0.8in]{geometry}

%% Title
\def\@maketitle{%
  \newpage
%  \fontfamily{officinasans}\selectfont
  \mbox{}\hfill\textit{\today}\par
  \let \footnote \thanks
%  \begin{quote}
    {\noindent\LARGE\bfseries \@title \par}%
    \vskip 0.5em%
    \mbox{}\hfill{\large\bfseries
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}}\par
    \par
%    \end{quote}
  \vskip 1.5em}

%% Abstract
\renewenvironment{abstract}{%
  \if@twocolumn%
  \section*{\abstractname}%
  \else%
  \quotation%
  {\bfseries\abstractname}\fi\\}%
{\if@twocolumn\else\endquotation\fi\vskip 1.5em}

%% Captions
\def\figurename{Fig.}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{\footnotesize #1--- #2}%
  \ifdim \wd\@tempboxa >\hsize
  \footnotesize #1--- #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}

%% allow figures to take the whole page
\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}
\renewcommand{\textfraction}{0.0}

%% Sectionining
\def\@removefromreset#1#2{{%
  \expandafter\let\csname c@#1\endcsname\@removefromreset
  \def\@elt##1{%
    \expandafter\ifx\csname c@##1\endcsname\@removefromreset
    \else
      \noexpand\@elt{##1}%
    \fi}%
  \expandafter\xdef\csname cl@#2\endcsname{%
    \csname cl@#2\endcsname}}}
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\large\bfseries\boldmath}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\bfseries\boldmath}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\normalsize\bfseries\itshape\boldmath}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {\smallskipamount}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}


\renewcommand \thesection {\@arabic\c@section}
\@removefromreset{section}{chapter}

%% Lists
% Lists

\def\enumerate{%
  \ifnum \@enumdepth >\thr@@\@toodeep\else
    \advance\@enumdepth\@ne
    \edef\@enumctr{enum\romannumeral\the\@enumdepth}%
      \expandafter
      \list
        \csname label\@enumctr\endcsname
        {\itemsep=0pt\parsep=0pt\topsep=0pt\listparindent=15\p@\usecounter\@enumctr\def\makelabel##1{\hss\llap{##1}}}%
  \fi}
\def\itemize{%
  \ifnum \@itemdepth >\thr@@\@toodeep\else
    \advance\@itemdepth\@ne
    \edef\@itemitem{labelitem\romannumeral\the\@itemdepth}%
    \expandafter
    \list
      \csname\@itemitem\endcsname
      {\itemsep=0pt\parsep=0pt\topsep=0pt\def\makelabel##1{\hss\llap{##1}}}%
  \fi}


%% Float placement
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{0.85}
\renewcommand{\textfraction}{0.15}

%% Date stamps
\def\Modified#1{\def\@Modified{#1}}
\def\@Modified{\relax}

%% Headings
\def\ps@headings{%
  \let\@oddfoot\@empty\let\@evenfoot\@empty
  \def\@evenhead{\thepage\hfil\small{\slshape\leftmark}\qquad {\sffamily\@Modified}\hfil
}%
  \def\@oddhead{\hfil{\small{\slshape\rightmark}\qquad {\sffamily\@Modified}}\hfil\thepage}%
  \let\@mkboth\markboth
  \def\chaptermark##1{%
    \markboth {{%
        \ifnum \c@secnumdepth >\m@ne
          \if@mainmatter
            \@chapapp\ \thechapter. \ %
          \fi
        \fi
        ##1}}{}}%
  \def\sectionmark##1{%
    \markboth {{%
        \ifnum \c@secnumdepth >\z@
        \thesection \ \ %
        \fi
        ##1}}{%
        \ifnum \c@secnumdepth >\z@
        \thesection \ \ %
        \fi
        ##1}}
}
%\pagestyle{headings}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EQNARRAY with reduced spacing around tab characters - AV
\def\eqnarray{%
  \stepcounter{equation}%
  \def\@currentlabel{\p@equation\theequation}%
  \global\@eqnswtrue
  \setlength{\arraycolsep}{0.25\arraycolsep}
  \m@th
  \global\@eqcnt\z@
  \tabskip\@centering
  \let\\\@eqncr
  $$\everycr{}\halign to\displaywidth\bgroup
  \hskip\@centering$\displaystyle\tabskip\z@skip{##}$\@eqnsel
  &\global\@eqcnt\@ne\hskip \tw@\arraycolsep \hfil${##}$\hfil
  &\global\@eqcnt\tw@ \hskip \tw@\arraycolsep
  $\displaystyle{##}$\hfil\tabskip\@centering
  &\global\@eqcnt\thr@@ \hb@xt@\z@\bgroup\hss##\egroup
  \tabskip\z@skip
  \cr
  }

% Tablular
\RequirePackage{array}
\setlength{\extrarowheight}{2pt}

% Packages
\RequirePackage{graphicx}
\RequirePackage{url,hyperref}

% Hyper references
\def\HREF#1#2{\href{#1}{#2}\footnote{\url{#1}}}

\endinput
%% 
%% End of file `article.cls'.




