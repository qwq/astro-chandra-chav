1) Create an image. In addition to your favorite tools, this can be done
   using filtevt. Assuming that the name of the events file is set in
   par.file:

% filtevt -reg undefined -o img.fits -binfilt "[bin (X,Y) = 10]" @par.file

   This command bins X and Y columns in the event file by 10. No region
   filtering is performed because we set "-reg undefined"

2) Choose your spectrum region in SAOimage. This region can be a mix of
   include/exclude type regions. Save region file to disk as, say, "spec.reg"

3) Edit par.file and set the name of the region file and the image for which 
   it was created. 

   You will need to keep the image for the rest of spectral extraction.
   Alternatively, you can save the FITS header to a file and use this header
   file instead of the original image as the regwcs= parameter

4) Filter events in this region.

% filtevt -o temp.fits @par.file

5) Create the spectrum

% extrspec -evtfile temp.fits -o spec.pha @par.file

6) Control that the spectrum was extracted in the right region. 

% saoimage -fits spec.pha

   You should see something resembling the image in your selected
   region. Not quite the right image because "spec.pha" was binned in
   detector coordinates while you probably selected regions in the sky
   coordinates.



5A) To create spectra in each chip separately, use something like

% foreach chip ( s0 s1 s2 s3 s4 s5 )
    extrspec -evtfile temp.fits -o spec_$chip.pha -chip $chip @par.file
  end

  After this, you will have those spectra of spec_s0.pha ... spec_s5.pha which 
  had at least one photon. In the primary header of spec_*.pha there is an
  image of intersection of your region and the selected chip. 

  There is a special "chip" ACIS-I which is all four I0-I3 together. You
  will have to rely on the same response in these chips in analyzing your
  spectrum.

