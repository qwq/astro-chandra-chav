\documentclass[11pt,twoside]{article}
\usepackage{fullpage}
\usepackage{timesfonts}

\newenvironment{howdoi}{\par\bigskip\noindent
  \slshape{\bfseries How do I ...} \itshape}{}
\newenvironment{cani}{\par\bigskip\noindent
  \slshape{\bfseries Can I ...} \itshape}{}
\newenvironment{whydoi}{\par\bigskip\noindent
  \slshape{\bfseries Why do I ...} \itshape}{}
\newenvironment{question}{\par\bigskip\noindent
  \slshape{\bfseries \Large ???} \itshape}{}
\newenvironment{sol}{\normalfont\small\begin{quote}}{\end{quote}}
\newenvironment{option}[1]{\medskip\noindent{\bfseries\texttt{#1}}
  \hspace*{\parindent}\everypar={\hangindent=\parindent\hangafter=1}}{\par}

\newenvironment{options}[1]{\par\bigskip\bigskip
\textbf{#1:}\par}{}

\def\itempar{\noindent\hangindent=\parindent\hangafter=1\relax}

\pagestyle{headings}
\begin{document}
\title{Alexey's ACIS programs HOWTO}
\author{Alexey Vikhlinin}
\date{\today}
\maketitle

\tableofcontents

\section{What can this software do?}

This document covers the usage of several programs I wrote in anticipation
of the first \emph{Chandra} data. The software kit includes programs and
scripts that facilitate data filtering, image and spectra extraction,
exposure map generation, and generation of ARF and RMF files for spectral
fitting.

\begin{option}{badpixfilter}
  
  Exclude bad pixels from the ACIS data.
\end{option}

\begin{option}{filtevt}
  
  The script that makes it easy to filter photons by region, by GTI etc. You 
  specify these filters in a human-understandable form in a parameter file,
  and \texttt{filtevt} will make the appropriate FITSIO filters on the fly.
\end{option}

\begin{option}{extrspec}
  
  The script that extracts spectra by running FTOOL \verb+extractor+.
\end{option}

\begin{option}{runextrspec}
  
  Runs \texttt{extrspec} and optionally runs \texttt{calcarf} and
  \texttt{calcrmf}. Spectra are extracted in a set of regions and chips.
\end{option}

\begin{option}{calcarf}

  Calculates the ARF file for the PHA spectrum.
\end{option}

\begin{option}{calcrmf}

  Calculates the RMF file for the PHA spectrum.
\end{option}

\begin{option}{chipmap}

  Create instrument maps in detector coordinates.
\end{option}

\begin{option}{aspecthist}

  Extract the aspect offset histogram.
\end{option}
  
\begin{option}{convaspect}

  Convolves instrument maps with aspect histogram,
\end{option}
  

\begin{question}
  What external packages and calibration data do I need?
  \begin{sol}
    You need FTOOLS and the most recent ZHTOOLS. You may also need to setup
    your environment for using CIAO (no CIAO tools are called directly; the
    CIAO environment is needed for a library that converts between different
    CIAO coordinate systems).
    
    All calibration data are defined in parameter files. You can have a
    private copy of these data, or point the file names to a standard place.
  \end{sol}
\end{question}


\section{Science thread}


\subsection{Preparing the data}

\subsection{Setting up parameter file}

\subsection{Making image}

\subsection{Making exposure maps}

\subsection{Extracting spectra}






\section{Event filtering}

\subsection{\texttt{BADPIXFILTER} --- apply bad pixel filter}
This program applies the bad pixel filtering. This operation can be 
performed also by \texttt{filtevt}, but very inefficiently.

This program is ACIS-only because the detector size is hard-coded.

\begin{options}{Options}

\begin{option}{-evtfile \textit{events.fits}}

  Input events file name.
\end{option}

\begin{option}{-o \textit{outfile}}  Output file name.
\end{option}

\begin{option}{badpixfilter=\textit{bad\_pixel\_file}}

  Bad pixels list. The bad pixel file is a text file with the format\\
\texttt{ccdid    chipxmin chipxmax    chipymin  chipymax}\\
one rectangle per line.
\end{option}

\end{options}

\subsection{\texttt{FILTEVT} --- filter event lists}

This tool applies varies filters to the event lists. You can also define
a binning filter in the FITSIO format and then \texttt{filtevt} outputs
images.

\begin{options}{Options}

\begin{option}{-evtfile \textit{events.fits}}

  Input events file name.
\end{option}

\begin{option}{-o \textit{outfile}}  Output file name.
\end{option}

\begin{option}{badpixfilter=\textit{bad\_pixel\_file}}

  Bad pixels list. The bad pixel file is a text file with the format\\
\texttt{ccdid    chipxmin chipxmax    chipymin  chipymax}\\
one rectangle per line.
\end{option}

\begin{option}{filtbadpix=\textit{no}}

  Bad pixel filtering may be time-consuming. You might want to do it once in 
  the very beginning and then operate on clean event lists. The bad pixel
  file is still needed for programs that calculate exposure maps. So, we
  provide the option \texttt{filtbadpix=no} which means that event lists
  should not be filtered off the bad pixels, but exposure maps still should
  do.

  NOTE: bad pixel filtering is performed very inefficiently. Please use
  the \texttt{badpixfilter} program to do that.

\end{option}

\begin{option}{gtifile=\textit{gti.fits}}

  File with the good time interval extension for time filtering. If not set, 
  GTI will be searched for in the events file itself.
\end{option}

\begin{option}{filtgti=\textit{no}}

  Similar to \texttt{filtbadpix=no}.
\end{option}

\begin{option}{-reg \textit{region\_file} 
    -exclreg \textit{region\_to\_exclude}}
  
  SAOimage regions file that specify what areas (in sky coordinates) should
  be preserved or excluded. If any of these parameters is set, you need to
  set \texttt{-regwcs}
\end{option}

\begin{option}{-regwcs \textit{img.fits}} 

  FITS image in whose frame \texttt{reg} or \texttt{exclreg} were made. This 
  image must contain WCS in the header so that we know how to transfor the
  regions into the event file coordinate system.
\end{option}

\begin{option}{-rowfilt \textit{rowfilter}} 

Any valid FITSIO row filter. 
\end{option}

\begin{option}{-binfilt \textit{binfilter}} 

Any valid FITSIO bin filter. If this is set the output will be an image.
\end{option}




\end{options}


\section{Spectrum extraction}

Generally, you make spectra with the following procedure:

\itempar 1) make an image in the sky coordinates. You can use
\texttt{filtevt} for that.

\itempar 2) Display the image, choose regions and save them to a file (with
SAOimage or SAOtng or ds9\footnote{With SAOtng, make sure that you save the
  region coordinates in pixels, not in degrees in the sky etc. With ds9, on
  the contrary save the region in sky coordinates and do not set \texttt{regwcs}}).

\itempar 3) Edit parameter file to point \texttt{regwcs} to your image. You
will need to keep this image for the rest of spectral extraction.

\itempar 4) Run \texttt{extrspec} or better, \texttt{runextrspec}, to
extract spectra.

\subsection{\texttt{EXTRSPEC} - extract a spectrum in a single region}

\emph{ \texttt{EXTRSPEC} does not do any region filtering. You are supposed
  first to filter your events file (e.g., with \texttt{filtevt}) and then to
  use the filterevent list as an input to \texttt{extrspec}.  }

\begin{options}{Program-specific options}

  \begin{option}{-evtfile \texttt{filtered\_events.fits}}
    
    Region-filtered events file. \texttt{Extrspec} will extract the spectrum
    for all events in this file, with no region filtering whatsoever.
    Therefore, you normally should filter your events (e.g., with
    \texttt{filtevt}) and only then call \texttt{extrspec}.
  \end{option}

  \begin{option}{-o \textit{outfile.pha}}
    
    Output PHA file name
  \end{option}

  \begin{option}{-chip \texttt{chipname}}
    
    Extract spectrum only for photons in this particular chip. Different
    formats of the chip names are accepted --- \texttt{acis-i3}, \texttt{s2}
    etc. There is a special ``chip'' \texttt{acis-i} which means all four
    \texttt{i0-i3} together.
  \end{option}
  
\end{options}

\begin{options}{Parameter file options}
  Normally, you won't need to change any of these options between different
  runs. Put them in the parameter file:

  \begin{option}{ecol=\textit{energy\_column\_name}}

    Name of the spectral channel column. Usually, equals PI or PHA
  \end{option}

  \begin{option}{gtifile=\textit{gti.fits}}

    File with the good time interval. If not set, GTI extesion will be taken 
    from the events file.
  \end{option}

  \begin{option}{specbin=\textit{binfactor}}

    Bin the spectral channels in the output spectrum by
    \texttt{\textit{binfactor}}. If you want to do this to increase the
    number of photons per channel, use FTOOL \texttt{grppha}
    instead. However, you do need to bin PHA channels by two to match the
    current ACIS response matrices.
  \end{option}


  \begin{option}{fixphaspec=\textit{yes/no}}

    ACIS events file have the PHA maximum value of some 32000. Even if
    binned by two, this results in a spectrum with 16000 channels. Response
    matrices, however, cover only 2048 (binned by 2) channels. If
    \texttt{fixphaspec=yes}, the channels above \textrm{maxspecchans} in the 
    PHA file will be discarded. 
    
  \end{option}

  \begin{option}{maxspecchans=maxchan}

    The number of spectral channels which should be in the output PHA file.
    Generally, you want \texttt{fixphaspec=yes} and
    \texttt{maxspecchans=2048} if you extract spectral in \textit{PHA}
    channels, and \texttt{fixphaspec=no} for \textit{PI} channels.
  \end{option}

  \begin{option}{bindetmap=\textit{binfactor}}
    
    \texttt{Extrspec} puts an image of your region in detector coordinates
    into the primary header of the PHA file. This information is used for
    the subsequent ARF calculation. \texttt{bindetmap} sets the binning
    factor for this image. \texttt{bindetmap=8}, which makes $4''$ pixels,
    is a reasonable choice.
  \end{option}

  \begin{option}{edetmapmin=\textit{energy\_min}, 
      edetmapmax=\textit{energy\_max}}

    Minimum/maximum energy (eV) for the map image. The default energy range
    is 500--2000 eV.
  \end{option}


  
  \begin{option}{xcol=\textit{xcolname}, ycol=\textit{ycolname}}
    
    Name of the column with X- and Y- sky coordinates. default is
    \texttt{X} and \texttt{Y}.
  \end{option}

  \begin{option}{xdetcol=\textit{xdetname}, xdetcol=\textit{xdetname}}
    
    Name of the column with X- and Y- detector coordinates. default is
    \texttt{DETX} and \texttt{DETY}.
  \end{option}

  
\end{options}



\begin{howdoi}
How do I extract spectra in each chip separately?
\begin{sol}
  1) Script \texttt{runextrspec} does that. 2) If you still want to use
  extrspec, run something like
\begin{verbatim}
 % foreach chip ( s0 s1 s2 s3 s4 s5 )
    extrspec -evtfile temp.fits -o spec_$chip.pha -chip $chip @par.file
  end
\end{verbatim}
  
  After this, you will have those \verb+spec_s0.pha+ ...  \verb+spec_s5.pha+
  with at least one photon. In the primary header of \verb+spec_*.pha+ there
  is an image of intersection of your region and the selected chip.
  
  There is a special ``chip'' called ACIS-I which is all four I0--I3 together.
\end{sol}
\end{howdoi}



\subsection{\texttt{RUNEXTRSPEC} --- extract spectra in a set of regions and 
  in individual chips.}

This is the program you want to use for spectral extraction. It takes an
SAOimage region file, and runs \texttt{extrspec} for each region and for
each chip. It also runs \texttt{calcarf} on the output spectra.

\begin{options}{Program-specific options}

  \begin{option}{-o \textit{outkey}}

    The root name for output files. For example, \texttt{-o sp} will
    produce output spectra named \texttt{sp\_\textit{reg}.pha}.
  \end{option}

  \begin{option}{-regs \textit{SAOimage\_region\_file}}
    
    The SAOimage region file with a set of regions for which you want to
    extract spectra. By default, \texttt{runextrspec} will use keywords
    \texttt{``reg1''}, \texttt{``reg2''} and so on for the output names. You
    can change this by specifying the keyword name after each region:
\begin{verbatim}
circle(256,256,10) regA
box(256,256,10,10,30) regB
\end{verbatim}
    will result in output spectra \verb+*_regA.pha+ and \verb+*_regB.pha+.
  \end{option}

  \begin{option}{-together}
    
    The region is not split into individual components. This is handy for the
    background spectra generation.
  \end{option}

  \begin{option}{-chips \textit{chip\_list}}
    
    By default, \texttt{runextrspec} will run \texttt{extrspec} on each of
    the 9 chips for each region. If you know that you regions are located in
    chips \texttt{s2} and \texttt{s3}, you can reduce the execution time by
    using \texttt{-chips s2,s3}.
  \end{option}

  \begin{option}{-noarf}

    Do not run \texttt{calcarf} on the output spectra.
  \end{option}

  \begin{option}{-dormf}

    Run \texttt{calcrmf} on the output spectra.
  \end{option}


  \begin{option}{-noheader}
    
    If \texttt{calcarf} is being run, it updates the PHA headers to point to
    the correct RMF and ARF files. If you don't want this, use
    \texttt{-noheader}.
  \end{option}

  
\end{options}

\begin{options}{Other options}
  \texttt{runextrspec} runs \texttt{extrspec} many times. Therefore, you
  have to set all the options required by \texttt{extrspec} expect for
  \texttt{-reg} and \texttt{-chip}. If \texttt{-noarf} is unset,
  \texttt{runextrspec} will also call \texttt{calcarf}, and so you need to
  provide options for this program as well.
\end{options}


\subsection{\texttt{CALCARF} --- generate an ARF file for the PHA spectrum}

\begin{options}{Program-specific options}

  \begin{option}{-o \textit{outfile}}
    Output ARF file
  \end{option}

  \begin{option}{-phafile \textit{phafile}}
    Input pha file
  \end{option}

  \begin{option}{updateheader=\textit{no}}
    Do not update the PHA file header. By default, the header is updated.
  \end{option}

  \begin{option}{-correct\_qe\_cti \textit{yes}}

    Apply a position-dependent correction for the CCD QE. By default, there
    is no correction. This option should be used with the FI chips and the
    focal plane temperature --110\,C only.
  \end{option}

  \begin{option}{-correct\_qeu \textit{yes}}

    Apply a position-dependent correction for the CCD QE by using the
    Quantum Efficieny Uniformity map.
  \end{option}

  \begin{option}{ccd\_qeu=\textit{qeu\_file.fits}}
    Calibration file for the QEU map.
  \end{option}

  \begin{option}{wmap=\textit{wmap.fits}}

    External weight map to be used if the WMAP in the PHA header is
    absent. This option serves to provide the possibility to generate ARFs
    for the CIAO-made PHAs.
  \end{option}



\end{options}

\subsection{\texttt{CALCRMF} --- generate the response matrix}
\label{sec:calcrmf}

This program generates the PI responce matrix for the input spectrum.

\begin{options}{Program-specific options}

  \begin{option}{-o \textit{outfile}}
    Output RMF file
  \end{option}

  \begin{option}{-phafile \textit{phafile}}
    Input pha file
  \end{option}

  \begin{option}{updateheader=\textit{no}}
    Do not update the PHA file header. By default, the header is updated.
  \end{option}

  \begin{option}{-rmfshift \textit{shift}}
    shift the matrix PI channels by that amount.
  \end{option}

  \begin{option}{one\_fef\_in\_reg=\textit{yes}}
    Find the average detector coordinates of the photons and then calculate
    the RMF at this point. The default behavior is to do a proper weighting
    of the RMFs with the source flux.
  \end{option}
  
  \bigskip 
  The following two options are designed to fix the bug in the
  response calibration data posted in the end of 1999.

  \begin{option}{-fixlowermf [yes]}
    This option tells to take the parameters of the negative
    centriod Gaussian components from the regions close to the read-out. See 
    also \texttt{-eminrmf}.
  \end{option}

  \begin{option}{-eminrmf \textit{minchan}}
    Set to zero RMF fit components with centroids below \textit{minchan}. As 
    of February 2000, this option defaults to 0 unless \texttt{-fixlowermf}
    is in effect, in which case default \textit{minchan} is -1000.
  \end{option}

  \begin{option}{wmap=\textit{wmap.fits}}

    External weight map to be used if the WMAP in the PHA header is
    absent. This option serves to provide the possibility to generate RMFs
    for the CIAO-made PHAs.
  \end{option}

\end{options}

\begin{options}{Calibration options}

  \begin{option}{-fptemp \textit{FP\_temp}}
    Focal plane temperature. Currently, supports only
    \texttt{fptemp=-110,-100,-90}.
  \end{option}
\end{options}



\section{Exposure and instrument maps}

\subsection{Outline}

Exposure map is generated by running the three programs:

\texttt{chipmap}~~~ generates chip map in detector coordinates with or
without vignetting.

\texttt{aspecthist}~~~ creates the aspect histogram from the aspect offset 
file.

\texttt{convaspect}~~~ convolves the chip mask (or any image for that
matter) with the aspect histogram.





\subsection{\texttt{CHIPMAP} --- generate chip map in detector coordinates}

This program generates the chip map image appropriate for the aim point of
your observation. Briefly, you select chips and \texttt{chipmap} puts them
on the detector plane. If the bad pixel file is set, bad pixels and columns
are subtracted from the map. Optionally, the map can be multiplied by the
mirror vignetting and/or the relative average quantum efficiencies of the
CCDs.

\begin{options}{Program-specific options}

\begin{option}{-o \textit{outfile}}
  Output chip map name
\end{option}

\begin{option}{-bin \textit{bin\_factor}}

Set binning factor for the chip map. If \texttt{\textit{bin\_factor}=4}, the 
map will cover the whole detector plane with $2''$ pixels. 

A preferred way to set the binning factor is with the \texttt{-refimg}
parameter.
\end{option}

\begin{option}{-refimg \textit{image.fits}}
  
  Set the chip map binning factor to match \texttt{\textit{image.fits}}. The
  program will try to read \texttt{CDELT1} and \texttt{CDELT2} keywords from
  the image and divide them by the default ACIS pixel size ($\approx 0.5''$)
  to determine the binning factor.
\end{option}

\begin{option}{-chips \textit{list}}

  Chip list to be included in the map. The argument is a comma separated
  list of chip names, i.e.\ \texttt{s1,s2,i0,i2,i3}. There is a special chip 
  \texttt{i} which is equivalent to \texttt{i0,i1,i2,i3}.
\end{option}

\begin{option}{-refspec \textit{spectral\_model\_file}}

  The average vignetting and quinatum efficiency depend on the energy band
  and the source spectrum. This parameter points to the file that contains
  the user's spectral model. If it is set, you also have to define the energy
  band with \texttt{-emin} and \texttt{-emax}. If all three are set, the
  chip map will be multiplied by mirror vignetting and relative CCD QEs (see 
  \texttt{-refchip}).
\end{option}

\begin{option}{-refchip \textit{chip} \textnormal{or} -refccd \textit{chip}}

  Average QEs are normalized so that the QE is 1 in the reference chip. This
  parameter sets the reference CCD. The default is \texttt{I3}
\end{option}

\begin{option}{-emin \textit{emin}, -emax \textit{emax}}
  
  Set the energy band (in keV) for vignetting and CCD QE calculations.
\end{option}

\begin{option}{useroll=yes}
  
  Rotate the output map by the average roll angle during the observation.
  \textsl{Note:} you normally don't need this. The chip map gets rotated
  during the convolution with the aspect histogram
\end{option}
\end{options}

\begin{options}{General options}

\begin{option}{evtfile=\textit{events.fits}}
  
  The events file name. It will be opened to determin the PIXLIB Focal Plane 
  system standard (e.g., \texttt{fp-1.1}) and the aim point. If you don't
  set \texttt{-evtfile}, you have to supply \texttt{-aimpoint} and
  \texttt{-fpsys}.
\end{option}

\begin{option}{badpixfilter=\textit{bad\_pixel\_file}}
  
  If this parameter is set, bad pixels and columns from the file will be 
  excluded from the chip map.
\end{option}

\bigskip
If you include vignetting and CCD QE corrections to the chip map, you have
to supply the calibrations files \texttt{hrma\_onaxis\_area=...},
\texttt{hrma\_vignetting=...}, and \texttt{ccd\_qe\_XX=...} for each chip.

\end{options}

\bigskip

\begin{howdoi}
  How do I create the spectral model file?
  \begin{sol}
    1) Run XSPEC. 2) Select your model. 3) Set dummy response matrix (with
    \verb+dummyrsp+ command). 4) Set the history file name: \verb+xhist myspec+
    5) \verb+dump model+. 6) Quit xspec and run \verb+rdhis myspec.xhs+. Save
    output to a file. This file can be given to \verb+chipmap+.

    NOTE that \verb+rdhis+ is not included into XANADU Linux distribution. I
    have a port to this platform. Contact me
    (\verb+alexey@head-cfa.harvard.edu+) if you need it.
  \end{sol}
\end{howdoi}

\begin{cani}
  Can I use \texttt{fake} spectra as the spectral model?
  \begin{sol}
    No. The spectra produced by the XSPEC \texttt{fake} command do not contain
    the energy boundaries of spectral channels.
  \end{sol}
\end{cani}

\begin{cani}
  Can I generate a vignetting map for a particular energy?
  \begin{sol}
    Yes. If you want it for, say, 2.0 keV, use the spectral model file:
\begin{verbatim}
2.0 1.0
\end{verbatim}
  \end{sol}
\end{cani}




\subsection{\texttt{ASPECTHIST} --- create aspect histograms}

This program reads the aspect file and saves the aspect histogram in
separate roll angle bins. The prime reason for \texttt{aspecthist} existense
is to save \texttt{convaspect} the trouble of reading and sorting the aspect
offset file from Lev1 data.

\begin{options}{Program-specific options}

  \begin{option}{-aspfile \textit{XXX\_aoff1.fits}}
    
    Set the aspect offset file from the Lev1 data.
  \end{option}
  
  \begin{option}{-o \textit{outfile}}
    Output aspect histogram file name.
  \end{option}
  
  \begin{option}{-offsetbin \texttt{binsize}}

    Bin size (in $0.5''$ pixels) for the aspect offset histogram. It makes
    sense to use the same bin size as in the chip map file. Default is 1.
  \end{option}

  \begin{option}{-rollbin \texttt{rollbinsize}}
     
    Bin size (in degrees) for histogramming roll angles. Default is 0.02
    degrees which corresponds to $\pm 0.25''$ shifts at $25'$ off-axis.
  \end{option}

\end{options}

\begin{options}{General options}

  \begin{option}{-gtifile \textit{gti.fits}}
    
    List of good time intervals. NOTE: if this parameter is unset, the
    program issues a warning but continues execution.

    The total duration of the good time interval is written to the output
    file as the \texttt{EXPOSURE} keyword.

  \end{option}
\end{options}  



\subsection{\texttt{CONVASPECT} --- convolve the chip map with the aspect
  histogram}

This program convolves the chip map with the aspect histogram and applies
roll angle rotations. The output map has the WCS keywords.

\begin{options}{Program-specific options}

  \begin{option}{-chipmap \textit{chipmap.fits}}
    Input chip map image.
  \end{option}
    
  \begin{option}{-asphist \textit{aspecthist.fits}}
    Aspect histogram file made  by \texttt{aspecthist}.
  \end{option}
    
  \begin{option}{-o \textit{outfile}}
    Output map name.
  \end{option}

  \begin{option}{inverse=\textit{yes}}

    Apply inverse aspect shifts and roll angle rotations.
    This is useful if you want to convert sky regions into
    detector coordinates.
  \end{option}

  \begin{option}{-noexp}

    Do not normalize the output map by the \texttt{EXPOSURE} keyword from
    the aspect histogram file. To normalize by exposure is the default
    behavior.
  \end{option}

\end{options}

\bigskip
\begin{howdoi}
  \texttt{chipmap}$\rightarrow$\texttt{aspecthist}$\rightarrow$\texttt{convaspect}
  chain creates the exposure map that covers the entire focal plane. My
  images are subsections of that plane. Can I generate exposure map
  specifically for my image regions?
  \begin{sol}
    Yes. Generate a big map with
    \texttt{chipmap}$\rightarrow$\texttt{aspecthist}$\rightarrow$\texttt{convaspect} 
    and then project it onto your image region. The following
    \texttt{ZHTOOLS} command would do that
\begin{verbatim}
% projectimg from=bigmap.fits onto=image.fits out=smallmap.fits
\end{verbatim}
    \end{sol}
\end{howdoi}


\section{Troubleshooting}

\subsection{I am getting a FITSIO error ...}

\begin{whydoi}
  Why do I get a error message like
\begin{verbatim}
FITSIO status = 105: couldn't create the named file
failed to create the following file: (ffinit)
img.fits
Error while closing HDU number 0 (ffchdu).
\end{verbatim}
  
  \begin{sol}
    This is because FITSIO cannot overwrite existing files. \texttt{rm} the
    output file before running a program or put the exclamation mark in
    front of the output file name:
\begin{verbatim}
  % filtevt evtfile=events.fits -binfilter "[bin (X,Y) = 4]" \!img.fits
\end{verbatim}
  \end{sol}
\end{whydoi}

\clearpage
\appendix

\section{Parameter files}


\subsection{Parameter file format}


Here is a self-explanatory example of the parameter file:

\noindent\makebox[\linewidth]{\hrulefill}
\begin{verbatim}
xname=X         ! Name of the X-column
yname =Y        ! Name of the Y-column
zname = 'Z col'  - name of the Z-column; note quotes because the value 
                   has spaces.

 zname = Z    - this line is discarded because there is no letter in the
first position.

This line is discarded too because its begining is not in the name=value
format.

The next line tells to include the contents of /home/user/calpars.par
@include{/home/user/calpars.par}

You can use environment variables or the values of other parameters in the
parameter values. This is useful for the definition of directory trees:
CALDIR = /data/CALDB/data/chandra/
hrma_onaxis_area = $CALDIR/tel/hrma/bcf/effarea/hrmaD1999-07-22axeffaN0004.fits
@include{$HOME/calpars.par}
\end{verbatim}
\noindent\makebox[\linewidth]{\hrulefill}

IRAF-style parameter files also can be parsed, although I did not do a lot
of testing. Avoid IRAF par.\ files because they are usually
incomprehensible.


\subsection{Command line versus parameter file arguments}



My programs do not make a distinction between command-line and parameter
file arguments. You are supposed to supply the parameter file as
\verb+@par.file+ in the command line. The program behaves as if the
par.file parameters are given in the command line. This provides an easy way 
of resetting several parameters without editing the par.file. For example,
\begin{verbatim}
 % prog xname=XXX @par.file
\end{verbatim}
is equivalent to
\begin{verbatim}
 % prog xname=XXX xname=X yname=Y zname='Z col'
\end{verbatim}
and the program will determine that \texttt{xname=XXX} because this is found
first.

Command line arguments may be given in the \texttt{name=value} (with
optional spaces on either side of \texttt{=}) or in the \texttt{-name value} 
format.



\end{document}


