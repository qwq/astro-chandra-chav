\contentsline {section}{\numberline {1}What can this software do?}{2}
\contentsline {section}{\numberline {2}Science thread}{3}
\contentsline {subsection}{\numberline {2.1}Preparing the data}{3}
\contentsline {subsection}{\numberline {2.2}Setting up parameter file}{3}
\contentsline {subsection}{\numberline {2.3}Making image}{3}
\contentsline {subsection}{\numberline {2.4}Making exposure maps}{3}
\contentsline {subsection}{\numberline {2.5}Extracting spectra}{3}
\contentsline {section}{\numberline {3}Event filtering}{3}
\contentsline {subsection}{\numberline {3.1}\texttt {BADPIXFILTER} --- apply bad pixel filter}{3}
\contentsline {subsection}{\numberline {3.2}\texttt {FILTEVT} --- filter event lists}{3}
\contentsline {section}{\numberline {4}Spectrum extraction}{4}
\contentsline {subsection}{\numberline {4.1}\texttt {EXTRSPEC} - extract a spectrum in a single region}{4}
\contentsline {subsection}{\numberline {4.2}\texttt {RUNEXTRSPEC} --- extract spectra in a set of regions and in individual chips.}{6}
\contentsline {subsection}{\numberline {4.3}\texttt {CALCARF} --- generate an ARF file for the PHA spectrum}{6}
\contentsline {subsection}{\numberline {4.4}\texttt {CALCRMF} --- generate the response matrix}{7}
\contentsline {section}{\numberline {5}Exposure and instrument maps}{8}
\contentsline {subsection}{\numberline {5.1}Outline}{8}
\contentsline {subsection}{\numberline {5.2}\texttt {CHIPMAP} --- generate chip map in detector coordinates}{8}
\contentsline {subsection}{\numberline {5.3}\texttt {ASPECTHIST} --- create aspect histograms}{9}
\contentsline {subsection}{\numberline {5.4}\texttt {CONVASPECT} --- convolve the chip map with the aspect histogram}{10}
\contentsline {section}{\numberline {6}Troubleshooting}{10}
\contentsline {subsection}{\numberline {6.1}I am getting a FITSIO error ...}{10}
\contentsline {section}{\numberline {A}Parameter files}{11}
\contentsline {subsection}{\numberline {A.1}Parameter file format}{11}
\contentsline {subsection}{\numberline {A.2}Command line versus parameter file arguments}{11}
