      implicit none
      real e, ccd_qe_main, ccd_qe, corrarf_spatial

      real egrid(10000), qe1(10000), qe2(10000)
      integer i

      integer ccd_id 

      ccd_id = 3

      do i=1,10000
        egrid(i)=i*0.001
      enddo

      do i=1,10000
        qe1(i) = corrarf_spatial (egrid(i),ccd_id,60,50)
        print '(f6.3,1x,1pe11.5)',egrid(i),qe1(i)
      enddo
      


*      e=4.510
*      print*,e,ccd_qe_main(e,0)
*      print*,e,ccd_qe_main(e,1)
*      print*,e,ccd_qe_main(e,2)
*      print*,e,ccd_qe_main(e,3)
*      print*,e,ccd_qe_main(e,6)
*      print*,e,ccd_qe_main(e,7)
*
*      print*
*      print*,e,ccd_qe_main(e,0)
*      print*,e,ccd_qe_main(e,1)
*      print*,e,ccd_qe_main(e,2)
*      print*,e,ccd_qe_main(e,3)
*      print*,e,ccd_qe_main(e,6)
*      print*,e,ccd_qe_main(e,7)

*      e=0.665
*      print*,e,
*     ~    ccd_qe_main(e,7)/ccd_qe_main(5.898,7),
*     ~    ccd_qe_main(e,5)/ccd_qe_main(5.898,5) 


*      e=0.665
*      print*, e, ccd_qe_main(e,3), ccd_qe_main(e,5), ccd_qe_main(e,7)
*
*      e=1.000
*      print*, e, ccd_qe_main(e,3), ccd_qe_main(e,5), ccd_qe_main(e,7)
*
*      e=1.487
*      print*, e, ccd_qe_main(e,3), ccd_qe_main(e,5), ccd_qe_main(e,7)
*
*      e=4.510
*      print*, e, ccd_qe_main(e,3), ccd_qe_main(e,5), ccd_qe_main(e,7)
*
*      e=4.9
*      print*, e, ccd_qe_main(e,3), ccd_qe_main(e,5), ccd_qe_main(e,7)
*
*
*      e=5.898
*      print*, e, ccd_qe_main(e,3), ccd_qe_main(e,5), ccd_qe_main(e,7)
*
*
*      e=6.5
*      print*, e, ccd_qe_main(e,6), ccd_qe_main(e,7)
*
*      e=0.618
*      print*, e, ccd_qe_main(e,6), ccd_qe_main(e,7)
*      e=0.638
*      print*, e, ccd_qe_main(e,6), ccd_qe_main(e,7)
*      e=0.706
*      print*, e, ccd_qe_main(e,6), ccd_qe_main(e,7)
*
*      print*, 0.67,
*     ~    (1.5*ccd_qe_main(0.64,6)+ccd_qe_main(0.705,6))/2.5,
*     ~    (1.5*ccd_qe_main(0.64,7)+ccd_qe_main(0.705,7))/2.5

      
      end
