      implicit none
      integer ne
      parameter (ne=28)
      real e(ne), qeu(ne)
      data e /0.1000, 0.1212, 0.1468, 0.1778, 0.2154, 0.2610, 0.3162, 0.3831,
     ~    0.4642, 0.5623, 0.6813, 0.8254, 1.0000, 1.2115, 1.4678, 1.7783,
     ~    1.8200, 1.8400, 2.1544, 2.6102, 3.1623, 3.8312, 4.6416, 5.6234,
     ~    6.8129, 8.2540, 10.0000, 12.000/

      integer iccd, ix, iy, cx, cy, ie
      integer stepx,stepy
      real ccd_qe_qeu

      print '(''                          CORRECTION FOR E (keV)'')'
      print '(''                     '',196(1A))',('-',ie=1,7*ne)
      print
     ~    '(''C'','' XMIN'','' XMAX'','' YMIN'','' YMAX'',26(1x,f6.4),2(1x,f6.3))',(e(ie),ie=1,ne-2),e(ne-1),e(ne)
      print '(219(1A))',('-',ie=1,7*ne+23)
      do iccd = 0,7
        if (iccd.eq.5.or.iccd.eq.7) then
          stepx=1
          stepy=16
        else
          stepx=4
          stepy=32
        endif
        do ix = 1,1024,stepx
          do iy = 1,1024,stepy
            cx = ix+stepx/2
            cy = iy+stepy/2
            do ie=1,ne
              qeu(ie) = ccd_qe_qeu (e(ie),iccd,cx,cy)
              qeu(ie) = min(qeu(ie),1.0)
            enddo
            print '(i1,2(1x,i4,1x,i4),28(1x,f6.4))',
     ~          iccd,ix,ix+stepx-1,iy,iy+stepy-1,
     ~          (qeu(ie),ie=1,ne)
          enddo
        enddo
      enddo

      end








