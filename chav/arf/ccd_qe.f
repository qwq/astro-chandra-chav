      real function ccd_qe (egrid,l,ne,chipx,chipy,iccd)
! CANNOT BE USED WITH DIFFERENT EGRIDS!
!
!
      implicit none
      integer ne,l
      real egrid(ne), chipx, chipy
      integer iccd

      integer nccd
      parameter (nccd=10)
      integer nregmax           ! max number of regions in all CCDs
      parameter (nregmax=4*nccd)
      integer nemax             ! max number of energy channels
      parameter (nemax=20000)
      real qe_main (nemax,nregmax)
      logical inidata (nregmax)
      integer iqemain (0:nccd-1,20) ! index for qe_main arrays; assume no more 
                                ! than 20 regions per ccd
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      logical corrcti, corrqeu, newcorrcti
      save iqemain,qe_main,inidata, corrcti, corrqeu, newcorrcti
      character*80 arg
      logical no, defined
      external no, defined
      
      integer i,ireg,iqe
      real qe

      real ccd_qe_main, ccd_qe_corr, ccd_qe_qeu, ccd_qe_corr_jul2000
      external ccd_qe_main, ccd_qe_corr, ccd_qe_qeu, ccd_qe_corr_jul2000

      real lowecor, elowecor
      save lowecor, elowecor

      logical correct_cont, correct_cont_spatial, correct_cont_caldb
      save correct_cont, correct_cont_spatial, correct_cont_caldb

      logical tweak_s3_qe
      save tweak_s3_qe
      real corr

      logical yes, yespar
      external yes, yespar
      real corrarf, corrarf_spatial, corrarf_caldb
      external corrarf, corrarf_spatial, corrarf_caldb

      real dead_area_cor
      save dead_area_cor

      character password*80


      include 'calcomments.inc'
      


      if (firstcall) then

        do i=1,nregmax
          inidata(i)=.true.
        enddo

        do i=0,nccd-1
          do ireg=1,20
            iqemain(i,ireg)=i+1 ! but we can set different regions within
                                ! the CCD
          enddo
        enddo

        call get_cl_par ('correct_qe_cti',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL CORRECT FOR THE CTI DROP IN QE'
          corrcti=.true.
          if (arg.eq.'new') then
            newcorrcti = .true.
            print*,'USING NEW RELATIONS BASED ON JULY 2000 DATA'
          else
            newcorrcti = .false.
          endif
          NCALCOM = NCALCOM + 1
          CALCOMNAME(NCALCOM) = 'AVQECTI'
          if (newcorrcti) then
            CALCOMVALUE(NCALCOM) = 'new'
            CALCOMCOM(NCALCOM) = 'ccd qe cti based on july 2000 data'
          else
            CALCOMVALUE(NCALCOM) = 'old'
            CALCOMCOM(NCALCOM) = 'old qe cti correction'
          endif
        else
          corrcti=.false.
        endif

        call get_cl_par ('correct_qeu',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL USE THE QEU MAP'
          corrqeu=.true.
        else
          corrqeu=.false.
        endif

        call get_pv_default ('dead_area_cor',dead_area_cor,0.0,'e')
        if (dead_area_cor.ne.0.0) then
          write (0,*) 'DEAD AREA CORRECTION AT CHIPY=1024 IN FI:',
     ~        dead_area_cor
        endif
        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'FIDEADARE'
        write (CALCOMVALUE(NCALCOM),*) dead_area_cor
        CALCOMCOM(NCALCOM) = '%fmax dead area correction in FI CCDs'
        


        call get_pv_default ('ccd_qe_lowecor',lowecor,1.0,'e')
        if (lowecor.ne.1.0) then
          print *,'I WILL MULTIPLY QE in FI chips below 1.832 keV by ',lowecor
          call get_pv_default ('e_qe_lowecor',elowecor,1.832,'e')
        endif
        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'AVQELOW'
        write (CALCOMVALUE(NCALCOM),*) lowecor
        CALCOMCOM(NCALCOM) = '%fccd QE low E correction in FI chips'

        correct_cont = .false.
        correct_cont_spatial = .false.
        correct_cont_caldb = .false.
        call get_cl_par ('correct_acis_contamination',arg)


        if (yes(arg)) then
          correct_cont = .true.
          correct_cont_spatial = .false.
          correct_cont_caldb = .false.
        endif
        if (arg.eq.'spatial') then
          correct_cont=.false.
          correct_cont_spatial = .true.
          correct_cont_caldb = .false.
        endif
        if (arg.eq.'caldb') then
*          call get_cl_par ('contam_password',password)
*          if (password.ne.'��authorized') then
*            call exiterror
*     ~          ('SORRY, CHAV DOES NOT SUPPORT RELEASED CALIBRATION; PLEASE SWITCH TO CIAO')
*          endif
          correct_cont=.false.
          correct_cont_spatial = .false.
          correct_cont_caldb = .true.
        endif
        if (correct_cont) print *,'OBF_CONTAMINATION+CORRECTION = ON'
        if (correct_cont_spatial) then
          print *,'OBF_CONTAMINATION+CORRECTION = SPATIAL'
        endif
        if (correct_cont_caldb) then
          print *,'OBF_CONTAMINATION+CORRECTION = CALDB'
        endif

        tweak_s3_qe = yespar('tweak_s3_qe')
        if (tweak_s3_qe) then
          print*,'S3_QE_TWEAK=ON'
          NCALCOM = NCALCOM + 1
          CALCOMNAME(NCALCOM) = 'S3TWEAK'
          CALCOMVALUE(NCALCOM) = 'yes'
          CALCOMCOM(NCALCOM) = 'Applied tweak to s3 qe'
        else
          print*,'S3_QE_TWEAK=OFF'
        endif
          

        firstcall = .false.
      endif
            
      ireg = 1                  ! but we can determine the region based on
                                ! chipx,chipy and iccd
      
      iqe = iqemain(iccd,ireg)
      
      if (inidata(iqe)) then
        do i=1,ne
          if (iccd.eq.7.and.tweak_s3_qe) then
            if (egrid(i).gt.3.5) then
              qe_main(i,iqe)=ccd_qe_main(egrid(i)/1.05,iccd)
            else
              corr = 1.05-0.16*log10(egrid(i))
              if (corr.lt.1.0) corr = 1.0
              qe_main(i,iqe)=ccd_qe_main(egrid(i),iccd)*corr
            endif
          else
            qe_main(i,iqe)=ccd_qe_main (egrid(i),iccd)
          endif
        enddo
        inidata(iqe)=.false.
      endif

      qe = qe_main(l,iqe)

      if (corrcti) then
        if (newcorrcti) then
          qe = qe*ccd_qe_corr_jul2000 (egrid(l),iccd,chipx,chipy)
        else
          qe = qe*ccd_qe_corr (egrid(l),iccd,chipx,chipy)
        endif
      endif

      if (corrqeu) then
        qe = qe*ccd_qe_qeu (egrid(l),iccd,nint(chipx),nint(chipy))
      endif

      if ( iccd.ne.7.and.iccd.ne.5.and.egrid(l).lt.elowecor ) then
        qe = qe * lowecor
      endif

      if (correct_cont_spatial) then
        qe = qe * corrarf_spatial (egrid(l),iccd,nint(chipx),nint(chipy))
      else
        if (correct_cont_caldb) then
          qe = qe * corrarf_caldb (egrid(l),iccd,nint(chipx),nint(chipy))
        else
          if (correct_cont) then
            qe = qe * corrarf(egrid(l),iccd,nint(chipx),nint(chipy))
          endif
        endif
      endif

      if (iccd.ne.7.and.iccd.ne.5) then
        qe = qe / (1+dead_area_cor*(chipy+1024.0)/2048)
      endif

      ccd_qe = qe

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function ccd_qe_corr_jul2000 (e,iccd,chipx,chipy)
      implicit none
      real e, chipx,chipy
      integer iccd

c the same correction is used for all chips and chipx
      real emin,efit,afit
      parameter (
     ~    emin=2.7,             ! energy where the linear drop starts
     ~    efit=6.0,             ! energy at which the chipy-fit is done
     ~    afit=0.155)           ! half amplitude at chipy=1024
      real y0(0:9), yw(0:9) 
      data y0 /540.0, 540.0, 540.0, 540.0, ! centroid of the error function
     ~    335.0, 335.0, 335.0, 335.0, 335.0, 335.0/
      data yw /350.0, 350.0, 350.0, 350.0, ! width of the error function
     ~    200.0, 200.0, 200.0, 200.0, 200.0, 200.0 /
      save y0, yw

      real erf

      if (iccd.eq.7.or.iccd.eq.5) then
        ccd_qe_corr_jul2000 = 1.0
      else
        if (e.le.emin) then
          ccd_qe_corr_jul2000 = 1.0
        else
          ccd_qe_corr_jul2000 = 1.0 - 
     ~        afit*(erf((chipy-y0(iccd))/yw(iccd))+1.0)
     ~        *log(e/emin)/log(efit/emin)
        endif
      endif
        
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function ccd_qe_corr (e,iccd,chipx,chipy)
      implicit none
      real e, chipx,chipy
      integer iccd

c the same correction is used for all chips and chipx
      real emin,efit,afit,y0,yw
      parameter (
     ~    emin=2.7,             ! energy where the linear drop starts
     ~    efit=6.0,             ! energy at which the chipy-fit is done
     ~    afit=0.1,             ! half amplitude at chipy=1024
     ~    y0=300.0,             ! centroid of the error function
     ~    yw=170.0              ! width of the error function
     ~    )
      real erf

      if (iccd.eq.7.or.iccd.eq.5) then
        ccd_qe_corr = 1.0
      else
        if (e.le.emin) then
          ccd_qe_corr = 1.0
        else
          ccd_qe_corr = 1.0 - 
     ~        afit*(erf((chipy-y0)/yw)+1.0)*log(e/emin)/log(efit/emin)
        endif
      endif
        
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function ccd_qe_main (e,iccd)
      implicit none
c 
c CCD+filter QE
c I e   real    -- photon energy [keV]
c
c Interpolates over cal data points. Since one typically calculates the onaxis
c area array only once, we can afford using cubic spline interpolation
c
c USES  spline from NR
c

      real e
      integer iccd

c
      integer nemax, nchips
      parameter (nemax=11000)
      parameter (nchips=10)
      integer ne(0:nchips-1)
      logical dataread (0:nchips-1)
      real e0(nemax,0:nchips-1), ea(nemax,0:nchips-1)
*     real work(nemax,0:nchips-1)
      data dataread /
     ~    .false., .false., .false., .false., .false.,
     ~    .false., .false., .false., .false., .false. /
      save ne, e0, ea, dataread
      
      character*200 calfile
      logical defined
      character*80 chipname
      integer chipid

      integer ntweak, unit, status, i
      real etweak(nemax*10), qetweak(nemax*10)
      character parname*80, parvalue*200
      integer lnblnk, newunit

      integer qexpand

      real a

      logical headerdone
      data headerdone /.false./
      save headerdone

      include 'calcomments.inc'

      if (.not.dataread(iccd)) then
        call rp_q_expand_vars (qexpand)
        call rp_set_expand_vars (1)
        call get_cl_par ('ccd_qe',calfile)
        if (.not.defined(calfile)) then
          chipid=iccd
          call chip_id_to_normname (chipid,chipname)
          call get_cl_par ('ccd_qe_'//chipname,calfile)
          if (.not.defined(calfile)) then
            call exiterror('ccd_qe is not set for '//chipname)
          endif
          NCALCOM = NCALCOM + 1
          write (CALCOMNAME(NCALCOM),'(''CCDQE'',i1)') iccd
          CALCOMVALUE(NCALCOM) = calfile
          CALCOMCOM(NCALCOM) = ' '
        else
          if (.not.headerdone) then
            headerdone = .true.
            NCALCOM = NCALCOM + 1
            CALCOMNAME(NCALCOM) = 'CCDQE'
            CALCOMVALUE(NCALCOM) = calfile
            CALCOMCOM(NCALCOM) = ' '
          endif
        endif
        call rp_set_expand_vars (qexpand)
        
c  if calfile=1, set QE to 1
        if (calfile.eq.'1') then
          ne(iccd)=2
          e0(1,iccd)=0.0
          e0(2,iccd)=20.0
          ea(1,iccd)=1
          ea(1,iccd)=1
          return

c  else, normal read
        else
        
c        1) Read cal. data
          call read_ccd_qe (calfile,ne(iccd),e0(1,iccd),ea(1,iccd),iccd)

c        2) Is there any tweak for this CCD?
          write (parname,'(''ccd_qe_tweak_'',i1)') iccd
          call get_cl_par (parname,parvalue)
          if (defined(parvalue)) then
            write (0,*) 'APPLY QE TWEAK FOR CCD#',iccd,': '
     ~          ,parvalue(1:lnblnk(parvalue))
            NCALCOM = NCALCOM + 1
            write (CALCOMNAME(NCALCOM),'(''QETWEAK'',i1)') iccd
            CALCOMVALUE(NCALCOM) = parvalue
            CALCOMCOM(NCALCOM) = ' '
            
            ntweak = 0
            unit = newunit()
            status = 0
            open (unit,file=parvalue,status='old')
            do while (status.eq.0) 
              read (unit,*,iostat=status) etweak(ntweak+1), qetweak(ntweak+1)
              if (status.eq.0) ntweak = ntweak + 1
            enddo
            close (unit)
            do i=1,ne(iccd)
              call lin_interpolate (etweak,qetweak,ntweak,e0(i,iccd),a)
              ea(i,iccd)=ea(i,iccd)*a
            enddo
          endif
        endif
*c        2) Ini spline
*        call spline(e0(1,iccd),ea(1,iccd),ne(iccd),
*     ~      1.0e30,1.0e30,work(1,iccd))
        dataread(iccd)=.true.
      endif

      if (e.lt.e0(1,iccd).or.e.gt.e0(ne(iccd),iccd)) then
        a = 0
      else
*        call splint (e0(1,iccd),ea(1,iccd),work(1,iccd),ne(iccd),e,a)
        call lin_interpolate (e0(1,iccd),ea(1,iccd),ne(iccd),e,a)
      endif
      ccd_qe_main = a
      
      return
      end



*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      
      real function ccd_qe_qeu (e,iccd,chipx,chipy)
      implicit none
      real e
      integer iccd,chipx,chipy

      logical firstcall
      data firstcall /.true./
      save firstcall

      logical new_qeu
      real qeuMn(1024,64,0:9)
      save new_qeu, qeuMn
      real bad_grade_fraction
      real bad_grade_ratio
      external bad_grade_ratio

      logical qexpand
      character*200 calfile
      logical defined
      external defined
      
c
      integer nemax, nchips, nchipxmax, nchipymax
      parameter (nemax=300,nchipxmax=32,nchipymax=32)
      parameter (nchips=10)
      integer ne(0:nchips-1), nchipx(0:nchips-1), nchipy(0:nchips-1)
      logical dataread (0:nchips-1)
      real e0(nemax,0:nchips-1), qeu(nemax*nchipxmax*nchipymax,0:nchips-1)
      real crpix(3,0:nchips-1), crval(3,0:nchips-1), cdelt(3,0:nchips-1)
      data dataread /
     ~    .false., .false., .false., .false., .false.,
     ~    .false., .false., .false., .false., .false. /
      save ne, nchipx, nchipy, e0, qeu, dataread, crpix, crval, cdelt
      integer ichipx1,ichipx2,ichipy1,ichipy2
      real wchipx1,wchipx2,wchipy1,wchipy2
      integer chipxold,chipyold,iccdold
      data iccdold,chipxold,chipyold /-1,-1,-1/
      save ichipx1,ichipx2,ichipy1,ichipy2,wchipx1,wchipx2,wchipy1,wchipy2,
     ~    chipxold,chipyold,iccdold
      integer ie1,ie2,we1,we2
      real wchipx,wchipy, wqe
      logical yespar
      integer xbin
      save xbin
      integer ii
      real qeuMn_mean
      integer nqeuMn_mean

      logical headerdone
      data headerdone /.false./
      save headerdone
      include 'calcomments.inc'

      if (firstcall) then
        firstcall = .false.
        new_qeu = yespar ('new_qeu')
        if (new_qeu) then
          call rp_q_expand_vars (qexpand)
          call rp_set_expand_vars (1)
          call get_parameter_value_s ('qeuMn',calfile)
          call rp_set_expand_vars (qexpand)
          call read_qeu_Mn (calfile,qeuMn,1024,64,10)
          NCALCOM = NCALCOM + 1
          CALCOMNAME(NCALCOM) = 'QEUMN'
          CALCOMVALUE(NCALCOM) = calfile
          CALCOMCOM(NCALCOM) = ' '
        endif
        call get_pv_default ('wmap_xbin',xbin,0,'j')
c Note: wmap_xbin is set by calc_arf (xbin = 1/cdelt1)
        print*, 'xbin=',xbin
      endif
        
      if (new_qeu) then
        bad_grade_fraction = bad_grade_ratio (e,iccd)
        if (xbin.eq.0) then
          qeuMn_mean = qeuMn(chipx,chipy/16+1,iccd)
        else
          qeuMn_mean = 0
          nqeuMn_mean = 0
          do ii=max(1,chipx-xbin/2+1),min(chipx+xbin/2,1024)
            qeuMn_mean = qeuMn_mean + qeuMn(ii,chipy/16+1,iccd)
            nqeuMn_mean = nqeuMn_mean + 1
          enddo
          if (nqeuMn_mean.gt.0) then
            qeuMn_mean = qeuMn_mean / nqeuMn_mean
          else
            qeuMn_mean = 1
          endif
        endif
        if (iccd.eq.7.or.iccd.eq.5) then
          ccd_qe_qeu = 1 - bad_grade_fraction +
     ~        bad_grade_fraction*exp(log(qeuMn_mean)*e/5.9)
        else
          ccd_qe_qeu = 1 - bad_grade_fraction + bad_grade_fraction*qeuMn_mean
        endif
        return
      endif

      if (.not.dataread(iccd)) then
        call rp_q_expand_vars (qexpand)
        call rp_set_expand_vars (1)
        call get_cl_par ('ccd_qeu',calfile)
        if (.not.defined(calfile)) then
          call exiterror('ccd_qeu is not set')
        endif
        if (.not.headerdone) then
          headerdone=.true.
          NCALCOM = NCALCOM + 1
          CALCOMNAME(NCALCOM) = 'QEU'
          CALCOMVALUE(NCALCOM) = calfile
          CALCOMCOM(NCALCOM) = ' '
        endif
        call rp_set_expand_vars (qexpand)
        call read_ccd_qeu (calfile,
     ~      ne(iccd),nchipx(iccd),nchipy(iccd),
     ~      crpix(1,iccd), crval(1,iccd), cdelt(1,iccd),
     ~      nemax*nchipxmax*nchipymax,
     ~      e0(1,iccd),qeu(1,iccd),iccd)
        dataread(iccd)=.true.
*        if (yespar('show_qeu_map')) then
*          do ie1 = 1,ne(iccd)
*            if (e0(ie1,iccd).gt.7.05.and.e0(ie1,iccd).lt.7.1) then
*              print*,e0(ie1,iccd)
*              call show_qeu_map (qeu(1,iccd),ie1,ne(iccd),nchipx(iccd)
*     ~            ,nchipy(iccd))
*            endif
*          enddo
*        endif
      endif

      if (chipx.ne.chipxold.or.chipy.ne.chipyold.or.iccd.ne.iccdold) then
        wchipy = (chipy-crval(3,iccd))/cdelt(3,iccd)+crpix(3,iccd)
        
        wchipx = (chipx-crval(2,iccd))/cdelt(2,iccd)+crpix(2,iccd)
        if (nchipx(iccd).le.4) then ! do not do extrapolation
          ichipx1 = nint(wchipx)
          ichipx1 = min(max(ichipx1,1),nchipx(iccd))
          ichipx2 = ichipx1
        else
          if (wchipx.lt.1.0) then
            ichipx1 = 1
            ichipx2 = 2
          else if (wchipx.gt.nchipx(iccd)) then
            ichipx1 = nchipx(iccd)-1
            ichipx2 = nchipx(iccd)
          else
            ichipx1 = int(wchipx)
            ichipx2 = ichipx1+1
          endif
        endif

        if (wchipy.lt.1.0) then
          ichipy1 = 1
          ichipy2 = 2
        else if (wchipy.gt.nchipy(iccd)) then
          ichipy1 = nchipy(iccd)-1
          ichipy2 = nchipy(iccd)
        else
          ichipy1 = int(wchipy)
          ichipy2 = ichipy1+1
        endif
      
        if (ichipx1.ne.ichipx2) then
          call get_weights (wchipx,float(ichipx1),float(ichipx2),wchipx1
     ~        ,wchipx2)
        else
          wchipx1=0.5
          wchipx2=0.5
        endif
        
        call get_weights (wchipy,float(ichipy1),float(ichipy2),wchipy1,wchipy2)
        
        chipxold = chipx
        chipyold = chipy
        iccdold  = iccd

      endif

      call find_e_inter_grid_ccd_qe (e,e0(1,iccd),ne(iccd),ie1,ie2,we1,we2)
      
      call interpolate_qeu (qeu(1,iccd),
     ~    ie1,ie2,ichipx1,ichipx2,ichipy1,ichipy2,
     ~    we1,we2,wchipx1,wchipx2,wchipy1,wchipy2,
     ~    ne(iccd), nchipx(iccd),nchipy(iccd),
     ~    wqe)

      ccd_qe_qeu=wqe
      
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine interpolate_qeu (qeu,
     ~    ie1,ie2,ichipx1,ichipx2,ichipy1,ichipy2,
     ~    we1,we2,wchipx1,wchipx2,wchipy1,wchipy2,
     ~    ne, nchipx,nchipy,
     ~    wqe)
      implicit none
      integer ne, nchipx,nchipy
      real qeu(ne,nchipx,nchipy)
      integer ie1,ie2,ichipx1,ichipx2,ichipy1,ichipy2
      real we1,we2,wchipx1,wchipx2,wchipy1,wchipy2
      real wqe

      wqe =
     ~    qeu(ie1,ichipx1,ichipy1)*we1*wchipx1*wchipy1+
     ~    qeu(ie1,ichipx1,ichipy2)*we1*wchipx1*wchipy2+
     ~    qeu(ie1,ichipx2,ichipy1)*we1*wchipx2*wchipy1+
     ~    qeu(ie1,ichipx2,ichipy2)*we1*wchipx2*wchipy2+
     ~    qeu(ie2,ichipx1,ichipy1)*we2*wchipx1*wchipy1+
     ~    qeu(ie2,ichipx1,ichipy2)*we2*wchipx1*wchipy2+
     ~    qeu(ie2,ichipx2,ichipy1)*we2*wchipx2*wchipy1+
     ~    qeu(ie2,ichipx2,ichipy2)*we2*wchipx2*wchipy2
      
*      print*,ie1,ichipx1,ichipy1,qeu(ie1,ichipx1,ichipy1)
*
*      print*,ie1,ie2,ichipx1,ichipx2,ichipy1,ichipy2,
*     ~    qeu(ie1,ichipx1,ichipy1)*we1*wchipx1*wchipy1+
*     ~    qeu(ie1,ichipx1,ichipy2)*we1*wchipx1*wchipy2+
*     ~    qeu(ie1,ichipx2,ichipy1)*we1*wchipx2*wchipy1+
*     ~    qeu(ie1,ichipx2,ichipy2)*we1*wchipx2*wchipy2+
*     ~    qeu(ie2,ichipx1,ichipy1)*we2*wchipx1*wchipy1+
*     ~    qeu(ie2,ichipx1,ichipy2)*we2*wchipx1*wchipy2+
*     ~    qeu(ie2,ichipx2,ichipy1)*we2*wchipx2*wchipy1+
*     ~    qeu(ie2,ichipx2,ichipy2)*we2*wchipx2*wchipy2

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine find_e_inter_grid_ccd_qe (e,e0,ne,ie1,ie2,we1,we2)
      implicit none
      real e
      integer ne
      real e0(ne)
      integer ie1,ie2,we1,we2
      integer i

      if (e.le.e0(1)) then
        ie1=1
        ie2=2
      else if (e.ge.e0(ne)) then
        ie1=ne-1
        ie2=ne
      else
        i = ne/2
        call hunt (e0,ne,e,i)
        ie1=i
        ie2=i+1
      endif

      call get_weights (e,e0(ie1),e0(ie2),we1,we2)
      return
      end
        

**xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*
*      subroutine show_qeu_map (qeu,ie,ne, nchipx,nchipy)
*      implicit none
*      integer ne, nchipx,nchipy,ie
*      real qeu(ne,nchipx,nchipy)
*      real gov (32,32)
*      
*      integer i,j
*      do i=1,32
*        do j=1,32
*          gov(i,j)=0
*        enddo
*      enddo
*      
*      do i=1,nchipx
*        do j=1,nchipy
*          gov(i,j)=qeu(ie,i,j)
*        enddo
*      enddo
*
*      call saoimage (gov,32,32,'e')
*      end
*
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function corrarf (E,iccd,chipx,chipy)
      implicit none
      real E
      integer iccd,chipx,chipy
c
c Prints out exposure associated with the GTI file
c It is the user's responsibility to point to correct extension in the GTI
c file
      
      character*200 gtifile, contfile
      logical defined

      integer nt, ntmax
      parameter (ntmax=16384)
      double precision tstart(ntmax), tstop(ntmax), texp, ratio, avratio
      double precision mjdref, t, mjd0, mjd
      double precision tau

      character*200 startname,stopname, comment

      integer unit
      integer status,colnum,i
      logical anyf

c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      integer ncont
      real econt(10000), taucont(10000)
      save ncont, econt, taucont

      real w1,w2,taunorm, extranorm
      logical warned
      save taunorm

      include 'calcomments.inc'

      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('gti',gtifile)
        if (defined(gtifile)) then
          status = 0
          call ftgiou (unit,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          
          call ftnopn (unit,gtifile,0,status)
          if (status.ne.0) then ! not a fits file
            call exit_fitsio (gtifile,status)
          endif
        else
          call get_cl_par ('gtifile',gtifile)
          if (.not.defined(gtifile)) then
            write (0,*) 'gtifile=?'
            call exit(1)
          endif
          status = 0
          call ftgiou (unit,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          call ftnopn (unit,gtifile,0,status)
          call FTMNHD (unit, -1,'GTI',0,status)
          if (status.ne.0) then
            status = 0
            call FTMNHD (unit, -1,'STDGTI',0,status)
          endif
        endif
        
        
        call ftgnrw (unit,nt,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        if (nt.gt.ntmax) then
          write (0,*) 'Too many gti: ',nt,' (',ntmax,' maximum allowed)'
          call exit(1)
        endif
        
        call get_cl_par ('stopname',stopname)
        if (.not.defined(stopname)) stopname = 'STOP'
        call get_cl_par ('startname',startname)
        if (.not.defined(startname)) startname = 'START'
        
        call ftgkyd (unit,'MJDREF',mjdref,comment,status)
        call sla_cldj (1999,1,1,mjd0,i)
        
        call FTGCNO (unit,.false.,startname,colnum,status)
        call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstart,anyf,status)
        call FTGCNO (unit,.false.,stopname,colnum,status)
        call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstop,anyf,status)
        call ftclos (unit,status)
        call ftfiou (unit,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
      
        texp = 0
        avratio = 0
        warned = .false.
        
        do i=1,nt
          t = 0.5*(tstop(i)+tstart(i))
          t = t / 86400         ! convert to days
          mjd = mjdref + t
          if (mjd-mjd0.gt.386.0) then
*          ratio = (6.86997E-03 -1.86443E-06*(mjd-mjd0))/0.0072
            ratio = exp(-0.582*(1-exp(-(mjd-mjd0-204.0)/620.0)))
          else
            if (.not.warned) then
              write (0,*) 
     ~            'warning: correction may be poor for data prior 21/01/00'
              warned = .true.
            endif
*          ratio = 1.38-0.23*log10(mjd-mjd0-200.0)
            ratio = exp(-0.582*(1-exp(-(mjd-mjd0-204.0)/620.0)))
          endif
          texp = texp + tstop(i)-tstart(i)
          avratio = avratio + ratio*(tstop(i)-tstart(i))
        enddo
        
        ratio = avratio/texp
        tau = - log(ratio)*(0.67/0.286)**3
        write (0,*) sngl(ratio),' - loss of effective area at 0.67 keV'

c Read in the contamination absorption file
        call get_cl_par ('qe_cont',contfile)
        if (.not.defined(contfile)) then
          write (0,*) 'qe_cont=?'
          call exit(1)
        endif
        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CCDCONT'
        CALCOMVALUE(NCALCOM) = contfile
        CALCOMCOM(NCALCOM) = 'ACIS contamination tau(E)'

        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CONTSPAT'
        CALCOMVALUE(NCALCOM) = 'no'
        CALCOMCOM(NCALCOM) = 'spatially-dependent correction?'
       
        
c Find normalization using the ext cal. source data
        call load_qe_cont (contfile,econt,taucont,ncont)
        w1 = -log(ratio)
        call lin_interpolate (econt,taucont,ncont,0.67,w2)
        taunorm = w1/w2
        call get_pv_default ('corrarf_norm',extranorm,1.0,'e')
        taunorm = taunorm * extranorm
        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CONTNORM'
        write (CALCOMVALUE(NCALCOM),*) extranorm
        CALCOMCOM(NCALCOM) = 'extra norm for ACIS contamination correction'
      endif

      call lin_interpolate (econt,taucont,ncont,E,w1)
      w1 = w1*taunorm
      corrarf = exp(-w1)
      
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function corrarf_spatial (E,iccd,chipx,chipy)
c
c  Spatial- and time-dependent QE correction for contamination on the ACIS OBF
c
c  Performance optimized for repetitive call at different energies and the
c  same location on the CCD
      implicit none 
      real E
      integer iccd, chipx, chipy

      real date                 ! observation date, years (e.g., 2000.37)
      save date

      real contcor
      save contcor
      logical firstcall
      data firstcall /.true./
      save firstcall


      character*200 contfile
      integer ncont
      real econt1(10000), taucont1(10000), taurefL
      real taucont(20000) ! 20 keV , 1 eV step
      save taucont, taurefL, econt1, taucont1, ncont

      integer iccdold, chipxold, chipyold
      data iccdold /-1/
      save iccdold, chipxold, chipyold
      real tau_ECS
      save tau_ECS
      
      real taucenter, tauborder, tautop, offaxis

      logical defined
      real w1,w, ee(4), we(4)
      integer i
      real tau_time_dep, tau_spat_dep_i, tau_spat_dep_s

      include 'calcomments.inc'

      if (firstcall) then
        call get_pv_default ('contcor',contcor,0.0,'e')
        if (contcor.ne.0) then
          print*,'EXTRA CONTAMINATION - ',contcor,' opt depths at Mn/Fe-L'
          NCALCOM = NCALCOM + 1
          CALCOMNAME(NCALCOM) = 'EXTRACON'
          write (CALCOMVALUE(NCALCOM),*) contcor
          CALCOMCOM(NCALCOM) = 'EXTRA CONTAMINATION (tau for Mn/Fe-L)'
        endif
        call corrarf_get_obs_date (date)
        call get_cl_par ('qe_cont',contfile)
        if (.not.defined(contfile)) then
          write (0,*) 'qe_cont=?'
          call exit(1)
        endif
        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CCDCONT'
        CALCOMVALUE(NCALCOM) = contfile
        CALCOMCOM(NCALCOM) = 'ACIS contamination tau(E)'

        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CONTSPAT'
        CALCOMVALUE(NCALCOM) = 'yes'
        CALCOMCOM(NCALCOM) = 'spatially-dependent correction?'

        call load_qe_cont (contfile,econt1,taucont1,ncont)

        do i=1,20000
          call lin_interpolate (econt1,taucont1,ncont,0.001*i,w)
          taucont(i)=w
        enddo
        firstcall = .false.
      endif

      if (chipy.ne.chipyold.or.chipx.ne.chipxold.or.iccd.ne.iccdold) then
        if (iccd.ne.iccdold) then
c line energies and weights are from the April 2004 memo
          ee(1)=0.706
          ee(2)=0.683
          ee(3)=0.618
          ee(4)=0.559
          if (iccd.eq.5.or.iccd.eq.7) then
            we(1)=0.33
            we(2)=0.58
            we(3)=0.03
            we(4)=0.06
          else
            we(1)=0.39
            we(2)=0.54
            we(3)=0.02
            we(4)=0.05
          endif
c weighting of tau, not flux is from the April 2004 memo
          taurefL = 0
          do i=1,4
            call lin_interpolate (econt1,taucont1,ncont,ee(i),w1)
            taurefL = taurefL + we(i)*w1
          enddo
        endif
        
c Recalculate tau_ECS at the new point
        if (iccd.lt.4) then     ! ACIS-I
          taucenter = tau_time_dep (date,3)
          tauborder = tau_time_dep (date,4)
! tau_spat_dep_i assumes that the CCD size is 8x8 arcmin exactly
          if (iccd.eq.0.or.iccd.eq.3) then
            offaxis = sqrt(((1024.0-chipx)**2+(1024.0-chipy)**2))/1024.0*8.0
          else
            offaxis = sqrt((chipx**2+(1024.0-chipy)**2))/1024.0*8.0
          endif
          tau_ECS = tau_spat_dep_i (offaxis,taucenter,tauborder) + contcor
        else                    ! ACIS-S
          taucenter = tau_time_dep (date,0)
          tauborder = tau_time_dep (date,1)
          tautop    = tau_time_dep (date,2)
          offaxis = chipy
          tau_ECS = tau_spat_dep_s (offaxis,taucenter,tauborder,tautop)
     ~        + contcor
        endif
        iccdold = iccd
        chipxold = chipx
        chipyold = chipy
      endif

      i = nint(E*1000.0)
      i = min(max(i,1),20000)
      
      corrarf_spatial = exp(-taucont(i)*tau_ECS/taurefL)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_qe_cont (file,e,tau,n)
      implicit none
      character*(*) file
      integer n
      real e(*), tau(*)
      
      integer unit
      integer newunit
      integer status
      character*1024 buf

      unit = newunit()
      open (unit,file=file,status='old')
      n = 0
      status = 0
      do while (status.eq.0)
        read (unit,'(a)',iostat=status) buf
        if (status.eq.0) then
          if (buf(1:1).ne.'#') then
            read (buf,*,iostat=status) e(n+1),tau(n+1)
            if (status.eq.0) then
              n = n + 1
              tau(n)=-log(tau(n)) ! the file actually stores the 
                                ! absorption coefficient, not optical depth
            endif
          endif
        endif
      enddo

      close(unit)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function tau_spat_dep_i (offaxis,taucenter,tauborder)
      implicit none
      real offaxis,taucenter,tauborder
c
c Fit to the spatial dependence of contamination on ACIS-I
c
c offaxis   - offaxis angle in arcmin
c
c taucenter - optical depth at the center (8'x8' square centered on the aim
c             point) 
c tauborder - optical depth in the 2'-wide strip along the edge of the array
c
c NOTE: this function wrongly assumes that the CCD size is 8x8 arcmin exactly
c
c -> Thu Jul 10 00:51:51 2003
c
      real off0, off1
      real gamma 
      parameter (gamma=2.0)

      off0=3.24**gamma
      off1=8.07**gamma

c Eq. 17 & 8 in the April, 2004 memo
      tau_spat_dep_i = taucenter +
     ~    (tauborder-taucenter)*(offaxis**gamma-off0)/(off1-off0)

      return
      end


**xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*
*      real function tau_spat_dep_s (chipy,taucenter,taubottom)
*      implicit none 
*      real chipy, taucenter, taubottom
*c
*c  Fit to the spatial dependence of contamination on ACIS-S
*c
*c  taucenter - optical depth at the center
*c  taubottom - average optical depth at chipy=1:128
*c
*c -> Thu Jul 10 00:51:02 2003      
*c
*      real ycenter, alpha
*      parameter (ycenter=540.0)
*      parameter (alpha=4.5)
*
*c Eq. 7 & 11 in the April, 2004 memo
*      tau_spat_dep_s = taucenter +
*     ~    (taubottom-taucenter)*abs((chipy-ycenter)/(64.0-ycenter))**alpha
*      return
*      end
      
**xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*
*      real function tau_spat_dep_s (chipy,taucenter,taubottom,tautop)
*      implicit none 
*      real chipy, taucenter, taubottom, tautop
*c
*c  Fit to the spatial dependence of contamination on ACIS-S
*c
*c  taucenter - optical depth at the center
*c  taubottom - average optical depth at chipy=1:128
*c
*c -> Fri Apr  9 21:57:06 2004
*c
*      real decaybot, decaytop ! exponential decay scale, pixels
*      parameter (decaybot=100.0)
*      parameter (decaytop=100.0)
*
*c Eq. 7 & 11 in the April, 2004 memo
*      tau_spat_dep_s = taucenter +
*     ~    (taubottom-taucenter)*exp(-chipy/decaybot)/exp(-64.0/decaybot) +
*     ~    (tautop-taucenter)*exp(-(1024-chipy)/decaytop)/exp(-64.0/decaytop)
*      return
*      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function tau_spat_dep_s (chipy,taucenter,taubottom,tautop)
      implicit none 
      real chipy, taucenter, taubottom, tautop
c
c  Fit to the spatial dependence of contamination on ACIS-S
c
c  taucenter - optical depth at the center
c  taubottom - average optical depth at chipy=1:128
c
c -> Mon Apr 12 20:58:15 2004
c
      real alphabot,alphatop,yc,ybot,ytop
      parameter (
     ~    alphabot=5.5,
     ~    alphatop=4.5,
     ~    yc = 512.0,
     ~    ybot = 64.0,
     ~    ytop = 964.0
     ~    )

c Eq. 7 & 11 in the April, 2004 memo
      if (chipy.le.yc) then
        tau_spat_dep_s = taucenter +
     ~      (taubottom-taucenter)*
     ~      abs(chipy-yc)**alphabot/abs(ybot-yc)**alphabot
      else
        tau_spat_dep_s = taucenter +
     ~      (tautop-taucenter)*
     ~      abs(chipy-yc)**alphatop/abs(ytop-yc)**alphatop
      endif
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function tau_time_dep (year,mode)
      implicit none
      real year
      integer mode
c
c Returns contamination optical depth at the ECS line for center of ACIS-S
c (mode=0), bottom of ACIS-S (mode=1), top of ACIS-S (mode=2), center of ACIS
c I (mode=3), and border of ACIS-I (mode=4) as a function of observation date
c (e.g., year=2003.27) 
c
c -> Mon Apr 12 21:02:57 2004
c
      real d

      logical firstcall
      real t0(1000),taufi(1000),tauci(1000),taufs3(1000),taucs3(1000),
     ~    tauts3(1000)
      integer nt0
      logical interpolation_mode
      save firstcall, interpolation_mode, t0,taufi,tauci,taufs3,taucs3,tauts3,
     ~    nt0
      character file*200
      logical defined
      integer unit, status, newunit
      real tau_of_t
      data firstcall /.true./

      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('cont_time_dep',file)
        if (defined(file)) then
          unit = newunit()
          open (unit,file=file,status='old')
          status = 0
          nt0 = 0
          do while (status.eq.0)
            read (unit,*,iostat=status) t0(nt0+1),
     ~          taufi(nt0+1), tauci(nt0+1),
     ~          taufs3(nt0+1), taucs3(nt0+1), tauts3(nt0+1)
            if (status.eq.0) then
              nt0 = nt0 + 1
            endif
          enddo
          close (unit)
          interpolation_mode = .true.
          write (0,*) 'I WILL USE INTERPOLATION MODE IN CONTAMINATION TIME DEP'
        else
          interpolation_mode = .false.
        endif
      endif

      if (interpolation_mode) then
        if      (mode.eq.0) then ! ACIS-S-center
          call lin_interpolate(t0,taucs3,nt0,year,tau_of_t)
        else if (mode.eq.1) then ! ACIS-S-bottom
          call lin_interpolate(t0,taufs3,nt0,year,tau_of_t)
        else if (mode.eq.2) then ! ACIS-S-top
          call lin_interpolate(t0,tauts3,nt0,year,tau_of_t)
        else if (mode.eq.3) then ! ACIS-I-center
          call lin_interpolate(t0,tauci,nt0,year,tau_of_t)
        else if (mode.eq.4) then ! ACIS-I-border
          call lin_interpolate(t0,taufi,nt0,year,tau_of_t)
        else
          call exiterror ('unknown mode')
        endif
       

        tau_time_dep = tau_of_t

      else

        d = year - (1999.0+204.0/365.0)

c The fit parameters are from the April, 2004 memo with correction on Apr 9
        if      (mode.eq.0) then ! ACIS-S-center
          tau_time_dep = 0.63997*(1.0-exp(-d/2.05405))
        else if (mode.eq.1) then ! ACIS-S-bottom
          tau_time_dep = 0.87532*(1.0-exp(-d/1.40934))
        else if (mode.eq.2) then ! ACIS-S-top
          tau_time_dep = 0.81544*(1.0-exp(-d/1.53616))
        else if (mode.eq.3) then ! ACIS-I-center
          tau_time_dep = 0.67499*(1.0-exp(-d/1.4042))
        else if (mode.eq.4) then ! ACIS-I-border
          tau_time_dep = 0.96650*(1.0-exp(-d/1.2951))
        else
          call exiterror ('unknown mode')
        endif
      endif

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine corrarf_get_obs_date (date)
c  Get date of observation: 1) attempt to read the GTI file
c                           2) if not found, read TSTART from the EVT header
      implicit none
      real date
      
      character*200 gtifile, evtfile
      logical qgti
      integer nt, ntmax
      parameter (ntmax=16384)
      double precision tstart(ntmax), tstop(ntmax)
      logical defined, yespar
      integer status, unit, colnum
      logical anyf
      double precision mjdref, t, mjd0, mjd
      character*80 comment, stopname, startname
      double precision tstartkey, tave, texp
      integer i
      
      
      call get_cl_par ('corrarf_obs_date',gtifile)
      if (defined(gtifile)) then
        if (yespar('expert_mode')) then
          write (0,*) 'OK, assume you are an expert'
          read (gtifile,*) date
          return
        endif
      endif
      
      qgti = .true.
      
      call get_cl_par ('gti',gtifile)
      if (defined(gtifile)) then
        status = 0
        call ftgiou (unit,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        
        call ftnopn (unit,gtifile,0,status)
        if (status.ne.0) then   ! not a fits file
          call exit_fitsio (gtifile,status)
        endif
      else
        call get_cl_par ('gtifile',gtifile)
        if (.not.defined(gtifile)) then ! attempt to read TSTART from 
                                ! EVT header
          qgti = .false.
          call get_cl_par ('evtfile',evtfile)
          if (.not.defined(evtfile)) then
            call exiterror ('gtifile or evtfile are needed for corrarf')
          endif
          status = 0
          call ftgiou (unit,status)
          if (status.ne.0) call exit_fitsio (evtfile,status)
          call ftnopn (unit,evtfile,0,status)
          if (status.ne.0) then
            call exit_fitsio (evtfile,status)
          endif
          call ftgkyd (unit,'MJDREF',mjdref,comment,status)
          call ftgkyd (unit,'TSTART',tstartkey,comment,status)
          if (status.ne.0) then
            call exit_fitsio (evtfile,status)
          endif
          nt = 1
          tstart(1)=tstartkey
          tstop(1)=tstartkey+1
          call ftclos(unit,status)
          call ftfiou(unit,status)
          if (status.ne.0) then
            call exit_fitsio (evtfile,status)
          endif
        else
          status = 0
          call ftgiou (unit,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          call ftnopn (unit,gtifile,0,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          call FTMNHD (unit, -1,'GTI',0,status)
          if (status.ne.0) then
            status = 0
            call FTMNHD (unit, -1,'STDGTI',0,status)
          endif
        endif
      endif

      if (qgti) then            ! read GTI file, if needed
        call ftgnrw (unit,nt,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        if (nt.gt.ntmax) then
          write (0,*) 'Too many gti: ',nt,' (',ntmax,' maximum allowed)'
          call exit(1)
        endif
      
        call get_cl_par ('stopname',stopname)
        if (.not.defined(stopname)) stopname = 'STOP'
        call get_cl_par ('startname',startname)
        if (.not.defined(startname)) startname = 'START'
        
        call ftgkyd (unit,'MJDREF',mjdref,comment,status)
      
        call FTGCNO (unit,.false.,startname,colnum,status)
        call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstart,anyf,status)
        call FTGCNO (unit,.false.,stopname,colnum,status)
        call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstop,anyf,status)
        call ftclos (unit,status)
        call ftfiou (unit,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
      endif
      
c     calculate mjd at the start 1999.0
      call sla_cldj (1999,1,1,mjd0,i)

      
      tave = 0
      texp = 0
      do i=1,nt
        t = 0.5*(tstop(i)+tstart(i))
        if ((t-tstart(1))/86400.gt.90.0) then
          write (0,*)
     ~        'WARNING: contamination correction innacurate since obs spreads over 3months'
        endif
        t = t / 86400           ! convert to days
        mjd = mjdref + t
        tave = tave + (mjd-mjd0)/365.0*(tstop(i)-tstart(i))
        texp = texp + (tstop(i)-tstart(i))
      enddo
      
      tave = tave/texp 

      date = 1999.0+tave
      
      write (0,*) 'Contamination is computed for t=',date

      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function bad_grade_ratio (E,iccd)
      implicit none
      real E
      integer iccd
      integer n_fi
      parameter (n_fi=17)
      real e_fi(n_fi), fract_fi(n_fi)
      data e_fi /0.25, 0.30, 0.40, 0.50, 0.70, 0.90, 1.20, 1.50, 1.80, 2.00,
     ~    2.60, 4.00, 4.93, 6.00, 8.00, 10.00, 12.00/
      data fract_fi /0.00368636, 0.00438072, 0.00793651, 0.0115647, 0.0183567,
     ~    0.0271427, 0.0443425, 0.0641086, 0.0902475, 0.0436692,
     ~    0.0596372, 0.111774, 0.155908, 0.201254, 0.252394, 0.276859,
     ~    0.292806 /
      save e_fi, fract_fi
      integer ibin
      data ibin /8/
      save ibin
      integer i1, i2
      real w

      if (iccd.eq.7) then
        if (e.gt.1.837) then
          bad_grade_ratio = 0.315*(1-0.0225*(e-1.837)**1.25)
        else
          bad_grade_ratio = 0.32*(1-0.45*(0.5/e)**0.5)
        endif
      elseif (iccd.eq.5) then
        if (e.gt.1.837) then
          bad_grade_ratio = 0.22*(1-0.045*(e-1.837)**1.25)
        else
          bad_grade_ratio = 0.21*(1-0.45*(0.5/e)**0.5)
        endif
      else
        call hunt(e_fi,n_fi,E,ibin)
        if (ibin.eq.0.or.ibin.eq.n_fi) then
          if (E.lt.e_fi(1)) then
            bad_grade_ratio = fract_fi(1)
          else
            bad_grade_ratio = fract_fi(n_fi)
          endif
        else
          i1 = ibin
          i2 = ibin+1
          w = (e_fi(i2)-E)/(e_fi(i2)-e_fi(i1))
          bad_grade_ratio = w*fract_fi(i1)+(1.0-w)*fract_fi(i2)
        endif
      endif

      bad_grade_ratio = max(bad_grade_ratio,0.0)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_qeu_Mn (calfile, qeu, nx, ny, nccd)
      implicit none
      character calfile*(*)
      integer nx,ny,nccd
      real qeu(nx,ny,nccd)

      integer unit,status, naxis, naxes(3)
      logical anyf
      character*80 message

      message = calfile
      status = 0
      call ftgiou (unit,status)
      call ftnopn (unit,calfile,0,status)
      if (status.ne.0) call perror_fitsio (calfile,status)
      
      call ftgidm (unit,naxis,status)
      if (status.ne.0) call perror_fitsio (calfile,status)
      if (naxis.ne.3) call exiterror ('expect 3D image in '//message)

      call ftgisz(unit,3,naxes,status)
      if (status.ne.0) call perror_fitsio (calfile,status)
      if (
     ~    naxes(1).ne.nx .or.
     ~    naxes(2).ne.ny .or.
     ~    naxes(3).ne.nccd ) then
        call exiterror ('wrong dimensions in '//message)
      endif
      
      call FTG3DE (unit,0,0.0,nx,ny,nx,ny,nccd,qeu,anyf,status)

      call ftclos(unit,status)
      call ftfiou(unit,status)
      if (status.ne.0) call perror_fitsio (calfile,status)

      return
      end

        
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function corrarf_caldb (E,ccdid,chipx,chipy)
c
c  Spatial- and time-dependent QE correction for contamination on the ACIS OBF
c
c  Performance optimized for repetitive call at different energies and the
c  same location on the CCD
      implicit none 
      real E
      integer ccdid, chipx, chipy

      real date                 ! observation date, years (e.g., 2000.37)
      save date
      integer ncomp, ncompmax
      parameter (ncompmax=5)
      integer it1(ncompmax,0:9),it2(ncompmax,0:9)
      real wt1(ncompmax,0:9),wt2(ncompmax,0:9)
      save it1,it2,wt1,wt2
      save ncomp

      logical firstcall
      data firstcall /.true./
      save firstcall

      logical no_caldb_spatial
      save no_caldb_spatial

      character*200 contfile

      logical defined, nopar
      double precision ww1,ww2
      integer i, icomp, iccd
      real tau, zz

c Contamination data
      integer nemax, ntmax
      parameter (nemax=1000,ntmax=16384)
      integer necont(ncompmax,0:9), ntcont(ncompmax,0:9)
      real econt(nemax,ncompmax,0:9)
      real mu(nemax,ncompmax,0:9)
      double precision tcont(ntmax,ncompmax,0:9)
      real tau0(ntmax,ncompmax,0:9), t0
      real tau1(ntmax,ncompmax,0:9), t1
      real fxy(1024,1024,ncompmax,0:9)
      save necont, ntcont, econt, mu, tcont, tau0, tau1, fxy

      real mu2(20000,ncompmax,0:9)
      save mu2

      real contcor
      save contcor

      logical yespar

      include 'calcomments.inc'

c

      if (firstcall) then
        no_caldb_spatial = nopar('caldb_spatial')


        call get_pv_default ('contcor',contcor,1.0,'e')
        if (contcor.ne.0) then
          print*,'EXTRA CONTAMINATION - ',contcor,' opt depth factor'
          NCALCOM = NCALCOM + 1
          CALCOMNAME(NCALCOM) = 'EXTRACON'
          write (CALCOMVALUE(NCALCOM),*) contcor
          CALCOMCOM(NCALCOM) = 'EXTRA CONTAMINATION (tau for Mn/Fe-L)'
        endif
        call corrarf_get_obs_date (date)
        call get_cl_par ('qe_cont',contfile)
        if (.not.defined(contfile)) then
          write (0,*) 'qe_cont=?'
          call exit(1)
        endif
        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CCDCONT'
        CALCOMVALUE(NCALCOM) = contfile
        CALCOMCOM(NCALCOM) = 'ACIS contamination tau(E)'

        NCALCOM = NCALCOM + 1
        CALCOMNAME(NCALCOM) = 'CONTCALD'
        CALCOMVALUE(NCALCOM) = 'yes'
        CALCOMCOM(NCALCOM) = 'CALDB-style contamination correction?'

        call load_qe_cont_caldb (contfile,nemax,ntmax,
     ~      econt,mu,necont,tcont,tau0,tau1,ntcont,fxy,ncompmax,ncomp)

*        call saoimage (fxy(1,1,1,0),1024,1024,'e')
*        call saoimage (fxy(1,1,2,0),1024,1024,'e')

        iccd = ncomp
        call get_pv_default ('ncontcompmax',ncomp,iccd,'j')

c Find time interpolation
        do iccd=0,9
          do icomp=1,ncomp
            if (date.le.tcont(1,icomp,iccd)) then
              it1(icomp,iccd)=1
              it2(icomp,iccd)=2
            elseif (date.gt.tcont(ntcont(icomp,iccd),icomp,iccd)) then
              it1(icomp,iccd)=ntcont(icomp,iccd)-1
              it2(icomp,iccd)=ntcont(icomp,iccd)
            else
              it1(icomp,iccd)=ntcont(icomp,iccd)/2
              call hunt8(tcont(1,icomp,iccd),ntcont(icomp,iccd),dble(date)
     ~            ,it1(icomp,iccd))
              it2(icomp,iccd)=it1(icomp,iccd)+1
            endif

            call get_weights8 (
     ~          dble(date),
     ~          tcont(it1(icomp,iccd),icomp,iccd),
     ~          tcont(it2(icomp,iccd),icomp,iccd),
     ~          ww1,ww2)
            wt1(icomp,iccd)=ww1
            wt2(icomp,iccd)=ww2

          enddo
        enddo
*        do i=1,ntcont(1,1)
*          write (0,*) i,tcont(i,1,1)
*        enddo
*        write (0,*) wt1(1,1), wt2(1,1),it1(1,1),date,tcont(it1(1,1),1,1)
*        call exit(0)

c Precalculate energy dependence at 1eV points
        do i=1,20000
          do icomp=1,ncomp
            do iccd=0,9
              if (i/1000.0.lt.econt(1,icomp,iccd) .or.
     ~            i/1000.0.gt.econt(necont(icomp,iccd),icomp,iccd)) then
                mu2(i,icomp,iccd)=0
              else
                call lin_interpolate (
     ~              econt(1,icomp,iccd),mu(1,icomp,iccd),necont(icomp,iccd),
     ~              i/1000.0,mu2(i,icomp,iccd)
     ~              )
              endif
            enddo
          enddo
        enddo

        firstcall = .false.

        if (yespar('no_fluffium')) then
           ncomp = 1
        endif

      endif


      iccd=ccdid
      tau = 0
      i = nint(E*1000.0)
      i = min(max(i,1),20000)

      do icomp=1,ncomp
        t0 = tau0(it1(icomp,iccd),icomp,iccd)*wt1(icomp,iccd) +
     ~      tau0(it2(icomp,iccd),icomp,iccd)*wt2(icomp,iccd)
        
        if (no_caldb_spatial) then
          zz = t0
        else
          t1 = tau1(it1(icomp,iccd),icomp,iccd)*wt1(icomp,iccd) +
     ~         tau1(it2(icomp,iccd),icomp,iccd)*wt2(icomp,iccd)

          zz = t0 + fxy(chipx,chipy,icomp,iccd) * t1
        endif
        tau = tau + zz*mu2(i,icomp,iccd)

*        print '(i1,2x,10(f6.4,1x))',icomp,E,t0,t1,fxy(chipx,chipy,icomp,iccd),zz,mu2(i,icomp,iccd),zz*mu2(i,icomp,iccd),tau
      enddo

      corrarf_caldb = exp(-tau*contcor)

      return
      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_qe_cont_caldb (calfile,nemax,ntmax,
     ~      econt,mu,necont,tcont,tau0,tau1,ntcont,fxy,ncompmax,ncomp)
      implicit none
      integer ncompmax
      character calfile*(*)
      integer nemax,ntmax
      integer necont(ncompmax,0:9), ntcont(ncompmax,0:9)
      real econt(nemax,ncompmax,0:9)
      real mu(nemax,ncompmax,0:9)
      double precision tcont(ntmax,ncompmax,0:9)
      real tau0(ntmax,ncompmax,0:9)
      real tau1(ntmax,ncompmax,0:9)
      real fxy(1024,1024,ncompmax,0:9)
      integer ncomp


      integer iccd, icomp, it

c FITSIO stuff
      integer unit
      integer status,blocksize,colnum,hdutype
      logical anyf
      character*80 extname,comment
      integer chipid


      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call ftopen (unit,calfile,0,blocksize,status)
      if (status.ne.0) then     ! not a fits file
        call exit_fitsio (calfile,status)
      endif

      do iccd=0,9

c MOVE TO THE RIGHT HDU
        chipid = -1
        do while (chipid.ne.iccd)
          call FTMRHD(unit,1,hdutype,status)
          if (status.ne.0) then ! this means that we're at the end of the file
                                ! but have not found the QE table
            write (0,*) 'ERROR calibration data for ccd ',iccd
     ~          ,' was not found in ',calfile
            call exit(1)
          endif

          if (hdutype.eq.2) then ! binary table
            call ftgkys (unit,'EXTNAME',extname,comment,status)
            if (status.ne.0) call exit_fitsio (calfile,status)
            if (extname.eq.'AXAF_CONTAM') then
              call ftgkyj (unit,'CCD_ID',chipid,comment,status)
              if (status.ne.0) call exit_fitsio (calfile,status)
            endif
          endif
        enddo

        call load_qe_cont_caldb_ccd (calfile,unit,nemax,ntmax,
     ~      econt(1,1,iccd),mu(1,1,iccd),necont(1,iccd),
     ~      tcont(1,1,iccd),tau0(1,1,iccd),tau1(1,1,iccd),ntcont(1,iccd),
     ~      fxy(1,1,1,iccd), status, ncompmax, ncomp)

      enddo

      call ftclos (unit,status)
      status = 0
      call ftfiou(unit,status)
      status = 0

c Convert time to years
c Herman has:
c time = (date-1998)*86400*365.25)
c date = time/(86400*365.25)+1998

      do iccd=0,9
        do icomp = 1,ncomp
          do it = 1,ntcont(icomp,iccd)
            tcont(it,icomp,iccd) = tcont(it,icomp,iccd)/(86400*365.25)+1998
*            if (iccd.eq.7.and.icomp.eq.1) print*,tcont(it,icomp,iccd)
          enddo
        enddo
      enddo
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_qe_cont_caldb_ccd (calfile,unit,nemax,ntmax,
     ~    econt,mu,necont,tcont,tau0,tau1,ntcont,fxy,status,
     ~    ncompmax, ncomp)
      implicit none
      character calfile*(*)
      integer unit, status
      integer ncompmax, ncomp
      integer nemax,ntmax
      integer necont(ncompmax), ntcont(ncompmax)
      real econt(nemax,ncompmax)
      real mu(nemax,ncompmax)
      double precision tcont(ntmax,ncompmax)
      real tau0(ntmax,ncompmax)
      real tau1(ntmax,ncompmax)
      real fxy(1024,1024,ncompmax)
      integer colnum, icomp
      logical anyf
      
      call FTGNRW (unit, ncomp, status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      if (ncomp.gt.ncompmax) then
        call exiterror ('insufficient ncompmax in load_qe_cont_caldb_ccd')
      endif

      call FTGCNO (unit,.false.,'n_energy',colnum,status)
      call ftgcvj (unit,colnum,1,1,ncomp,0,necont,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'energy',colnum,status)
      do icomp=1,ncomp
        if (necont(icomp).gt.nemax) then
          call exiterror ('increase nemax in corrarf_caldb')
        endif
        call ftgcve (unit,colnum,icomp,1,necont(icomp),0.0,
     ~      econt(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'mu',colnum,status)
      do icomp=1,ncomp
        call ftgcve (unit,colnum,icomp,1,necont(icomp),0.0,
     ~      mu(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

       
      call FTGCNO (unit,.false.,'n_time',colnum,status)
      call ftgcvj (unit,colnum,1,1,ncomp,0,ntcont,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'time',colnum,status)
      do icomp=1,ncomp
        if (ntcont(icomp).gt.ntmax) then
          call exiterror ('increase ntmax in corrarf_caldb')
        endif
        call ftgcvd (unit,colnum,icomp,1,ntcont(icomp),0.0d0,
     ~      tcont(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'tau0',colnum,status)
      do icomp=1,ncomp
        call ftgcve (unit,colnum,icomp,1,ntcont(icomp),0.0,
     ~      tau0(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'tau1',colnum,status)
      do icomp=1,ncomp
        call ftgcve (unit,colnum,icomp,1,ntcont(icomp),0.0,
     ~      tau1(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'fxy',colnum,status)
      do icomp=1,ncomp
        call ftgcve (unit,colnum,icomp,1,1024*1024,0.0,
     ~      fxy(1,1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      return
      end
