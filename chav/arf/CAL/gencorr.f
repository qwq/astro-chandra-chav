c The program reads the rdb data table taken from 
c http://hea-www.harvard.edu/MST/simul/orbit/eff-area/offaxis/index.html
c (ea-hrma.rdb)
c energy  theta   phi     shell   aeff    aeff_err
c N       N       N       S       N       N
c 0.1     0       0       hrma    989.10917563    3.05957533610899
c 0.2     0       0       hrma    836.19518925    2.5868072274196
c
c 
c The program outputs the ratio of area/onaxis as a function of energy, theta,
c and phi to the file offcorr-hrma.dat
c It also produces fake calibration for phi=360 by copiing phi=0 data
c

      implicit none
      integer ne, ntheta, nphi
      parameter (ne=100)
      parameter (ntheta=7)
      parameter (nphi=13)

      double precision ea(ne,ntheta,nphi)
      real e(ne), theta(ntheta), phi(nphi)
      data theta /0.0, 2.0, 5.0, 10.0, 15.0, 20.0, 25.0/
      data phi /0., 30., 60., 90., 120., 150., 180., 210., 240., 270., 300.,
     ~    330., 360./
      
      integer status
      character*80 wshell
      integer fscanf
      real we,wt,wphi
      double precision wa

      integer status,i,ie,itheta,iphi

      do i=1,ne
        e(i)=0.1*i
      enddo

      open(1,file='ea-hrma.rdb',status='old')
      read(1,*)
      read(1,*)
      do while (fscanf(1,"%f %f %f %s %lf",we,wt,wphi,wshell,wa).eq.5)
        read (1,*)
        call fixstring(wshell)
        if (wshell.eq.'hrma') then
          ie = 0
          do i=1,ne
            if (abs(we-e(i)).lt.0.001) then
              ie = i
            endif
          enddo

          itheta=0
          do i=1,ntheta
            if (abs(wt-theta(i)).lt.0.001) then
              itheta=i
            endif
          enddo

          iphi=0
          do i=1,nphi
            if (abs(wphi-phi(i)).lt.0.001) then
              iphi=i
            endif
          enddo

          if (ie.gt.0.and.iphi.gt.0.and.itheta.gt.0) then
            ea(ie,itheta,iphi)=wa
          endif
        endif
      enddo
      close(1)


c Normilize by the onaxis area
      do itheta=2,ntheta
        do iphi=1,nphi-1
          do ie=1,ne
            ea(ie,itheta,iphi)=ea(ie,itheta,iphi)/ea(ie,1,1)
          enddo
        enddo
      enddo

c theta=0 ratio is 1 for all phi's
      do ie=1,ne
        do iphi=1,nphi
          ea(ie,1,iphi)=1
        enddo
      enddo
      
c  phi=0 data is copied to phi=360
      do ie=1,ne
        do itheta=1,ntheta
          ea(ie,itheta,nphi)=ea(ie,itheta,1)
        enddo
      enddo

c Output
      open(1,file='offcorr-hrma.dat')
      write (1,*) ne,ntheta,nphi,' --- ne, ntheta, nphi'
      write (1,*) 'E:'
      write (1,'(<ne>(1x,f5.1))') e
      write (1,*) 'theta:'
      write (1,'(<ntheta>(f3.0,1x))') theta
      write (1,*) 'phi:'
      write (1,'(<nphi>(f4.0,1x))') phi
      write (1,*) 'ie itheta iphi correction:'
      do ie=1,ne
        do itheta=1,ntheta
          do iphi=1,nphi
            write (1,'(i3,1x,i2,1x,i2,1x,f9.7,2x,f4.1,1x,f4.0,1x,f4.0)') 
     ~          ie, itheta, iphi, ea(ie,itheta,iphi), 
     ~          e(ie), theta(itheta), phi(iphi)
          enddo
        enddo
      enddo
      close(1)
      

      
      end

