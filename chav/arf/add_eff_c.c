#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fitsio.h>
#include <math.h>
#ifndef NOREADPAR
#include <readpar.h>
#endif

float *expmap;
int nxexp, nyexp;
double xrefval_img, yrefval_img, xrefpix_img, yrefpix_img, 
  xinc_img, yinc_img, rot_img; 
char coordtype_img[100];
double xrefval_tbl, yrefval_tbl, xrefpix_tbl, yrefpix_tbl, 
  xinc_tbl, yinc_tbl, rot_tbl; 
char coordtype_tbl[100];

char arg[200];

float hrma_vignetting_ (float *, float *, float *);
float ccd_qe_ (float *, int *, int *, float *, float *, int *);
float ccd_qe_main_ (float *, int *);

int REFCCD;

int DOMIRROR, DOQE, DOEXP;

extern int iter_evt (long , long , long , long , int , iteratorCol *, void *);   /* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */

int add_eff_ () {
  fitsfile *fptr;
  iteratorCol cols[100]; /* STRUCTURE USED BY THE ITERATOR FUNCTION */
  int ncols;
  long rows_per_loop, offset;
  int status;
  int xcol, ycol;

  char evtfile[200];
  char refccd[200];

  get_command_line_par ("refccd",refccd);
  if ( !defined(refccd) ) {
    REFCCD = 3;
  } else if ( refccd[0] == 'i' || refccd[0] == 'I' ) {
    sscanf(refccd+1,"%d",&REFCCD);
  } else if ( refccd[0] == 's' || refccd[0] == 'S' ) {
    sscanf(refccd+1,"%d",&REFCCD);
    REFCCD +=4;
  } else {
    sscanf(refccd+1,"%d",&REFCCD);
  }

  get_command_line_par ("noqe",arg);
  if ( !defined(arg)) { DOQE = 1; } else { DOQE = 0; }

  get_command_line_par ("nomirror",arg);
  if ( !defined(arg)) { DOMIRROR = 1; } else { DOMIRROR = 0; }

  get_command_line_par ("exposure_correction",arg);
  if ( defined(arg)) {
    DOEXP = 1; 
    Load_Exp_Map (arg);
  } 
  else {
    DOEXP = 0;
  }

  get_cl_or_read_par ("evtfile","Input events file",evtfile);

  if (!defined(evtfile)) {
    fprintf (stderr,"evtfile=?\n");
    exit(1);
  }

  /* Open events file */
  fits_open_file(&fptr, evtfile, READWRITE, &status); /* open file */

  /* move to the EVENTS binary table extension */
  if (fits_movnam_hdu(fptr, BINARY_TBL, "EVENTS", 0, &status) )
    fits_report_error(stderr, status);    /* print out error messages */
  
  if ( DOEXP ) { ncols = 10; } else { ncols = 8; }

  /* define input column structure members for the iterator function */
  fits_iter_set_by_name(&cols[0], fptr, "CCD_ID",  TINT,  InputCol);
  fits_iter_set_by_name(&cols[1], fptr, "CHIPX",  TINT,  InputCol);
  fits_iter_set_by_name(&cols[2], fptr, "CHIPY",  TINT,  InputCol);
  fits_iter_set_by_name(&cols[3], fptr, "DETX", TDOUBLE,  InputCol);
  fits_iter_set_by_name(&cols[4], fptr, "DETY",  TDOUBLE,  InputCol);
  fits_iter_set_by_name(&cols[5], fptr, "ENERGY",  TFLOAT,  InputCol);
  fits_iter_set_by_name(&cols[6], fptr, "EFF",  TFLOAT,  InputOutputCol);
  fits_iter_set_by_name(&cols[7], fptr, "EFFINV",  TFLOAT,  InputOutputCol);
  if ( DOEXP ) {
    fits_iter_set_by_name(&cols[8], fptr, "X",  TDOUBLE,  InputCol);
    fits_iter_set_by_name(&cols[9], fptr, "Y",  TDOUBLE,  InputCol);
    
    status = 0;
    fits_get_colnum (fptr, 0, "X", &xcol, &status);
    if (status !=0 ) { fprintf (stderr,"X: "); fits_report_error(stderr, status); }
    fits_get_colnum (fptr, 0, "Y", &ycol, &status);
    if (status !=0 ) { fprintf (stderr,"Y: "); fits_report_error(stderr, status); }

    fits_read_tbl_coord (fptr, xcol, ycol,
			 &xrefval_tbl,&yrefval_tbl,
			 &xrefpix_tbl,&yrefpix_tbl,
			 &xinc_tbl,&yinc_tbl,
			 &rot_tbl,coordtype_tbl,&status);
    fits_report_error(stderr, status);
  }

  fits_report_error(stderr, status);
  

  rows_per_loop = 0;  /* use default optimum number of rows */
  offset = 0;         /* process all the rows */

  fits_iterate_data(ncols, cols, offset, rows_per_loop,
		    iter_evt, 0L, &status);

  /* update checksums */
  fits_write_chksum (fptr,&status);
  /* and close file */
  fits_close_file(fptr, &status);      /* all done */
  if (status)
    fits_report_error(stderr, status);  /* print out error messages */

}

int exit_fitsio (int status)
{
  fits_report_error(stderr, status);
  exit(1);
}


/*--------------------------------------------------------------------------*/
int iter_evt (long totalrows, long offset, long firstrow, long nrows,
             int ncols, iteratorCol *cols, void *user_strct ) 
{
  int ii, status = 0;
  
  /* Declare variables static to preserve their values between calls */
  /* 1) Data arrays: */ 
  static int *ccd_id, *chipx, *chipy;
  static double *detx, *dety;
  static float *eff, *energy, *effinv;
  static double *x, *y;
  
  int i,j,ipix;
  int ie;

  static float egrid[12000];
  static float refqe[12000];
  static int ne=12000;
  float fdetx, fdety, theta, phi, fchipx, fchipy;

  double ra, dec, xpix, ypix;
  int iimg, jimg;

  /*--------------------------------------------------------*/
  /*  Initialization procedures: execute on the first call  */
  /*--------------------------------------------------------*/
  if (firstrow == 1)
    {
      
      for (i=0;i<12000;i++) {
	egrid[i]=i/1000.0;
	if (egrid[i]>0.0) {
	  if ( DOQE ) {
	    refqe[i] =  ccd_qe_main_ (&egrid[i],&REFCCD);
	  } else {
	    refqe[i] = 1;
	  }
	}
      }

      if (fits_iter_get_datatype(&cols[0]) != TINT ||
	  fits_iter_get_datatype(&cols[1]) != TINT ||
	  fits_iter_get_datatype(&cols[2]) != TINT ||
	  fits_iter_get_datatype(&cols[3]) != TDOUBLE ||
	  fits_iter_get_datatype(&cols[4]) != TDOUBLE ||
	  fits_iter_get_datatype(&cols[5]) != TFLOAT ||
	  fits_iter_get_datatype(&cols[6]) != TFLOAT ||
	  fits_iter_get_datatype(&cols[7]) != TFLOAT )
	return(-2);  /* bad data type */
      
      /* assign the input pointers to the appropriate arrays and null ptrs*/
      ccd_id      = (int *) fits_iter_get_array(&cols[0]);
      chipx       = (int *) fits_iter_get_array(&cols[1]);
      chipy       = (int *) fits_iter_get_array(&cols[2]);
      detx        = (double *) fits_iter_get_array(&cols[3]);
      dety        = (double *) fits_iter_get_array(&cols[4]);
      energy      = (float *) fits_iter_get_array(&cols[5]);
      eff         = (float *) fits_iter_get_array(&cols[6]);
      effinv      = (float *) fits_iter_get_array(&cols[7]);
      if ( DOEXP ) {
	x        = (double *) fits_iter_get_array(&cols[8]);
	y        = (double *) fits_iter_get_array(&cols[9]);
      }	
    }
  
  
  for (i = 1; i <= nrows; i++)
    {

      status = 0;

      ie = (int) energy[i]; if (ie>=12000) ie=11999; 
      if (ie < 0) ie=1;

      fchipx = chipx[i];
      fchipy = chipy[i];

      eff[i] = 1;

      if ( DOQE ) {
	eff[i] *=  ccd_qe_ (egrid,&ie,&ne,&fchipx,&fchipy,&ccd_id[i]);
      }


      /* c         b) convert detx,dety to theta,phi */
      fdetx = detx[i];  fdety = dety[i];
      detpix2pol_acis_ (&fdetx,&fdety,&theta,&phi);
      
      if ( DOMIRROR ) {
	eff[i] *= hrma_vignetting_(&egrid[ie],&theta,&phi);
      }

      if  ( DOEXP ) {
	status = 0;

	fits_pix_to_world (x[i], y[i], xrefval_tbl, yrefval_tbl,
			   xrefpix_tbl, yrefpix_tbl, xinc_tbl, yinc_tbl,
			   rot_tbl, coordtype_tbl, &ra, &dec,
			   &status);

	fits_world_to_pix (ra, dec, xrefval_img, yrefval_img,
			   xrefpix_img, yrefpix_img, xinc_img, yinc_img,
			   rot_img, coordtype_img, &xpix, &ypix,
			   &status);

	iimg = floor(xpix+0.5);
	jimg = floor(ypix+0.5);

	if (iimg>=1&&iimg<=nxexp && jimg>=1&&jimg<=nyexp) {
	  ii = (jimg-1)*nxexp+iimg;
	  eff[i] *= expmap[ii-1];
	} else {
	  eff[i] = -1;
	}

      }

      if (eff[i]>0) {
	effinv[i]=refqe[i]/eff[i];
      } else {
	effinv[i]=0;
      }

/*       fprintf (stderr,"%d %g %d %d %d %g\n",i,eff[i],chipx[i],chipy[i],ccd_id[i],energy[i]); */
    }
  
  return(0);  /* return successful status */
}


int Load_Exp_Map (char *file) {
  fitsfile *fptr;
  int status = 0;
  int naxis; long naxes[10];
  long fpixel[10];
  float null; int anynull;
  
  if (fits_open_file(&fptr, file, READONLY, &status))
    fits_report_error(stderr, status);  /* print out error messages */
  
  
  if (fits_get_img_dim (fptr, &naxis, &status))
    fits_report_error(stderr, status);  /* print out error messages */
  
  if (naxis!=2) {
    fprintf (stderr,"Error: not a 2D image in %s\n",file);
    exit(1);
  }

  if (fits_get_img_size (fptr, 10, naxes, &status)) 
    fits_report_error(stderr, status);  /* print out error messages */
  
  nxexp=naxes[0];
  nyexp=naxes[1];

  expmap = (float *) malloc(sizeof(float)*nxexp*nyexp);
  if (expmap==NULL) {
    fprintf (stderr,"Error: cannot allocate memory in Load_Exp_Map");
    exit(1);
  }


  fpixel[0] = 1;
  fpixel[1] = 1;
  null = 0;
  if (fits_read_pix(fptr, TFLOAT, fpixel, nxexp*nyexp,
		    &null, expmap, &anynull, &status))
    fits_report_error(stderr, status);  /* print out error messages */

  if (fits_read_img_coord (fptr,
			   &xrefval_img,&yrefval_img,
			   &xrefpix_img,&yrefpix_img,
			   &xinc_img,&yinc_img,
			   &rot_img,coordtype_img,&status)) 
    fits_report_error(stderr, status);


  fits_close_file(fptr, &status);      /* all done */
  if (status) {
    fits_report_error(stderr, status);  /* print out error messages */
    exit (1);
  }
  fprintf (stderr,"exp map loaded OK\n");
}
  
  
 
