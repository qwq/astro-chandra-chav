      real function corrarf (E)
      implicit none
      real E
c
c Prints out exposure associated with the GTI file
c It is the user's responsibility to point to correct extension in the GTI
c file
      
      character*200 gtifile, arffile, contfile, outfile
      logical defined

      integer nt, ntmax
      parameter (ntmax=16384)
      double precision tstart(ntmax), tstop(ntmax), texp, ratio, avratio
      double precision mjdref, t, mjd0, mjd
      double precision tau

      character*200 startname,stopname, comment

      integer unit
      integer status,colnum,i
      logical anyf

c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      integer ncont
      real econt(10000), taucont(10000)
      save econt, taucont

      integer ecol1,ecol2,acol
      real elow, ehigh, a
      real w1,w2,taunorm, extranorm
      logical warned
      save taunorm


      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('gti',gtifile)
        if (defined(gtifile)) then
          status = 0
          call ftgiou (unit,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          
          call ftnopn (unit,gtifile,0,status)
          if (status.ne.0) then ! not a fits file
            call exit_fitsio (gtifile,status)
          endif
        else
          call get_cl_par ('gtifile',gtifile)
          if (.not.defined(gtifile)) then
            write (0,*) 'gtifile=?'
            call exit(1)
          endif
          status = 0
          call ftgiou (unit,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          call ftnopn (unit,gtifile,0,status)
          call FTMNHD (unit, -1,'GTI',0,status)
          if (status.ne.0) then
            status = 0
            call FTMNHD (unit, -1,'STDGTI',0,status)
          endif
        endif
        
        
        call ftgnrw (unit,nt,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        if (nt.gt.ntmax) then
          write (0,*) 'Too many gti: ',nt,' (',ntmax,' maximum allowed)'
          call exit(1)
        endif
        
        call get_cl_par ('stopname',stopname)
        if (.not.defined(stopname)) stopname = 'STOP'
        call get_cl_par ('startname',startname)
        if (.not.defined(startname)) startname = 'START'
        
        call ftgkyd (unit,'MJDREF',mjdref,comment,status)
        call sla_cldj (1999,1,1,mjd0,i)
        
        call FTGCNO (unit,.false.,startname,colnum,status)
        call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstart,anyf,status)
        call FTGCNO (unit,.false.,stopname,colnum,status)
        call ftgcvd (unit,colnum,1,1,nt,0.0d0,tstop,anyf,status)
        call ftclos (unit,status)
        call ftfiou (unit,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
      
        texp = 0
        avratio = 0
        warned = .false.
        
        do i=1,nt
          t = 0.5*(tstop(i)+tstart(i))
          t = t / 86400         ! convert to days
          mjd = mjdref + t
          if (mjd-mjd0.gt.386.0) then
*          ratio = (6.86997E-03 -1.86443E-06*(mjd-mjd0))/0.0072
            ratio = exp(-0.582*(1-exp(-(mjd-mjd0-204.0)/620.0)))
          else
            if (.not.warned) then
              write (0,*) 
     ~            'warning: correction may be poor for data prior 21/01/00'
              warned = .true.
            endif
*          ratio = 1.38-0.23*log10(mjd-mjd0-200.0)
            ratio = exp(-0.582*(1-exp(-(mjd-mjd0-204.0)/620.0)))
          endif
          texp = texp + tstop(i)-tstart(i)
          avratio = avratio + ratio*(tstop(i)-tstart(i))
        enddo
        
        ratio = avratio/texp
        tau = - log(ratio)*(0.67/0.286)**3
        write (0,*) sngl(ratio),' - loss of effective area at 0.67 keV'

c Read in the contamination absorption file
        call get_cl_par ('qe_cont',contfile)
        if (.not.defined(contfile)) then
          write (0,*) 'qe_cont=?'
          call exit(1)
        endif

c Find normalization using the ext cal. source data
        call load_qe_cont (contfile,econt,taucont,ncont)
        w1 = -log(ratio)
        call lin_interpolate (econt,taucont,ncont,0.67,w2)
        taunorm = w1/w2
        call get_parameter_value_default ('corrarf_norm',extranorm,1.0,'e')
        taunorm = taunorm * extranorm
      endif

      call lin_interpolate (econt,taucont,ncont,E,w1)
      w1 = w1*taunorm
      corrarf = exp(-w1)
      
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_qe_cont (file,e,tau,n)
      implicit none
      character*(*) file
      integer n
      real e(*), tau(*)
      
      integer unit
      integer newunit
      integer status
      character*1024 buf

      unit = newunit()
      open (unit,file=file,status='old')
      n = 0
      status = 0
      do while (status.eq.0)
        read (unit,'(a)',iostat=status) buf
        if (status.eq.0) then
          if (buf(1:1).ne.'#') then
            read (buf,*,iostat=status) e(n+1),tau(n+1)
            if (status.eq.0) then
              n = n + 1
              tau(n)=-log(tau(n)) ! the file actually stores the 
                                ! absorption coefficient, not optical depth
            endif
          endif
        endif
      enddo

      close(unit)
      return
      end

      
