c NEW contamination model for ACIS-I (spatial+time dependendence) + new spatial model for S.
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

*      subroutine load_qe_cont_caldb (calfile,nemax,ntmax,
*     ~      econt,mu,necont,tcont,tau0,tau1,ntcont,fxy)
      implicit none
      character calfile*200
      integer nemax,ntmax
      parameter (nemax=20000, ntmax=20000)
      integer necont(3,0:9), ntcont(3,0:9)
      real econt(nemax,3,0:9)
      real mu(nemax,3,0:9)
      double precision tcont(ntmax,3,0:9)
      real tau0(ntmax,3,0:9)
      real tau1(ntmax,3,0:9)
      real fxy(1024,1024,3,0:9)


      integer iccd, icomp, it

c FITSIO stuff
      integer unit
      integer status,blocksize,colnum,hdutype
      logical anyf
      character*80 extname,comment
      integer chipid

c
      call getarg (1,calfile)


      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call ftopen (unit,calfile,1,blocksize,status)
      if (status.ne.0) then     ! not a fits file
        call exit_fitsio (calfile,status)
      endif

      do iccd=0,9

c MOVE TO THE RIGHT HDU
        chipid = -1
        do while (chipid.ne.iccd)
          call FTMRHD(unit,1,hdutype,status)
          if (status.ne.0) then ! this means that we're at the end of the file
                                ! but have not found the QE table
            write (0,*) 'ERROR calibration data for ccd ',iccd
     ~          ,' was not found in ',calfile
            call exit(1)
          endif

          if (hdutype.eq.2) then ! binary table
            call ftgkys (unit,'EXTNAME',extname,comment,status)
            if (status.ne.0) call exit_fitsio (calfile,status)
            if (extname.eq.'AXAF_CONTAM') then
              call ftgkyj (unit,'CCD_ID',chipid,comment,status)
              if (status.ne.0) call exit_fitsio (calfile,status)
            endif
          endif
        enddo

        call load_qe_cont_caldb_ccd (calfile,unit,nemax,ntmax,
     ~      econt(1,1,iccd),mu(1,1,iccd),necont(1,iccd),
     ~      tcont(1,1,iccd),tau0(1,1,iccd),tau1(1,1,iccd),ntcont(1,iccd),
     ~      fxy(1,1,1,iccd), iccd, status)

      enddo

      call ftclos (unit,status)
      status = 0
      call ftfiou(unit,status)
      status = 0

c Convert time to years
c Herman has:
c time = (date-1998)*86400*365.25)
c date = time/(86400*365.25)+1998

*      do iccd=0,9
*        do icomp = 1,2
*          do it = 1,ntcont(icomp,iccd)
*            tcont(it,icomp,iccd) = tcont(it,icomp,iccd)/(86400*365.25)+1998
**            if (iccd.eq.7.and.icomp.eq.1) print*,tcont(it,icomp,iccd)
*          enddo
*        enddo
*      enddo
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_qe_cont_caldb_ccd (calfile,unit,nemax,ntmax,
     ~    econt,mu,necont,tcont,tau0,tau1,ntcont,fxy,iccd,status)
      implicit none
      character calfile*(*)
      integer unit, status

      integer iccd
      
      integer nemax,ntmax
      integer necont(3), ntcont(3)
      real econt(nemax,3)
      real mu(nemax,3)
      double precision tcont(ntmax,3)
      real tau0(ntmax,3)
      real tau1(ntmax,3)
      real fxy(1024,1024,3)
      integer colnum, icomp
      logical anyf
      
      integer ibin
      real w1,w2
      real f_F,E_F, tau_F_e, tau_F_f
      real f_Mn,E_Mn, tau_Mn_e, tau_Mn_f
      real tau_L
      parameter (f_F  = 0.6, E_F=0.679)
      parameter (f_Mn = 0.4, E_Mn=0.638)
      integer i, it

      real offaxis, tauy
      integer chipx,chipy

      real offset, T2, T, tauinf, tauinf2, const, taugr
      double precision date, d

      real taufit (2048,2048)
      logical firstcall
      data firstcall /.true./
      save firstcall, taufit


      real ee(100000), tau_elem(100000), tau_C(100000), tau_O(100000),
     ~    tau_F(100000), exafs_C(10000)
      real tau_Cl(100000)
      real tau_L_C, tau_L_O, tau_L_Cl, tau_L_elem
      integer ne
      save ee, tau_elem, tau_C, tau_O, tau_F, exafs_C, tau_Cl
      save tau_L_C, tau_L_O, tau_L_Cl, tau_L_elem
      save ne

      real tC,eC,tO,tCl,tF, tE

      real efold, tauLexp
      save tauLexp

      real Delta, tauyc

      double precision par(8), fitc, fitd


      if (firstcall) then
        firstcall = .false.
        call load_cont_data_5 (ee,tau_elem,tau_C,tau_O,tau_F,
     ~      exafs_C,tau_Cl,ne,
     ~      tau_L_C, tau_L_O, tau_L_Cl,
     ~      tau_L_elem)
        efold = 0.63
        tau_F_e=exp(-(E_F/efold)**2)
        tau_Mn_e=exp(-(E_Mn/efold)**2)
        tauLexp = -log(f_F*exp(-tau_F_e)+f_Mn*exp(-tau_Mn_e))

        call read_fits_image ('i.fit',taufit,2048,2048,'e')

      endif

      call FTGCNO (unit,.false.,'n_energy',colnum,status)
      call ftgcvj (unit,colnum,1,1,1,0,necont,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'energy',colnum,status)
      do icomp=1,1
        if (necont(icomp).gt.nemax) then
          call exiterror ('increase nemax in corrarf_caldb')
        endif
        call ftgcve (unit,colnum,icomp,1,necont(icomp),0.0,
     ~      econt(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'mu',colnum,status)
      do icomp=1,1
        call ftgcve (unit,colnum,icomp,1,necont(icomp),0.0,
     ~      mu(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

       
      call FTGCNO (unit,.false.,'n_time',colnum,status)
      call ftgcvj (unit,colnum,1,1,1,0,ntcont,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'time',colnum,status)
      do icomp=1,1
        if (ntcont(icomp).gt.ntmax) then
          call exiterror ('increase ntmax in corrarf_caldb')
        endif
        call ftgcvd (unit,colnum,icomp,1,ntcont(icomp),0.0d0,
     ~      tcont(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'tau0',colnum,status)
      do icomp=1,1
        call ftgcve (unit,colnum,icomp,1,ntcont(icomp),0.0,
     ~      tau0(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'tau1',colnum,status)
      do icomp=1,1
        call ftgcve (unit,colnum,icomp,1,ntcont(icomp),0.0,
     ~      tau1(1,icomp),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'fxy',colnum,status)
      do icomp=1,1
        call ftgcve (unit,colnum,icomp,1,1024*1024,0.0,
     ~      fxy(1,1,icomp),anyf,status)
      enddo

c      call saoimage (fxy(1,1,1),1024,1024,'e')

      if (status.ne.0) call exit_fitsio (calfile,status)

*c Delete fluffium
*      call ftdrow (unit,2,1,status)
*      if (status.ne.0) call exit_fitsio (calfile,status)

c Recompute mu
      do icomp=1,1
         
         if (icomp.eq.3) then ! make energy grid for component 3
            necont(icomp)=necont(1)
            do i=1,necont(icomp)
               econt(i,icomp)=econt(i,1)
            enddo
         endif

         do i=1,necont(icomp)
            call locate (ee,ne,econt(i,icomp),ibin)
            if (ibin.lt.1) then
               w1 = 1
               w2 = 0
               ibin = 1
            else if (ibin.ge.ne) then
               ibin = ne-1
               w1=0
               w2=1
            else
               call get_weights (econt(i,icomp),ee(ibin),ee(ibin+1),w1,w2)
            endif

            tE = exp(-(econt(i,icomp)/efold)**2)/tauLexp
            if (icomp.eq.1) then
               tC = (w1*tau_C(ibin)+w2*tau_C(ibin+1))/tau_L_C
               eC = w1*exafs_C(ibin)+w2*exafs_C(ibin+1)
               tC = tC*((eC-1)*1.0+1.0)
               tO = 0.18*(w1*tau_O(ibin)+w2*tau_O(ibin+1))/tau_L_O
               tF = (w1*tau_F(ibin)+w2*tau_F(ibin+1))/tau_L_elem*(1+0.288256)

               mu(i,icomp) = (tC+tO+0.75*tE+tF)/(1+0.75+0.18)
            endif
         enddo
      enddo

c and write out mu
      call FTGCNO (unit,.false.,'n_energy',colnum,status)
      call ftpclj (unit,colnum,1,1,1,necont,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'energy',colnum,status)
      do icomp=1,1
        call ftpcle (unit,colnum,icomp,1,necont(icomp),econt(1,icomp),status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'mu',colnum,status)
      do icomp=1,1
        call ftpcle (unit,colnum,icomp,1,necont(icomp),mu(1,icomp),status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)


c Replace fxy
      if (iccd.lt.4) then

        if (iccd.eq.3) then
          do chipx=1,1024
            do chipy=1,1024
              fxy(chipx,chipy,1)=taufit(chipx,chipy)
            enddo
          enddo

        else if (iccd.eq.1) then
          do chipx=1,1024
            do chipy=1,1024
              fxy(chipx,chipy,1)=taufit(1024+chipx,chipy)
            enddo
          enddo

        else if (iccd.eq.2) then
          do chipx=1,1024
            do chipy=1,1024
              fxy(chipx,chipy,1)=taufit(1024-chipx+1,2048-chipy+1)
            enddo
          enddo

        else if (iccd.eq.0) then
          do chipx=1,1024
            do chipy=1,1024
              fxy(chipx,chipy,1)=taufit(2048-chipx+1,2048-chipy+1)
            enddo
          enddo
          
        endif

      else
        ! double-exponent fit for ACIS-S
        Delta = 83.215739488628628
        tauyc = 2*exp(-512/Delta)
        do chipy=1,1024
          tauy = (exp(-chipy/Delta)+exp(-(1024-chipy)/Delta))
          tauy = (tauy-tauyc)*1.1396218002995975
          do chipx=1,1024
            fxy(chipx,chipy,1)=tauy
            fxy(chipx,chipy,3)=0
          enddo
        enddo

      endif
      call FTGCNO (unit,.false.,'fxy',colnum,status)
      do icomp=1,1
        call ftpcle (unit,colnum,icomp,1,1024*1024,fxy(1,1,icomp),status)
      enddo
*      call saoimage (fxy(1,1,1),1024,1024,'e')

c Replace time dependence
      ntcont(3)=ntcont(1)
      do icomp = 1,1
         do it = 1,ntcont(icomp)
            if (it.gt.1) then
               date = 49208255.9999983d0 + 86400*365.25*(it-1)/12
               tcont(it,icomp) = date
            else
               tcont(1,icomp) = 49208255.9999983d0
               date=tcont(1,icomp)
            endif
        
            date = date/(86400*365.25)+1998

            d = date - 1999.0 - 204.0/365.0


            if (icomp.eq.1) then

c / tau_L fit at the center of S3 + A1795:
              par(1)=0.67879999
              par(2)=2.83330059
              par(3)=0.11085909
              par(4)=5.71868334
              par(5)=4.23796390
              par(6)=0          !0.09700000
              par(7)=9.12998952
              par(8)=0.04954484
              
              fitc=par(1)*(1-exp(-d/par(2))) +
     ~             par(3)*(1-exp(-max(d-par(5),0.0d0)/par(4)))+par(6)
              if (d.gt.par(7)) then
               fitc = fitc + (d-par(7))*par(8)
              endif

c / edge-to-center fit
              par(1)= 0.23199999
              par(2)= 0.89340001
              par(3)= 0.08369070
              par(4)= 2.62094479
              par(5)= 4.50000000
              par(6)= 0.00000000
              par(7)=10.03261845
              par(8)= 0.16930231
              
              fitd=par(1)*(1-exp(-d/par(2))) +
     ~             par(3)*(1-exp(-max(d-par(5),0.0d0)/par(4)))+par(6)
              if (d.gt.par(7)) then
               fitd = fitd + (d-par(7))*par(8)
              endif

              tau0(it,1)=fitc
              
              if (iccd.lt.4) then
                tau1(it,1) = fitd/0.53
              else
                tau1(it,1) = fitd/0.56
              endif
               
*            else if (icomp.eq.2) then
*               tau0(it,icomp) = 0.63*(1-exp(-max(d-1.27,0.0)/6.2))
*               tau1(it,icomp) = 0.2126*(1-exp(-max(d-5.886,0.0)/3.922))
*
*            else if (icomp.eq.3) then
*               tau0(it,icomp) = 0.097
*               tau1(it,icomp) = 0
            endif
         enddo
      enddo

c // write it out
      call FTGCNO (unit,.false.,'n_time',colnum,status)
      call ftpclj (unit,colnum,1,1,1,ntcont,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'time',colnum,status)
      do icomp=1,1
        call ftpcld (unit,colnum,icomp,1,ntcont(icomp),tcont(1,icomp),status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'tau0',colnum,status)
      do icomp=1,1
        call ftpcle (unit,colnum,icomp,1,ntcont(icomp),tau0(1,icomp),status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'tau1',colnum,status)
      do icomp=1,1
        call ftpcle (unit,colnum,icomp,1,ntcont(icomp),tau1(1,icomp),status)
      enddo
      if (status.ne.0) call exit_fitsio (calfile,status)


      return
      end
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_cont_data_5 (e,tau_elem,tau_C,tau_O,tau_F,
     ~    exafs_C,tau_Cl,ne,
     ~    tau_L_C, tau_L_O, tau_L_Cl, tau_L_elem)
      implicit none
      integer ne
      real e(*), tau_elem(*), tau_C(*), tau_O(*), tau_F(*), exafs_C(*)
      real tau_Cl(*)
      real tau_L_C, tau_L_O, tau_L_Cl
      character file*200
      integer unit, newunit
      integer status
      
      integer ibin
      real w1,w2
      real f_F,E_F, tau_F_e, tau_F_C, tau_F_O, tau_F_Cl
      real f_Mn,E_Mn, tau_Mn_e, tau_Mn_C, tau_Mn_O, tau_Mn_Cl
      real tau_L_elem
      parameter (f_F  = 0.6, E_F=0.679)
      parameter (f_Mn = 0.4, E_Mn=0.638)
      integer i
      real gov
      
      unit = newunit()
      
      file = 'tau_components_caldbgrid.dat'
      
      open (unit,file=file,status='old')
      ne = 0
      status = 0
      do while (status.eq.0)
         read (unit,*,iostat=status) e(ne+1), tau_elem(ne+1),
     ~        tau_C(ne+1), tau_O(ne+1), tau_F(ne+1), exafs_C(ne+1),
     ~        gov, tau_Cl(ne+1)
         if (status.eq.0) then
            ne = ne + 1
         endif
      enddo
      
      close(unit)
      
c     Renorm to unit tau for the L-complex
      call locate (e,ne,E_F,ibin)
      call get_weights (E_F,e(ibin),e(ibin+1),w1,w2)
      tau_F_e = w1*tau_elem(ibin)+w2*tau_elem(ibin+1)
      tau_F_C = w1*tau_C(ibin)+w2*tau_C(ibin+1)
      tau_F_O = w1*tau_O(ibin)+w2*tau_O(ibin+1)
      tau_F_Cl = w1*tau_Cl(ibin)+w2*tau_Cl(ibin+1)
      

      call locate (e,ne,E_Mn,ibin)
      call get_weights (E_Mn,e(ibin),e(ibin+1),w1,w2)
      tau_Mn_e = w1*tau_elem(ibin)+w2*tau_elem(ibin+1)
      tau_Mn_C = w1*tau_C(ibin)+w2*tau_C(ibin+1)
      tau_Mn_O = w1*tau_O(ibin)+w2*tau_O(ibin+1)
      tau_Mn_Cl = w1*tau_Cl(ibin)+w2*tau_Cl(ibin+1)
      
      tau_L_elem = -log(f_F*exp(-tau_F_e)+f_Mn*exp(-tau_Mn_e))
      tau_L_C = -log(f_F*exp(-tau_F_C)+f_Mn*exp(-tau_Mn_C))
      tau_L_O = -log(f_F*exp(-tau_F_O)+f_Mn*exp(-tau_Mn_O))
      tau_L_Cl = -log(f_F*exp(-tau_F_Cl)+f_Mn*exp(-tau_Mn_Cl))
      print*,tau_L_elem, tau_L_C, tau_L_O, tau_L_Cl
      do i=1,ne
         tau_C(i) = tau_C(i)/exafs_C(i)
      enddo
      
      return
      end
