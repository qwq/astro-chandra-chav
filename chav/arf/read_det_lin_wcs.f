      subroutine read_det_lin_wcs (unit,file,crval1,crval2,crpix1,crpix2
     ~    ,cdelt1,cdelt2, crota2, ctype1, ctype2)
      implicit none
      integer unit
      real crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      character*(*) ctype1, ctype2, file

      integer status
      character*80 comment, wfile

      logical mykeywords
      logical wmap_in_chip
      logical yespar

      wmap_in_chip = yespar ('wmap_in_chip')

      status = 0
      wfile=file
      mykeywords = .true.
      call ftgkys (unit,'CTYPE1',ctype1,comment,status)
      if (status.eq.0) then
        if (ctype1.ne.'DETX-LIN') then
          mykeywords = .false.
        endif
      else
        mykeywords = .false.
        status = 0
      endif

      call ftgkys (unit,'CTYPE2',ctype2,comment,status)
      if (status.eq.0) then
        if (ctype2.ne.'DETY-LIN') then
          mykeywords = .false.
        endif
      else
        mykeywords = .false.
        status = 0
      endif

      if (mykeywords) then
        call ftgkye (unit,'CRPIX1',crpix1,comment,status)
        if (status.ne.0) call exit_fitsio ('CRPIX1, '//wfile,status)
        call ftgkye (unit,'CRPIX2',crpix2,comment,status)
        if (status.ne.0) call exit_fitsio ('CRPIX2, '//wfile,status)
        call ftgkye (unit,'CRVAL1',crval1,comment,status)
        if (status.ne.0) call exit_fitsio ('CRVAL1, '//wfile,status)
        call ftgkye (unit,'CRVAL2',crval2,comment,status)
        if (status.ne.0) call exit_fitsio ('CRVAL2, '//wfile,status)
        call ftgkye (unit,'CDELT1',cdelt1,comment,status)
        if (status.ne.0) call exit_fitsio ('CDELT1, '//wfile,status)
        call ftgkye (unit,'CDELT2',cdelt2,comment,status)
        if (status.ne.0) call exit_fitsio ('CDELT2, '//wfile,status)
        call ftgkye (unit,'CROTA2',crota2,comment,status)
        if (status.ne.0) then
          crota2 = 0.0
          status = 0
        endif
      else
        call ftgkys (unit,'CTYPE1P',ctype1,comment,status)
        if (status.ne.0) call exit_fitsio ('CTYPE1P, '//wfile,status)
!        if (ctype1.ne.'detx'.and.ctype1.ne.'DETX') then
        if ((wmap_in_chip.and.(ctype1.eq.'chipx'.or.ctype1.eq.'CHIPX')).or
     ~      .(ctype1.eq.'detx'.or.ctype1.eq.'DETX')) then
          ctype1='DETX-LIN'
        else
          call exiterror ('ctype1!=detx-lin && ctype1p!=detx')
        endif

        call ftgkys (unit,'CTYPE2P',ctype2,comment,status)
        if (status.ne.0) call exit_fitsio ('CTYPE2P, '//wfile,status)
!        if (ctype2.ne.'dety'.and.ctype2.ne.'DETY') then
        if ((wmap_in_chip.and.(ctype2.eq.'chipy'.or.ctype2.eq.'CHIPY')).or
     ~      .(ctype2.eq.'dety'.or.ctype2.eq.'DETY')) then
          ctype2='DETY-LIN'
        else
          call exiterror ('ctype2!=dety-lin && ctype2p!=dety')
        endif
        call ftgkye (unit,'CRPIX1P',crpix1,comment,status)
        if (status.ne.0) call exit_fitsio ('CRPIX1, '//wfile,status)
        call ftgkye (unit,'CRPIX2P',crpix2,comment,status)
        if (status.ne.0) call exit_fitsio ('CRPIX2, '//wfile,status)
        call ftgkye (unit,'CRVAL1P',crval1,comment,status)
        if (status.ne.0) call exit_fitsio ('CRVAL1, '//wfile,status)
        call ftgkye (unit,'CRVAL2P',crval2,comment,status)
        if (status.ne.0) call exit_fitsio ('CRVAL2, '//wfile,status)
        call ftgkye (unit,'CDELT1P',cdelt1,comment,status)
        if (status.ne.0) call exit_fitsio ('CDELT1, '//wfile,status)
        call ftgkye (unit,'CDELT2P',cdelt2,comment,status)
        if (status.ne.0) call exit_fitsio ('CDELT2, '//wfile,status)
        call ftgkye (unit,'CROTA2P',crota2,comment,status)
        if (status.ne.0) then
          crota2 = 0.0
          status = 0
        endif
      endif
      
      return
      end

