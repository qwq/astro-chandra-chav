*      subroutine read_ccd_qeu (calfile,
*     ~    ne,nchipx,nchipy,
*     ~    crpix,crval,cdelt,
*     ~    ndatamax,
*     ~    e0,qeu,
*     ~    iccd)
      implicit none
c
c Reads calibration data for ccd qeu
c
c O ne       integer    - the number of enery grid points
c O nchipx   integer    - the number of chipx grid points
c O nchipy   integer    - the number of chipy grid points
c O crpix(3) real       
c O crval(3) real       arrays to specify the transformation from the image
c O cdelt(3) real       to the data values
c I ndatamax integer    - maximum size of the QEU map
c O e0(ne)   real       - energy grid for QE
c O qeu(ne*nchipx*nchipy)  real       - QEU map
c i iccd    integer    - ccd; it is ignored when reading the ASCII cal files
c
      character*80 calfile
      integer ne, nchipx, nchipy, ndatamax
      integer nemax, nchipxmax, nchipymax
      parameter (nemax=300,nchipxmax=32,nchipymax=32)
      parameter (ndatamax=nemax*nchipxmax*nchipymax)
      real qeu(ndatamax),e0(nemax)
      real crpix(3), crval(3), cdelt(3)
      integer iccd

      integer unit,status
      character*200 calfilelocal

      integer chipid, ndat
      integer blocksize, hdutype, colnum, datacode, width
      character*80 extname, comment, keyname
      logical anyf
      integer naxis,naxes(10)
      logical defined
      integer rwmode, i
      
      
      call get_cl_par ('qeu',calfile)
      if (.not.defined(calfile)) then
        write(0,*) 'qeu=?'
        call exit(1)
      endif

      calfilelocal = calfile
      call putmessage ('CCD QEU: '//calfilelocal)
      
      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      rwmode = 1
      call ftopen (unit,calfile,rwmode,blocksize,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      
      status = 0
      do while (status.eq.0)
        call FTMRHD(unit,1,hdutype,status)
        if (status.eq.0) then
          if (hdutype.eq.2) then ! binary table
            call ftgkys (unit,'EXTNAME',extname,comment,status)
            if (status.ne.0) call exit_fitsio (calfile,status)
            if (extname.eq.'AXAF_QEU') then
              call ftgkyj (unit,'CCD_ID',chipid,comment,status)
              if (status.ne.0) call exit_fitsio (calfile,status)
              iccd = chipid

              call FTGCNO (unit,.false.,'ENERGY',colnum,status)
              call FTGTCL (unit,colnum, datacode,ne,width,status)
              if (status.ne.0) call exit_fitsio (calfile,status)
              call ftgcve (unit,colnum,1,1,ne,0.0,e0,anyf,status)
              if (status.ne.0) call exit_fitsio (calfile,status)


              call FTGCNO (unit,.false.,'QEU',colnum,status)
              call FTGTCL (unit,colnum, datacode,ndat,width,status)
              if (status.ne.0) call exit_fitsio (calfile,status)
              if (ndat.gt.ndatamax) then
                write (0,*) 'QEU array is too small to load data for CCD '
     ~              ,iccd
                call exit(1)
              endif
              call ftgcve (unit,colnum,1,1,ndat,0.0,qeu,anyf,status)
              if (status.ne.0) call exit_fitsio (calfile,status)

              call ftgtdm (unit,colnum,10,naxis,naxes,status)
              if (status.ne.0) call exit_fitsio (calfile,status)
              if (naxis.ne.3) then
                call exiterror ('Not a 3D image in the QEU column')
              endif
              if (naxes(1).ne.ne) then
                call exiterror ('first dim of the QEU image != ne')
              endif
              nchipx = naxes(2)
              nchipy = naxes(3)

              write (keyname,'(''2CRVL'',i2)') colnum
              call rmblanks(keyname)
              call ftgkye (unit,keyname,crval(2),comment,status)
              write (keyname,'(''2CRPX'',i2)') colnum
              call rmblanks(keyname)
              call ftgkye (unit,keyname,crpix(2),comment,status)
              write (keyname,'(''2CDLT'',i2)') colnum
              call rmblanks(keyname)
              call ftgkye (unit,keyname,cdelt(2),comment,status)

              write (keyname,'(''3CRVL'',i2)') colnum
              call rmblanks(keyname)
              call ftgkye (unit,keyname,crval(3),comment,status)
              write (keyname,'(''3CRPX'',i2)') colnum
              call rmblanks(keyname)
              call ftgkye (unit,keyname,crpix(3),comment,status)
              write (keyname,'(''3CDLT'',i2)') colnum
              call rmblanks(keyname)
              call ftgkye (unit,keyname,cdelt(3),comment,status)

              call update_qeu_array (qeu, ne, nchipx, nchipy, iccd, e0,
     ~            crpix, crval, cdelt)
              
              print*,(qeu(i),i=1,5)
              call ftpcle (unit,colnum,1,1,ndat,qeu,status)
              if (status.ne.0) call exit_fitsio (calfile,status)

              
            endif
          endif
        endif
      enddo
      

      status = 0
      
      call ftclos(unit,status)
      status = 0
      call ftfiou(unit,status)
      status = 0
      
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine update_qeu_array (qeu, ne, nchipx, nchipy, iccd, e0,
     ~    crpix, crval, cdelt)
      implicit none
      integer ne, nchipx, nchipy, iccd
      real qeu (ne,nchipx, nchipy)
      real e0(ne)
      real crpix(3), crval(3), cdelt(3)

      integer ie, ix, iy
      real e, chipx, chipy

c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      logical corrcti, newcorrcti
      real lowecor
      save corrcti, newcorrcti, lowecor
      
      real qe
      character*80 arg
      logical defined, no
      real ccd_qe_corr, ccd_qe_corr_jul2000
      external ccd_qe_corr, ccd_qe_corr_jul2000

      if (firstcall) then
        call get_cl_par ('correct_qe_cti',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL CORRECT FOR THE CTI DROP IN QE'
          corrcti=.true.
          if (arg.eq.'new') then
            newcorrcti = .true.
            print*,'USING NEW RELATIONS BASED ON JULY 2000 DATA'
          else
            newcorrcti = .false.
          endif
        else
          corrcti=.false.
        endif
        
        call get_pv_default ('ccd_qe_lowecor',lowecor,1.0,'e')
        if (lowecor.ne.1.0) then
          print *,'I WILL MULTIPLY QE below 1.832 keV by ',lowecor
        endif
        
        firstcall = .false.
      endif
       

      do ie=1,ne
        do ix=1,nchipx
          do iy=1,nchipy
            
            e = e0(ie)
            
            chipx = crval(2) + (ix-crpix(2))*cdelt(2)
            chipy = crval(3) + (iy-crpix(3))*cdelt(3)
            
            qe = 1
            if (corrcti) then
              if (newcorrcti) then
                qe = qe*ccd_qe_corr_jul2000 (e,iccd,chipx,chipy)
              else
                qe = qe*ccd_qe_corr (e,iccd,chipx,chipy)
              endif
            endif
            
            if ( iccd.ne.7.and.iccd.ne.5.and.e.lt.1.832 ) then
              qe = qe * lowecor
            endif
            
            if (iccd.ne.7.and.iccd.ne.5) then
              qeu (ie,ix,iy) = qe
            endif
          enddo
        enddo
      enddo

      return
      end








