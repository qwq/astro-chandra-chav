      implicit none
      real chipy, t

      real tau_time_dep, tau_spat_dep_s
      real taucenter, taubottom, tautop
      real tau

      chipy = 177

      do t=1999.6,2004.4,0.1
        taucenter = tau_time_dep(t,0)
        taubottom = tau_time_dep(t,1)
        tautop    = tau_time_dep(t,2)
        tau = tau_spat_dep_s (chipy,taucenter,taubottom,tautop)
        print*,t,tau
      enddo

      end
        

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function tau_spat_dep_i (offaxis,taucenter,tauborder)
      implicit none
      real offaxis,taucenter,tauborder
c
c Fit to the spatial dependence of contamination on ACIS-I
c
c offaxis   - offaxis angle in arcmin
c
c taucenter - optical depth at the center (8'x8' square centered on the aim
c             point) 
c tauborder - optical depth in the 2'-wide strip along the edge of the array
c
c NOTE: this function wrongly assumes that the CCD size is 8x8 arcmin exactly
c
c -> Thu Jul 10 00:51:51 2003
c
      real off0, off1
      real gamma 
      parameter (gamma=2.0)

      off0=3.24**gamma
      off1=8.07**gamma

c Eq. 17 & 8 in the April, 2004 memo
      tau_spat_dep_i = taucenter +
     ~    (tauborder-taucenter)*(offaxis**gamma-off0)/(off1-off0)

      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function tau_spat_dep_s (chipy,taucenter,taubottom,tautop)
      implicit none 
      real chipy, taucenter, taubottom, tautop
c
c  Fit to the spatial dependence of contamination on ACIS-S
c
c  taucenter - optical depth at the center
c  taubottom - average optical depth at chipy=1:128
c
c -> Mon Apr 12 20:58:15 2004
c
      real alphabot,alphatop,yc,ybot,ytop
      parameter (
     ~    alphabot=5.5,
     ~    alphatop=4.5,
     ~    yc = 512.0,
     ~    ybot = 64.0,
     ~    ytop = 964.0
     ~    )

c Eq. 7 & 11 in the April, 2004 memo
      if (chipy.le.yc) then
        tau_spat_dep_s = taucenter +
     ~      (taubottom-taucenter)*
     ~      abs(chipy-yc)**alphabot/abs(ybot-yc)**alphabot
      else
        tau_spat_dep_s = taucenter +
     ~      (tautop-taucenter)*
     ~      abs(chipy-yc)**alphatop/abs(ytop-yc)**alphatop
      endif
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function tau_time_dep (year,mode)
      implicit none
      real year
      integer mode
c
c Returns contamination optical depth at the ECS line for center of ACIS-S
c (mode=0), bottom of ACIS-S (mode=1), top of ACIS-S (mode=2), center of ACIS
c I (mode=3), and border of ACIS-I (mode=4) as a function of observation date
c (e.g., year=2003.27) 
c
c -> Mon Apr 12 21:02:57 2004
c
      real d

      d = year - (1999.0+204.0/365.0)

c The fit parameters are from the April, 2004 memo with correction on Apr 9
      if      (mode.eq.0) then       ! ACIS-S-center
        tau_time_dep = 0.63997*(1.0-exp(-d/2.05405))
      else if (mode.eq.1) then  ! ACIS-S-bottom
        tau_time_dep = 0.87532*(1.0-exp(-d/1.40934))
      else if (mode.eq.2) then  ! ACIS-S-top
        tau_time_dep = 0.81544*(1.0-exp(-d/1.53616))
      else if (mode.eq.3) then  ! ACIS-I-center
        tau_time_dep = 0.67499*(1.0-exp(-d/1.4042))
      else if (mode.eq.4) then  ! ACIS-I-border
        tau_time_dep = 0.96650*(1.0-exp(-d/1.2951))
      else
        call exiterror ('unknown mode')
      endif

      return
      end
