#include <math.h>
#include <stdlib.h>
#include <stdio.h>

int asp_convolve_lists (
			 float *mask, int nx, int ny,
			 float *masklist, int *ind_masklist, int nmasklist,
			 float *asphist, int *ind_asphist, int nasplist,
			 int naspx, int naspy,
			 float rollmask, int binmask, 
			 float refx, float refy,
			 float rollasp, 
			 float crval1, float crpix1, float cdelt1,
			 float crval2, float crpix2, float cdelt2,
			 int inverse
			 );

int rotate_list (float *list, int *ind, int n, int nx, int ny,
		 float angle, float refx, float refy,
		 float **rotlist, int **rotind, int *nrot);

int img_to_list_c (float *img, float minval, 
		   int nx, int ny, float **list, int **ind, int *nrot);

int convolve_lists_wcs (
			float *mask, int nx, int ny,
			float *list, int *ind, int nlist,
			int binmask,
			float *asp, int *indasp, int nasp,
			int naspx, int naspy,
			float crval1, float crpix1, float cdelt1,
			float crval2, float crpix2, float cdelt2,
			float rotangle, int inverse
			);

#define nint(x) ((x)<0 ? ((int)(x-0.5)) :((int)(x+0.5)))

/*---------------------------------------------------------------------------*/
int asp_convolve_lists_ (
			 float *mask, int *nx, int *ny,
			 float *masklist, int *ind_masklist, int *nmasklist,
			 float *asphist, int *ind_asphist, int *nasplist,
			 int *naspx, int *naspy,
			 float *rollmask, int *binmask, 
			 float *refx, float *refy,
			 float *rollasp, 
			 float *crval1, float *crpix1, float *cdelt1,
			 float *crval2, float *crpix2, float *cdelt2,
			 int *inverse
			 )
{
  /*
    subroutine asp_convolve_lists (
    ~      mask,nx,ny,
    ~      masklist,ind_masklist,nmasklist,
    ~      asphist, ind_asphist, nasplist,
    ~      rollmask, binmask,
    ~      rollasp, crval1, crpix1, cdelt1, crval2, crpix2, cdelt2,
    ~	   inverse)
  */

  /* change pointers to values where possible */
  return asp_convolve_lists (mask, *nx, *ny, 
			     masklist, ind_masklist, *nmasklist,
			     asphist,  ind_asphist,  *nasplist,
			     *naspx, *naspy,
			     *rollmask, *binmask, *refx, *refy,
			     *rollasp,
			     *crval1,*crpix1,*cdelt1,*crval2,*crpix2,*cdelt2,
			     *inverse);
}


/*---------------------------------------------------------------------------*/

int asp_convolve_lists (
			 float *mask, int nx, int ny,
			 float *masklist, int *ind_masklist, int nmasklist,
			 float *asphist, int *ind_asphist, int nasplist,
			 int naspx, int naspy,
			 float rollmask, int binmask,
			 float refx, float refy,
			 float rollasp, 
			 float crval1, float crpix1, float cdelt1,
			 float crval2, float crpix2, float cdelt2,
			 int inverse
			 )
{
  register int i;
  
  float *rotmasklist=NULL;
  int *ind_rotmasklist=NULL;
  int nrotmasklist;
  int freerotmask;
  float rotangle;
  
  if (fabs(rollmask-rollasp)>0.0) 
    {
      if (!inverse) {
	rotangle = rollasp-rollmask;
      }
      else {
	rotangle = -(rollasp-rollmask);
      }
      
      fprintf (stderr," ... rotating mask list by %g degrees\n",rotangle);
      rotate_list (masklist,ind_masklist,nmasklist,nx,ny,
		   -rotangle, refx, refy,
		   &rotmasklist,&ind_rotmasklist,&nrotmasklist);
      fprintf (stderr,"       %d positive pixels in the rotated mask\n",
	       nrotmasklist);
      freerotmask = 1;
    }
  else
    {
      rotmasklist = masklist;
      ind_rotmasklist = ind_masklist;
      nrotmasklist = nmasklist;
      freerotmask = 0;
    }
  
  convolve_lists_wcs (
		      mask,nx,ny,
		      rotmasklist, ind_rotmasklist, nrotmasklist,
		      binmask,
		      asphist,ind_asphist,nasplist,
		      naspx, naspy,
		      crval1,crpix1,cdelt1,crval2,crpix2,cdelt2,
		      -rotangle,inverse);
  
  return 0;
}
/*---------------------------------------------------------------------------*/
int convolve_lists_wcs (
			float *mask, int nx, int ny,
			float *list, int *ind, int nlist,
			int binmask,
			float *asp, int *indasp, int nasp,
			int naspx, int naspy,
			float crval1, float crpix1, float cdelt1,
			float crval2, float crpix2, float cdelt2,
			float rotangle, int inverse
			)
{
  float shiftx, shifty;
  register int l, Nlist, Nx, Ny, m, nxny, ii, jj, ishiftx, ishifty, k;
  int i,j;
  float dx,dy, sina, cosa;
  int shiftind;

  nxny = nx*ny;
  Nlist = nlist;
  Nx = nx;
  Ny = ny;

  /*  for (l=0;l<nxny;l++)
      mask[l]=0.0; */
  
  sina = sin(rotangle*3.14159265358979/180.0);
  cosa = cos(rotangle*3.14159265358979/180.0);

  for (k=0;k<nasp;k++)
    {
      fprintf (stderr,"   %6d  of %6d\r",k+1,nasp);
      i = indasp[k]-naspx*(indasp[k]/naspx);
      j = (indasp[k]-i)/naspx+1;

      shiftx = crval1+(i-crpix1)*cdelt1;
      shifty = crval2+(j-crpix2)*cdelt2;

      if (inverse) { /* we need to apply inverse shifts and 
			rotate them. see rotate_shift.tex */
	shiftx = -shiftx; shifty = -shifty;
	dx = shiftx*cosa-shifty*sina;
	dy = shiftx*sina+shifty*cosa;
	shiftx = dx;
	shifty = dy;
      }
      
      ishiftx = nint(shiftx/binmask);
      ishifty = nint(shifty/binmask);
      
      shiftind = ishifty*Nx+ishiftx - 1; /* -1 is needed to convert FORTRAN 
                                            index to C */
      for (l=0;l<Nlist;l++) 
        {
          m = ind[l]+shiftind;
          if ( m>=0 && m<nxny) mask[m] += asp[k]*list[l];
          
        }
    }
  fprintf (stderr,"\n");
  return 0;
}

/*---------------------------------------------------------------------------*/
int rotate_list (float *list, int *ind, int n, int nx, int ny,
		 float angle, float refx, float refy,
		 float **rotlist, int **rotind, int *nrot)
{
  float *img;
  float boundval = 0;
  int subpix = 3;
  float minval = 0.0;
  if (( img = (float *)malloc (nx*ny*sizeof(float))) == NULL) {
    perror ("rotate_list"); exit(1);
  }
  
  list_to_img (list,ind,n,nx,ny,img);
  rotate_img_main_ (img,&nx,&ny,&refx,&refy,&angle,&boundval,&subpix);
  img_to_list_c (img,minval,nx,ny,rotlist,rotind,nrot);
  if (rotind == NULL || rotlist == NULL ) {
    fprintf (stderr,
	     "Something is wrong with memory allocation in img_to_list\n");
    exit(1);
  }

  /*  saoimage_ (img,&nx,&ny,"e"); */
  free(img);

  return 0;

}

/*---------------------------------------------------------------------------*/
int list_to_img (float *list, int *ind, int n, int nx, int ny, float *img)
{
  register int i;

  for (i=0;i<nx*ny;i++)
    img[i]=0;

  for (i=0;i<n;i++)
    img[ind[i]-1]=list[i];

  return 0;
}

/*---------------------------------------------------------------------------*/
int img_to_list_c (float *img, float minval, 
		 int nx, int ny, float **listp, int **indp, int *nrot)
{
  register int i, k;
  float *list; int *ind;
  k = 0;
  for (i=0;i<nx*ny;i++)
    {
      if (img[i]>minval)
	k++;
    }
  *nrot = k;

  /* free list and index arrays if already allocated */
  if (*listp != NULL) free(*listp);
  if (*indp != NULL)  free(*indp);
  
  /* allocate memory for list and index */
  if ( ( *listp = (float *)malloc(sizeof(float)*(*nrot)) ) == NULL ) {
    perror ("img_to_list");
    exit(1);
  }
  if ( ( *indp = (int *)malloc(sizeof(int)*(*nrot)) ) == NULL ) {
    perror ("img_to_list");
    exit(1);
  }
  ind = *indp;
  list = *listp;

  /* convert image to list */
  k = 0;
  for (i=0;i<nx*ny;i++)
    {
      if (img[i]>minval) {
	list[k]=img[i];
	ind[k]=i+1;
	k++;
      }
    }

  k = 0;
  return 0;
}

