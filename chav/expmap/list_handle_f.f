      subroutine img_to_list (img,n,minval,values,ind,nval)
      implicit none
c
c Converts the image (that part with img>minval) to list of pixels and values
c
      integer n
      real img(n), minval
      real values(*)
      integer ind(*)
      integer nval

      integer i,k
      
      k = 0
      do i=1,n
        if (img(i).gt.minval) then
          k = k + 1
          ind(k)    = i
          values(k) = img(i)
        endif
      enddo

      nval = k
      return
      end
