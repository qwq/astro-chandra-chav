      subroutine read_gti_exp (gtifile, exposure)
c
c Total exposure in the GTI file
c
      implicit none
      character*(*) gtifile
      real exposure

      double precision sum
      double precision tstop, tstart

      integer icolstart, icolstop
      integer unitgti, status
      integer i,ngti
      character*80 comment
      logical anyf
      character*200 buff

c 1) Open file
      status = 0
      call ftgiou (unitgti,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
      call ftnopn (unitgti,gtifile,0,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
       
c 2) Go to the GTI extension
      call read_gti_goto_GTI_ext (unitgti,gtifile,'*GTI*')
      
c 3) Find start/stop col name
      call read_gti_find_cols (unitgti,gtifile,'*START*', '*STOP*',
     ~    icolstart,icolstop)

c 4) Read the number of gtis
      call ftgkyj (unitgti,'NAXIS2',ngti,comment,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)

c 5) Read file row by row
      sum = 0
      buff = gtifile
      do i=1,ngti
        call ftgcvd (unitgti,icolstart,i,1,1,0.0d0,tstart,anyf,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        if (anyf) call exiterror('Tstart has undefined values in '//buff)
          
        call ftgcvd (unitgti,icolstop,i,1,1,0.0d0,tstop,anyf,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        if (anyf) call exiterror('Tstop has undefined values in '//buff)
        
        sum = sum + tstop - tstart
      enddo

      exposure = sum

      call ftclos(unitgti,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_gti_goto_GTI_ext (unitgti,gtifile,extspec)
      implicit none
      integer unitgti
      character*(*) gtifile, extspec

      integer status, hdutype
      character*80 extname
      logical match, exact
      character*80 comment
      integer lnblnk
      character*200 buff1, buff2
      
      status = 0
      call ftmahd(unitgti,1,hdutype,status)
      
      do while (status.eq.0)
        call ftmrhd (unitgti,1,hdutype,status)
        if (status.eq.0) then
          call ftgkys(unitgti,'EXTNAME',extname,comment,status)
          if (status.ne.0) call exit_fitsio (gtifile,status)
          call ftcmps (extspec,extname,.false.,match,exact)
          if (match) return     ! found it!
        endif
      enddo

C if we're here, extension was not found
      buff1 = extspec
      buff2 = gtifile
      call exiterror ('Extension '//buff1(1:lnblnk(buff1))//
     ~    ' was not found in '//buff2)
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine read_gti_find_cols (unitgti,gtifile,startcol,stopcol,
     ~    icolstart,icolstop)
      implicit none
      integer unitgti
      character*(*) gtifile, startcol, stopcol
      integer icolstart,icolstop

      integer status
      
      status = 0
      call ftgcno (unitgti,.false.,startcol,icolstart,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
      
      call ftgcno (unitgti,.false.,stopcol,icolstop,status)
      if (status.ne.0) call exit_fitsio (gtifile,status)
      
      return
      end
