      subroutine setup_chip_norms (
     ~    nchips,ichip,chipvals,refchip,
     ~    elo,ehi,cnts,ne,emin,emax)
      implicit none
c
c Make relative chip normalizations for the given source spectrum
c
c It is assumed the the spectrum table is generated for the ideal
c instrument with unit effective area and diagonal resp
c (e.g., with dummyrsp in xspec)
c
      integer nchips
      integer ichip(nchips)
      real chipvals (nchips)
      character*(*) refchip
      integer ne
      real elo(ne), ehi(ne), cnts(ne)
      real emin,emax

      character*80 chipname
      integer irefchip
      
      integer negridmax
      parameter (negridmax=11000)

      integer status, ich
      real countsref, counts(0:10)
      character*80 buff
      
c Read ccd_qe for the refchip

      call chip_name_to_idname (refchip,chipname)
      read (chipname,*,iostat=status) irefchip
      buff = refchip
      if (status.ne.0) call exiterror ('Cannot parse chip name: '//buff)
       
c Predict reference counts and counts in each chip
      call setup_chip_norms_modcounts (elo,ehi,cnts,ne, emin,emax, 
     ~     irefchip, countsref)
       
       if (countsref.le.0.0) then
         call exiterror ('Zero reference counts for the ref. spectrum')
       endif
       
       do ich=1,nchips
         call setup_chip_norms_modcounts (elo,ehi,cnts,ne, emin,emax, 
     ~       ichip(ich),counts(ichip(ich)))
         if (counts(ichip(ich)).le.0.0) then
           write (0,*) 'ERROR: Zero counts for the ref. spectrum in chip',
     ~         ichip(ich)
           call exit(1)
         endif
       enddo

c Calculate relative normalizations
       do ich=1,nchips
         chipvals(ich)=counts(ichip(ich))/countsref
       enddo

       return
       end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine setup_chip_norms_modcounts (elo,ehi,spec,ne, emin,emax, 
     ~    iccd,counts)
      implicit none
c
c Multiply model spectrum by QE and mirror area and calculate the number of
c counts
c
      integer ne
      real elo(ne), ehi(ne), spec(ne)
      real emin,emax
      integer iccd
      real counts
      integer ie
      integer negridmax
      parameter (negridmax=11000)
      real ccd_qe
      
      real de, ee
      parameter (de=0.02)       ! width of the energy grid
      logical found
      real hrma_onaxis_ea, ccd_qe_main
      external hrma_onaxis_ea, ccd_qe_main

c 
      counts = 0
c     do ee = emin, emax, de
      ee = emin
10    if (ee .le. emax ) then
        ee = ee + de
        ie = 1
        found = .false.
        do while (.not.found .and. ie.le.ne)
          if ( ee.ge.elo(ie).and.ee.le.ehi(ie) ) then
            found = .true.
          else
            ie = ie + 1
          endif
        enddo
        if (found) then
          ccd_qe = ccd_qe_main(ee,iccd)
          counts = counts + hrma_onaxis_ea(ee)*ccd_qe*de*spec(ie)
        endif
      goto 10
      endif
c     enddo

      
      return
      end
