      subroutine read_spec_table (file,emin,emax,cnts,n)
      implicit none
      character*(*) file
      real emin(*), emax(*), cnts(*)
      integer n
      integer unit,newunit,status
      character*200 line
      character*80 word(100)
      integer nwords
c fitsio variables
      character*80 filem


      filem = file
c     Now, try it as a text spectrum table
      unit = newunit()
      open(unit,file=file,status='old')
      read (unit,'(a)',iostat=status) line
      if (status.ne.0) call exiterror ('Empty energy grid file: '//filem)
      
      if (line(1:21).eq.' History file header:') then 
                                ! This is XSPEC history file made by rdhis
        call read_spec_table_xspechis (file,unit,emin,emax,cnts,n)
        close(unit)
        return
      endif
! Else, this must be a plane table in the format 
! E weight
      call splitwords(line,word,nwords)
      if (nwords.ne.2) then
        call exiterror ('Unknown format of the energy grid file: '//filem)
      endif
      rewind(unit)
      n = 0
      status=0
      do while (status.eq.0)
        read (unit,'(a)',iostat=status) line
        if (status.eq.0) then
          call splitwords(line,word,nwords)
          if (nwords.lt.2) then
            status = 1
          else
            read (word(1),*,iostat=status) emin(n+1)
            if (status.eq.0) then
              read (word(2),*,iostat=status) cnts(n+1)
              if (status.eq.0) then
                n = n + 1
                emax(n)=emin(n)+0.05
                emin(n)=emin(n)-0.05
              endif
            endif
          endif
        endif
      enddo
      close(unit)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_spec_table_xspechis (file,unit,emin,emax,cnts,n)
      implicit none
      character*(*) file
      integer unit, n
      real emin(*),emax(*),cnts(*)
      character*1000 line
      character*80 word(100)
      integer nwords
      integer status
      integer i,k
      character*80 filem

      filem = file
      n = 0
      status = 0
      line = '----'
      do while (status.eq.0.and.
     ~    line(1:24).ne.' ***** Package \"spectrum' .and.
     ~    line(1:22).ne.' ***** Package \"photon')
        read (unit,'(a)',iostat=status) line
        if (status.ne.0) then
          call exiterror 
     ~        ('Package spectrum not found in the XSPEC his file: '//filem)
        endif
      enddo

      read (unit,'(a)') line
      i = index(line,'=')
      read (line(i+1:),*) n
      read (unit,*)
      read (unit,*)
      do i=1,n
        read (unit,'(a)') line
        k = index(line,':')
        if (k.gt.0) line(k:k)=' '
        call splitwords (line,word,nwords)
        read (word(2),*) emin(i)
        read (word(4),*) emax(i)
        read (word(5),*) cnts(i)
        cnts(i)=cnts(i)*(emax(i)-emin(i))
      enddo

      return
      end


