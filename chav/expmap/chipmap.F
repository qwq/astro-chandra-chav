#include <zhtools.h>

      implicit none
      integer nx,ny             ! size of the output det map
      integer bin               ! binning factor
      integer ichip(10), nchips ! which chips to include to the map
      real chipvals(10), defval
      integer ich
      character*200 outfile, refchip, refspec
      logical defined

      character*80 fpsys
      character*200 aimpoint
      character*80 arg, parname

c fitsio variables
      integer unitimg
      
c Energy grid
      integer nemax, ne
      parameter (nemax=40000)
      real ehi(nemax),elo(nemax),cnts(nemax), emin,emax

c
      integer i
      logical yespar, yes
      logical vignetting, qe
      logical useroll
      real roll_average
      real pixsize

      ZHDECL(real,pmask)
      ZHDECL(real,pwork)

      logical usesubarray
      integer subarray_firstrow
      integer subarray_nrows

      logical out_in_chip

      logical correct_cont, correct_cont_spatial
      
*--------------------------------------------------------------------



c 0) Read parameters
      call get_cl_par ('o',outfile)
      if (.not.defined(outfile)) call usage

c     
      call get_cl_par ('chips',arg)
      if (.not.defined(arg)) call usage
      call form_chip_list (arg,nchips,ichip)
      do i=1,nchips
        print*,'Chip #',i,'   is CCD#',ichip(i)
      enddo

      out_in_chip = yespar('inchip')
        
      correct_cont = .false.
      correct_cont_spatial = .false.
      call get_cl_par ('correct_acis_contamination',arg)
      if (yes(arg)) then
        correct_cont = .true.
        correct_cont_spatial = .false.
      endif
      if (arg.eq.'spatial') then
        correct_cont=.false.
        correct_cont_spatial = .true.
      endif
      if (correct_cont) print *,'OBF_CONTAMINATION+CORRECTION = ON'
      if (correct_cont_spatial) then
        print *,'OBF_CONTAMINATION+CORRECTION = SPATIAL'
      endif

c Read binning factor
      if (out_in_chip) then
        nx=1024
        ny=1024
        bin = 1
      else
        unitimg=0
        call read_bin_factor (bin,unitimg,pixsize)
      
        nx = 8192/bin
        ny = 8192/bin
        if (nx*bin.lt.8192) nx = nx + 1
        if (ny*bin.lt.8192) ny = ny + 1
      endif
      

c  Read aimpoint and fpsys
      useroll = yespar ('useroll')

      usesubarray = .true.
      call get_cl_par ('nosubarray',arg)
      usesubarray = .not. defined(arg)
      if (usesubarray) then
        call get_cl_par ('nosubarrays',arg)
        usesubarray = .not. defined(arg)
      endif
      call read_pixlib_pars_work (aimpoint, fpsys, useroll, roll_average,
     ~    usesubarray, subarray_firstrow, subarray_nrows)
      
c 1) Ini pixlib
      call ini_pixlib (aimpoint,fpsys)

c 2) Setup chipvalues

      vignetting = .false.
      qe = .false.
c    2a) Read energy grid file if present
      call get_cl_par ('refspec',refspec)
      if (defined(refspec)) then
        call read_spec_table (refspec,elo,ehi,cnts,ne)
*        print*,'Generate mask for the energy grid:'
*        print*,'     Emin       Emax       Counts'
*        do i=1,ne
*          print*,elo(i),ehi(i),cnts(i)
*        enddo
*        print*,'-------------------------------------'
        call get_cl_par ('emin',arg)
        if (.not.defined(arg)) call exiterror ('emin=?')
        read (arg,*) emin
        call get_cl_par ('emax',arg)
        if (.not.defined(arg)) call exiterror ('emax=?')
        read (arg,*) emax
        vignetting = .true.
        qe = .true.
      endif

      call get_cl_par ('nomirror',arg)
      if (defined(arg)) vignetting = .false.

      call get_cl_par ('noqe',arg)
      if (defined(arg)) qe = .false.

c    2b) Find the reference chip. I3 by default
      if (qe) then
        call get_cl_par ('refccd',refchip)
        if (.not.defined(refchip)) call get_cl_par ('refchip',refchip)
        if (defined(refchip).and..not.defined(refspec)) then
          call exiterror ('If you set refchip, you should set refspec')
        endif
        if (.not.defined(refchip)) refchip = 'I3'
      endif

c    2c) If the users wants chip numbers map, set it up
      call get_cl_par ('maskval',arg)
      if (arg.eq.'chipid'.or.arg.eq.'ccdid'.or.arg.eq.'ccd') then
        defval = -1
        do ich=1,nchips
          chipvals(ich)=ichip(ich)
        enddo

      else
        defval = 0
c If the refspec was set, set up relative chip norms
        if (defined(refspec)) then
          if (qe) then
            if (vignetting) then
              call setup_chip_norms (nchips,ichip,chipvals,refchip,
     ~            elo,ehi,cnts,ne,emin,emax)
            else
              call setup_chip_norms_qe (nchips,ichip,chipvals,refchip,
     ~            elo,ehi,cnts,ne,emin,emax)
            endif
          else
            do ich=1,nchips
              chipvals(ich)=1
              write (parname,'(''chipnorm'',i1)') ichip(ich)
              call get_cl_par (parname,arg)
              if (defined(arg)) read (arg,*) chipvals(ich)
            enddo
          endif
        else                    ! Use norm of 1 for all chips or chipnorm pars
          do ich=1,nchips
            chipvals(ich)=1
            write (parname,'(''chipnorm'',i1)') ichip(ich)
            call get_cl_par (parname,arg)
            if (defined(arg)) read (arg,*) chipvals(ich)
          enddo
        endif
      endif

      print*,'Chipmask values: '
      do i=1,nchips
        print*,'  CCD#',ichip(i),' -> ',chipvals(i)
      enddo
      print*,'-------------------------------------'
      
c Allocate memory
      ZHMALLOC(pmask,nx*ny,'e')
      ZHMALLOC(pwork,nx*ny,'e')
        
c 3) Call makemask
      call make_chip_mask_0 (nx,ny,bin,nchips,ichip,chipvals,defval,
     ~    outfile, aimpoint, fpsys, vignetting, elo,ehi,cnts,ne, emin, emax,
     ~    useroll, roll_average, pixsize,ZHVAL(pmask),ZHVAL(pwork),
     ~    usesubarray, subarray_firstrow, subarray_nrows, out_in_chip,
     ~    correct_cont, correct_cont_spatial)
      
      call exit(0)
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
      subroutine make_chip_mask_0 (nx,ny,bin,nchips,ichip,chipvals,defval,
     ~    outfile, aimpoint, fpsys, vignetting, elo, ehi,cnts,ne, emin, emax,
     ~    useroll,  roll, pixsize,mask,work,
     ~    usesubarray, subarray_firstrow, subarray_nrows, out_in_chip,
     ~    correct_cont, correct_cont_spatial)
      implicit none
c
c Allocates array and calls make_chip_mask
c
c
      integer nx,ny, bin
      integer nchips, ichip(nchips)
      real chipvals(nchips), defval
      character*(*) outfile, aimpoint, fpsys
      logical vignetting, useroll
      integer ne
      real elo(ne), ehi(ne), cnts(ne), emin,emax
      real roll, pixsize
      
      real mask(nx*ny)
      real work(nx*ny)

      logical usesubarray
      integer subarray_firstrow, subarray_nrows
      logical out_in_chip
      logical correct_cont, correct_cont_spatial

      integer unitout, status, bitpix, naxis, naxes(10), pcount, gcount
      character*80 comments(100)
      integer i
      real refmaskx, refmasky

      real eaverage
      logical corrcti, corrqeu
      character*80 arg
      logical defined, no
      logical qe

      qe = .true.
      call get_cl_par ('noqe',arg)
      if (defined(arg)) then
        qe = .false.
      endif
      
      if (qe) then
        call get_cl_par ('correct_qe_cti',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL CORRECT FOR THE CTI DROP IN QE'
          corrcti=.true.
        else
          corrcti=.false.
        endif
        
        call get_cl_par ('correct_qeu',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL APPLY THE QEU MAP'
          corrqeu=.true.
        else
          corrqeu=.false.
        endif
      else
        corrcti = .false.
        corrqeu = .false.
      endif
      

      if (corrcti.or.corrqeu) then
        call calc_corr_cti_eaverage (emin,emax,elo,ehi,cnts,ne,eaverage)
      endif
      
      call make_chip_mask (mask,nx,ny,bin,nchips,ichip,chipvals,defval,corrcti
     ~    ,corrqeu,eaverage,work,
     ~    usesubarray, subarray_firstrow, subarray_nrows, out_in_chip,
     ~    correct_cont, correct_cont_spatial)

c Multiply by vignetting if required
      if (vignetting) then
        call mult_chip_mask_vign (mask,nx,ny,bin, elo, ehi,cnts,ne, emin, emax
     ~      )
      endif

      if (.not.out_in_chip) then
        if (useroll) then
          print*,' Rotate chip mask by ',roll,' degrees'
          call rotate_img_main (mask,nx,ny,nx/2.0,ny/2.0,-roll,0.0,3)
        endif
      endif

c Write results
      status = 0
      call ftgiou (unitout,status)
      if (status.ne.0) call exit_fitsio (outfile,status)
      call unlink(outfile)
      call ftinit(unitout,outfile,0,status)
      bitpix=-32
      naxis=2
      naxes(1)=nx
      naxes(2)=ny
      pcount = 0
      gcount = 0
      call ftphpr(unitout,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .false.,status)
      call ftpkyj (unitout,'BINFACT',bin,'Bin factor in DET coordinates',
     ~    status)
      do i=1,nchips
        comments(i)='Chips the mask is made for'
      enddo
      call ftpknj (unitout,'CHIP',1,nchips,ichip,comments,status)
      
      if (.not.out_in_chip) then
        call ftpkys (unitout,'AIMPOINT',aimpoint,'Aim point',status)
        call ftpkys (unitout,'FPSYS',fpsys,'CXC FP coordinate system',status)
        if (useroll) then
          call ftpkye (unitout,'ROLL_AV',roll,5
     ~        ,'Roll angle for map generation',status)
        endif

c  We assume that the chip mask is always made for the entire image
c  therefor the reference point is in DETX,DETY = (4096.5,4096.5)
        refmaskx = (4096.5-0.5)/bin+0.5-0.5/bin
        refmasky = (4096.5-0.5)/bin+0.5-0.5/bin
*      call ftpkyf (unitout,'ROTREF_X',refmaskx,5,
*     ~    'X-reference point for mask rotation',status)
*      call ftpkyf (unitout,'ROTREF_Y',refmasky,5,
*     ~    'Y-reference point for mask rotation',status)
        call ftpkyf (unitout,'CRPIX1',refmaskx,5,
     ~      'X-reference point for mask rotation',status)
        call ftpkyf (unitout,'CRPIX2',refmasky,5,
     ~      'Y-reference point for mask rotation',status)
        
        call ftpkye (unitout,'CDELT1',-pixsize,10,'mask pixel size (degrees)',
     ~      status)
        call ftpkye (unitout,'CDELT2',pixsize,10,'mask pixel size (degrees)',
     ~      status)
      endif

      call ftp2de (unitout,0,nx,nx,ny,mask,status)
      call ftclos (unitout,status)
      if (status.ne.0) call exit_fitsio (outfile,status)
 
      

      return
      end


      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine usage
      write (0,*) 
     ~    'Usage: chipmap o=outfile chips=chiplist bin=bin -or- refimg=img'
      call exit(1)
      end

