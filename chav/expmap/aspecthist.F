#include <zhtools.h>

      program aspecthist
      implicit none
c
c Creates aspect histogram file. If the aspect file is not GTI-filtered 
c (as is indicated by its name), try to read -gtifile parameter
c
      character*1000 aspfile
      character*200 aspext, gtifile, outfile
      character*80 asptype

      logical defined
      
      integer status
      integer unitasp
      character*80 comment
      character*1000 filetype,infile,tempfile,extspec,rowfilter,
     ~    binspec,colspec, name

      logical delete_tempfile

      integer nasp

      integer getpid, pid

      ZHDECL(real,pdx)
      ZHDECL(real,pdy)
      ZHDECL(real,pdroll)
      ZHDECL(real,pstf_y)
      ZHDECL(real,pstf_z)

*--------------------------------------------------------------------------
      call get_cl_par ('aspfile',aspfile)
      if (.not.defined (aspfile)) call exiterror ('-aspfile is undefined')
      call get_cl_par ('o',outfile)
      if (.not.defined (outfile)) call exiterror ('-o is undefined')
      
c 1) Parse file name into components
      status = 0
      call FTIURL (aspfile,filetype, infile, tempfile, extspec, rowfilter,
     ~    binspec, colspec, status)
      if (status.ne.0) call exit_fitsio(aspfile,status)
      
c  1a) If tempfile is unset, make up a temp. name
      if (tempfile.eq.' ') then
        delete_tempfile = .true.
        pid = getpid()
        write (tempfile,'(''/tmp/aspfile'',i6.6)') pid
      else
        delete_tempfile = .false.
      endif

c  1b) Determine whether rowfilter contains gtifilter
      if (index(rowfilter,'gtifilter').eq.0) then ! no gti filter
        call get_cl_par ('gtifile',gtifile)
        if (.not.defined(gtifile)) then
          call putwarning ('aspecthist',
     ~        'no GTI either as a part of aspfile or as -gtifile'
     ~        )
          call putwarning ('aspecthist',
     ~        ' create aspect histogram without GTI filter')
        else
                                ! Append gtifile to rowfilter
          if (rowfilter .ne. ' ')  call strcat (rowfilter,' &&')
          call strcat (rowfilter,'gtifilter( "')
          call strcat (rowfilter,gtifile)
          call strcat (rowfilter,'" )')
        endif
      endif

c  1c) Find extension spec
      if (extspec.eq.' ') then
        call get_cl_par ('aspectname',aspext)
        if (.not.defined(aspext)) then
          aspext = 'ASPOFF'
        endif
        extspec = aspext
      else
        aspext = extspec
      endif


c   1d) Create name. We will leave out all col and bin filters
      name = filetype
      call strcat (name,infile)
      call strcat (name,'(')
      call strcat (name,tempfile)
      call strcat (name,')')
      call strcat (name,'[')
      call strcat (name,extspec)
      call strcat (name,']')
      call strcat (name,'[')
      call strcat (name,rowfilter)
      call strcat (name,']')

c 2) Open aspect file with ftnopn so that we are at the aspect extension
      status = 0
      call ftgiou (unitasp,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftnopn (unitasp,name,0,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      
c 3) Determine the data type: ASPOFF (from the aspect camera) or ASPSOL (from
c    gyros)
      call ftgkys (unitasp,'HDUCLAS2',asptype,comment,status)
      if (status.ne.0) then
        call putwarning ('aspecthist','HDUCLAS2 keyword not found')
        call putwarning ('aspecthist','assume aspect data is ASPOFF')
        asptype = 'ASPOFF'
        status = 0
      endif

c
c 3a) Determine the number of data points
      call ftgkyj (unitasp,'NAXIS2',nasp,comment,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      print*,' ...',nasp,' aspect data points'

c
c Allocate memory
      ZHMALLOC(pdx,nasp,'e')
      ZHMALLOC(pdy,nasp,'e')
      ZHMALLOC(pdroll,nasp,'e')
      ZHMALLOC(pstf_y,nasp,'e')
      ZHMALLOC(pstf_z,nasp,'e')

      if (asptype.eq.'ASPOFF') then
        call make_aspect_histo_aspoff (aspfile,unitasp, outfile, nasp,
     ~      ZHVAL(pdx),ZHVAL(pdy),ZHVAL(pdroll),ZHVAL(pstf_y),ZHVAL(pstf_z))
      endif

      ZHFREE(ZHVAL(pdx))
      ZHFREE(ZHVAL(pdy))
      ZHFREE(ZHVAL(pdroll))
      ZHFREE(ZHVAL(pstf_y))
      ZHFREE(ZHVAL(pstf_z))

c  Final cleanup
      if (delete_tempfile) call unlink (tempfile)

      call exit(0)
      
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine make_aspect_histo_aspoff (aspfile,unitasp,outfile,nasp,
     ~    dx,dy,droll,stf_y,stf_z)
      implicit none
c
c Does real work in creating the aspect histogram from the ASPOFF data
c
c
c
      integer unitasp,nasp
      character*(*) outfile, aspfile
      real dx(nasp), dy(nasp), droll(nasp), stf_y(nasp), stf_z(nasp)
      
      integer colnum
      logical anyf
      integer i,j,ia,nrollbin,k
      real rollbin,offsetbin,offsetmax
      real roll1
      integer nx,ny, ngood, nbad, na
      real dxmin,dxmax,dymin,dymax

      real crval1,crval2,crpix1,crpix2,cdelt1,cdelt2

      real floor
      external floor

      integer nasphistmax
      parameter (nasphistmax=512*512)
      integer asphist(nasphistmax)
      double precision rollav
      integer npoints
      
* FITSIO variables
      integer status, unitout
      integer oldext, hdutype
      integer tfields
      character*80 tform(10), ttype(10), tunit(10)
      integer naxis, naxes(10)
      character*80 card

      character*200 gtifile
      logical defined
      real exposure
      character*200 buff
      character*80 comment

      real pix2mm
      parameter (pix2mm=41.6892) ! assuming 1 pixel is 23.987 mu
      double precision sinroll, cosroll, deg2rad
      parameter (deg2rad = 3.14159265358979d0/180.0)
      double precision rollnom

      status = 0

c  0) Nominal roll
      call ftgkyd (unitasp,'ROLL_NOM',rollnom,comment,status)
      buff = aspfile
      if (status.ne.0) call exit_fitsio ('ROLL_NOM '//buff,status)
      

c  1) Read roll and offset columns
      call ftgcno (unitasp,.false.,'ROLL_OFFSETS',colnum,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftgcve (unitasp,colnum,1,1,nasp,0.0,droll,anyf,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      if (anyf) then
        buff = aspfile
        call putwarning ('make_aspect_histo_aspoff',
     ~      'undefined rolls in '//buff)
      endif

      call ftgcno (unitasp,.false.,'X_OFFSETS',colnum,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftgcve (unitasp,colnum,1,1,nasp,0.0,dx,anyf,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      if (anyf) then
        buff = aspfile
        call putwarning ('make_aspect_histo_aspoff',
     ~      'undefined xoffs in '//buff)
      endif

      call ftgcno (unitasp,.false.,'Y_OFFSETS',colnum,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftgcve (unitasp,colnum,1,1,nasp,0.0,dy,anyf,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      if (anyf) then
        buff = aspfile
        call putwarning ('make_aspect_histo_aspoff',
     ~      'undefined yoffs in '//buff)
      endif

      call ftgcno (unitasp,.false.,'STF_Y',colnum,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftgcve (unitasp,colnum,1,1,nasp,0.0,stf_y,anyf,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      if (anyf) then
        buff = aspfile
        call putwarning ('make_aspect_histo_aspoff',
     ~      'undefined stf_ys in '//buff)
      endif

      call ftgcno (unitasp,.false.,'STF_Z',colnum,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftgcve (unitasp,colnum,1,1,nasp,0.0,stf_z,anyf,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      if (anyf) then
        buff = aspfile
        call putwarning ('make_aspect_histo_aspoff',
     ~      'undefined stf_zs in '//buff)
      endif

c   1a) Remove bins with too big an offset
      call get_pv_default ('offsetmax',offsetmax,3.0,'e') 
                                ! This reads comm. line argument offsetbin; 
                                ! if not set, offsetmax=3 arcmin
      ngood = 0
      nbad = 0
      i = 0
      do ia=1,nasp

        sinroll = sin(-(droll(ia)+rollnom)*deg2rad)
        cosroll = cos(-(droll(ia)+rollnom)*deg2rad)

        dx(ia)=dx(ia) + ( stf_y(ia)*cosroll + stf_z(ia)*sinroll ) * pix2mm
        dy(ia)=dy(ia) + ( stf_y(ia)*sinroll - stf_z(ia)*cosroll ) * pix2mm

        if (sqrt(dx(ia)**2+dy(ia)**2)*0.5/60.0 .gt. offsetmax ) then
          write (0,*) 'ASPOFF: Offsets are outside ',offsetmax,'arcmin:',
     ~        dx(ia),dy(ia)
          nbad = nbad + 1
        else
          ngood = ngood + 1
          i = i + 1
          droll(i)=droll(ia)
          dx(i)=dx(ia)
          dy(i)=dy(ia)
        endif
      enddo
      na = i
      if (ngood.lt.0.9*(ngood+nbad)) then
        call putwarning ('ASPOFF'
     ~      ,'more than 90% of the exposure is discarded due to big offsets')
      endif

C FROM NOW ON WE USE NA AS THE NUMBER OF ASPECT POINTS

c   2) Transform roll into -180:180 range
      do ia=1,na
        do while (droll(ia).lt.-180.0)
          droll(ia)=droll(ia)+360.0
        enddo
        do while (droll(ia).gt.180.0)
          droll(ia)=droll(ia)-360.0
        enddo
      enddo

c   3) Sort by roll
      call sort3__ (na,droll,dx,dy) ! sort3__ sorts droll and makes the 
                                ! corresponding re-arrangments in dx and dy

c   4) Determine the roll bin size and find the number of roll bins
      call get_pv_default ('rollbin',rollbin,0.02,'e') !This reads comm. line
                                ! argument rollbin; if not set, rollbin=0.02
      
      nrollbin = 1
      roll1 = droll(1)
      do ia=1,na
        if (droll(ia).gt.roll1+rollbin) then
          nrollbin = nrollbin + 1
          roll1 = droll(ia)
        endif
      enddo

      print*,'The number of roll bins is ',nrollbin

c   5) Find the offset histogram image size
      call get_pv_default ('offsetbin',offsetbin,1.0,'e') 
                                ! This reads comm. line argument offsetbin; 
                                ! if not set, offsetbin=1
      print*,'Offset bin size is',offsetbin,'pix'
      dxmin = 1e10
      dxmax = -1e10
      dymin = 1e10
      dymax = -1e10
      do ia=1,na
        dxmin = min(dx(ia),dxmin)
        dxmax = max(dx(ia),dxmax)
        dymin = min(dy(ia),dymin)
        dymax = max(dy(ia),dymax)
      enddo

      print*,dxmin,dxmax,dymin,dymax
      nx = floor(dxmax/offsetbin)-floor(dxmin/offsetbin)+2
      ny = floor(dymax/offsetbin)-floor(dymin/offsetbin)+2
      print*,'Aspect histogram fits a ',nx,' by ',ny,' image'
      if (nx*ny.gt.nasphistmax) then
        call exiterror ('aspect hist. is too big (I allow for up to 512x512)')
      endif

c   6) Create the aspect hist file
      status = 0
      call ftgiou (unitout,status)
      if (status.ne.0) call exit_fitsio (outfile,status)
      call ftinit (unitout,outfile,0,status)
      if (status.ne.0) call exit_fitsio (outfile,status)

c    6a) Copy primary header from the aspect file
*       remember the extension we were at in the aspect file
      call ftghdn (unitasp,oldext,status)
*       move to the prime header in the aspect file
      call ftmahd (unitasp,1,hdutype,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
*       Copy extension
      call ftcopy (unitasp,unitout,0,status)
      if (status.ne.0) call exit_fitsio (outfile,status)
*       and finally move to the old extension in the aspect file
      call ftmahd (unitasp,oldext,hdutype,status)

c    6b) Create the aspect histogram extension
      call ftcrhd (unitout,status)
      tfields = 2
      ttype(1) = 'ROLLAV'
      ttype(2) = 'ASPHIST'
      tform(1) = '1E'
      write (tform(2),*) nx*ny,'J'
      call rmblanks(tform(2))
      tunit(1) = 'degrees'
      tunit(2) = 'npoints'
      
      call ftphbn (unitout,0,tfields,ttype,tform,tunit,'ASPECT_HISTOGRAM',
     ~    0,status)

c    6c) Define structure of the asphist image
      naxis = 2
      naxes(1)=nx
      naxes(2)=ny
      call ftptdm (unitout,2,naxis,naxes,status)

c Explanation to the math in the code below:
      if (nx.gt.1) then
        crpix1 = nint(nx/2.0)
        cdelt1 = offsetbin
        crval1 = 0.5*(dxmin+dxmax)
      else
        crpix1 = 1
        cdelt1 = offsetbin
        crval1 = 0.5*(dxmin+dxmax)
      endif
        
      if (ny.gt.1) then
        crpix2 = nint(ny/2.0)
        cdelt2 = offsetbin
***        crval2 = offsetbin*(ny/2-1+floor(dymin/offsetbin))+0.5*offsetbin
        crval2 = 0.5*(dymin+dymax)
      else
        crpix2 = 1
        cdelt2 = offsetbin
        crval2 = 0.5*(dymin+dymax)
      endif

c     Put these keys into the table
      call ftpkys (unitout,'2CTYP1','XOFF-LIN','X-offset',status)
      call ftpkys (unitout,'2CUNI1','pixels','',status)
      call ftpkye (unitout,'2CRPX1',crpix1,5,'',status)
      call ftpkye (unitout,'2CRVL1',crval1,5,'',status)
      call ftpkye (unitout,'2CDLT1',cdelt1,5,'',status)
      call ftpkys (unitout,'2CTYP2','YOFF-LIN','Y-offset',status)
      call ftpkys (unitout,'2CUNI2','pixels','',status)
      call ftpkye (unitout,'2CRPX2',crpix2,5,'',status)
      call ftpkye (unitout,'2CRVL2',crval2,5,'',status)
      call ftpkye (unitout,'2CDLT2',cdelt2,5,'',status)
      if (status.ne.0) call exit_fitsio (outfile,status)

c    6d) copy nominal roll and pointing keywords from the aspect file
      call ftgcrd (unitasp,'ROLL_NOM',card,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftprec (unitout,card,status)
      if (status.ne.0) call exit_fitsio (outfile,status)

      call ftgcrd (unitasp,'RA_NOM',card,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftprec (unitout,card,status)
      if (status.ne.0) call exit_fitsio (outfile,status)

      call ftgcrd (unitasp,'DEC_NOM',card,status)
      if (status.ne.0) call exit_fitsio (aspfile,status)
      call ftprec (unitout,card,status)
      if (status.ne.0) call exit_fitsio (outfile,status)


c     Go over the aspect records, make images, and write them to the table
      roll1 = -3600.0
      nrollbin = 0
      do ia = 1, na
        if (droll(ia).gt.roll1+rollbin) then
          k = 0
          do i=1,nx*ny
            k = k + asphist(i)
          enddo
          if (nrollbin.gt.0) then
            rollav = rollav/npoints
            call ftpcle (unitout,1,nrollbin,1,1,sngl(rollav),status)
                                ! rollav is double precision!
            call ftpclj (unitout,2,nrollbin,1,nx*ny,asphist,status)
            if (status.ne.0) call exit_fitsio (outfile,status)
          endif
          roll1 = droll(ia)
          nrollbin = nrollbin + 1
          do i = 1,nx*ny
            asphist(i)=0
          enddo
          rollav = 0
          npoints = 0
        endif
        
c 
        npoints = npoints + 1
        rollav = rollav + droll(ia)
        i = nint(crpix1+(dx(ia)-crval1)/cdelt1)
***        j = floor(crpix2+(dy(ia)-crval2)/cdelt2+0.5)
        j = nint(crpix2+(dy(ia)-crval2)/cdelt2)
        k = i+(j-1)*nx
        if (k.lt.1.or.k.gt.nx*ny) then
          write (0,*) dx(ia),dy(ia),dxmin,dxmax,dymin,dymax
          write (0,*) crval1,crpix1,cdelt1,'  ',crval2,crpix2,cdelt2
          write (0,*) i,j
          write (0,*) 'wrong pixel calculations in ASPOFF'
          call exit(0)
        endif
        asphist(k)=asphist(k)+1
      enddo
      
      if (npoints.gt.0) then
        rollav = rollav/npoints
        call ftpcle (unitout,1,nrollbin,1,1,sngl(rollav),status)
                                ! rollav is double precision!
        call ftpclj (unitout,2,nrollbin,1,nx*ny,asphist,status)
        if (status.ne.0) call exit_fitsio (outfile,status)
      endif



c  8) If gtifile is set, read it, and calculate the total exposure
      call get_cl_par ('gtifile',gtifile)
      if (defined(gtifile)) then
        call read_gti_exp (gtifile, exposure)
      else
        exposure = -1.0
      endif

c   8b) write exposure keyword to the asphist file
      call ftpkyf (unitout,'EXPOSURE',exposure,2,
     ~    '[s] Total exposure in the GTI file',status)

      call ftclos (unitout,status)
      if (status.ne.0) call exit_fitsio (outfile,status)

      return
      end
