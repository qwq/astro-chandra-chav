c The idea of this program is to provide normalizations for the chip mask
c that are independent of vignetting but include chip QEs so that the
c normalization of the reference chip is 1.
c
c For that, we need a model spectrum. This model spectrum can be produced in
c XSPEC and should include all mirror area variations. Since the usual ARF
c files are a product of mirror area and CCD QE, we first deconvolve the
c model spectrum  (i.e., divide it by the refccd QE), and then predict counts
c in each chip (i.e. multiply the deconvolved spectrum by QE in each chip)
c
c
c
c NOTE THAT THESE PROGRAMS ARE ALMOST USELESS BECAUSE ONE USUALLY WANTS AN
c INSTRUMENT MAP THAT INCLUDES MIRROR AS WELL AS QEs
c

      subroutine setup_chip_norms_qe (
     ~    nchips,ichip,chipvals,refchip,
     ~    elo,ehi,cnts,ne,emin,emax)
      implicit none
c
c Make relative chip normalizations for the given source spectrum
c
c For each chip, we predict the number of counts in the (emin,emax) band
c given the source spectrum
c
c It is assumed the the spectrum table is generated for the reference chip
c ('refchip')
c
      integer nchips
      integer ichip(nchips)
      real chipvals (nchips)
      character*(*) refchip
      integer ne
      real elo(ne), ehi(ne), cnts(ne)
      real emin,emax

      real spec(40000)
      character*80 chipname
      character*200 qefile
      integer irefchip
      
      real e0(10000,0:9), qe(10000,0:9)
      integer nqe(0:9)

      integer status, ich
      logical defined
      real countsref, counts(0:10)
      character*80 buff
      integer qexpand
      
c Read ccd_qe for the refchip
      call chip_name_to_idname (refchip,chipname)
      read (chipname,*,iostat=status) irefchip
      buff = refchip
      if (status.ne.0) call exiterror ('Cannot parse chip name: '//buff)
      
      call rp_q_expand_vars (qexpand)
      call rp_set_expand_vars (1)
      call get_cl_par ('ccd_qe',qefile)
      if (.not.defined(qefile)) then
        call chip_id_to_normname (irefchip,chipname)
        call strlow(chipname)
        call get_cl_par ('ccd_qe_'//chipname,qefile)
        if (.not.defined(qefile)) then
          call exiterror ('ccd_qe is not set for '//chipname)
        endif
      endif
      
      call read_ccd_qe (qefile,nqe(irefchip),e0(1,irefchip),qe(1,irefchip)
     ~    ,irefchip)
      
c Read ccd_qe for each of the chips
      do ich = 1, nchips
        call get_cl_par ('ccd_qe',qefile)
        if (.not.defined(qefile)) then
          call chip_id_to_normname (ichip(ich),chipname)
          call strlow(chipname)
          call get_cl_par ('ccd_qe_'//chipname,qefile)
          if (.not.defined(qefile)) then
            call exiterror ('ccd_qe is not set for '//chipname)
          endif
        endif
        call read_ccd_qe 
     ~      (qefile,nqe(ichip(ich)),e0(1,ichip(ich)),qe(1,ichip(ich))
     ~      ,ichip(ich))
      enddo
      call rp_set_expand_vars (qexpand)
      
c Correct the reference spectrum for the refchip QE
      call setup_chip_norms_qe_corcounts (elo,ehi,cnts,ne, emin,emax, 
     ~    e0(1,irefchip),qe(1,irefchip),nqe(irefchip), spec)
      
c Predict reference counts and counts in each chip
      call setup_chip_norms_qe_modcounts (elo,ehi,spec,ne, emin,emax, 
     ~    e0(1,irefchip),qe(1,irefchip),nqe(irefchip), countsref)
      
      if (countsref.le.0.0) then
        call exiterror ('Zero reference counts for the ref. spectrum')
      endif
      
      do ich=1,nchips
        call setup_chip_norms_qe_modcounts (elo,ehi,spec,ne, emin,emax, 
     ~      e0(1,ichip(ich)),qe(1,ichip(ich)),nqe(ichip(ich)), 
     ~      counts(ichip(ich)))
        if (counts(ichip(ich)).le.0.0) then
          write (0,*) 'ERROR: Zero counts for the ref. spectrum in chip',
     ~        ichip(ich)
          call exit(1)
        endif
      enddo

c Calculate relative normalizations
      do ich=1,nchips
        chipvals(ich)=counts(ichip(ich))/countsref
      enddo
      
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine setup_chip_norms_qe_modcounts (elo,ehi,spec,ne, emin,emax, 
     ~    e0,qe,nqe, counts)
      implicit none
c
c Multiply model spectrum by QE and calculate the number of counts
c
      integer ne
      real elo(ne), ehi(ne), spec(ne)
      real emin,emax
      integer nqe
      real e0(nqe), qe(nqe)
      real counts
      real we, qeav
      integer ie

      counts=0
      do ie=1,ne
        we = 0.5*(elo(ie)+ehi(ie))
        if (we.ge.emin.and.we.le.emax) then
          call setup_chip_norms_qe_avqe (e0,qe,nqe, elo(ie), ehi(ie), qeav)
          counts=counts+spec(ie)*qeav
        endif
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine setup_chip_norms_qe_corcounts (elo,ehi,cnts,ne, emin,emax, 
     ~     e0,qe,nqe, spec)
      implicit none
c
c Divide reference spectrum by QE and save result to spec(*)
c
c
      integer ne
      real elo(ne), ehi(ne), cnts(ne)
      real emin,emax
      integer nqe
      real e0(nqe), qe(nqe)
      real spec(*)
      real we, qeav
      integer ie
      
      do ie=1,ne
        we = 0.5*(elo(ie)+ehi(ie))
        if (we.ge.emin.and.we.le.emax) then
          call setup_chip_norms_qe_avqe (e0,qe,nqe, elo(ie), ehi(ie), qeav)
          spec(ie)=cnts(ie)/qeav
         else
           spec(ie)=cnts(ie)
         endif
       enddo

       return
       end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine setup_chip_norms_qe_avqe (e0,qe,nqe, elo, ehi, qeav)
      implicit none
      integer nqe
      real e0(nqe), qe(nqe)
      real elo, ehi, qeav

      integer nqebin, inearqe, iqe
      real we

      nqebin = 0
      qeav = 0
      inearqe = 1
      we = 0.5*(elo+ehi)
      do iqe = 1,nqe

        if (iqe.gt.1) then
          if (abs(we-e0(iqe)).lt.abs(we-e0(inearqe))) then
            inearqe = iqe
          endif
        endif
        
        if (e0(iqe).ge.elo.and.e0(iqe).le.ehi)  then
          nqebin = nqebin + 1
          qeav = qeav + qe(iqe)
        endif

      enddo
      if (nqebin.gt.0) then     ! More then one qe bin falled inside our 
                                ! interval
        qeav = qeav/nqebin
      else                      ! Find nearest qe bin
        qeav = qe(inearqe)
      endif

      return
      end
