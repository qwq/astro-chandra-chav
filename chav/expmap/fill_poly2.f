      subroutine fill_poly2 (x,y,n,value,img,valimg,poscode,nx,ny,bin,
     ~    out_in_chip, ccd_id)
      implicit none
      integer n, nx, ny 
      real x(n), y(n), value, img(nx,ny), valimg(nx,ny)
      integer poscode(nx,ny)
      integer bin
      logical out_in_chip
      integer ccd_id

      real xmin,xmax, ymin, ymax
      integer ixmin,ixmax
      integer i,j, ix, iy1,iy2, iy
      real xp
      real y1seg(1000), y2seg(1000)
      
      integer nsegments, iseg

      integer fill_poly_pixelize, fill_poly_orig_to_binned
      real fill_poly_depixelize
      external fill_poly_pixelize, fill_poly_depixelize, 
     ~    fill_poly_orig_to_binned


      real chipx1,chipy1,chipx2,chipy2, detx,dety
      real chipx,chipy
      integer chipid, chipid1,chipid2
      real ccd_qe_corr, ccd_qe_qeu
      external ccd_qe_corr, ccd_qe_qeu

      real corrarf, corrarf_spatial
      external corrarf, corrarf_spatial

c -- find polygon boundaries
      xmin = x(1)
      ymin = y(1)
      xmax = x(1)
      ymax = y(1)
      do i=2,n
        xmin = min(xmin,x(i))
        xmax = max(xmax,x(i))
        ymin = min(ymin,y(i))
        ymax = max(ymax,y(i))
      enddo
      
c -- express xmin xmax in terms of unbinned pixels
      ixmin = fill_poly_pixelize (xmin) - 1
      ixmax = fill_poly_pixelize (xmax) + 1
      
c -- go one unbinned pixel at a time,
      do ix = ixmin,ixmax
        i = fill_poly_orig_to_binned (ix,bin)
        xp = fill_poly_depixelize (ix)

        if (i.ge.1.and.i.le.nx) then

c     -- find itersections of the vertical line at this X with polygon
c        boundaries,
          call fill_poly_find_segments (xp,ymin-1.0,ymax+1.0,x,y,n,
     ~        nsegments, y1seg, y2seg)
c      -- and draw these intersections
          do iseg = 1, nsegments
            iy1 = fill_poly_pixelize (y1seg(iseg))
            iy2 = fill_poly_pixelize (y2seg(iseg))

* I use the fact that a stright line on a chip transforms, with sufficient
* accuracy, to a straight line on the detx,dety. So instead of calculating
* chip coordinates in each point, I calculate them at the boundaries and then
* interpolate.
            detx = xp
            dety = fill_poly_depixelize (iy1)
            if (out_in_chip) then
              chipx1=detx
              chipy1=dety
              chipid1 = ccd_id
            else
              call det2chip_bin (detx,dety,chipx1,chipy1,chipid1,bin)
            endif
            dety = fill_poly_depixelize (iy2)
            if (out_in_chip) then
              chipx2=detx
              chipy2=dety
              chipid2 = ccd_id
            else
              call det2chip_bin (detx,dety,chipx2,chipy2,chipid2,bin)
            endif
            if (chipid1.ne.chipid2) then
              call exiterror
     ~            ('Problem with chipid in det2chip_bin in fill_poly')
            endif

            do iy = iy1,iy2
              j = fill_poly_orig_to_binned (iy,bin)
              
              if (j.ge.1.and.j.le.ny) then
                chipx  = chipx1+(iy-iy1)*(chipx2-chipx1)/(iy2-iy1)
                chipy  = chipy1+(iy-iy1)*(chipy2-chipy1)/(iy2-iy1)
                chipx  = min(1023.0,max(chipx,2.0))
                chipy  = min(1023.0,max(chipy,2.0))
                chipid = chipid1

                img(i,j) = img(i,j) + value/bin**2
                valimg(i,j)=value

                call chip_to_poscode(nint(chipx),nint(chipy),chipid,
     ~              poscode(i,j))
              endif
            enddo
          enddo
        endif
      enddo
        
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine fill_poly_find_segments (xx,ymin,ymax,x,y,n,nseg,y1seg,y2seg)
      implicit none
      real xx,ymin,ymax
      integer n
      real x(n), y(n)
      integer nseg
      real y1seg(*), y2seg(*)

c
c Find intersection of (xx,ymin)-(xx,ymax) with each vertex of the polygon
c then sort intersection points
c
c It is assumed that ymin,ymax are outside the polygon boundaries
c
c
      real yy(1000)
      integer k, i
      real ax,ay,bx,by, x1,x2

      
      k = 0                     ! k is the inetrsection counter
      do i=1,n
        ax=x(i)
        if (i.lt.n) then
          bx=x(i+1)
        else
          bx=x(1)
        endif
        
        if (ax.lt.bx) then
          x1 = ax
          x2 = bx
        else
          x1 = bx
          x2 = ax
        endif

        if (xx.gt.x1.and.xx.le.x2) then ! Must have an intersection
          
          ay=y(i)
          if (i.lt.n) then
            by=y(i+1)
          else
            by=y(1)
          endif
          
          if (ax.eq.bx) then    ! Side parallel to our line
            k = k + 1
            yy(k) = ay
            k = k + 1
            yy(k) = by
          else                  ! one intersection
            k=k+1
            yy(k)=ay+(xx-ax)*(by-ay)/(bx-ax)
          endif
          
        endif
      enddo

      if (k.eq.0) then
        nseg = 0
        return
      endif

      if ((k/2)*2.ne.k) then
        write (0,*) 
     ~      'Error: not even number of segs in fill_poly_find_segments'
        call exit(1)
      endif
      call sort (k,yy)
      
      nseg = k/2
      do i=1,nseg
        y1seg(i)=yy((i-1)*2+1)
        y2seg(i)=yy((i-1)*2+2)
      enddo
      return
      end

c%
c% Fitsio bins the $x,y$ images so that the center of pixel 1,1 is 1.5,1.5 in
c% real space.
c% 
c% This means the the centroid of pixel $i$ is $i+0.5$ and the pixel that
c% contains the point $x$ is $\mathrm{floor}(x)$.
c%
c% The transformation corresponding to blocking by factor $b$ can be found
c% from the condition that point 1.0 transforms to 1.0, and point $1.0+b$
c% transform to 2.0; this is easily solved to show that
c% $x \rightarrow (x-1)/b+1$.

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      integer function fill_poly_pixelize (x)
      implicit none
c
c Returns image pixel in which x will be located
c
      real x
      integer fill_poly_floor
      external fill_poly_floor

      fill_poly_pixelize = fill_poly_floor(x)
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      real function fill_poly_depixelize (i)
      implicit none
c
c Returns real value corresponding to the center of pixel i
c
      integer i
      fill_poly_depixelize = i+0.5
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      integer function fill_poly_orig_to_binned (iorig,bin)
c
c Binned pixel corresponding to unbinned pixel iorig
c

      implicit none
      integer iorig,bin
      real x
      real fill_poly_depixelize
      integer fill_poly_pixelize
      external fill_poly_depixelize, fill_poly_pixelize

      x = fill_poly_depixelize (iorig)
      x = (x-1.0)/bin+1.0
      fill_poly_orig_to_binned = fill_poly_pixelize(x)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      integer function fill_poly_eorig_to_binned (xorig,bin)
c
c Binned pixel corresponding to unbinned value xorig
c
      implicit none
      real xorig
      integer bin
      real x
      integer fill_poly_pixelize
      external fill_poly_pixelize

      x = (xorig-1.0)/bin+1.0
      fill_poly_eorig_to_binned = fill_poly_pixelize(x)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      integer function fill_poly_floor (x)
      implicit none
      real x

      if (x.lt.0.0) then
        fill_poly_floor=int(x)-1
      else
        fill_poly_floor=int(x)
      endif
      return
      end
