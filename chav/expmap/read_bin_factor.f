      subroutine read_bin_factor (bin,unitimg,pixsize)
      implicit none
      integer bin, unitimg
      real pixsize
c
c The program will attempt to open the image file if necessary and will leave
c it open.
c
      integer status
      character*200 arg, comment
      logical defined
      real cdelt1, cdelt2, cdelt0
      parameter (cdelt0=0.492/3600) ! approximate Chandra pixel size

c Read binning factor
c The user may set binning either with -bin parameter or as -img parameter
c pointing to a Chandra image with WCS
      
      call get_cl_par ('bin',arg)
      if (defined(arg)) then
        read (arg,*) bin
        pixsize = cdelt0*bin
        print*,'   Bin factor is ',bin, '   pixel size is ',pixsize,' deg'
      else
        call get_cl_par ('refimg',arg)
        if (.not.defined(arg)) then
          write (0,*) 'You have to set binning with either -bin parameter'
          write (0,*) 'our implicitly with -refimg ..'
          call exit(1)
        endif
        
c       open fits image and read cdelt1, cdelt2 keys
        status = 0
        call ftgiou (unitimg,status)
        if (status.ne.0) call exit_fitsio (arg,status)
        call ftnopn (unitimg,arg,0,status) 
        if (status.ne.0) call exit_fitsio (arg,status)
        call ftgkye (unitimg,'CDELT1',cdelt1,comment,status)
        if (status.ne.0) call exit_fitsio ('CDELT1, '//arg,status)
        call ftgkye (unitimg,'CDELT2',cdelt2,comment,status)
        if (status.ne.0) call exit_fitsio ('CDELT2, '//arg,status)
        
        bin = nint(0.5*(abs(cdelt1)+abs(cdelt2))/cdelt0)
        pixsize = 0.5*(abs(cdelt1)+abs(cdelt2))
        print*,'   Bin factor is ',bin, '   pixel size is ',pixsize,' deg'
      endif
      
      return
      end
