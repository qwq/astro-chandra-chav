      implicit none
      integer n
      parameter (n=512)
      real img(n,n)
      real a(n)
      integer i,j
      real angle

      do i=n/2-5,n/2+5
        do j=n/2-5,n/2+40
          img(i,j)=1
        enddo
      enddo
      img(256,256)=0
      call write_fits_image ('gov',img,n,n,'e','e')

      angle=100.0
      call rotate_img_main (img,n,n, n/2.0,n/2.0, angle, 1.0,3)

      call write_fits_image ('govrot',img,n,n,'e','e')
      end

          
