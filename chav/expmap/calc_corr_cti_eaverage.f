      subroutine calc_corr_cti_eaverage (emin,emax,elo,ehi,cnts,ne,eaverage)
      implicit none
      integer ne
      real elo(ne), ehi(ne), cnts(ne)
      real emin,emax
      real eaverage

      integer negridmax
      parameter (negridmax=11000)
      real emirror(negridmax), amirror(negridmax), spec(negridmax)
      integer nemirror
      real de
      parameter (de=0.02)       ! width of the energy channel for
                                ! calculation of the mirror area arrays
      real ee
      real hrma_onaxis_ea
      external hrma_onaxis_ea
      real cntonaxis, etot,corrtot
      integer ie
      logical found
      real ccd_qe_corr
      external ccd_qe_corr

c 1) Form onaxis area array and spectrum
      nemirror = 0
      cntonaxis = 0
c     do ee = emin, emax, de
      ee = emin
10    if (ee .le. emax ) then
        ee = ee + de
        nemirror = nemirror + 1
        emirror (nemirror) = ee
        amirror (nemirror) = hrma_onaxis_ea(ee)
        ie = 1
        found = .false.
        do while (.not.found .and. ie.le.ne)
          if ( ee.ge.elo(ie).and.ee.le.ehi(ie) ) then
            found = .true.
          else
            ie = ie + 1
          endif
        enddo
        if (found) then
          spec(nemirror) = amirror(nemirror)*de*cnts(ie)/(ehi(ie)-elo(ie))
        else
          spec(nemirror) = 0.0
        endif
        cntonaxis = cntonaxis + spec(nemirror)
      goto 10
      endif
c     enddo
      if (cntonaxis.le.0.0d0) then
        call exiterror ('Zero normalization for onaxis spectrum')
      endif

      corrtot=0
      etot = 0
      do ie=1,nemirror
        corrtot = corrtot + spec(ie)*ccd_qe_corr(emirror(ie),3,512.0,1000.0)
        etot = etot + emirror(ie)*spec(ie)*ccd_qe_corr(emirror(ie),3,512.0
     ~      ,1000.0)
      enddo
      eaverage = etot/corrtot

      print*,'AVERAGE ENERGY FOR CTI CORRECTION = ',eaverage
      return
      end
