      subroutine make_chip_mask (mask,nx,ny,bin,nchips,ichip,chipval,defval
     ~    ,corrcti,corrqeu,eaverage,work,
     ~    usesubarray, subarray_firstrow, subarray_nrows, out_in_chip,
     ~    correct_cont, correct_cont_spatial)
      implicit none
      integer nx,ny,bin,nchips,ichip(nchips)
      real mask(nx,ny), chipval(nchips), defval, work(nx,ny)
      integer chipid
      logical corrcti,corrqeu
      real eaverage
      logical usesubarray, out_in_chip, correct_cont, correct_cont_spatial
      integer subarray_firstrow, subarray_nrows

      integer nb
      parameter (nb = 8)
      integer icb(nb), jcb(nb)
      data icb /2, 512, 1023, 1023, 1023,  512,    2,    2/
      data jcb /2,   2,    2,  512, 1023, 1023, 1023,  512/
      real xb(nb), yb(nb)
*      real triangles(1000)
*      integer ntri,itri
*      real xtri(3), ytri(3)

      integer nnb, iicb(nb), jjcb(nb)

      real chipx,chipy,detx,dety

      integer nbadreg, nbadregmax
      parameter (nbadregmax=1000)
      integer ccdidbad(nbadregmax), chipxminbad(nbadregmax), 
     ~    chipxmaxbad(nbadregmax), chipyminbad(nbadregmax),
     ~    chipymaxbad(nbadregmax)
      character*200 line, badpixfile
      integer unit,newunit, status
      logical defined
      integer wc,wxmin,wxmax,wymin,wymax, i, j, k, l, ibad, ich

      integer fill_poly_eorig_to_binned
      external fill_poly_eorig_to_binned
      
      real manualshiftx, manualshifty

      real wx,wy
      integer iccd

      logical qwindow
      character cwindow*80
      integer cxmin,cxmax,cymin,cymax


      call get_cl_par ('window',cwindow)
      if (defined(cwindow)) then
        read (cwindow,*) cxmin, cxmax, cymin, cymax
        qwindow = .true.
      else
        qwindow = .false.
      endif

          

      if (usesubarray) then
        do i=1,nb
          jcb(i)=max(
     ~        min(jcb(i),subarray_firstrow+subarray_nrows)
     ~        ,subarray_firstrow) 
        enddo
      endif

      if (qwindow) then
        write (0,*) cxmin,cxmax,cymin,cymax
        nnb = 4
        iicb(1)=cxmin
        jjcb(1)=cymin
        iicb(2)=cxmin
        jjcb(2)=cymax
        iicb(3)=cxmax
        jjcb(3)=cymax
        iicb(4)=cxmax
        jjcb(4)=cymin
      else
        nnb = nb
        do i=1,nnb
          iicb(i)=icb(i)
          jjcb(i)=jcb(i)
        enddo
      endif


      if (.not. out_in_chip) then
        call get_pv_default ('manualshiftx',manualshiftx,0.0,'e')
        call get_pv_default ('manualshifty',manualshifty,0.0,'e')
        call det2chip (4096.5,4096.5,wx,wy,iccd)
      else
        manualshiftx=0
        manualshifty=0
      endif

c 0) Load bad pixels
      nbadreg = 0
      call get_cl_par ('badpixfilter',badpixfile)
      if (defined(badpixfile)) then
        unit = newunit()
        open (unit,file=badpixfile,status='old')
        status = 0
        do while (status.eq.0)
          read (unit,'(a)',iostat=status) line
          if (status.eq.0) then
            read (line,*,iostat=status) wc,wxmin,wxmax,wymin,wymax
            if (status.eq.0) then
              nbadreg = nbadreg + 1
              ccdidbad(nbadreg) = wc
              chipxminbad(nbadreg) = wxmin
              chipxmaxbad(nbadreg) = wxmax
              chipyminbad(nbadreg) = wymin
              chipymaxbad(nbadreg) = wymax
            else
              status=0
            endif
          endif
        enddo
        close(unit)
      endif

      
c 1) Set mask to zero
      do i=1,nx
        do j=1,ny
          mask(i,j)=0
        enddo
      enddo
      
c 2) Go over chips
      do ich=1,nchips
        chipid = ichip(ich)

        write(0,*) ' -- making chip mask for ccd #', chipid

c      2a) Prepare chip corners
        do k=1,nnb
          chipx = iicb(k)
          chipy = jjcb(k)
          if (out_in_chip) then
            detx=chipx
            dety=chipy
          else
            call chip2det (chipx,chipy,chipid,detx,dety)
*          print*,chipid,chipx,chipy,detx,dety
          endif
          if (detx.gt.0.0.and.dety.gt.0.0) then
            xb(k) = detx+manualshiftx
            yb(k) = dety+manualshifty
          else
            write (0,*) 'Could not convert chip coordinates',iicb(k),jjcb(k)
     ~          ,' for chip',chipid
            call exit(1)
          endif
        enddo

c      2b) Fill polygon
        call fill_poly (xb,yb,nnb,chipval(ich)-defval,mask,work,nx,ny,bin
     ~      ,eaverage,corrcti,corrqeu,out_in_chip,chipid,correct_cont,
     ~      correct_cont_spatial)

c      2c) Subtract bad pixels

        do ibad = 1,nbadreg
          if (ccdidbad(ibad).eq.chipid) then
            do i=chipxminbad(ibad),chipxmaxbad(ibad)
              do j=chipyminbad(ibad),chipymaxbad(ibad)
                chipx = i
                chipy = j
                if (out_in_chip) then
                  detx = chipx
                  dety = chipy
                else
                  call chip2det (chipx,chipy,chipid,detx,dety)
                endif
                if (detx.gt.0.0.and.dety.gt.0.0) then
                  k = fill_poly_eorig_to_binned (detx+manualshiftx,bin)
                  l = fill_poly_eorig_to_binned (dety+manualshifty,bin)
                  if (k.ge.1.and.k.le.nx.and.l.ge.1.and.l.le.ny) then
                    mask(k,l)=max(mask(k,l)-work(k,l)/bin**2,0.0)
                  endif
                else
                  write (0,*) 'Could not convert chip coordinates',icb(k)
     ~                ,jcb(k),' for chip',chipid
                  call exit(1)
                endif
              enddo
            enddo
          endif
        enddo
      enddo

c 3) Subtract 1 from the mask so that chipid=0 really has 0 value
      do i=1,nx
        do j=1,ny
          mask(i,j)=mask(i,j)+defval
        enddo
      enddo

      
      return
      end
