#include <zhtools.h>

      program convaspect
      implicit none
c
c
c Convolves an image with aspect motions
c
c
c
      character*200 mapfile, outfile, asphistfile
      logical defined
      
      integer status, unitmap, blocksize, bitpix, naxis, naxes(3), pcount,
     ~    gcount
      logical simple, extend
      integer nx,ny
      ZHDECL(real,pmask)

*--------------------------------------------------------------------

      call get_cl_par ('chipmap',mapfile)
      if (.not.defined(mapfile)) call exiterror ('-chipmap is undefined')

      call get_cl_par ('asphist',asphistfile)
      if (.not.defined(asphistfile)) call exiterror ('-asphist is undefined')
      
      call get_cl_par ('o',outfile)
      if (.not.defined(outfile)) call exiterror ('-o is undefined')
      

c  1) Open chip mask file
      status = 0
      call ftgiou (unitmap,status)
      if (status.ne.0) call exit_fitsio (mapfile,status)
      call ftopen (unitmap,mapfile,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (mapfile,status)

c  2) Read image size and required info
      call ftghpr (unitmap,3,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~    ,status)
      if (status.ne.0)  call exit_fitsio (mapfile,status)
      if (naxis.ne.2)  call exiterror ('Not a 2-d image in '//mapfile)
      
      nx = naxes(1)
      ny = naxes(2)

      ZHMALLOC(pmask,nx*ny,'e')

      call convaspect_1 (mapfile, outfile, asphistfile, unitmap, nx, ny,
     ~    ZHVAL(pmask))


      call exit(0)
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine convaspect_1 (mapfile, outfile, asphistfile, unitmap, nx, ny
     ~    ,mask)
c
c Allocates memory for the image; determine the mask list size,
c read average mask roll and bin factor, opens the aspect hist file
c and finds the aspect histogram size. reads keywords and calls convaspect_2
c
      implicit none
      character*(*) mapfile, outfile, asphistfile
      integer unitmap, nx,ny
      real mask(nx*ny)
      logical anyf
      integer status, unitasp, blocksize, naxis, naxes(10)
      character*80 comment, keyname
      integer icolroll, icolasp

      real rollmask
      integer binmask
      integer naspx,naspy
      real crval1, crval2,crpix1,crpix2,cdelt1,cdelt2

      integer nmasklist, i

      ZHDECL(real,pasphist)
      ZHDECL(integer,pind_asphist)
      ZHDECL(real,pmasklist)
      ZHDECL(integer,pind_masklist)
      

c   1) Read the chip mask
      status = 0
      print *,' read chip mask'
      call ftg2de (unitmap,0,0.0,nx,nx,ny,mask,anyf,status)
      if (status.ne.0) call exiterror (mapfile,status)

c   2) Find the number of positive elements
      nmasklist = 0
      do i=1,nx*ny
        if (mask(i).gt.0.0) then
          nmasklist = nmasklist + 1
        endif
      enddo
      print*,nmasklist,' positive pixels in chipmask'

c   3) Read keywords from the chip mask file
      call ftgkye (unitmap,'ROLL_AV',rollmask,comment,status)
      if (status.ne.0) then
        rollmask = 0.0
        status = 0
      endif
      
      call ftgkyj (unitmap,'BINFACT',binmask,comment,status)
      if (status.ne.0) then
        call putwarning ('convaspect_1',
     ~      'BINFACT not found in chip mask; calculate from CDELT2')
        status = 0
        call ftgkye (unitmap,'CDELT2',cdelt2,comment,status)
        if (status.ne.0) then
          call putwarning ('convaspect_1',
     ~        'CDELT2 not found in chip mask; assume binfact=1')
          binmask = 1
          status = 0
        else
          binmask = nint (cdelt2/1.36666667e-4)
        endif
      endif
      
c   4) Open aspect histogram file
      call ftgiou (unitasp,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      call ftopen (unitasp,asphistfile,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
c   4a) go to the ASPECT_HISTOGRAM extension
      call ftmnhd (unitasp,-1,'ASPECT_HISTOGRAM',0,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
c   4b) Find ROLLAV and ASPHIST column numbers
      call ftgcno (unitasp,.false.,'ROLLAV',icolroll,status)
      call ftgcno (unitasp,.false.,'ASPHIST',icolasp,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)

c   4c) Find information about ASPHIST
      call ftgtdm (unitasp,icolasp,3,naxis,naxes,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      if (naxis.ne.2) call exiterror ('Not a 2D image in ASPHIST column')
      naspx = naxes(1)
      naspy = naxes(2)

      write (keyname,'(i3,a)') icolasp,'CRVL1'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crval1,comment,status)

      write (keyname,'(i3,a)') icolasp,'CRVL2'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crval2,comment,status)

      write (keyname,'(i3,a)') icolasp,'CDLT1'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,cdelt1,comment,status)

      write (keyname,'(i3,a)') icolasp,'CDLT2'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,cdelt2,comment,status)

      write (keyname,'(i3,a)') icolasp,'CRPX1'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crpix1,comment,status)

      write (keyname,'(i3,a)') icolasp,'CRPX2'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crpix2,comment,status)

c    5) Call convaspect_2

      ZHMALLOC (pasphist,naspx*naspy,'e')
      ZHMALLOC (pind_asphist,naspx*naspy,'j')
      ZHMALLOC (pmasklist,nmasklist,'e')
      ZHMALLOC (pind_masklist,nmasklist,'j')

      call convaspect_2 (
     ~    mapfile, outfile, asphistfile, unitmap, nx, ny,
     ~    unitasp, mask, rollmask, binmask, nmasklist,
     ~    icolroll, icolasp, naspx, naspy,
     ~    crval1, crpix1, cdelt1, crval2, crpix2, cdelt2,
     ~    ZHVAL(pasphist),ZHVAL(pind_asphist),ZHVAL(pmasklist),ZHVAL(pind_masklist))
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine convaspect_2 (
     ~    mapfile, outfile, asphistfile, unitmap, nx, ny,
     ~    unitasp, mask, rollmask, binmask, nmasklist,
     ~    icolroll, icolasp, naspx, naspy,
     ~    crval1, crpix1, cdelt1, crval2, crpix2, cdelt2,
     ~    asphist,ind_asphist,masklist,ind_masklist)
c
c Convert chipmask to list and set it to zero
c Reads aspect histograms from the aspect file.
c Convert aspect histogram to list
c Call convolve two lists and add to the image (C routines for better
c memory management)
c
      implicit none
      character*(*) mapfile, outfile, asphistfile
      integer unitmap, nx,ny, unitasp
      real mask(nx*ny)
      real rollmask
      integer binmask, nmasklist, icolroll, icolasp, naspx, naspy
      real crval1, crpix1, cdelt1, crval2, crpix2, cdelt2
      real asphist (naspx*naspy)
      integer ind_asphist(naspx*naspy)
      real masklist (nmasklist)
      integer ind_masklist(nmasklist)

      integer nasppoints, status
      logical anyf
      character*80 comment
      integer k, i, iasp, nasplist
      real rollasp, rollnom
      double precision ra_nom, dec_nom
      real refmaskx, refmasky, cdelt1mask, cdelt2mask

      integer unitout, bitpix, naxis, naxes(2), pcount, gcount

      double precision sum
      
      character*80 arg
      logical yespar, defined, normexp
      integer inverse
      real exposure
*-------------------------------------------------------

      
c   0) Determnine whether we want to do an inverse transform (i.e. sky -> det)
      if (yespar ('inverse')) then
        inverse = 1
      else
        inverse = 0
      endif

c   0b) Do we want to normalize the map by exposure?
      call get_cl_par ('noexp',arg)
      if (defined(arg)) then
        normexp = .false.
      else
        normexp = .true.
      endif
      

c   1) Convert image to list
      call img_to_list (mask,nx*ny,0.0,masklist,ind_masklist,k)
      if (k.ne.nmasklist) then
        write (0,*) 'Expect ',nmasklist,' >0 mask values, got ',k
        call exit(1)
      endif
c   1a) And set it to zero
      do i=1,nx*ny
        mask(i)=0.0
      enddo

c   2) Find the number of aspect points
      status = 0
      call ftgkyj (unitasp,'NAXIS2',nasppoints, comment, status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      
c   2a) read nominal roll and nomina ra and dec from aspect file
      call ftgkye (unitasp,'ROLL_NOM',rollnom, comment, status)
      call ftgkyd (unitasp,'RA_NOM',ra_nom, comment, status)
      call ftgkyd (unitasp,'DEC_NOM',dec_nom, comment, status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)

c   2a-1)
      if (normexp) then
        call ftgkye (unitasp,'EXPOSURE',exposure,comment,status)
        if (status.ne.0) call exit_fitsio (asphistfile,status)
        if (exposure.lt.0.0) then
          call exiterror ('Exposure < 0.0 in the aspect file')
        endif
      endif

c   2b) read reference point from the chipmask file. If not set, calculate it
c       assuming the the center of the unbinned system was at pixel 4096.5
      call ftgkye (unitmap,'CRPIX1',refmaskx, comment, status)
      if (status.ne.0) then
        call putwarning ('convaspect_2',
     ~      'no CRPIX1 in the chip mask; use default')
        refmaskx = (4096.5-0.5)/binmask+0.5-0.5/binmask
        status = 0
      endif
      call ftgkye (unitmap,'CRPIX2',refmasky, comment, status)
      if (status.ne.0) then
        call putwarning ('convaspect_2',
     ~      'no CRPIX2 in the chip mask; use default')
        refmasky = (4096.5-0.5)/binmask+0.5-0.5/binmask
        status = 0
      endif

c
c Read pixel size in the mask
      call ftgkye (unitmap,'CDELT1',cdelt1mask, comment, status)
      if (status.ne.0) then
        call putwarning ('convaspect_2',
     ~      'no CDELT1 in the chip mask; use default')
        status = 0
        cdelt1mask = -1.3663888888889E-04*binmask
      endif
      call ftgkye (unitmap,'CDELT2',cdelt2mask, comment, status)
      if (status.ne.0) then
        call putwarning ('convaspect_2',
     ~      'no CDELT2 in the chip mask; use default')
        status = 0
        cdelt2mask = 1.3663888888889E-04*binmask
      endif
      
      sum = 0                   ! total weight in the aspect histogram

c   3) Go row-by-row in the aspect file, read aspect histogram and average
c      roll and call convolution of lists
      do iasp = 1, nasppoints
c   3a) Read average roll
        call ftgcve (unitasp,icolroll,iasp,1,1,0.0,rollasp,anyf,status)
        if (status.ne.0) call exit_fitsio (asphistfile,status)
        if (anyf) then
          write (0,*) 'Warning: undefined roll in row ',iasp,
     ~        ' of the aspect histogram file'
        endif
        
c   3b) Read aspect histogram
        call ftgcve (unitasp,icolasp,iasp,1,naspx*naspy,0.0,asphist,anyf
     ~      ,status)
        if (status.ne.0) call exit_fitsio (asphistfile,status)
        if (anyf) then
          write (0,*) 'Warning: undefined asphist in row ',iasp,
     ~        ' of the aspect histogram file'
        endif

c   3c) Convert aspect histogram to list
        call img_to_list (asphist,naspx*naspy,0.0,asphist,ind_asphist,nasplist
     ~      )
c   3c-1) Normalize aspect histogram to 1
        do i=1,nasplist
          sum = sum + asphist(i)
        enddo
        

c   3d) Call convolution of the lists
c
        print*,rollmask,rollasp,rollnom
        call asp_convolve_lists (
     ~      mask,nx,ny,
     ~      masklist,ind_masklist,nmasklist,
     ~      asphist, ind_asphist, nasplist,
     ~      naspx, naspy,
     ~      rollmask, binmask, refmaskx, refmasky,
     ~      (rollasp+rollnom), 
     ~      crval1, crpix1, cdelt1, crval2, crpix2, cdelt2,
     ~      inverse)

      enddo

c    4) Normalize the map
      do i=1,nx*ny
        mask(i)=mask(i)/sum
      enddo

c    4a) normalize by exposure if required
      if (normexp) then
        do i=1,nx*ny
          mask(i)=mask(i)*exposure
        enddo
      endif

c    5) Write output file
      status = 0
      call ftgiou (unitout,status)
      if (status.ne.0) call exit_fitsio (outfile,status)
      call ftinit (unitout,outfile,0,status)
      if (status.ne.0) call exit_fitsio (outfile,status)
      bitpix=-32
      naxis=2
      naxes(1)=nx
      naxes(2)=ny
      pcount = 0
      gcount = 1
      call ftphpr(unitout,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .false.,status)
      call ftp2de (unitout,0,nx,nx,ny,mask,status)

c    5b) Write pos keywords
      call ftpkyf (unitout,'CRPIX1',refmaskx,5,'reference pixel',status)
      call ftpkyd (unitout,'CRVAL1',ra_nom,10, 'reference value',status)
      call ftpkye (unitout,'CDELT1',cdelt1mask,10, 'pixel size',status)
      call ftpkys (unitout,'CTYPE1','RA---TAN', 'Coordinate type',status)

      call ftpkyf (unitout,'CRPIX2',refmasky,5,'reference pixel',status)
      call ftpkyd (unitout,'CRVAL2',dec_nom,10, 'reference value',status)
      call ftpkye (unitout,'CDELT2',cdelt2mask,10, 'pixel size',status)
      call ftpkys (unitout,'CTYPE2','DEC--TAN', 'Coordinate type',status)
      

      call ftclos (unitout,status)
      if (status.ne.0) call exit_fitsio (outfile)

      return
      end


