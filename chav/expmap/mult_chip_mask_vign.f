      subroutine mult_chip_mask_vign (mask,nx,ny,bin, 
     ~    elo, ehi,cnts,ne, emin,emax)
c
c Multiply the instrument map by vignetting. 
c
c
      implicit none
      integer nx,ny
      real mask(nx,ny)
      integer bin
      integer ne
      real elo(ne),ehi(ne),cnts(ne), emin, emax

      integer negridmax
      parameter (negridmax=11000)
      real emirror(negridmax), amirror(negridmax), spec(negridmax)
      integer nemirror
      real de
      parameter (de=0.02)       ! width of the energy channel for
                                ! calculation of the mirror area arrays
      real ee
      real hrma_onaxis_ea, hrma_vignetting
      external hrma_onaxis_ea, hrma_vignetting

      integer ie
      logical found
      
      integer i,j
      real detx,dety, theta, phi
      double precision cntonaxis, cntoffaxis

      integer binvign0, nvign0
      parameter (binvign0 = 16) ! (calculate it every 8")
      parameter (nvign0 = 8192/binvign0)
      real vign0(nvign0,nvign0)
      real detx1,detx2,dety1,dety2,wx1,wx2,wy1,wy2,v
      integer i1,i2,j1,j2,ii,jj

c 1) Form onaxis area array and spectrum
      nemirror = 0
      cntonaxis = 0
c     do ee = emin, emax, de
      ee = emin
10    if (ee .le. emax ) then
        ee = ee + de
        nemirror = nemirror + 1
        emirror (nemirror) = ee
        amirror (nemirror) = hrma_onaxis_ea(ee)
        ie = 1
        found = .false.
        do while (.not.found .and. ie.le.ne)
          if ( ee.ge.elo(ie).and.ee.le.ehi(ie) ) then
            found = .true.
          else
            ie = ie + 1
          endif
        enddo
        if (found) then
          spec(nemirror) = amirror(nemirror)*de*cnts(ie)
        else
          spec(nemirror) = 0.0
        endif
        cntonaxis = cntonaxis + spec(nemirror)
      goto 10
      endif
c     enddo
      if (cntonaxis.le.0.0d0) then
        call exiterror ('Zero normalization for onaxis spectrum')
      endif

c  2) Form the vignetting grid array
      print*,'calculate vignetting grid'
c  2a) set it to zero
      do i=1,nvign0
        do j=1,nvign0
          vign0(i,j)=0
        enddo
      enddo

c  2b) Project the main chip mask onto vign0
      do i=1,nx
        do j=1,ny
          if (mask(i,j).gt.0.0) then
            detx = bin*i+0.5-0.5*bin
            dety = bin*j+0.5-0.5*bin
            i1 = int(detx/binvign0)
            i2 = i1+1
            j1 = int(dety/binvign0)
            j2 = j1+1
            i1 = max(i1,1)
            i2 = min(i2,nvign0)
            j1 = max(j1,1)
            j2 = min(j2,nvign0)
            do ii=i1,i2
              do jj=j1,j2
                vign0(ii,jj)=1
              enddo
            enddo
          endif
        enddo
      enddo
          
c   2c) And calculate vign0 where needed
      do i=1,nvign0
        print '(i3,"% done\r",$)',nint(100.0*float(i)/nvign0)
        call flush(6)
        do j=1,nvign0
          if (vign0(i,j).gt.0.01) then
            detx = binvign0*i+0.5-0.5*binvign0
            dety = binvign0*j+0.5-0.5*binvign0
            call detpix2pol_acis (detx,dety,theta,phi)
            cntoffaxis = 0
            do ie=1,nemirror
              cntoffaxis = cntoffaxis +
     ~            spec(ie)*hrma_vignetting(emirror(ie),theta,phi)
            enddo
            vign0(i,j) = cntoffaxis/cntonaxis
          else
            vign0(i,j) = 0
          endif
        enddo
      enddo
      print*,'done'

c  2) Go over image and do linear interpolation over precalculated grid
      print*,'Correct for mirror vignetting'
      do i=1,nx
        print '(i3,"% done\r",$)',nint(100.0*float(i)/nx)
        call flush(6)
        do j=1,ny
          if (mask(i,j).gt.0.0) then
            detx = bin*i+0.5-0.5*bin
            dety = bin*j+0.5-0.5*bin
*            call detpix2pol_acis (detx,dety,theta,phi)
*            cntoffaxis = 0
*            do ie=1,nemirror
*              cntoffaxis = cntoffaxis +
*     ~            spec(ie)*hrma_vignetting(emirror(ie),theta,phi)
*            enddo
*            mask(i,j)=mask(i,j)*cntoffaxis/cntonaxis
            i1 = int(detx/binvign0)
            i2 = i1+1
            j1 = int(dety/binvign0)
            j2 = j1+1
            if (i1.ge.1.and.i2.le.nvign0.and.j1.ge.1.and.j2.le.nvign0) then
              detx1 = binvign0*i1+0.5-0.5*binvign0
              detx2 = binvign0*i2+0.5-0.5*binvign0
              dety1 = binvign0*j1+0.5-0.5*binvign0
              dety2 = binvign0*j2+0.5-0.5*binvign0
              wx1 = (detx2-detx)/(detx2-detx1)
              wy1 = (dety2-dety)/(dety2-dety1)
              wx2 = 1.0-wx1
              wy2 = 1.0-wy1
              v   = wx1*(wy1*vign0(i1,j1)+wy2*vign0(i1,j2)) +
     ~              wx2*(wy1*vign0(i2,j1)+wy2*vign0(i2,j2))
              mask (i,j) = mask(i,j)*v
            else
              mask (i,j) = 0.0
            endif
          endif
        enddo
      enddo
      print*,'done'
      return
      end

      

