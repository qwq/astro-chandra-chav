      subroutine det2chip_bin (det_x,det_y,chipx,chipy,iccd,bin)
      implicit none
      real det_x,det_y,chipx,chipy
      integer iccd, bin
      real detx, dety
      integer ncorstep, icorstep
      parameter (ncorstep=16)
      real corx(ncorstep)
      real cory(ncorstep)
      data corx /0.25, 0.0, -0.25,  0.0, 0.5, 0.0, -0.5,  0.0, 0.75, 0.0,
     ~    -0.75, 0.0, 1.0, 0.0, -1.0, 0.0 /
      data cory /0.0, 0.25,  0.0, -0.25, 0.0, 0.5,  0.0, -0.5, 0.0, 0.75,
     ~    0.0,  -0.75, 0.0, 1.0, 0.0, -1.0/
      real manualshiftx, manualshifty

c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      save manualshiftx, manualshifty, corx,cory

*------------------------------------------------------------------------
      
      if (firstcall) then
        call get_pv_default ('manualshiftx',manualshiftx,0.0,'e')
        call get_pv_default ('manualshifty',manualshifty,0.0,'e')
        firstcall = .false.
      endif

      detx=det_x-manualshiftx
      dety=det_y-manualshifty

      call det2chip (detx,dety,chipx,chipy,iccd)
      if (iccd.lt.0) then
        icorstep=0
        do while (iccd.lt.0.and.icorstep.lt.ncorstep)
          icorstep=icorstep+1
          call det2chip (
     ~        detx+bin*corx(icorstep),
     ~        dety+bin*cory(icorstep),
     ~        chipx,chipy,iccd)
        enddo
      endif
      if (iccd.lt.0) then
        write (0,*) ' ! could not get chip coordinates for detx,dety=',
     ~      detx,dety
        call exit(1)
      endif
      

      return
      end










