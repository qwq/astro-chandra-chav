      subroutine ccd_qe2 (egrid,l,ne,chipx,chipy,iccd,
     ~    qemain,qetot)
! CANNOT BE USED WITH DIFFERENT EGRIDS!
!
!
c same as ccd_qe but returns both main and spatially-dependent parts
c
      implicit none
      integer ne,l
      real egrid(ne), chipx, chipy
      integer iccd
      real qemain, qetot

      integer nccd
      parameter (nccd=10)
      integer nregmax           ! max number of regions in all CCDs
      parameter (nregmax=4*nccd)
      integer nemax             ! max number of energy channels
      parameter (nemax=10000)
      real qe_main (nemax,nregmax)
      logical inidata (nregmax)
      integer iqemain (0:nccd-1,20) ! index for qe_main arrays; assume no more 
                                ! than 20 regions per ccd
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      logical corrcti, corrqeu, newcorrcti
      save iqemain,qe_main,inidata, corrcti, corrqeu, newcorrcti
      character*80 arg
      logical no, defined
      external no, defined
      
      integer i,ireg,iqe
      real qe

      real ccd_qe_main, ccd_qe_corr, ccd_qe_qeu, ccd_qe_corr_jul2000
      external ccd_qe_main, ccd_qe_corr, ccd_qe_qeu, ccd_qe_corr_jul2000

      real lowecor, elowecor
      save lowecor, elowecor

      logical correct_cont, correct_cont_spatial, correct_cont_caldb
      save correct_cont, correct_cont_spatial, correct_cont_caldb

      logical tweak_s3_qe
      save tweak_s3_qe
      real corr

      logical yes, yespar
      external yes, yespar
      real corrarf, corrarf_spatial, corrarf_caldb
      external corrarf, corrarf_spatial, corrarf_caldb

      real dead_area_cor
      save dead_area_cor

      if (firstcall) then

        do i=1,nregmax
          inidata(i)=.true.
        enddo

        do i=0,nccd-1
          do ireg=1,20
            iqemain(i,ireg)=i+1 ! but we can set different regions within
                                ! the CCD
          enddo
        enddo

        call get_cl_par ('correct_qe_cti',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL CORRECT FOR THE CTI DROP IN QE'
          corrcti=.true.
          if (arg.eq.'new') then
            newcorrcti = .true.
            print*,'USING NEW RELATIONS BASED ON JULY 2000 DATA'
          else
            newcorrcti = .false.
          endif
        else
          corrcti=.false.
        endif

        call get_cl_par ('correct_qeu',arg)
        if (defined(arg).and.(.not.no(arg))) then
          print*,'I WILL USE THE QEU MAP'
          corrqeu=.true.
        else
          corrqeu=.false.
        endif

        call get_pv_default ('dead_area_cor',dead_area_cor,0.0,'e')
        if (dead_area_cor.ne.0.0) then
          write (0,*) 'DEAD AREA CORRECTION AT CHIPY=1024 IN FI:',
     ~        dead_area_cor
        endif

        call get_pv_default ('ccd_qe_lowecor',lowecor,1.0,'e')
        if (lowecor.ne.1.0) then
          print *,'I WILL MULTIPLY QE in FI chips below 1.832 keV by ',lowecor
          call get_pv_default ('e_qe_lowecor',elowecor,1.832,'e')
        endif

        correct_cont = .false.
        correct_cont_spatial = .false.
        call get_cl_par ('correct_acis_contamination',arg)
        if (yes(arg)) then
          correct_cont = .true.
          correct_cont_spatial = .false.
          correct_cont_caldb = .false.
        endif
        if (arg.eq.'spatial') then
          correct_cont=.false.
          correct_cont_spatial = .true.
          correct_cont_caldb = .false.
        endif
        if (arg.eq.'caldb') then
          correct_cont=.false.
          correct_cont_spatial = .false.
          correct_cont_caldb = .true.
        endif
        if (correct_cont) print *,'OBF_CONTAMINATION+CORRECTION = ON'
        if (correct_cont_spatial) then
           print *,'OBF_CONTAMINATION+CORRECTION = SPATIAL'
        endif
        if (correct_cont_caldb) then
          print *,'OBF_CONTAMINATION+CORRECTION = CALDB'
        endif

        tweak_s3_qe = yespar('tweak_s3_qe')
        if (tweak_s3_qe) then
          print*,'S3_QE_TWEAK=ON'
        else
          print*,'S3_QE_TWEAK=OFF'
        endif
          

        firstcall = .false.
      endif
            
      ireg = 1                  ! but we can determine the region based on
                                ! chipx,chipy and iccd
      
      iqe = iqemain(iccd,ireg)


      if (inidata(iqe)) then
        do i=1,ne
          if (iccd.eq.7.and.tweak_s3_qe) then
            if (egrid(i).gt.3.5) then
              qe_main(i,iqe)=ccd_qe_main(egrid(i)/1.05,iccd)
            else
              corr = 1.05-0.16*log10(egrid(i))
              if (corr.lt.1.0) corr = 1.0
              qe_main(i,iqe)=ccd_qe_main(egrid(i),iccd)*corr
            endif
          else
            qe_main(i,iqe)=ccd_qe_main (egrid(i),iccd)
          endif
        enddo
        inidata(iqe)=.false.
      endif

      qe = qe_main(l,iqe)
      qemain = qe

      if (corrcti) then
        if (newcorrcti) then
          qe = qe*ccd_qe_corr_jul2000 (egrid(l),iccd,chipx,chipy)
        else
          qe = qe*ccd_qe_corr (egrid(l),iccd,chipx,chipy)
        endif
      endif

      if (corrqeu) then
        qe = qe*ccd_qe_qeu (egrid(l),iccd,nint(chipx),nint(chipy))
      endif

      if ( iccd.ne.7.and.iccd.ne.5.and.egrid(l).lt.elowecor ) then
        qe = qe * lowecor
      endif

      if (correct_cont_spatial) then
        qe = qe * corrarf_spatial (egrid(l),iccd,nint(chipx),nint(chipy))
      else
         if (correct_cont_caldb) then
            qe = qe * corrarf_caldb (egrid(l),iccd,nint(chipx),nint(chipy))
         else
            if (correct_cont) then
               qe = qe * corrarf(egrid(l),iccd,nint(chipx),nint(chipy))
            endif
         endif
      endif

      if (iccd.ne.7.and.iccd.ne.5) then
        qe = qe / (1+dead_area_cor*(chipy+1024.0)/2048)
      endif

      qetot = qe

      return
      end
