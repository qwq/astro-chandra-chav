      subroutine form_refspec (refspec,emin,emax,elo,ehi,cnts,ne,nemax)
      implicit none
      character refspec*(*)
      real emin, emax
      integer nemax, ne
      real elo(nemax), ehi(nemax), cnts(nemax)

      real ee(0:10000)

      character pvalue*80

c We are in this program is refspec was not set explicitly to an external file
c
c Instead, we'll try to check the mekal parameters and make the reference
c spectrum on the fly
c

      real T, abund, z, nH, e, absorp

      integer i


      logical defined
      real sigism
      external sigism

c     If spec=flat, use a flat spectrum for weights and ignore the rest
      call get_cl_par ('spec',pvalue)
      if (defined(pvalue).and.pvalue.eq.'flat') then

      write (0,*) 'Writing flat spectral weights'
      refspec = 'FLAT'

      ne = min(20,nemax)

      do i=0,ne
        ee(i)=log(emin)+(log(emax/emin)/ne)*i
        ee(i)=exp(ee(i))
      enddo

      do i=1,ne
        elo(i)=ee(i-1)
        ehi(i)=ee(i)
      enddo

      do i=1,ne
        cnts(i)=1e-5
      enddo

      else
c     The original behavior

      call get_cl_par ('T',pvalue)
      if (.not.defined(pvalue)) then
        refspec = 'undefined'
        return
      endif

      call get_cl_par ('emin',pvalue)
      if (.not.defined(pvalue)) then
        refspec = 'undefined'
        return
      endif

      call get_cl_par ('emax',pvalue)
      if (.not.defined(pvalue)) then
        refspec = 'undefined'
        return
      endif

      call get_parameter_value ('T',T,'e')
      call get_pv_default ('abund',abund,0.3,'e')
      call get_pv_default ('z',z,0.0,'e')
      call get_pv_default ('nH',nH,2e20,'e')
      if (nH.lt.1e10) nH = nH * 1e22


      call get_parameter_value ('emin',emin,'e')
      call get_parameter_value ('emax',emax,'e')


      refspec = 'MEKAL'

      ne = min(20,nemax)

      do i=0,ne
        ee(i)=log(emin)+(log(emax/emin)/ne)*i
        ee(i)=exp(ee(i))
      enddo

      do i=1,ne
        elo(i)=ee(i-1)
        ehi(i)=ee(i)
      enddo

      call mekal (T, abund, z, ne, ee, cnts)

      do i=1,ne
        e = 0.5*(elo(i)+ehi(i))*1000
        absorp = sigism(e)*nH
        if (absorp.lt.50.0) then
          absorp=exp(-absorp)
        else
          absorp=0.0
        endif
        cnts(i)=cnts(i)*absorp
      enddo

      endif

      return
      end

