#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include <math.h>
#include "readpar.h"


int goodgrade ( short grade );

#define NMAX 10000
#define RMAX 20

int main (int argc, char *argv) {
  extern int iter_evt ();/* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */
  fitsfile *ifptr, *ofptr;
  char evtfile[200],outfile[200],badpixfile[200];
  char extname[80], comment[80];
  long nrows;
  short ccdid[NMAX], chipx[NMAX], chipy[NMAX], grade[NMAX]; 
  unsigned char databuf[NMAX][512];
  int iccd,i,j;
  int chipxcol, chipycol, ccdidcol, gradecol, expnocol, energycol;
  int status, match, exact, anynul, hdutype;
  long naxes[100], naxes0; int nfound;
  long irow, nout;
  long expno, expnoold;
  int iread;
  short NS; long NL; float NE;
  int nphotgood,nphotbad,iphot,nrealbad;
  int chipx_good[NMAX], chipy_good[NMAX], chipx_bad[NMAX],chipy_bad[NMAX], 
    indx_good[NMAX], indx_bad[NMAX], goodphot[NMAX], badphot[NMAX];
  float energy[NMAX];
  int chx,chy;
  long dx,dy,rmax2;
  
  
  Ini_ReadPar(argc,argv);
  
  get_command_line_par ("evtfile",evtfile);
  get_command_line_par ("o",outfile);
  
  if (!defined(evtfile)) {
    fprintf (stderr,"evtfile=?\n");
    exit(1);
  }
  if (!defined(outfile)) {
    fprintf (stderr,"o=?\n");
    exit(1);
  }

  status = 0; 

  if (fits_open_file(&ifptr, evtfile, READONLY, &status)) /* open file */
    exit_fitsio(status);
  
  if (fits_create_file(&ofptr, outfile, &status)) /* create new FITS file */
    exit_fitsio(status);
  
  strcpy(extname,"junk");
  fits_compare_str (extname,"events", CASEINSEN, &match, &exact);
  while (!match) {
    /* copy the extension to output file */
    if (
	fits_copy_hdu (ifptr,ofptr,0,&status)
	)
      exit_fitsio(status);
    
    /* move to the next extension */
    if (
	fits_movrel_hdu (ifptr,1,&hdutype,&status)
	) {
      fprintf (stderr,"ERROR: EVENTS extension not found in the input file!\n");
      exit_fitsio(status);
    }
    
    if (hdutype==BINARY_TBL) {
      fits_read_key (ifptr,TSTRING,"EXTNAME",extname,comment,&status);
      fits_get_num_rows (ifptr,&nrows,&status);
      if (status)
	exit_fitsio(status);
    }
    fits_compare_str (extname,"events", CASEINSEN, &match, &exact);
  }
  
  /* we can be here only if we are in the events extension */
  if (fits_read_keys_lng(ifptr, "NAXIS", 1, 2, naxes, &nfound, &status) )
    exit_fitsio(status);

  fits_get_colnum(ifptr,CASEINSEN,"CHIPX",&chipxcol,&status);
  fits_get_colnum(ifptr,CASEINSEN,"CHIPY",&chipycol,&status);
  fits_get_colnum(ifptr,CASEINSEN,"CCD_ID",&ccdidcol,&status);
  fits_get_colnum(ifptr,CASEINSEN,"EXPNO",&expnocol,&status);
  fits_get_colnum(ifptr,CASEINSEN,"GRADE",&gradecol,&status);
  fits_get_colnum(ifptr,CASEINSEN,"ENERGY",&energycol,&status);
  if (status) 
    exit_fitsio(status);
  
  /* Copy header */
  if (fits_copy_header(ifptr,ofptr,&status))
    exit_fitsio(status);
  

  nout = 0;
  iread = 0;
  expnoold = -1;
  NS=0; NL=0; NE=0;
  naxes0=naxes[0];
  rmax2=RMAX*RMAX;
  for (irow=1;irow<=nrows;irow++)
    {
      if (fits_read_tblbytes(ifptr,irow,1,naxes0,databuf[iread],&status))
	exit_fitsio(status);
      
      fits_read_col (ifptr,TLONG,expnocol,irow,1,1,&NL,&expno,&anynul,&status);
      fits_read_col (ifptr,TSHORT,chipxcol,irow,1,1,&NS,&chipx[iread],&anynul,&status);
      fits_read_col (ifptr,TSHORT,chipycol,irow,1,1,&NS,&chipy[iread],&anynul,&status);
      fits_read_col (ifptr,TSHORT,gradecol,irow,1,1,&NS,&grade[iread],&anynul,&status);
      fits_read_col (ifptr,TSHORT,ccdidcol,irow,1,1,&NS,&ccdid[iread],&anynul,&status);
      fits_read_col (ifptr,TFLOAT,energycol,irow,1,1,&NE,&energy[iread],&anynul,&status);
      if (status)
	exit_fitsio(status);

      iread++;
      if (iread>=NMAX) {
	fprintf (stderr,"ERROR: too many photons in one exposure\n");
	exit(1);
      }

      if ( expno != expnoold || irow == nrows) {
	if (expnoold>=0) {
	  
	  for (iccd=0;iccd<10;iccd++) { /* go over different chips */
	    
	    nphotgood = 0; nphotbad = 0; nrealbad = 0;
	    for (iphot=0;iphot<iread-1;iphot++) {
	      if (ccdid[iphot]==iccd) {
		if (goodgrade(grade[iphot])) { /* separate good and bad grades */
		  chipx_good[nphotgood]=chipx[iphot];
		  chipy_good[nphotgood]=chipy[iphot];
		  indx_good[nphotgood]=iphot;
		  goodphot[nphotgood]=1;
		  nphotgood++;
		} else {
		  chipx_bad[nphotbad]=chipx[iphot];
		  chipy_bad[nphotbad]=chipy[iphot];
		  indx_bad[nphotbad]=iphot;
		  if ((grade[iphot]==5||grade[iphot]==7)&&(energy[iphot]>500&&energy[iphot]<6000)) {
		    nrealbad++;
		    badphot[nphotbad]=1;
		  }
		  else {
		    badphot[nphotbad]=0;
		  }
		  nphotbad++;
		}
	      }
	    }
	    if (nrealbad>0) { /* go over the "good photons" */
	      for (i=0;i<nphotgood;i++) {
		chx=chipx_good[i]; chy=chipy_good[i];
		for (j=0;j<nphotbad;j++) {
		  if (badphot[j]) {
		    dx = chipx_bad[j]-chx;
		    dy = chipy_bad[j]-chy;
		    if (dx*dx+dy*dy < rmax2 ) {
		      goodphot[i]=0;
		      break;
		    }
		  }
		}
	      }
	    }

	    /* write out the data */
	    /* Save all "bad photons" */
	    for (i=0;i<nphotbad;i++) { 
	      iphot = indx_bad[i];
	      nout ++;
	      if (fits_write_tblbytes(ofptr,nout,1,naxes0,databuf[iphot],&status))
		exit_fitsio(status);
	    }
	    
	    for (i=0;i<nphotgood;i++) { /* Save selected "good photons" */
	      if (goodphot[i]) {
		iphot = indx_good[i];
		nout ++;
		if (fits_write_tblbytes(ofptr,nout,1,naxes0,databuf[iphot],&status))
		  exit_fitsio(status);
	      }
	    }
	    
	  }
	}
	expnoold=expno;
	memcpy(databuf[0],databuf[iread],naxes0);
	chipx[0]=chipx[iread];
	chipy[0]=chipy[iread];
	grade[0]=grade[iread];
	ccdid[0]=ccdid[iread];
	iread=0;
      }
    }
  
  if ( fits_update_key(ofptr, TLONG, "NAXIS2", &nout, 0, &status) )
    exit_fitsio(status);
  if ( fits_write_chksum (ofptr,&status) )
    exit_fitsio(status);
  
  /* Copy the rest */
  while ( ! fits_movrel_hdu (ifptr,1,&hdutype,&status) ) {
    if (
	fits_copy_hdu (ifptr,ofptr,0,&status)
	)
      exit_fitsio(status); 
  }
  status = 0;
  fits_close_file (ifptr,&status);
  status = 0;
  fits_close_file (ofptr,&status);

  exit(0);
}

int exit_fitsio (int status)
{
  fits_report_error(stderr, status);
  exit(1);
}


/***********************************************************/
int goodgrade(short grade) {
  return ( grade != 7 && grade != 5 && grade != 1 );
}
