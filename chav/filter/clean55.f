      implicit none
      character*200 evtfile
      
      integer unit, status, blksize
      integer phascol, gradecol, ccdidcol
      logical anyf
      
      integer nphot
      integer phas(5,5)
      
      integer iphot
      integer iargc, iarg, narg
      integer qflag
      logical QSTAT

      integer ibit
      integer ifirst, usestatus, usefltgrade, usegrade
      character*80 arg
      logical isdigit
      external isdigit
      real fptemp
      logical qfptemp
      character*80 comment, tform, tformphas
      integer iccd

      real ctitrail
      integer spthresh
      common /c_an55/ ctitrail, spthresh
      logical lray(32)

      ibit = 20
      ifirst = 1
      usestatus = 0
      usefltgrade = 0
      usegrade = 0
      ctitrail = 0.027
      spthresh = 14

      qfptemp = .true.

c Parse command line arguments
      narg = iargc()
      if (narg.lt.1) call usage

      iarg = 1
      do while (iarg.le.narg)
        call getarg (iarg,arg)
        if (arg(1:1).eq.'-') then
          ifirst = iarg + 1

          if (arg(2:2).eq.'b') then ! -bN - set N-th bit in STATUS
            read (arg(3:),*,iostat=status) ibit
            if (status.ne.0) call usage
            usestatus = 1 
          endif
          
          if (arg.eq.'-fg') then ! -fg - use fltgrade
            usefltgrade = 1
          endif
          
          if (arg(1:1).eq.'-'.and.isdigit(arg(2:2))) then
            read (arg(2:),*) fptemp
            qfptemp = .false.
          endif
          
          if (arg.eq.'-g') then ! -g - use fltgrade
            usegrade = 1
          endif

          if (arg.eq.'-ct') then ! set CTI trail coefficient
            iarg = iarg + 1
            ifirst = iarg + 1
            call getarg (iarg,arg)
            read (arg,*) ctitrail
            print *,'CTI train coeff = ',ctitrail
          endif

          if (arg.eq.'-spth') then ! set "split threshold"
            iarg = iarg + 1
            ifirst = iarg + 1
            call getarg (iarg,arg)
            read (arg,*) spthresh
          endif

          
        endif
        iarg = iarg + 1
      enddo

c Can update only one of STATUS, FLTGRADE, GRADE
      if (usestatus+usefltgrade+usegrade.gt.1) then
        call usage
      endif
      if (usestatus+usefltgrade+usegrade.eq.0) usestatus=1

      print*,usestatus,ibit
      
c Loop over files in the command line
      do iarg=ifirst,narg
        call getarg (iarg,evtfile)

        status = 0
        call ftgiou (unit,status)
      
        call ftopen (unit,evtfile,1,blksize,status)
        call ftmnhd (unit,-1,'EVENTS',0,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)

*c Read focal plane temperature
*        if (qfptemp) then
*          call ftgkye (unit,'FP_TEMP',fptemp,comment,status)
*          if (status.ne.0) then
*            write (0,*) 'Focal plane temperature is not found in the header'
*            write (0,*) 'please set it explicitly with -110 or -120 switches'
*            call exit_fitsio ('FP_TEMP',status)
*          endif
*          fptemp = fptemp - 273.0
*        endif
        fptemp = -120           ! it does not matter, currently
      
        call ftgnrw (unit,nphot,status)

        call ftgcno (unit,.false.,'PHAS',phascol,status)
        write (tformphas,'(''TFORM'',i3)') phascol
        call rmblanks (tformphas)
        call ftgkys (unit,tformphas,tform,comment,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
        if (tform(1:2).ne.'25') then
          write (0,'(a)') 'ERROR: doesn''t look like a 5x5 PHAS column'
          call exit(1)
        endif
        
        call ftgcno (unit,.false.,'CCD_ID',ccdidcol,status)
        if (usestatus.eq.1) then
          call ftgcno (unit,.false.,'STATUS',gradecol,status)
        else if (usefltgrade.eq.1) then
          call ftgcno (unit,.false.,'FLTGRADE',gradecol,status)
        else if (usegrade.eq.1) then
          call ftgcno (unit,.false.,'GRADE',gradecol,status)
        endif
      
        do iphot=1,nphot
          call ftgcvj (unit,phascol,iphot,1,25,0,phas,anyf,status)
          call ftgcvj (unit,ccdidcol,iphot,1,1,0,iccd,anyf,status)
          if (status.ne.0) call exit_fitsio (evtfile,status)

          call an_phas5 (phas,qflag,iccd,fptemp)
          
          if (usestatus.eq.1) then
            call ftgcx(unit,gradecol,iphot,1,32,lray,status)
            QSTAT = qflag .ne. 0
            lray(32-ibit)=QSTAT
            call ftpclx (unit,gradecol,iphot,1,32,lray,status)
          else
            if (qflag.ne.0) then
              call ftpclj (unit,gradecol,iphot,1,1,255,status)
            endif               ! else, leave grade alone
          endif
        enddo
        
        call ftclos (unit,status)
        call ftfiou (unit,status)
        
        if (status.ne.0) call exit_fitsio (evtfile,status)
      enddo

      call exit(0)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

       subroutine an_phas5 (phas,fltgrd,iccd,fptemp)
       implicit none
       integer phas (5,5), fltgrd, iccd
       integer i,j
       
       real ctitrail
       integer spth
       common /c_an55/ ctitrail, spth

       integer spthresh
       save spthresh

       real fptemp

       logical firstcall
       data firstcall /.true./
       save firstcall
       real cti_tail(0:9)
       save cti_tail

       if (firstcall) then
         firstcall = .false.
         do i=0,9
           cti_tail(i)=ctitrail
         enddo
         spthresh = spth
       endif

       fltgrd = 0

       i=1
       do j=1,5
         if (phas(i,j).gt.spthresh) then
           fltgrd=1
           return
         endif
       enddo
       
       i=5
       do j=1,5
         if (phas(i,j).gt.spthresh) then
           fltgrd=1
           return
         endif
       enddo
       
       j=1
       do i=1,5
         if (phas(i,j).gt.spthresh) then
           fltgrd=1
           return
         endif
       enddo

       j=5
       do i=1,5
         if (i.ne.1.and.i.ne.5) then
           if (phas(i,j).gt.spthresh+phas(i,3)*cti_tail(iccd)) then
             fltgrd=1
             return
           endif
         else
           if (phas(i,j).gt.spthresh) then
             fltgrd=1
             return
           endif
         endif
       enddo
       
       return
       end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine exit_fitsio (message,status)
      implicit none
      character*(*) message
      integer status

      call perror_fitsio (message,status)
      call exit(1)
      return
      end

      subroutine perror_fitsio (message,status)
      implicit none
      character*(*) message
      integer lnblnk
      integer status
      character*80 errtext
      
      if (status.ne.0) then
        call FTGERR (status,errtext)
        do while (errtext.ne.' ')
          write(0,'(a,a,a)')message(1:lnblnk(message)),': '
     ~        ,errtext(1:lnblnk(errtext))
          call FTGMSG (errtext)
        enddo
      endif
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine usage
      write (0,*)
     ~    'Usage: clean55 [-bN | -fg | -g] [-ct coeff] '/
     ~    /'[-spth sp_thresh] file1.fits file2.fits ...'
      call exit(1)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      logical function isdigit (c)
      implicit none
      character*1 c
      integer status, i

      read (c,'(i1)',iostat=status) i
      isdigit = status .eq. 0
      return
      end

      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rmblanks (s)
c Remove blanks from a string
      implicit none
      character*(*) s
      
      integer lnblnk,len,i,n

      len=lnblnk(s)
      n=0
      do i=1,len
        if(s(i:i).ne.' '.and.s(i:i).ne.'\t')then
          n=n+1
          s(n:n)=s(i:i)
        endif
      enddo
      
      do i=n+1,len
        s(i:i)=' '
      enddo
      
      return
      end
