/* Must be compiled with the CFITSIO library and header files */
/* If ReadPar library is not available, compile with -DNOREADPAR */
/* Usage:
 (with ReadPar)
  badpixfilter evtfile=dirty_evt.fits o=clean_evt.fits badpixfiler=badpix.dat

 (without ReadPar)
  badpixfilter dirty_evt.fits clean_evt.fits badpix.dat
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include <math.h>
#ifndef NOREADPAR
#include "readpar.h"
#endif

float *expmap;
int nxexp, nyexp;
double xrefval_img, yrefval_img, xrefpix_img, yrefpix_img, 
  xinc_img, yinc_img, rot_img; 
char coordtype_img[100];
double xrefval_tbl, yrefval_tbl, xrefpix_tbl, yrefpix_tbl, 
  xinc_tbl, yinc_tbl, rot_tbl; 
char coordtype_tbl[100];

int main (int argc, char *argv[]) {
  extern iter_evt ();   /* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */
  fitsfile *fptr;
  iteratorCol cols[100]; /* STRUCTURE USED BY THE ITERATOR FUNCTION */
  int ncols;
  long rows_per_loop, offset;
  int status;
  int xcol, ycol;

  char evtfile[200],expmapfile[200];

  Ini_ReadPar(argc,argv);
  
  /*  get_command_line_par ("evtfile",evtfile);
  get_command_line_par ("o",outfile);
  get_command_line_par ("badpixfilter",badpixfile); */
  
  get_cl_or_read_par ("expmap","Exposure map",expmapfile);

  get_cl_or_read_par ("evtfile","Input events file",evtfile);

  if (!defined(evtfile)) {
    fprintf (stderr,"evtfile=?\n");
    exit(1);
  }
  if (!defined(expmapfile)) {
    fprintf (stderr,"expmapfile=?\n");
    exit(1);
  }


  /* Load EXP MAP */
  Load_Exp_Map (expmapfile);

  /* Open events file */
  fits_open_file(&fptr, evtfile, READWRITE, &status); /* open file */

  /* move to the EVENTS binary table extension */
  if (fits_movnam_hdu(fptr, BINARY_TBL, "EVENTS", 0, &status) )
    fits_report_error(stderr, status);    /* print out error messages */
  
  ncols = 3;

  /* define input column structure members for the iterator function */
  fits_iter_set_by_name(&cols[0], fptr, "X", TDOUBLE,  InputCol);
  fits_iter_set_by_name(&cols[1], fptr, "Y",  TDOUBLE,  InputCol);
  fits_iter_set_by_name(&cols[2], fptr, "SPECWGHT",  TFLOAT,  InputOutputCol);

  /* get WCS infor for X and Y columns */
  if (fits_get_colnum (fptr, 0, "X", &xcol, &status) )
    fits_report_error(stderr, status);
  if (fits_get_colnum (fptr, 0, "Y", &ycol, &status) )
    fits_report_error(stderr, status);
  if (fits_read_tbl_coord (fptr,xcol,ycol,
			   &xrefval_tbl,&yrefval_tbl,
			   &xrefpix_tbl,&yrefpix_tbl,
			   &xinc_tbl,&yinc_tbl,
			   &rot_tbl,coordtype_tbl,&status)) 
    fits_report_error(stderr, status);


  

  rows_per_loop = 0;  /* use default optimum number of rows */
  offset = 0;         /* process all the rows */

  fits_iterate_data(ncols, cols, offset, rows_per_loop,
		    iter_evt, 0L, &status);

  /* update checksums */
  fits_write_chksum (fptr,&status);
  /* and close file */
  fits_close_file(fptr, &status);      /* all done */
  if (status)
    fits_report_error(stderr, status);  /* print out error messages */

  

}

int exit_fitsio (int status)
{
  fits_report_error(stderr, status);
  exit(1);
}


/*--------------------------------------------------------------------------*/
int iter_evt (long totalrows, long offset, long firstrow, long nrows,
             int ncols, iteratorCol *cols, void *user_strct ) 
{
  int ii, status = 0;
  
  /* Declare variables static to preserve their values between calls */
  /* 1) Data arrays: */ 
  static double *x, *y;
  static float *specweight;
  
  int i,j,ipix;

  double ra,dec,xx,yy;

  /*--------------------------------------------------------*/
  /*  Initialization procedures: execute on the first call  */
  /*--------------------------------------------------------*/
  if (firstrow == 1)
    {

      if (fits_iter_get_datatype(&cols[0]) != TDOUBLE ||
	  fits_iter_get_datatype(&cols[1]) != TDOUBLE ||
	  fits_iter_get_datatype(&cols[2]) != TFLOAT)
	return(-2);  /* bad data type */
      
      /* assign the input pointers to the appropriate arrays and null ptrs*/
      x       = (double *) fits_iter_get_array(&cols[0]);
      y       = (double *) fits_iter_get_array(&cols[1]);
      specweight = (float *) fits_iter_get_array(&cols[2]);
    }
  
  
  for (ii = 1; ii <= nrows; ii++)
    {

      status = 0;

      fits_pix_to_world (x[ii],y[ii], xrefval_tbl, yrefval_tbl,
			 xrefpix_tbl, yrefpix_tbl, xinc_tbl, yinc_tbl,
			 rot_tbl, coordtype_tbl, &ra,&dec,&status);
      if (status!=0) {
	specweight[ii]=0;
      } else {
	fits_world_to_pix (ra, dec, xrefval_img, yrefval_img,
			   xrefpix_img, yrefpix_img, 
			   xinc_img, yinc_img,
			   rot_img, coordtype_img, &xx,&yy,&status);
	if (status!=0) {
	  specweight[ii]=0;
	} else {
	  i = xx;
	  j = yy;
	  if (i>=1&&i<=nxexp&&j>=1&&j<=nyexp) {
	    ipix = (i-1)+(j-1)*nxexp;
	    specweight[ii]=expmap[ipix];
	  }
	}
      }
    }
  
  return(0);  /* return successful status */
}


int Load_Exp_Map (char *file) {
  fitsfile *fptr;
  int status = 0;
  int naxis; long naxes[10];
  long fpixel[10];
  float null; int anynull;
  
  if (fits_open_file(&fptr, file, READONLY, &status))
    fits_report_error(stderr, status);  /* print out error messages */
  
  
  if (fits_get_img_dim (fptr, &naxis, &status))
    fits_report_error(stderr, status);  /* print out error messages */
  
  if (naxis!=2) {
    fprintf (stderr,"Error: not a 2D image in %s\n",file);
    exit(1);
  }

  if (fits_get_img_size (fptr, 10, naxes, &status)) 
    fits_report_error(stderr, status);  /* print out error messages */
  
  nxexp=naxes[0];
  nyexp=naxes[1];

  expmap = (float *) malloc(sizeof(float)*nxexp*nyexp);
  if (expmap==NULL) {
    fprintf (stderr,"Error: cannot allocate memory in Load_Exp_Map");
    exit(1);
  }


  fpixel[0] = 1;
  fpixel[1] = 1;
  null = 0;
  if (fits_read_pix(fptr, TFLOAT, fpixel, nxexp*nyexp,
		    &null, expmap, &anynull, &status))
    fits_report_error(stderr, status);  /* print out error messages */

  if (fits_read_img_coord (fptr,
			   &xrefval_img,&yrefval_img,
			   &xrefpix_img,&yrefpix_img,
			   &xinc_img,&yinc_img,
			   &rot_img,coordtype_img,&status)) 
    fits_report_error(stderr, status);


  fits_close_file(fptr, &status);      /* all done */
  if (status) {
    fits_report_error(stderr, status);  /* print out error messages */
    exit (1);
  }
  fprintf (stderr,"exp map loaded OK\n");
}
  
  
 
