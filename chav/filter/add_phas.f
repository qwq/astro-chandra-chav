      implicit none
      character*200 evtfile, outfile
      
      integer unit, status, blksize
      integer phascol, gradecol, ccdidcol
      logical anyf
      
      integer nphot
      integer phas(3,3)
      integer phastot(3,3)
      real phastote(3,3)
      double precision tot
      
      integer iphot
      integer iargc, iarg, narg
      integer qflag
      logical QSTAT

      integer ibit
      integer ifirst, usestatus, usefltgrade, usegrade
      character*80 arg
      logical isdigit
      external isdigit
      real fptemp
      logical qfptemp
      character*80 comment, tform, tformphas
      integer iccd

      real ctitrail
      integer spthresh
      common /c_an55/ ctitrail, spthresh
      logical lray(32)

      integer i,j

      ibit = 20
      ifirst = 1
      usestatus = 0
      usefltgrade = 0
      usegrade = 0
      ctitrail = 0.027
      spthresh = 14

      qfptemp = .true.

      do i=1,3
        do j=1,3
          phastot(i,j)=0
        enddo
      enddo

c Parse command line arguments
      narg = iargc()
      if (narg.lt.1) call usage

      iarg = 1
      
c Loop over files in the command line
      call getarg (iarg,evtfile)

      status = 0
      call ftgiou (unit,status)
      
      call ftopen (unit,evtfile,1,blksize,status)
      call ftmnhd (unit,-1,'EVENTS',0,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      
*c Read focal plane temperature
*        if (qfptemp) then
*          call ftgkye (unit,'FP_TEMP',fptemp,comment,status)
*          if (status.ne.0) then
*            write (0,*) 'Focal plane temperature is not found in the header'
*            write (0,*) 'please set it explicitly with -110 or -120 switches'
*            call exit_fitsio ('FP_TEMP',status)
*          endif
*          fptemp = fptemp - 273.0
*        endif
        fptemp = -120           ! it does not matter, currently
      
        call ftgnrw (unit,nphot,status)

        call ftgcno (unit,.false.,'PHAS',phascol,status)
        write (tformphas,'(''TFORM'',i3)') phascol
        call rmblanks (tformphas)
        call ftgkys (unit,tformphas,tform,comment,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
        if (tform(1:1).ne.'9') then
          write (0,'(a)') 'ERROR: doesn''t look like a 3x3 PHAS column'
          call exit(1)
        endif
        
      
        do iphot=1,nphot
          call ftgcvj (unit,phascol,iphot,1,9,0,phas,anyf,status)
          if (status.ne.0) call exit_fitsio (evtfile,status)
          do i=1,3
            do j=1,3
              phastot(i,j)=phastot(i,j)+phas(i,j)
            enddo
          enddo
        enddo
        

        call ftclos (unit,status)
        call ftfiou (unit,status)
        
        if (status.ne.0) call exit_fitsio (evtfile,status)

        call getarg (narg,outfile)
        
        tot = 0
        do i=1,3
          do j=1,3
            tot = tot + phastot(i,j)
          enddo
        enddo

        tot = max(tot,1.0d0)

        do i=1,3
          do j=1,3
            phastote(i,j) = phastot(i,j)/tot
          enddo
        enddo


      call write_fits_image (outfile,phastote,3,3,'e','e')

      call exit(0)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine exit_fitsio (message,status)
      implicit none
      character*(*) message
      integer status

      call perror_fitsio (message,status)
      call exit(1)
      return
      end

      subroutine perror_fitsio (message,status)
      implicit none
      character*(*) message
      integer lnblnk
      integer status
      character*80 errtext
      
      if (status.ne.0) then
        call FTGERR (status,errtext)
        do while (errtext.ne.' ')
          write(0,'(a,a,a)')message(1:lnblnk(message)),': '
     ~        ,errtext(1:lnblnk(errtext))
          call FTGMSG (errtext)
        enddo
      endif
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine usage
      write (0,*)
     ~    'Usage: clean55 [-bN | -fg | -g] [-ct coeff] '/
     ~    /'[-spth sp_thresh] file1.fits file2.fits ...'
      call exit(1)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      logical function isdigit (c)
      implicit none
      character*1 c
      integer status, i

      read (c,'(i1)',iostat=status) i
      isdigit = status .eq. 0
      return
      end

      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rmblanks (s)
c Remove blanks from a string
      implicit none
      character*(*) s
      
      integer lnblnk,len,i,n

      len=lnblnk(s)
      n=0
      do i=1,len
        if(s(i:i).ne.' '.and.s(i:i).ne.'\t')then
          n=n+1
          s(n:n)=s(i:i)
        endif
      enddo
      
      do i=n+1,len
        s(i:i)=' '
      enddo
      
      return
      end
