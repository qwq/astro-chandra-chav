/* Must be compiled with the CFITSIO library and header files */
/* If ReadPar library is not available, compile with -DNOREADPAR */
/* Usage:
 (with ReadPar)
  badpixfilter evtfile=dirty_evt.fits o=clean_evt.fits badpixfiler=badpix.dat

 (without ReadPar)
  badpixfilter dirty_evt.fits clean_evt.fits badpix.dat
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include <math.h>
#ifndef NOREADPAR
#include <readpar.h>
#endif

int main (int argc, char *argv[]) {
  extern int iter_evt ();/* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */
  fitsfile *ifptr, *ofptr, *maskptr;
  iteratorCol cols[100]; /* STRUCTURE USED BY THE ITERATOR FUNCTION */
  int ncols;
  long rows_per_loop, offset;
  char evtfile[200],outfile[200],maskfile[200];
  char buff[1000];
  int tfields[1000];
  char ttype[1000][80], tform[1000][80], tunit[1000][80];
  char extname[80], comment[80];
  long pcount,nrows;
  unsigned char databuf[16384];
  int i,ix,iy,N;
  double X,Y,RA,DEC;
  int xcol, ycol;
  int status, match, exact, anynul, hdutype;
  long naxes[100]; int nfound;
  long nread, nreadthistime;
  long irow, nout;
  short nulli = 0;

  short *mask;
  int nx, ny;
  int goodpix;
  double crpix1,crpix2,crval1,crval2,cdelt1,cdelt2,crota2;
  char ctype[80];
  double ecrpix1,ecrpix2,ecrval1,ecrval2,ecdelt1,ecdelt2,ecrota2;
  char ectype[80];


#ifndef NOREADPAR
  Ini_ReadPar(argc,argv);
  
  /*  get_command_line_par ("evtfile",evtfile);
  get_command_line_par ("o",outfile);
  get_command_line_par ("badpixfilter",badpixfile); */
  
  get_cl_or_read_par ("evtfile","Input events file",evtfile);
  get_cl_or_read_par ("o","output file",outfile);
  get_cl_or_read_par ("maskfilter","mask pixel filter",maskfile);

  if (!defined(evtfile)) {
    fprintf (stderr,"evtfile=?\n");
    exit(1);
  }
  if (!defined(outfile)) {
    fprintf (stderr,"o=?\n");
    exit(1);
  }
  if (!defined(maskfile)) {
    fprintf (stderr,"maskfilter=?\n");
    exit(1);
  }
#else
  if (argc!=4) {
    fprintf (stderr,"Usage: %s dirty_evt.fits clean_evt.fits mask.fits\n",argv[0]);
    exit(1);
  }
  strcpy(evtfile,argv[1]);
  strcpy(outfile,argv[2]);
  strcpy(maskfile,argv[3]);
#endif

  /* Load mask and its wcs */
  status = 0; 
  if (fits_open_file(&maskptr, maskfile, READONLY, &status)) /* open file */
    exit_fitsio(status);

  if (fits_get_img_size(maskptr, 2, naxes, &status))
    exit_fitsio (status);
  nx = naxes[0];
  ny = naxes[1];
  mask = (short *) malloc(sizeof(int)*nx*ny);

  if (fits_read_img (maskptr,TSHORT,1,nx*ny,&nulli,mask,&anynul,&status)) 
    exit_fitsio (status);
  
  if (fits_read_img_coord(maskptr,&crval1,&crval2,&crpix1,&crpix2,
                          &cdelt1,&cdelt2,&crota2,ctype,&status))
    exit_fitsio(status);
  


  /*open evt files and iterate */
  status = 0; 
  
  if (fits_open_file(&ifptr, evtfile, READONLY, &status)) /* open file */
    exit_fitsio(status);

  if (fits_create_file(&ofptr, outfile, &status)) /* create new FITS file */
    exit_fitsio(status);
  
  strcpy(extname,"junk");
  fits_compare_str (extname,"events", CASEINSEN, &match, &exact);
  while (!match) {
    /* copy the extension to output file */
    if (
	fits_copy_hdu (ifptr,ofptr,0,&status)
	)
      exit_fitsio(status);
    
    /* move to the next extension */
    if (
	fits_movrel_hdu (ifptr,1,&hdutype,&status)
	) {
      fprintf (stderr,"ERROR: EVENTS extension not found in the input file!\n");
      exit_fitsio(status);
    }
    
    if (hdutype==BINARY_TBL) {
      fits_read_key (ifptr,TSTRING,"EXTNAME",extname,comment,&status);
      fits_get_num_rows (ifptr,&nrows,&status);
      if (status)
	exit_fitsio(status);
    }
    fits_compare_str (extname,"events", CASEINSEN, &match, &exact);
  }
  
  /* we can be here only if we are in the events extension */
  if (fits_get_rowsize(ifptr,&nread,&status)) 
    exit_fitsio(status);
  if (fits_read_keys_lng(ifptr, "NAXIS", 1, 2, naxes, &nfound, &status) )
    exit_fitsio(status);

  fits_get_colnum(ifptr,CASEINSEN,"X",&xcol,&status);
  fits_get_colnum(ifptr,CASEINSEN,"Y",&ycol,&status);
  if (status) 
    exit_fitsio(status);
  
  if (fits_read_tbl_coord(ifptr,xcol,ycol,
                          &ecrval1,&ecrval2,&ecrpix1,&ecrpix2,
                          &ecdelt1,&ecdelt2,&ecrota2,ectype,&status))
    exit_fitsio(status);
  status = 0;
  fits_close_file (maskptr,&status);


  /* Copy header */
  if (fits_copy_header(ifptr,ofptr,&status))
    exit_fitsio(status);
  


  nout = 0;
  irow = 0;

  while (irow<nrows) {
    nreadthistime = nread;
    if (nrows-irow<nreadthistime) nreadthistime = nrows-irow;
    for (i=0;i<nreadthistime;i++) {
      irow ++;
      if (fits_read_tblbytes(ifptr,irow,1,naxes[0],databuf,&status))
	exit_fitsio(status);

      N = 0;
      fits_read_col (ifptr,TDOUBLE,xcol,irow,1,1,&N,&X,&anynul,&status);
      fits_read_col (ifptr,TDOUBLE,ycol,irow,1,1,&N,&Y,&anynul,&status);
      if (status)
	exit_fitsio(status);

      /* fprintf (stderr,"%lf %lf %lf %lf\n",X,Y,crpix1,ecrpix1); */
                                                                
      if (fits_pix_to_world (X,Y,ecrval1,ecrval2,ecrpix1,ecrpix2,
                             ecdelt1,ecdelt2,ecrota2,ectype,&RA,&DEC,&status))
        exit_fitsio(status);

      if (fits_world_to_pix (RA,DEC,crval1,crval2,crpix1,crpix2,
                             cdelt1,cdelt2,crota2,ctype,&X,&Y,&status))
        exit_fitsio(status);
      ix = floor(X+0.5);
      iy = floor(Y+0.5);
      goodpix = 0;
      if (ix>=1&&ix<=nx&&iy>=1&&iy<=ny) {
        if (mask[(iy-1)*nx+(ix-1)]==1) {
          goodpix=1;
        }
      }

      /* fprintf (stderr,"%d %d %lf %lf %lf %lf\n",ix,iy,X,Y,RA,DEC); */


      if (goodpix) {
	nout ++;
	if (fits_write_tblbytes(ofptr,nout,1,naxes[0],databuf,&status))
	  exit_fitsio(status);
      }
    }
  }
  if ( fits_update_key(ofptr, TLONG, "NAXIS2", &nout, 0, &status) )
    exit_fitsio(status);
  if ( fits_write_chksum (ofptr,&status) )
    exit_fitsio(status);
  
  /* Copy the rest */
  while ( ! fits_movrel_hdu (ifptr,1,&hdutype,&status) ) {
    if (
	fits_copy_hdu (ifptr,ofptr,0,&status)
	)
      exit_fitsio(status); 
  }
  status = 0;
  fits_close_file (ifptr,&status);
  status = 0;
  fits_close_file (ofptr,&status);

  free(mask);

  exit(0);
}

int exit_fitsio (int status)
{
  fits_report_error(stderr, status);
  exit(1);
}









