      subroutine load_trapmaps (ctifile)
      implicit none
      character ctifile*(*)

      include 'trapmaps.inc'
      
      integer unit, status
      integer iccd, nfound, nrows, i, chipx_lo, chipx_hi, chipy_lo, chipy_hi
      integer col_ccd_id, col_chipx_lo, col_chipy_lo, col_chipx_hi,
     ~    col_chipy_hi, col_npoints, col_pha, col_volume
      character name*80, comment*80
      logical anyf

      status = 0
      call ftgiou (unit,status)
      call ftnopn (unit,ctifile,0,status)

c // load lookup tables for volume-pha info
      call FTMNHD (unit,-1,'AXAF_CTI',0,status)
      if (status.ne.0) call exit_fitsio (ctifile,status)

c ..  Load trailing fractions
      call FTGKNE (unit,'FRCTRLY',0,10,fract1,nfound,status)
      if (status.ne.0) call exit_fitsio (ctifile,status)
      if (nfound.ne.10) then
        write (0,*) 'expected 10 FRCTRLY keys, found ',nfound
        call exit (1)
      endif
        
*      call FTGKNE (unit,'VFTRLY',0,10,fract2,nfound,status)
*      if (status.ne.0) call exit_fitsio (ctifile,status)
*      if (nfound.ne.10) then
*        write (0,*) 'expected 10 VFTRLY keys, found ',nfound
*        call exit (1)
*      endif

c .. load PHA-Volume relation
      call FTGCNO (unit,.false.,'CCD_ID',  col_ccd_id,status)
      call FTGCNO (unit,.false.,'CHIPX_LO',col_chipx_lo,status)
      call FTGCNO (unit,.false.,'CHIPX_HI',col_chipx_hi,status)
      call FTGCNO (unit,.false.,'CHIPY_LO',col_chipy_lo,status)
      call FTGCNO (unit,.false.,'CHIPY_HI',col_chipy_hi,status)
      call FTGCNO (unit,.false.,'NPOINTS',col_npoints,status)
      call FTGCNO (unit,.false.,'PHA',col_pha,status)
      call FTGCNO (unit,.false.,'VOLUME_Y',col_volume,status)

      if (status.ne.0) call exit_fitsio (ctifile,status)

      call FTGNRW (unit,nrows, status)
      do i=1,nrows
        call FTGCVJ (unit,col_ccd_id,i,1,1,0,iccd,anyf,status)
        call FTGCVJ (unit,col_chipx_lo,i,1,1,0,chipx_lo,anyf,status)
        call FTGCVJ (unit,col_chipx_hi,i,1,1,0,chipx_hi,anyf,status)
        call FTGCVJ (unit,col_chipy_lo,i,1,1,0,chipy_lo,anyf,status)
        call FTGCVJ (unit,col_chipy_hi,i,1,1,0,chipy_hi,anyf,status)
        if (status.ne.0) call exit_fitsio (ctifile,status)
        
        if ( chipx_lo.ne.1 .or. chipx_hi.ne.1024 .or. chipy_lo.ne.1
     ~      .or. chipy_hi.ne.1024 ) then
          write (0,*)
     ~        'expect volume-pha relation for whole chips, without subsections'
          call exit(1)
        endif

        call FTGCVJ (unit,col_npoints,i,1,1,0,volume_n(iccd),anyf,status)

        call FTGCVE (unit,col_pha,i,1,volume_n(iccd),0.0,
     ~      volume_pha(1,iccd),anyf,status)
        call FTGCVE (unit,col_volume,i,1,volume_n(iccd),0.0,
     ~      volume_volume(1,iccd),anyf,status)
        if (status.ne.0) call exit_fitsio (ctifile,status)
      enddo

      do iccd=0,9
        do i=1,volume_n(iccd)
          volume_pha(i,iccd) = log(max(volume_pha(i,iccd),0.1))
          volume_volume(i,iccd) = log(max(volume_volume(i,iccd),0.1))
        enddo
      enddo

c // Now, load the actual trap maps
      do iccd=0,9
        write (name,'(''AXAF_PCTI_IMAGE'',i1)') iccd
        call FTMNHD (unit,-1,name,0,status)
        if (status.ne.0) call exit_fitsio (ctifile,status)
        ! check that we have the right ccd_id
        call FTGKYJ (unit,'CCD_ID',i,comment,status)
        if (i.ne.iccd) then
          call exiterror ('I''m lost in the AXAF_PCTI_IMAGE extensions')
        endif
        call FTG2DE (unit,0,0.0,1024,1024,1024,trapmap(1,1,iccd),anyf,status)
*        call saoimage (trapmap(1,1,iccd),1024,1024,'e')
        if (status.ne.0) call exit_fitsio (ctifile,status)
      enddo

c // close and cleanup
      call ftclos(unit,status)
      call ftfiou(unit,status)
      if (status.ne.0) call exit_fitsio (ctifile,status)

      return
      end

