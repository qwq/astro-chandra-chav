  program augain

    implicit none
    
    interface
       subroutine calc_augain (e,iccd,inode,enominal)
         real (kind=4), intent(in), dimension(:) :: e
         real, intent(in) :: enominal
         integer, intent(in) :: iccd, inode
       end subroutine calc_augain
    end interface

    integer (kind=1), dimension(:), allocatable :: ccd_id, node_id
    real (kind=4), dimension(:), allocatable :: energy
    real :: enominal

    character (len=200) :: evtfile

    logical, external :: defined
    integer nrows, iccd, node

!   CFITSIO stuff
    integer (kind=4) :: unit, status, colnum
    character (len=200) :: comment
    logical :: anyf

! -----------------------------------------------------------------

    call get_cl_par ('evtfile',evtfile)
    if ( .not. defined(evtfile) ) call exiterror ('evtfile=??')

    call get_cl_par ('enominal',comment)
    if ( defined(comment) ) then
      read (comment,*) enominal
      if (enominal<20.0) enominal=enominal*1000
    else
      enominal = 9696.54
    endif

! ini cfitsio
    status = 0
    call ftgiou (unit,status)
    call ftnopn (unit,evtfile,0,status)
    if (status .ne. 0 ) call exit_fitsio (evtfile,status)

! move to the EVENTS extension in the input file
    call ftmnhd (unit,-1,'EVENTS',0,status)
    if (status .ne. 0 ) call exit_fitsio (evtfile,status)

    call ftgnrw (unit,nrows,status)
    if (status .ne. 0 ) call exit_fitsio ('',status)

! allocate memory
    allocate (ccd_id(nrows),node_id(nrows),energy(nrows))
    
! read columns
    call ftgcno (unit,0,'ccd_id',colnum,status)
    call ftgcvb (unit,colnum,1,1,nrows,0_1,ccd_id,anyf,status)
    if (status.ne.0) call exit_fitsio ('evt: ccd_id:',status)

    call ftgcno (unit,0,'node_id',colnum,status)
    call ftgcvb (unit,colnum,1,1,nrows,0_1,node_id,anyf,status)
    if (status.ne.0) call exit_fitsio ('evt: node_id:',status)

    call ftgcno (unit,0,'energy',colnum,status)
    call ftgcve (unit,colnum,1,1,nrows,0.0,energy,anyf,status)
    if (status.ne.0) call exit_fitsio ('evt: energy:',status)

    

    call ftclos (unit,status)
    call ftfiou (unit,status)
    if (status .ne. 0 ) call exit_fitsio (evtfile,status)
    
    do iccd=0,9
      !if (iccd==5.or.iccd==7) exit
      do node=0,3
        call calc_augain (pack(energy,MASK=ccd_id==iccd.and.node_id==node),&
             iccd,node,enominal)
      enddo
    enddo


  end program augain

! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  subroutine calc_augain (e,iccd,inode,enominal)
    implicit none
    real (kind=4), intent(in), dimension(:) :: e
    real, intent(in) :: enominal
    integer, intent(in) :: iccd, inode
    integer :: ne, i, w1,w2,w3, ibin, imax(1), iter
    double precision, dimension(-767:768) :: f,f1,f2
    double precision :: w, emean

    ne=size(e)
    if (ne<1000) return

! make the flux array
    f = 0
    do i=1,ne
      ibin = nint(e(i)-enominal)
      if (ibin>-768.and.ibin<769) f(ibin)=f(ibin)+1
    enddo
    
! convolve it with the boxes to find the maximum
    w2=nint(23.0/12.0*128)
    w1=nint(w2+2*(128/12.0))
    w3=nint(w2-2*(128/12.0))

    call conv_rect1_1D_d (f,f1,768*2,w1)
    call conv_rect1_1D_d (f1,f2,768*2,w2)
    call conv_rect1_1D_d (f2,f1,768*2,w3)

! find the location of the maximum
    imax = maxloc(f1)-768

! Iterate by computing the average in a -128:128 window
    ibin = imax(1)
    do iter=1,3
      w = 0
      emean = 0
      do i=max(ibin-128,-767),min(ibin+128,768)
        emean = emean + f(i)*i
        w = w + f(i)
      enddo
      emean = emean / w
      ibin = nint(emean)
    enddo

!    print*,iccd,inode,(emean+enominal)/1000.0
    print '(''augain_'',i1,''_'',i1,''='',f7.5)', &
         iccd,inode, enominal/(emean+enominal)
  end subroutine calc_augain

!---------------------------------------------------------------------
  subroutine conv_rect1_1D_d (row,drow,n,nrec)
!c% Double precision 1-D box convolution      
    implicit none
    integer n,nrec
    double precision row(n), drow(n)
    double precision drowsum
    double precision w
    integer ns1,ns2
    integer j,jj,jj1,jj2
    
    ns1=nrec/2
    ns2=nrec/2

!c..Compute first element        
    w=0.0
    do j=1-ns1,1+ns2
      if(j.le.0)then
        jj=j+n
      else
        jj=j
      endif
      w=w+row(jj)
    enddo
    drowsum = w
    drow(1) = drowsum
    
!c..Compute other elements          
    do j=2,n
      jj1=j-ns1-1
      if(jj1.le.0)then
        jj1=jj1+n
      endif
      
      jj2=j+ns2
      if(jj2.gt.n)then
        jj2=jj2-n
      endif
      
      drowsum = drowsum +row(jj2)-row(jj1)
      drow(j)=drowsum
    enddo
      
    return
  end subroutine conv_rect1_1D_d
