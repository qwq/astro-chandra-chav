      implicit none
      character ctifile*200, evtfile*200, grdimgfile*200, gainfile*200

      logical defined

      
      call get_cl_par ('ctifile',ctifile)
      if (.not.defined(ctifile)) then
        call exiterror ('ctifile=???')
      endif

      call get_cl_par ('evtfile',evtfile)
      if (.not.defined(evtfile)) then
        call exiterror ('evtfile=???')
      endif

      call get_cl_par ('grdimgfile',grdimgfile)
      if (.not.defined(grdimgfile)) then
        call exiterror ('grdimgfile=???')
      endif

      call get_cl_par ('gainfile',gainfile)
      if (.not.defined(gainfile)) then
        call exiterror ('gainfile=???')
      endif

      call load_trapmaps(ctifile)

      call load_grdimages(grdimgfile)

      call load_gains(gainfile)

      call process_evtfile_demi (evtfile)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
      subroutine process_evtfile_demi (evtfile)
      implicit none
      character evtfile*(*)

      include 'trapmaps.inc'
      include 'grdimages.inc'
      include 'gains.inc'

      integer unit, status
      
      integer col_ccd_id, col_chipx, col_chipy, col_fltgrade, col_pha
      integer ccd_id, chipx, chipy, fltgrade, pha
      logical anyf

      integer iter
      integer irow, i, j, nrows, ii, ibin
      real xpha, volume, deltaQ, deltaQ0, xpha0
      real volume1, volume2, volume3, volume_sh, voff, voffs
      real gimg(3,3)
      real energy
      real rand
      

      status = 0
      call ftgiou (unit,status)
      call ftnopn (unit,evtfile,1,status)
      
c // Go to events extension
      call FTMNHD (unit,-1,'EVENTS',0,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      
c // Column information
      call FTGCNO (unit,.false.,'CCD_ID',  col_ccd_id,status)
      call FTGCNO (unit,.false.,'CHIPX',col_chipx,status)
      call FTGCNO (unit,.false.,'CHIPY',col_chipy,status)
      call FTGCNO (unit,.false.,'FLTGRADE',col_fltgrade,status)
      call FTGCNO (unit,.false.,'PHA',col_pha,status)

      if (status.ne.0) call exit_fitsio (evtfile,status)

      call FTGNRW (unit,nrows, status)
      
c // process rows
      do irow=1,nrows
        if ((irow/10000)*10000.eq.irow) then
          write (0,'("\r",i3,"%",$)') nint(irow*100.0/nrows)
          call flush(0)
        endif
        call FTGCVJ (unit,col_ccd_id,irow,1,1,0,ccd_id,anyf,status)
        call FTGCVJ (unit,col_chipx,irow,1,1,0,chipx,anyf,status)
        call FTGCVJ (unit,col_chipy,irow,1,1,0,chipy,anyf,status)
        call FTGCVJ (unit,col_fltgrade,irow,1,1,0,fltgrade,anyf,status)
        call FTGCVJ (unit,col_pha,irow,1,1,0,pha,anyf,status)

c // first, "light" correction
        xpha = pha
        call lin_interpolate (
     ~      volume_pha(1,ccd_id),volume_volume(1,ccd_id),volume_n(ccd_id),
     ~      log(xpha),volume )
        volume = exp(volume)    ! because I took log(pha) and log(volume) 
                                ! when loading trap maps

        deltaQ = volume*trapmap(chipx,chipy,ccd_id)
        deltaQ0 = deltaQ
        xpha = pha + deltaQ

        xpha0 = xpha


c // find approximate energy
        ibin = GAINBIN(chipx,chipy,ccd_id)
        if (ibin.le.0) then
          write (0,*) 'no gain data for ccd=',ccd_id,chipx,chipy
          call exit(1)
        endif
        call lin_interpolate (GPHA(1,ibin),GENERGY(1,ibin),NGAINPOINTS(ibin),
     ~      xpha,energy)

        
        do iter=1,3
c // make average grade image for this energy
          do i=1,3
            do j=1,3
              gimg(i,j) = grdimg(i,j,fltgrade)
            enddo
          enddo
          
          voff = 1-gimg(2,2)
          voffs = voff*(max(1.0,energy)/5898.0)**eslope(fltgrade)
          voffs = min(voffs,1.0)

          do i=1,3
            do j=1,3
              gimg(i,j) = gimg(i,j)*voffs/voff*xpha0
              gimg(i,j) = max(gimg(i,j),0.0)
            enddo
          enddo
          gimg(2,2) = (1-voffs)*xpha0

c // find correction for each column
          deltaQ = 0
          do i=1,3
            ii = chipx + (i-2)
            
            if (ii.ge.1.and.ii.le.1024) then
              if (gimg(i,1).gt.13.0) then
                call lin_interpolate (
     ~              volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~              ,volume_n(ccd_id),log(gimg(i,1)),volume1)
                volume1 = exp(volume1)
              else
                volume1 = 0
              endif
              
              if (gimg(i,2).gt.13.0) then
                call lin_interpolate (
     ~              volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~              ,volume_n(ccd_id),log(gimg(i,2)),volume2)
                volume2 = exp(volume2)
              else
                volume2 = 0
              endif
              
              if (gimg(i,3).gt.13.0) then
                call lin_interpolate (
     ~              volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~              ,volume_n(ccd_id),log(gimg(i,3)),volume3)
                volume3 = exp(volume3)
              else
                volume3 = 0
              endif
              

              volume_sh = volume1 + max(volume2-volume1,0.0) + 
     ~            max(volume3-volume2-volume1,0.0)

              deltaQ = deltaQ + trapmap(ii,chipy,ccd_id) * volume_sh
c Redeposition (works poorly)
!     ~            + fract1(ccd_id)*(volume3-volume2)*trapmap(ii,chipy,ccd_id)

!     ~          (volume2+max(0.0,(volume3-volume2)))
            endif
          enddo
        
c
c dQ = n1*V1+n2*V2-n1*V1+f(n3*v3-n2*V2)   from Glenn's memo
c
          xpha0 = pha + deltaQ
*          if (fltgrade.eq.0) print*,fltgrade,iter,xpha0,gimg(2,2),deltaQ,deltaQ0
        enddo

        xpha = pha + deltaQ + (rand(0)-0.5)

*        print*,energy,pha,fltgrade, deltaQ, deltaQ0, voffs

        pha = nint(xpha)
        
        call FTPCLJ (unit,col_pha,irow,1,1,pha,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
      enddo
      write (0,*)

c // close and cleanup
      call ftclos(unit,status)
      call ftfiou(unit,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)

      return
      end
