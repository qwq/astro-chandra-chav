      subroutine load_grdimages (grdimgfile)
      implicit none
      character grdimgfile*(*)

      include 'grdimages.inc'
      
      integer unit, status
      logical anyf
      integer nfound

      status = 0
      call ftgiou (unit,status)
      call ftnopn (unit,grdimgfile,0,status)

c ..  Load E-scalings
      
      call FTGKNE (unit,'ESCL',0,256,eslope,nfound,status)
      if (status.ne.0) call exit_fitsio (grdimgfile,status)
      if (nfound.ne.256) then
        write (0,*) 'expected 256 ESCL keys, found ',nfound
        call exit (1)
      endif

      call ftg2de(unit,0,0.0,9,9,256,grdimg,anyf,status)


c // close and cleanup
      call ftclos(unit,status)
      call ftfiou(unit,status)
      if (status.ne.0) call exit_fitsio (grdimgfile,status)

      return
      end

