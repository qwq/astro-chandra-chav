#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include <math.h>

main(int argc, char *argv[])
{
  extern int pha_to_pi ();/* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */
  fitsfile *fptr;
  iteratorCol cols[100]; /* STRUCTURE USED BY THE ITERATOR FUNCTION */
  int ncols;
  long rows_per_loop, offset;
  
  int status, nkeys, keypos, hdutype, ii, jj;
  int iarg;
  
  if (argc==1) {
    fprintf (stderr,"Usage: %s file1.fits [file2.fits ...]\n",argv[0]);
    exit (1);
  }
  
  for (iarg=1;iarg<argc;iarg++) { /* Loop over file names */
    
    status = 0; 
    
    fits_open_file(&fptr, argv[iarg], READWRITE, &status); /* open file */
    
    /* move to the EVENTS binary table extension */
    if (fits_movnam_hdu(fptr, BINARY_TBL, "EVENTS", 0, &status) )
      fits_report_error(stderr, status);    /* print out error messages */
    
    ncols  = 6;   /* number of columns
		      We need: "ccd_id", "node_id", "chipy", "pha" and 
		      want to output "energy" and "PI"
		   */
    
    /* define input column structure members for the iterator function */
    fits_iter_set_by_name(&cols[0], fptr, "ccd_id", TSHORT,  InputCol);
    fits_iter_set_by_name(&cols[1], fptr, "node_id",TSHORT,  InputCol);
    fits_iter_set_by_name(&cols[2], fptr, "chipy",  TSHORT,  InputCol);
    fits_iter_set_by_name(&cols[3], fptr, "pha",    TLONG,   InputCol);
    fits_iter_set_by_name(&cols[4], fptr, "energy", TFLOAT,  InputOutputCol);
    fits_iter_set_by_name(&cols[5], fptr, "pi",     TLONG,   InputOutputCol);

    rows_per_loop = 0;  /* use default optimum number of rows */
    offset = 0;         /* process all the rows */

    /* apply the rate function to each row of the table */
    fits_iterate_data(ncols, cols, offset, rows_per_loop,
                      pha_to_pi, 0L, &status);

    fits_close_file(fptr, &status);      /* all done */

    if (status)
      fits_report_error(stderr, status);  /* print out error messages */
  }
  exit(0);
}
/*--------------------------------------------------------------------------*/
int pha_to_pi (long totalrows, long offset, long firstrow, long nrows,
             int ncols, iteratorCol *cols, void *user_strct ) 
{
  int ii, status = 0;
  
  /* Declare variables static to preserve their values between calls */
  /* 1) Data arrays: */ 
  static short *ccd_id, *node_id, *chipy;
  static long *pha, *pi;
  static float *energy;
  
  /* 2) Cal data: */
  static int firstcall = 1;
  static float a1[10][4], b1[10][4], c1[10][4],
    a2[10][4], b2[10][4], c2[10][4],
    x0[10][4], x[10][4];
  
  /* CTI correction coefficients file */
  char ctifile[]="/data/alexey1/chandra/CAL/cti_data.txt";
  FILE *cti;
  char line[1000];

  /* misc vars */
  float A1,B1,C1,A2,B2,C2,X0,X,PHA,CTI,EPS,Y;
  char date[80], comment[80]; int time;
  int iccd,inode;
  
  
  
  /* Read in the calibration data */
  if (firstcall) {
    firstcall = 0;
    printf ("Reading CTI corr. coeff. from %s\n",ctifile);
    cti=fopen(ctifile,"r");
    if (cti==NULL) {
      perror (ctifile);
      exit(1);
    }
    for (iccd=0;iccd<10;iccd++) {
      for (inode=0;inode<4;inode++) {
	a1[iccd][inode]=0;
	b1[iccd][inode]=0;
	c1[iccd][inode]=0;
	a2[iccd][inode]=0;
	b2[iccd][inode]=0;
	c2[iccd][inode]=0;
	x0[iccd][inode]=0;
	x[iccd][inode]=0;
      }
    }
    while ( fgets(line,1000,cti) != NULL ) {
      if (sscanf(line,"%d %d %e %e %e %e %e %e %e %e",&iccd,&inode,&A1,&B1,&C1,
		 &A2,&B2,&C2,&X0,&X)==10) {
	a1[iccd][inode]=A1;
	b1[iccd][inode]=B1;
	c1[iccd][inode]=C1;
	a2[iccd][inode]=A2;
	b2[iccd][inode]=B2;
	c2[iccd][inode]=C2;
	x0[iccd][inode]=X0;
	x[iccd][inode]=X;
      }
    }
  }

  /*--------------------------------------------------------*/
  /*  Initialization procedures: execute on the first call  */
  /*--------------------------------------------------------*/
  if (firstrow == 1)
    {
      if (ncols != 6)
	return(-1);  /* number of columns incorrect */
      
      if (fits_iter_get_datatype(&cols[0]) != TSHORT ||
	  fits_iter_get_datatype(&cols[1]) != TSHORT ||
	  fits_iter_get_datatype(&cols[2]) != TSHORT ||
	  fits_iter_get_datatype(&cols[3]) != TLONG  ||
	  fits_iter_get_datatype(&cols[4]) != TFLOAT ||
	  fits_iter_get_datatype(&cols[5]) != TLONG )
	return(-2);  /* bad data type */

      /* assign the input pointers to the appropriate arrays and null ptrs*/
      ccd_id       = (short *) fits_iter_get_array(&cols[0]);
      node_id      = (short *) fits_iter_get_array(&cols[1]);
      chipy        = (short *) fits_iter_get_array(&cols[2]);
      pha          = (long  *) fits_iter_get_array(&cols[3]);
      energy       = (float *) fits_iter_get_array(&cols[4]);
      pi           = (long  *) fits_iter_get_array(&cols[5]);

    }
  
  /*--------------------------------------------*/
  /*  Main loop: process all the rows of data */
  /*--------------------------------------------*/

  /*  NOTE: 1st element of array is the null pixel value!  */
  /*  Loop from 1 to nrows, not 0 to nrows - 1.  */

  for (ii = 1; ii <= nrows; ii++)
    {
      iccd=ccd_id[ii];
      inode=node_id[ii];
      if (x0[iccd][inode]!=0.0||x[iccd][inode]!=0.0) { /* have cal data for
							  this node */
	A1=a1[iccd][inode]; B1=b1[iccd][inode]; C1=c1[iccd][inode];
	A2=a2[iccd][inode]; B2=b2[iccd][inode]; C2=c2[iccd][inode];
	X0=x0[iccd][inode]; X = x[iccd][inode]/1000.0;
	PHA=pha[ii]; Y=chipy[ii];
	
	CTI = A1 + B1*PHA + C1*PHA*PHA;
	EPS = A2 + B2*PHA + C2*PHA*PHA;
	
	PHA = (1.0+CTI*Y+EPS*Y*Y) * PHA;
	energy[ii] = (X0+PHA)/X;
	pi[ii]=floor(energy[ii]/14.6);
	if (pi[ii]>1024) pi[ii]=1024;
	if (pi[ii]<0)    pi[ii]=0;
      }
    }
  
  /*----------------------------------------------------------------*/
  /*  End of precessing procedures:  after processing all the rows  */
  /*----------------------------------------------------------------*/
  
  if (firstrow + nrows - 1 == totalrows)
    {
      /*  Put history records */
      fits_get_system_time (date,&time,&status);

      fits_write_history (cols[0].fptr,
			  "---------------------------------------------",
			  &status);
      strcpy(comment,"PHA-TO-PI CTI CORRECTION done ");
      strcat(comment,date); strcat(comment," using cal file:");
      fits_write_history (cols[0].fptr,comment,&status);
      fits_write_history (cols[0].fptr,ctifile,&status);
    }
  return(0);  /* return successful status */
}

