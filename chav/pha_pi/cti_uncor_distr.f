      implicit none
      character ctifile*200, chipreg*200, grdimgfile*200, gainfile*200,
     ~    gradedistrfile*200

      logical defined

      real chipmap(1024,1024,0:9)
      integer cxmin, cxmax, cymin, cymax, cid
      integer i,j,ccdid
      real E, w

      real gradedistr(0:255)
      integer status, newunit, unit
      integer n
      
      call get_cl_par ('ctifile',ctifile)
      if (.not.defined(ctifile)) then
        call exiterror ('ctifile=???')
      endif

      call get_cl_par ('grdimgfile',grdimgfile)
      if (.not.defined(grdimgfile)) then
        call exiterror ('grdimgfile=???')
      endif

      call get_cl_par ('gainfile',gainfile)
      if (.not.defined(gainfile)) then
        call exiterror ('gainfile=???')
      endif

      do ccdid = 0,9
        do j=1,1024
          do i=1,1024
            chipmap(i,j,ccdid)=0
          enddo
        enddo
      enddo
      call get_cl_par ('chipreg',chipreg)
      if (defined(chipreg)) then
        n = lnblnk(chipreg)
        do i=1,n
          if (chipreg(i:i).eq.':'.or.chipreg(i:i).eq.',') then
            chipreg(i:i)=' '
          endif
        enddo
        read (chipreg,*) cxmin,cxmax,cymin,cymax,cid
        do ccdid = cid,cid
          do j=cymin,cymax
            do i=cxmin,cxmax
              chipmap(i,j,ccdid)=1
            enddo
          enddo
        enddo
      else
        call exiterror ('chipreg=???')
      endif

      call get_cl_par ('gradedistrfile',gradedistrfile)
      if (.not.defined(gradedistrfile)) then
        call exiterror ('gradedistrfile=???')
      endif
      do i=0,255
        gradedistr(i)=0
      enddo
      unit = newunit()
      status = 0
      open (unit,file=gradedistrfile,status='old')
      do while (status.eq.0)
        read (unit,*,iostat=status) i, w
        if (status.eq.0) then
          if (i.ge.0.and.i.lt.256) then
            gradedistr(i)=w
          else
            call exiterror ('grade must be >= 0 and < 256')
          endif
        endif
      enddo
      close (unit)

      call get_parameter_value ('E',E,'e')

      call load_trapmaps(ctifile)

      call load_grdimages(grdimgfile)

      call load_gains(gainfile)


      call process_evtfile_demi (chipmap,E,gradedistr)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
      subroutine process_evtfile_demi (chipmap,E,gradedistr)
      implicit none
      real chipmap(1024,1024,0:9)
      real E
      real gradedistr(0:255)

      include 'trapmaps.inc'
      include 'grdimages.inc'
      include 'gains.inc'

      integer unit, status
      
      integer col_ccd_id, col_chipx, col_chipy, col_fltgrade, col_pha
      integer ccd_id, chipx, chipy, fltgrade, pha
      logical anyf

      integer iter
      integer irow, i, j, nrows, ii, ibin
      real xpha, volume, deltaQ, deltaQ0, xpha0, xpha_def
      real volume1, volume2, volume3, volume_sh, voff, voffs
      real gimg(3,3)
      real energy
      real rand

      real deltaE(-1000:1000)

      double precision sum
      
      do i=-1000,1000
        deltaE(i)=0
      enddo
      

c // go chip by chip, pixel-by-pixel, and grade-by-grade
      do ccd_id = 0,9
        do chipx=1,1024
          do chipy=1,1024

            if (chipmap(chipx,chipy,ccd_id).gt.0.0) then

c // find central pha for given energy
              ibin = GAINBIN(chipx,chipy,ccd_id)
              if (ibin.le.0) then
                write (0,*) 'no gain data for ccd=',ccd_id,chipx,chipy
                call exit(1)
              endif
              call lin_interpolate (GENERGY(1,ibin),GPHA(1,ibin),
     ~            NGAINPOINTS(ibin),E*1000.0,xpha_def)

c // go over the grade distribution
              do fltgrade=0,255
                if (gradedistr(fltgrade).gt.0) then

                  pha = xpha_def

c // AND NOW, SIMULATE CORRECTION

c // first, "light" correction
                  xpha = pha
                  call lin_interpolate (
     ~                volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~                ,volume_n(ccd_id),log(xpha),volume )
                  volume = exp(volume)    
                                ! because I took log(pha) and log(volume) 
                                ! when loading trap maps

                  deltaQ = volume*trapmap(chipx,chipy,ccd_id)
                  deltaQ0 = deltaQ
                  xpha = pha + deltaQ

                  xpha0 = xpha


c // find approximate energy
                  ibin = GAINBIN(chipx,chipy,ccd_id)
                  if (ibin.le.0) then
                    write (0,*) 'no gain data for ccd=',ccd_id,chipx,chipy
                    call exit(1)
                  endif
                  call lin_interpolate (GPHA(1,ibin),GENERGY(1,ibin)
     ~                ,NGAINPOINTS(ibin),xpha,energy)

        
                  do iter=1,3
c // make average grade image for this energy
                    do i=1,3
                      do j=1,3
                        gimg(i,j) = grdimg(i,j,fltgrade)
                      enddo
                    enddo
          
                    voff = 1-gimg(2,2)
                    voffs = voff*(max(1.0,energy)/5898.0)**eslope(fltgrade)
                    voffs = min(voffs,1.0)

                    do i=1,3
                      do j=1,3
                        gimg(i,j) = gimg(i,j)*voffs/voff*xpha0
                        gimg(i,j) = max(gimg(i,j),0.0)
                      enddo
                    enddo
                    gimg(2,2) = (1-voffs)*xpha0

c // find correction for each column
                    deltaQ = 0
                    do i=1,3
                      ii = chipx + (i-2)
            
                      if (ii.ge.1.and.ii.le.1024) then
                        if (gimg(i,1).gt.13.0) then
                          call lin_interpolate (
     ~                        volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~                        ,volume_n(ccd_id),log(gimg(i,1)),volume1)
                          volume1 = exp(volume1)
                        else
                          volume1 = 0
                        endif
                        
                        if (gimg(i,2).gt.13.0) then
                          call lin_interpolate (
     ~                        volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~                        ,volume_n(ccd_id),log(gimg(i,2)),volume2)
                          volume2 = exp(volume2)
                        else
                          volume2 = 0
                        endif
              
                        if (gimg(i,3).gt.13.0) then
                          call lin_interpolate (
     ~                        volume_pha(1,ccd_id),volume_volume(1,ccd_id)
     ~                        ,volume_n(ccd_id),log(gimg(i,3)),volume3)
                          volume3 = exp(volume3)
                        else
                          volume3 = 0
                        endif
              

                        volume_sh = volume1 + max(volume2-volume1,0.0) + 
     ~                      max(volume3-volume2-volume1,0.0)

                        deltaQ = deltaQ + trapmap(ii,chipy,ccd_id) * volume_sh
c Redeposition (works poorly)
!     ~            + fract1(ccd_id)*(volume3-volume2)*trapmap(ii,chipy,ccd_id)

!     ~          (volume2+max(0.0,(volume3-volume2)))
                      endif
                    enddo
        
c
c dQ = n1*V1+n2*V2-n1*V1+f(n3*v3-n2*V2)   from Glenn's memo
c
                    xpha0 = pha + deltaQ
*          if (fltgrade.eq.0) print*,fltgrade,iter,xpha0,gimg(2,2),deltaQ,deltaQ0
                  enddo

                  xpha = pha + deltaQ + (rand(0)-0.5)

                  call lin_interpolate (GPHA(1,ibin),GENERGY(1,ibin)
     ~                ,NGAINPOINTS(ibin),xpha,energy)

                  ibin = nint(energy-E*1000.0)/5
                  
                  if (ibin.ge.-1000.and.ibin.le.1000) then
                    deltaE(ibin)=deltaE(ibin) +
     ~                  gradedistr(fltgrade)*chipmap(chipx,chipy,ccd_id)
                  endif
                  
                endif
                
              enddo

            endif
            
          enddo
        enddo
      enddo

      sum = 0
      do i=-1000,1000
        sum = sum + deltaE(i)
      enddo

      if (sum.gt.0) then
        do i=-1000,1000
          deltaE(i)=deltaE(i)/sum
        enddo
      endif

      do i=-1000,1000
        print*,i,deltaE(i)
      enddo

      


      return
      end

