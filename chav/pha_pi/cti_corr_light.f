      implicit none
      character ctifile*200, evtfile*200

      logical defined

      
      call get_cl_par ('ctifile',ctifile)
      if (.not.defined(ctifile)) then
        call exiterror ('ctifile=???')
      endif

      call get_cl_par ('evtfile',evtfile)
      if (.not.defined(ctifile)) then
        call exiterror ('evtfile=???')
      endif

      call load_trapmaps(ctifile)

      call process_evtfile_light(evtfile)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
      subroutine process_evtfile_light (evtfile)
      implicit none
      character evtfile*(*)

      include 'trapmaps.inc'

      integer unit, status
      
      integer col_ccd_id, col_chipx, col_chipy, col_fltgrade, col_pha
      integer ccd_id, chipx, chipy, fltgrade, pha
      logical anyf

      integer i, nrows, iter
      real xpha, volume, deltaQ
      real rand
      

      status = 0
      call ftgiou (unit,status)
      call ftnopn (unit,evtfile,1,status)
      
c // Go to events extension
      call FTMNHD (unit,-1,'EVENTS',0,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      
c // Column information
      call FTGCNO (unit,.false.,'CCD_ID',  col_ccd_id,status)
      call FTGCNO (unit,.false.,'CHIPX',col_chipx,status)
      call FTGCNO (unit,.false.,'CHIPY',col_chipy,status)
      call FTGCNO (unit,.false.,'FLTGRADE',col_fltgrade,status)
      call FTGCNO (unit,.false.,'PHA',col_pha,status)

      if (status.ne.0) call exit_fitsio (evtfile,status)

      call FTGNRW (unit,nrows, status)
      
c // process rows
      do i=1,nrows
        if ((i/10000)*10000.eq.i) then
          write (0,'("\r",i3,"%",$)') nint(i*100.0/nrows)
          call flush(0)
        endif
        call FTGCVJ (unit,col_ccd_id,i,1,1,0,ccd_id,anyf,status)
        call FTGCVJ (unit,col_chipx,i,1,1,0,chipx,anyf,status)
        call FTGCVJ (unit,col_chipy,i,1,1,0,chipy,anyf,status)
        call FTGCVJ (unit,col_fltgrade,i,1,1,0,fltgrade,anyf,status)
        call FTGCVJ (unit,col_pha,i,1,1,0,pha,anyf,status)

        xpha = pha
        do iter = 1,3
          call lin_interpolate (
     ~        volume_pha(1,ccd_id),volume_volume(1,ccd_id),volume_n(ccd_id),
     ~        log(xpha),volume )
          volume = exp(volume)  ! because I took log(pha) and log(volume) 
                                ! when loading trap maps
          
          deltaQ = volume*trapmap(chipx,chipy,ccd_id)
          
          xpha = pha + deltaQ
        enddo

        pha = nint(xpha + (rand(0)-0.5))
        
        call FTPCLJ (unit,col_pha,i,1,1,pha,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
      enddo
      write (0,*)

c // close and cleanup
      call ftclos(unit,status)
      call ftfiou(unit,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)

      return
      end
