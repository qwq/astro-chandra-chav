#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include <math.h>

#define NMAX 2048

int CHIPXMIN[10][NMAX];
int CHIPXMAX[10][NMAX];
int CHIPYMIN[10][NMAX];
int CHIPYMAX[10][NMAX];
int NCCDPOINTS[NMAX];
int NPOINTS[10][NMAX];
float PHA[10][NMAX][100];
float ENERGY[10][NMAX][100];

float pha[20000][100], energy[20000][100];

float lin_interpolate (float x, float *x0, float *y0, int n);

main(int argc, char *argv[])
{
  extern int pha_to_pi ();/* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */
  fitsfile *fptr;
  iteratorCol cols[100]; /* STRUCTURE USED BY THE ITERATOR FUNCTION */
  int ncols;
  long rows_per_loop, offset;
  char gainfile[200], feffile[200];
  
  int status, nkeys, keypos, hdutype, ii, jj;
  int iarg;

  long nrows;
  int ccdidcol,chipxmincol,chipxmaxcol,chipymincol,chipymaxcol,npointscol,
    phacol,energycol,phatype,energytype;
  long phanelem, energynelem,phawidth,energywidth;
  short chxmin[20000], chxmax[20000], chymin[20000], chymax[20000], ccdid[20000],
    npoints[20000];
  short NS=0; float NE=0;
  int anynul;
  int i,iccd,j;

  if (argc==1) {
    fprintf (stderr,"Usage: %s -fef fef.fits -gain gain.fits\n",argv[0]);
    exit (1);
  }
  
  strcpy (gainfile,"N O N E");
  strcpy (feffile,"N O N E");
  for (iarg=1;iarg<argc-1;iarg++) {
    if (strcmp(argv[iarg],"-gain")==0) {
      strcpy(gainfile,argv[iarg+1]);
      break;
    }
  }
  for (iarg=1;iarg<argc-1;iarg++) {
    if (strcmp(argv[iarg],"-fef")==0) {
      strcpy(feffile,argv[iarg+1]);
      break;
    }
  }
  if (strcmp(gainfile,"N O N E")==0 || strcmp(feffile,"N O N E")==0 ) {
    fprintf (stderr,"Usage: %s -fef fef.fits -gain gain.fits\n",argv[0]);
    exit (1);
  }

  
  printf("Reading gain file: %s\n",gainfile);
  
  /* Read gain data */
  status = 0;
  fits_open_file(&fptr, gainfile, READONLY, &status);
  fits_movnam_hdu (fptr,BINARY_TBL,"AXAF_DETGAIN",0,&status);
  fits_get_num_rows (fptr,&nrows,&status);
  fits_get_colnum(fptr,CASEINSEN,"CCD_ID",&ccdidcol,&status);
  fits_get_colnum(fptr,CASEINSEN,"CHIPX_MIN",&chipxmincol,&status);
  fits_get_colnum(fptr,CASEINSEN,"CHIPX_MAX",&chipxmaxcol,&status);
  fits_get_colnum(fptr,CASEINSEN,"CHIPY_MIN",&chipymincol,&status);
  fits_get_colnum(fptr,CASEINSEN,"CHIPY_MAX",&chipymaxcol,&status);
  fits_get_colnum(fptr,CASEINSEN,"NPOINTS",&npointscol,&status);
  fits_get_colnum(fptr,CASEINSEN,"PHA",&phacol,&status);
  fits_get_colnum(fptr,CASEINSEN,"ENERGY",&energycol,&status);
  fits_get_coltype(fptr,phacol,&phatype,&phanelem,&phawidth,&status);
  fits_get_coltype(fptr,energycol,&energytype,&energynelem,&energywidth,&status);

  fits_read_col (fptr,TSHORT,ccdidcol,1,1,nrows,&NS,ccdid,&anynul,&status);
  fits_read_col (fptr,TSHORT,chipxmincol,1,1,nrows,&NS,chxmin,&anynul,&status);
  fits_read_col (fptr,TSHORT,chipxmaxcol,1,1,nrows,&NS,chxmax,&anynul,&status);
  fits_read_col (fptr,TSHORT,chipymincol,1,1,nrows,&NS,chymin,&anynul,&status);
  fits_read_col (fptr,TSHORT,chipymaxcol,1,1,nrows,&NS,chymax,&anynul,&status);
  fits_read_col (fptr,TSHORT,npointscol,1,1,nrows,&NS,npoints,&anynul,&status);

  for (i=1;i<=nrows;i++) {
    fits_read_col (fptr,TFLOAT,phacol,i,1,phanelem,&NE,pha[i-1],&anynul,&status);
    fits_read_col (fptr,TFLOAT,energycol,i,1,energynelem,&NE,energy[i-1],&anynul,&status);
  }
  fits_close_file(fptr, &status);      /* all done */
  if (status) {
    fits_report_error(stderr, status);  /* print out error messages */ 
    exit(1);
  }

  
  for (i=0;i<10;i++)
    NCCDPOINTS[i]=0;
  
  for (i=0;i<nrows;i++) {
    iccd = ccdid[i];
    CHIPXMIN[iccd][NCCDPOINTS[iccd]]=chxmin[i];
    CHIPXMAX[iccd][NCCDPOINTS[iccd]]=chxmax[i];
    CHIPYMIN[iccd][NCCDPOINTS[iccd]]=chymin[i];
    CHIPYMAX[iccd][NCCDPOINTS[iccd]]=chymax[i];
    NPOINTS[iccd][NCCDPOINTS[iccd]]=npoints[i];
    for (j=0;j<100;j++) {
      PHA[iccd][NCCDPOINTS[iccd]][j]=pha[i][j];
      ENERGY[iccd][NCCDPOINTS[iccd]][j]=energy[i][j];
    }
    NCCDPOINTS[iccd]++;
  }


  

    
  status = 0; 
    
  printf ("Correcting file: %s\n",feffile);
  fits_open_file(&fptr, feffile, READWRITE, &status); /* open file */
    
  /* move to the EVENTS binary table extension */
  if (fits_movnam_hdu(fptr, BINARY_TBL, "FUNCTION", 0, &status) )
    fits_report_error(stderr, status);    /* print out error messages */
    
  ncols  = 7;     
  /* define input column structure members for the iterator function */
  fits_iter_set_by_name(&cols[0], fptr, "ccd_id", TLONG,  InputCol);
  fits_iter_set_by_name(&cols[1], fptr, "chipx_lo",  TLONG,  InputCol);
  fits_iter_set_by_name(&cols[2], fptr, "chipx_hi",  TLONG,  InputCol);
  fits_iter_set_by_name(&cols[3], fptr, "chipy_lo",  TLONG,  InputCol);
  fits_iter_set_by_name(&cols[4], fptr, "chipy_hi",  TLONG,  InputCol);
  fits_iter_set_by_name(&cols[5], fptr, "channel",   TFLOAT,  InputOutputCol);
  fits_iter_set_by_name(&cols[6], fptr, "energy", TFLOAT,  InputCol);


  rows_per_loop = 0;  /* use default optimum number of rows */
  offset = 0;         /* process all the rows */
  
  /* apply the rate function to each row of the table */
  fits_iterate_data(ncols, cols, offset, rows_per_loop,
		    pha_to_pi, 0L, &status);

  fits_close_file(fptr, &status);      /* all done */

  if (status)
    fits_report_error(stderr, status);  /* print out error messages */
  exit(0);
}
/*--------------------------------------------------------------------------*/
int pha_to_pi (long totalrows, long offset, long firstrow, long nrows,
             int ncols, iteratorCol *cols, void *user_strct ) 
{
  int ii, status = 0;
  
  /* Declare variables static to preserve their values between calls */
  /* 1) Data arrays: */ 
  static long *ccd_id, *chipx_lo, *chipx_hi, *chipy_lo, *chipy_hi;
  static float *energy, *channel;
  
  int iccd,chx,chy,ibin,i;
  float xpha;
  
  
  

  /*--------------------------------------------------------*/
  /*  Initialization procedures: execute on the first call  */
  /*--------------------------------------------------------*/
  if (firstrow == 1)
    {
      if (ncols != 7)
	return(-1);  /* number of columns incorrect */
      
      if (fits_iter_get_datatype(&cols[0]) != TLONG ||
	  fits_iter_get_datatype(&cols[1]) != TLONG ||
	  fits_iter_get_datatype(&cols[2]) != TLONG ||
	  fits_iter_get_datatype(&cols[3]) != TLONG ||
	  fits_iter_get_datatype(&cols[4]) != TLONG ||
	  fits_iter_get_datatype(&cols[5]) != TFLOAT||
	  fits_iter_get_datatype(&cols[6]) != TFLOAT
	  )
	return(-2);  /* bad data type */
      
      /* assign the input pointers to the appropriate arrays and null ptrs*/
      ccd_id       = (long *) fits_iter_get_array(&cols[0]);
      chipx_lo     = (long *) fits_iter_get_array(&cols[1]);
      chipx_hi     = (long *) fits_iter_get_array(&cols[2]);
      chipy_lo     = (long *) fits_iter_get_array(&cols[3]);
      chipy_hi     = (long *) fits_iter_get_array(&cols[4]);
      channel      = (float *) fits_iter_get_array(&cols[5]);
      energy       = (float *) fits_iter_get_array(&cols[6]);

    }
  
  /*--------------------------------------------*/
  /*  Main loop: process all the rows of data */
  /*--------------------------------------------*/

  /*  NOTE: 1st element of array is the null pixel value!  */
  /*  Loop from 1 to nrows, not 0 to nrows - 1.  */

  for (ii = 1; ii <= nrows; ii++)
    {
      iccd=ccd_id[ii];
      chx = (chipx_lo[ii]+chipx_hi[ii])/2;
      chy = (chipy_lo[ii]+chipy_hi[ii])/2;
      /* locate coeff */
      ibin = -1;
      for (i=0;i<NCCDPOINTS[iccd];i++) {
	if (
	    chx>=CHIPXMIN[iccd][i] && 
	    chx<=CHIPXMAX[iccd][i] &&
	    chy>=CHIPYMIN[iccd][i] && 
	    chy<=CHIPYMAX[iccd][i] ) {
	  ibin = i;
	  break;
	}
      }
      if (ibin<0) {
	fprintf (stderr,"Photon coordinates are out of bounds\n");
	exit(1);
      }
      
      channel[ii]=lin_interpolate(energy[ii]*1000.0,ENERGY[iccd][ibin],PHA[iccd][ibin],NPOINTS[iccd][ibin]);
    }

  
  return(0);  /* return successful status */
}


float lin_interpolate (float x, float *x0, float *y0, int n)
{
  int ibin;

  if (x<=x0[0]) {
    ibin=0;
  }
  else if (x>x0[n-1]) {
    ibin=n-2;
  } else {
    ibin=1;
    while (x>x0[ibin]) ibin++;
  }

  if (x0[ibin+1] != x0[ibin]) {
    return y0[ibin]+(y0[ibin+1]-y0[ibin])*(x-x0[ibin])/(x0[ibin+1]-x0[ibin]);
  } else {
    return y0[ibin];
  }
}
