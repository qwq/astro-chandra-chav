      subroutine load_gains (gainfile)
      implicit none
      character gainfile*(*)

      include 'gains.inc'
      
      integer unit, status
      integer iccd, nrows, i, j, ii, igainbin
      integer phanelem, energynelem,
     ~    chipx_min, chipx_max, chipy_min, chipy_max, npoints
      integer col_ccd_id, col_chipx_min, col_chipy_min, col_chipx_max,
     ~    col_chipy_max, col_npoints, col_pha, col_energy
      character name*80, comment*80
      logical anyf
      integer phatype, phawidth, energytype, energywidth

      do i=1,1024
        do j=1,1024
          do iccd=0,9
            GAINBIN(i,j,iccd)=-1
          enddo
        enddo
      enddo

      status = 0
      call ftgiou (unit,status)
      call ftnopn (unit,gainfile,0,status)

      call FTMNHD (unit,-1,'AXAF_DETGAIN',0,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)


      call FTGCNO (unit,.false.,'CCD_ID',  col_ccd_id,status)
      call FTGCNO (unit,.false.,'CHIPX_MIN',col_chipx_min,status)
      call FTGCNO (unit,.false.,'CHIPX_MAX',col_chipx_max,status)
      call FTGCNO (unit,.false.,'CHIPY_MIN',col_chipy_min,status)
      call FTGCNO (unit,.false.,'CHIPY_MAX',col_chipy_max,status)
      call FTGCNO (unit,.false.,'NPOINTS',col_npoints,status)
      call FTGCNO (unit,.false.,'PHA',col_pha,status)
      call FTGCNO (unit,.false.,'ENERGY',col_energy,status)

      call FTGTCL (unit,col_pha,phatype,phanelem,phawidth,status)
      call FTGTCL (unit,col_energy,energytype,energynelem,energywidth,status)

      if (status.ne.0) call exit_fitsio (gainfile,status)
      call FTGNRW (unit,nrows, status)

      igainbin = 1
      do ii=1,nrows
        call FTGCVJ (unit,col_ccd_id,ii,1,1,0,iccd,anyf,status)
        call FTGCVJ (unit,col_chipx_min,ii,1,1,0,chipx_min,anyf,status)
        call FTGCVJ (unit,col_chipx_max,ii,1,1,0,chipx_max,anyf,status)
        call FTGCVJ (unit,col_chipy_min,ii,1,1,0,chipy_min,anyf,status)
        call FTGCVJ (unit,col_chipy_max,ii,1,1,0,chipy_max,anyf,status)
        call FTGCVJ (unit,col_npoints,ii,1,1,0,npoints,anyf,status)

        if (status.ne.0) call exit_fitsio (gainfile,status)

        do i=chipx_min,chipx_max
          do j=chipy_min,chipy_max
            GAINBIN(i,j,iccd) = igainbin
          enddo
        enddo

        NGAINPOINTS(igainbin) = npoints
        if (npoints.gt.NGAINMAX) then
          write (0,*) 'increase max number of gain points: ',npoints,NGAINMAX
          call exit(1)
        endif

        call FTGCVE (unit,col_pha,ii,1,phanelem,0.0,GPHA(1,igainbin),anyf,status)
        call FTGCVE (unit,col_energy,ii,1,energynelem,0.0,GENERGY(1,igainbin),anyf,status)
        if (status.ne.0) call exit_fitsio (gainfile,status)

        igainbin = igainbin + 1
        if (igainbin .gt. GNMAX) then
          write (0,*) 'increase max number of gain regions: ',GNMAX,nrows
        endif
      enddo

c // close and cleanup
      call ftclos(unit,status)
      call ftfiou(unit,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)

      return
      end

