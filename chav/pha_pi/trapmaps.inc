      real trapmap(1024,1024,0:9)
      real volume_pha    (1000,0:9)
      real volume_volume (1000,0:9)
      integer volume_n (0:9)
      real fract1(0:9), fract2(0:9)
      common /ctrapmap/ trapmap, volume_pha, volume_volume, volume_n,
     ~    fract1, fract2
