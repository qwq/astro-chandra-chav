      subroutine fill_in_matr (matrix,nmatr,gfwhm,gpos,gampl,
     ~    eminthresh)
      implicit none
      integer nmatr
      real matrix(0:nmatr)
      real gfwhm(0:10),gpos(0:10),gampl(0:10)
      real eminthresh
      real norm, sum, arg
      integer i,j
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      real shift
      save shift
      real exparr (0:25000)
      save exparr

      integer ncomp
      real pos(100), ampl(100), sig(100)
      
      
      integer imin,imax
      real chan

      if (firstcall) then
        firstcall = .false.
        call get_pv_default ('rmfshift',shift,0.0,'e')
        do i=0,25000
          exparr(i)=exp(-i/1000.0)
        enddo
      endif

      imin = gfwhm(0)
      imax = gpos(0)
      norm = gampl(0)

      do i=0,nmatr
        matrix(i)=0
      enddo

      ncomp = 0
      do j=1,10
        if (gampl(j).ne.0.0.and.gpos(j)+shift.gt.eminthresh) then
          ncomp = ncomp + 1
          pos(ncomp) =  gpos(j)+shift
          ampl(ncomp) = gampl(j)
          sig(ncomp) = gfwhm(j)/2.35482
        endif
      enddo

      do i=0,nmatr
        if (i.ge.imin.and.i.le.imax) then
          chan = i
          do j=1,ncomp
*            arg = 0.5*((chan+0.5-pos(j))/sig(j))**2
            arg = 0.5*((chan-pos(j))/sig(j))**2
            if (arg.lt.25.0) then
              matrix(i)=matrix(i) + norm*ampl(j)*exp(-arg)
            endif
          enddo
        endif
      enddo

      sum = 0
      do i=0,nmatr
        sum = sum + matrix(i)
      enddo
      if (sum.gt.0.0) then
        do i=0,nmatr
          matrix(i)=matrix(i)/sum
        enddo
      endif

      return
      end

