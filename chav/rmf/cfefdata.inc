      integer nregmax, nreg, nemax	
      parameter (nregmax=10240, nemax=100)
      integer*2 fefregion (128,128,0:9) ! assume that region mask is unique
                                ! when binned by 8
      integer*2 fefcoords (3,nregmax) ! center of fef region in chip
                                ! coordinates; 1: chipx, 2: chipy, 3: iccd
      real fefdata (31,nemax,nregmax)
      integer nfefdata (nregmax)
      common /cfefdata/  fefregion, fefdata, nfefdata, nreg, fefcoords
