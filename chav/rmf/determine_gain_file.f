      subroutine determine_gain_file (name)
      implicit none
      character*(*) name
      character*200 arg
      logical defined

c If gainfile is set on the command line, use it
      call get_cl_par ('gainfile',arg)
      if (defined(arg)) then
        name = arg
        return
      else
        call exiterror ('gainfile=?')
      endif

      return
      end
