      subroutine find_coeff (e,eref,ne,g_fwhm,g_pos,g_ampl,nmax,
     ~    gfwhm,gpos,gampl)
      implicit none
      real e
      integer ne
      real eref(ne)
      integer nmax
      real g_fwhm(nmax,0:10),g_pos(nmax,0:10),g_ampl(nmax,0:10),
     ~    gfwhm(0:10),gpos(0:10),gampl(0:10)

      real w1,w2
      integer i
      integer ibin
      save ibin
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      
      if (firstcall) then
c       as per Alexey 1/13/07
c       firstcall = .false.
        ibin = ne/2
      endif

      if(e.le.eref(1))then
        ibin=1
        w1=1.0
        w2=0.0
      else if (e.ge.eref(ne)) then
        ibin=ne-1
        w1=0.0
        w2=1.0
      else
        call hunt (eref,ne-1,e,ibin)
        if (ibin.lt.1.or.ibin.gt.ne-1) then
          call exiterror ('unexpected error in find_coeff')
        endif
        call get_weights (e,eref(ibin),eref(ibin+1),w1,w2)
      endif


      do i=0,10
        gfwhm(i)= w1*g_fwhm(ibin,i) + w2*g_fwhm(ibin+1,i)
        gpos(i) = w1*g_pos(ibin,i)  + w2*g_pos(ibin+1,i)
        gampl(i)= w1*g_ampl(ibin,i) + w2*g_ampl(ibin+1,i)
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine find_weights (e,eref,ne,i1,i2,w1,w2)
      implicit none
      real e
      integer ne
      real eref(ne)
      real w1,w2
      integer i1,i2

      integer ibin
      save ibin
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      
      if (firstcall) then
c       as per Alexey 1/13/07
c       firstcall = .false.
        ibin = ne/2
      endif

      if(e.le.eref(1))then
        ibin=1
        w1=1.0
        w2=0.0
      else if (e.ge.eref(ne)) then
        ibin=ne-1
        w1=0.0
        w2=1.0
      else
        call hunt (eref,ne-1,e,ibin)
        if (ibin.lt.1.or.ibin.gt.ne-1) then
          call exiterror ('unexpected error in find_coeff')
        endif
        call get_weights (e,eref(ibin),eref(ibin+1),w1,w2)
      endif


      i1 = ibin
      i2 = ibin+1
      
      return
      end


