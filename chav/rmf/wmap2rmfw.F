#include <zhtools.h>

      program wmap2rmfw
c
c The program converts calcarf-style weight map into the calcrmf2 internal
c weight array
c
c Usage: 
c      wmap2rmfw wmap=PHA o=ttt.rmfw @par.file
c
c Uses:
c    1) ReadPar library
c
      implicit none

c File names
      character*200 wmapfile, outfile

c fitsio variable
      integer status, blocksize,naxis,pcount,gcount,naxes(10),bitpix
      integer unitpha, unitrmf, unitwmap
      logical simple, extend
      integer pharwmode

c wtarf variables
      integer chatter
      character*80 hist(100), comm(100)
      

c Detimage
      integer ndetimgx, ndetimgy
      real crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      character*80 ctype1,ctype2


c Utility functions
      logical defined, defpar, nopar

c Misc variables
      logical debug, qwmap
      character arg*200

      ZHDECL(real,pdetimg)

      integer flchan ! - from ref_pi_rmf
      integer binchan
      logical PICHAN
      integer i
      
      integer firstarg,lastarg, iargc
      integer iarg

c
*-------------------------------------------------------------------------
***      ieeer = ieee_handler ('set','underflow', myhandler)

C 1) READ COMMAND LINE ARGUMENTS

      call get_cl_par_position ('files',firstarg)
      if (firstarg.le.0) then
        firstarg = 0
        lastarg = 1
      else
        firstarg = firstarg + 1
        lastarg = iargc()
      endif

      
*-------------------------------------------------------------------------
      call ftgiou (unitwmap,status)
      if (status.ne.0) call exit_fitsio (wmapfile,status)

      do iarg=firstarg,lastarg,2
        if (iarg.eq.0) then
          call get_cl_par ('wmap',wmapfile)
          if (.not.defined(wmapfile)) call usage
          
          call get_cl_par ('o',outfile)
          if (.not.defined(outfile)) call usage
        else
          call getarg (iarg,wmapfile)
          call getarg (iarg+1,outfile)
        endif

        print*,wmapfile,outfile

        call ftnopn (unitwmap,wmapfile,0,status)
        if (status.ne.0) call exit_fitsio (wmapfile,status)
        
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c   b) Determine image size
        call ftghpr (unitwmap,10,simple,bitpix,naxis,naxes,pcount,gcount
     ~      ,extend,status)
        if (status.ne.0) call exit_fitsio (wmapfile,status)
        
        if (naxis.ne.2) then
          call exiterror ('no 2d image in '//wmapfile)
        endif
        
        if (naxes(1).eq.0.or.naxes(2).eq.0) then
          call exiterror ('no 2d image in '//wmapfile)
        endif
        
        ndetimgx = naxes(1)
        ndetimgy = naxes(2)
        if (debug) then
          print *,' WMAP contains ',ndetimgx,' by ',ndetimgy, ' image'
        endif
        
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
c    c) Read required keywords to convert image pixels to detx,dety
        call read_det_lin_wcs (unitwmap,wmapfile,crval1,crval2,crpix1,crpix2
     ~      ,cdelt1,cdelt2,crota2,ctype1,ctype2)
        
c  6) Calculate weights
        ZHMALLOC(pdetimg,ndetimgx*ndetimgy,'e')
        call calc_rmf_weights (
     ~      unitwmap, ndetimgx,ndetimgy,crval1,crval2,crpix1
     ~      ,crpix2,cdelt1,cdelt2,crota2,ZHVAL(pdetimg),outfile)
        
        call ftclos(unitwmap,status)
        if (status.ne.0) call exit_fitsio (wmapfile,status)
        
      enddo

c END OF CALCARF
      call exit(0)
      end
       
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine usage
      implicit none

      write (0,*) 'Usage: wmap2rmfw wmap=phafile o=rmfw'

      call exit(1)
      return
      end

