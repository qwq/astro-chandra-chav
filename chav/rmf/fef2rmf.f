      implicit none
      integer nmax
      parameter (nmax=10000)
      real efef(nmax),
                                ! b1_xlow(nmax), b1_xhi(nmax), b1_ampl(nmax),
     ~    g_fwhm(nmax,0:10), g_pos(nmax,0:10), g_ampl(nmax,0:10)
c g_fwhm(*,0) -   b1_xlow,  g_pos(*,0) - b1_xhi, g_ampl(*,0) - b1_ampl
      integer nfef

      real emin(nmax), emax(nmax), v1(nmax), v2(nmax), ehigh(nmax), elow(nmax)
      integer index(nmax)
      integer ne, nchan

      character*200 feffile, refrmf, rmffile
      integer i, lnblnk

      integer unitrmf, status, blocksize,colnum
      logical anyf

      integer nchanmax,nemax
      parameter (nchanmax=1024, nemax=1500)
      real matrix (0:nchanmax,nemax)
      real e, gfwhm(0:10), gpos(0:10), gampl(0:10)
      real eminthresh

      logical defined, yespar
      real lo_thresh
      integer imaxgrp,ngrp(nemax),
     ~    f_chan(nemax,100),n_chan(nemax,100)
      integer chatter, bitpix, naxis, naxes(10), pcount, gcount
      character*80 hist(30), comm(30)
      


      call get_cl_par ('fef',feffile)
      if ( .not. defined (feffile)) call exiterror ('fef=?')

      call get_cl_par ('ref_pi_rmf',refrmf)
      if ( .not. defined (refrmf)) call exiterror ('ref_pi_rmf=?')
      
      call get_cl_par ('o',rmffile)
      if ( .not. defined (rmffile)) then
        if (.not.yespar('show')) then
          call exiterror ('o=?')
        endif
      endif
      
      
      i = lnblnk(feffile)
      feffile (i+1:i+1) = char(0)

      call loadfef (feffile, nfef,
     ~    efef,
     ~    g_fwhm(1,0), g_pos(1,0), g_ampl(1,0),
     ~    g_fwhm(1,1), g_pos(1,1), g_ampl(1,1),
     ~    g_fwhm(1,2), g_pos(1,2), g_ampl(1,2),
     ~    g_fwhm(1,3), g_pos(1,3), g_ampl(1,3),
     ~    g_fwhm(1,4), g_pos(1,4), g_ampl(1,4),
     ~    g_fwhm(1,5), g_pos(1,5), g_ampl(1,5),
     ~    g_fwhm(1,6), g_pos(1,6), g_ampl(1,6),
     ~    g_fwhm(1,7), g_pos(1,7), g_ampl(1,7),
     ~    g_fwhm(1,8), g_pos(1,8), g_ampl(1,8),
     ~    g_fwhm(1,9), g_pos(1,9), g_ampl(1,9),
     ~    g_fwhm(1,10), g_pos(1,10), g_ampl(1,10) )


      call ftgiou (unitrmf,status)
      call ftopen (unitrmf,refrmf,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (refrmf,status)

      call ftmnhd (unitrmf,-1,'SPECRESP MATRIX',0,status)
      if (status.ne.0) then
        status = 0
        call ftmnhd (unitrmf,-1,'MATRIX',0,status)
      endif
      if (status.ne.0) call exit_fitsio ('SPECRESP, '//refrmf,status)
      
      call ftgnrw (unitrmf,ne, status)
      if (status.ne.0) call exit_fitsio ('NAXIS2, '//refrmf,status)
      
      call ftgcno (unitrmf,.false.,'ENERG_LO',colnum,status)
      if (status.ne.0) call exit_fitsio ('ENERG_LO, '//refrmf,status)
      call ftgcve (unitrmf,colnum,1,1,ne,0.0,elow,anyf,status)
      if (status.ne.0) call exit_fitsio ('ENERG_LO, '//refrmf,status)
      
      call ftgcno (unitrmf,.false.,'ENERG_HI',colnum,status)
      if (status.ne.0) call exit_fitsio ('ENERG_HI, '//refrmf,status)
      call ftgcve (unitrmf,colnum,1,1,ne,0.0,ehigh,anyf,status)
      if (status.ne.0) call exit_fitsio ('ENERG_HI, '//refrmf,status)
      
      
      call ftmnhd (unitrmf,-1,'EBOUNDS',0,status)
      if (status.ne.0) call exit_fitsio ('EBOUNDS, '//refrmf,status)
      call ftgnrw (unitrmf,nchan, status)
      if (status.ne.0) call exit_fitsio ('NAXIS2, '//refrmf,status)
      call ftgcno (unitrmf,.false.,'E_MIN',colnum,status)
      if (status.ne.0) call exit_fitsio ('E_MIN, '//refrmf,status)
      call ftgcve (unitrmf,colnum,1,1,nchan,0.0,emin,anyf,status)
      if (status.ne.0) call exit_fitsio ('E_MIN, '//refrmf,status)
      call ftgcno (unitrmf,.false.,'E_MAX',colnum,status)
      if (status.ne.0) call exit_fitsio ('E_MAX, '//refrmf,status)
      call ftgcve (unitrmf,colnum,1,1,nchan,0.0,emax,anyf,status)
      if (status.ne.0) call exit_fitsio ('E_MAX, '//refrmf,status)


      call get_pv_default ('eminrmf',eminthresh,-10000.0,'e')


      do i=1,ne
        e = 0.5*(ehigh(i)+elow(i))
        call find_coeff (e,efef,nfef,g_fwhm,g_pos,g_ampl,nmax,
     ~      gfwhm,gpos,gampl)
        call fill_in_matr (matrix(0,i),nchan-1,gfwhm,gpos,gampl,
     ~      eminthresh)
      enddo


c   Find grouping
      lo_thresh=1e-6
      imaxgrp=100
      chatter=1
      call grprmf(chatter, nchanmax+1, nchan, nemax, ne, 
     &    matrix, lo_thresh, imaxgrp, 
     &    0, ngrp, F_chan, N_chan, status)


      if (yespar('show')) then
        call saoimage (matrix,nchanmax+1,nemax,'e')
      endif

      if (defined(rmffile)) then
c Write out the rmf file
        call ftgiou (unitrmf,status)
        if (status.ne.0) call exit_fitsio (rmffile,status)
        
        call unlink(rmffile)
        call ftinit(unitrmf,rmffile,0,status)
        if (status.ne.0) call exit_fitsio (rmffile,status)
        
        bitpix=8
        naxis=0
        call ftphpr(unitrmf,.true.,bitpix,naxis,naxes,pcount,gcount,
     ~      .true.,status)
        call ftpdef(unitrmf,bitpix,naxis,naxes,pcount,gcount,status)
        
        chatter = 1
        
        call wtrmf3(unitrmf, chatter,
     &      0, hist,
     &      0, comm,'1.2.0', 'REDIST',
     &      'CHANDRA','ACIS', ' ', 'NONE', 1.0,
     &      'PI', 0,
     &      nchanmax+1, nchan, nemax, ne, elow, ehigh,
     &      imaxgrp, ngrp, F_chan, N_chan,
     &      matrix, lo_thresh, status)      
        
        
        call wtebd3(unitrmf, chatter, 
     &      0, hist, 
     &      0, comm,'1.2.0',
     &      'CHANDRA', 'ACIS', ' ', 'NONE', 1.0, 
     &      'PI', 0, nchan, emin, emax, status)
        
        call ftclos(unitrmf,status)
        if (status.ne.0) call exit_fitsio (rmffile,status)
      endif

      end










