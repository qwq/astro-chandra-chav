      subroutine calc_rmf_singlefef (
     ~    chantype,npichanmax, npichan, nechanmax, nechan, energ_lo, energ_hi,
     ~    imaxgrp, ngrp, F_chan, N_chan,
     ~    resp, lo_thresh, e_min, e_max,
     ~    unitpha, ndetimgx,ndetimgy,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,
     ~    detimg,
     ~    flchan
     ~    )
      implicit none

      character*(*) chantype
      integer npichanmax,nechanmax,npichan,nechan
      real energ_lo(nechanmax), energ_hi(nechanmax)
      integer imaxgrp,ngrp(nechanmax),F_chan(nechanmax,*),N_chan(nechanmax,*)
      real resp(npichanmax,nechanmax)
      real lo_thresh
      real e_min(npichanmax),e_max(npichanmax) ! nominal e bounds of PI chans
      integer unitpha, ndetimgx,ndetimgy
      real crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      real detimg(ndetimgx*ndetimgy)
      integer flchan


c   fitsio variables
      integer nhdu, status, hdutype
      logical anyf
      character*200 phafile

      logical no
      external no

      character*200 fefname

      include 'cfefdata.inc'
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      real weight(nregmax)

c   Misc
      double precision totweight
      real wght
      integer i,j,k,l,kreg,icomp
      real detx, dety, chipx,chipy
      real manualshiftx, manualshifty
      real detxmean, detymean, pixsize

      integer iccd,ichipx,ichipy
      character*80 fpsys
      character*200 aimpoint
      real roll_average

      logical defined, yespar
      logical useroll, firstrmf, debug
      character*200 filename

      integer unitrmf, blocksize
      character *80 matext
      integer chatter
      character*80 telescop, instrume, detnam, filter, rmfversn, hduclas3
      real areascal
      integer channel(8192)
      integer maxgrp

      double precision wmin,wmax,wtotmin,wtot

      character*200 refrmf
      integer nfefmax, nfef, nfef_low
      parameter (nfefmax=10000)
      real efef(nfefmax),
     ~    g_fwhm(nfefmax,0:10), g_pos(nfefmax,0:10),g_ampl(nfefmax,0:10)
      real e, gfwhm(0:10), gpos(0:10), gampl(0:10)
      real efef_low(nfefmax), g_fwhm_low(nfefmax,0:10),
     ~    g_pos_low(nfefmax,0:10), g_ampl_low(nfefmax,0:10)
      real gfwhm_low(0:10), gpos_low(0:10), gampl_low(0:10)
      real eminthresh
      real rspw(0:100000), rspw2(0:100000)

      integer nfef2max
      parameter (nfef2max=100)
      integer nppoints
      parameter (nppoints=1000)
      double precision cresp0(nppoints,nfef2max)
      double precision ppoint(nppoints), cresp(nppoints)
      integer i1,i2
      real w1,w2
      
      logical special low energy treatment

c Gain map
      
      integer NGAINREGMAX, NGAINEMAX
      parameter (NGAINREGMAX=2048, NGAINEMAX=80)
      integer gain_chipxmin (0:9,NGAINREGMAX), gain_chipxmax (0:9,NGAINREGMAX),
     ~    gain_chipymin (0:9,NGAINREGMAX), gain_chipymax (0:9,NGAINREGMAX)
      integer ngainccdreg (0:9)
      integer ngainpoints (0:9,NGAINREGMAX)
      real pha(NGAINEMAX,0:9,NGAINREGMAX)
      real energy(NGAINEMAX,0:9,NGAINREGMAX)
      logical pha_fef_to_pi_rmf

      save gain_chipxmin, gain_chipxmax, gain_chipymin, gain_chipymax,
     ~    ngainccdreg, ngainpoints, pha, energy, pha_fef_to_pi_rmf

      integer binfef
      save binfef

      integer ix,iy, igainreg, ipi, ioff, ich
      real w
      real phaoff(3)
      

*------------------------------------------------------------------------



*---------------------------------------------------
      phaoff(1) = -(0.5-1.0/6)
      phaoff(2) = 0.0
      phaoff(3) = (0.5-1.0/6)
      

*---------------------------------------------------
      do i=1,nppoints/2
        ppoint(i)=0.5/(1.02**(nppoints/2+1-i))
      enddo
      do i=nppoints/2+1,nppoints
        ppoint(i)=1.0d0-ppoint(nppoints-i+1)
      enddo


      call stf_offsets (manualshiftx, manualshifty)

      debug = yespar('debug')

      do i=1,nreg
        weight(i)=0
      enddo
      do i=1,npichanmax
        do j=1,nechanmax
          resp(i,j)=0
        enddo
      enddo


      if (firstcall) then

        call get_pv_default ('binfef',binfef,1,'j')

        firstcall = .false.
        call get_cl_par ('ref_pi_rmf',refrmf)
        if (.not.defined (refrmf)) call exiterror('ref_pi_rmf=?')
        
        status = 0
        call ftgiou (unitrmf,status)
        filename = refrmf
        call ftopen (unitrmf,filename,0,blocksize,status)
        if (status.ne.0) call exit_fitsio (filename,status)
        if (debug) then
          chatter = 10
        else
          chatter = 0
        endif
        
        matext = 'SPECRESP MATRIX'
        call ftmnhd (unitrmf,-1,matext,0,status)
        if (status.ne.0) then
          status = 0
          matext='MATRIX'
          call ftmnhd (unitrmf,-1,matext,0,status)
        endif
        if (status.ne.0) 
     ~      call exit_fitsio ('SPECRESP MATRIX, '//filename,status)
        
        call rdrmf3_info(unitrmf, chatter,matext,
     &      telescop, instrume, detnam, filter, areascal,
     &      chantype, flchan, 
     &      npichan, nechan, energ_lo, energ_hi,
     &      rmfversn,hduclas3,status)
        if (status.ne.0) call exit_fitsio ('MATRIX, '//filename,status)
        
        call ftmnhd (unitrmf,-1,'EBOUNDS',0,status)
        if (status.ne.0) call exit_fitsio ('EBOUNDS, '//filename,status)
        call rdebd3(unitrmf,chatter,npichanmax, 
     &      telescop,instrume,detnam,filter,areascal, 
     &      chantype, flchan,
     &      npichan,channel,e_min,e_max,rmfversn,status)
        
        
*        call get_cl_par ('fptemp',arg)
*        if (.not.defined(arg)) 
*     ~      call exiterror ('fptemp=? (focal plane temperature)')
*        read (arg,*) fpt
        
        call get_cl_par ('fef',fefname)
        if (.not.defined(fefname)) 
     ~      call exiterror ('fef=?')
        call loadfefdata (fefname)
*        do iccd=0,9
*          call saoimage (fefregion(1,1,iccd),128,128,'i')
*        enddo

        pha_fef_to_pi_rmf = yespar ('pha_fef_to_pi_rmf')

       if (pha_fef_to_pi_rmf) then ! load gain map
          call determine_gain_file (filename)
          call loadgain (filename,
     ~        gain_chipxmin, gain_chipxmax, gain_chipymin,
     ~        gain_chipymax, ngainccdreg, ngainpoints, pha, energy,
     ~        NGAINREGMAX, NGAINEMAX)
        endif
          

      endif
      
*------------------------------------------------------------------------
c INI PIXLIB
c  Read aimpoint and fpsys
      useroll=.false.
      call read_pixlib_pars (aimpoint, fpsys, useroll, roll_average)
      call ini_pixlib (aimpoint,fpsys)


*------------------------------------------------------------------------
c   1) Go to the primary header of the PHA file
      status = 0
      call ftflnm (unitpha,phafile,status)
      call ftghdn (unitpha,nhdu,status)
      call ftmahd (unitpha,1,hdutype,status)
      if (status.ne.0) call exit_fitsio (phafile,status)

*------------------------------------------------------------------------
c   2) Read in detector image
      call ftg2de (unitpha,0,0.0,ndetimgx,ndetimgx,ndetimgy,detimg,anyf,
     ~    status)
      if (status.ne.0) call exit_fitsio (phafile,status)

cccc IF the user wants to calculate the matrix just in the region center,
      if (yespar('one_fef_in_reg')) then ! Find the average point in the region
                                ! and calculate matrix in this position
        detxmean =0
        detymean =0
        totweight=0
        k = 0
        do j=1,ndetimgy
          do i=1,ndetimgx
            k = k + 1
            wght = detimg(k)
            if (abs(wght).gt.0.0) then
              ! Check out another instance of this conversion below
              ! if fix anything
              call pix2det (float(i),float(j),detx,dety,
     ~            crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2)
              detx=detx+manualshiftx
              dety=dety+manualshifty
              detxmean = detxmean + detx*wght
              detymean = detymean + dety*wght
              totweight = totweight + wght
            endif
          enddo
        enddo
        detx = detxmean/totweight
        dety = detymean/totweight
        pixsize = max(abs(cdelt1),abs(cdelt2))
        call det_coords_to_chip (detx,dety,pixsize,chipx,chipy,iccd)

        ichipx=nint(chipx)
        ichipy=nint(chipy)
        
        l = fefregion ((ichipx-1)/8,(ichipy-1)/8,iccd)
        weight(l)=1
        totweight = 1.0

      else


*------------------------------------------------------------------------
c   3) Initialize arrays and variables
        totweight = 0
        do i=1,nreg
          weight(i)=0
        enddo
        
*------------------------------------------------------------------------
c    Go over the image and calculate weights
        k = 0
        do j=1,ndetimgy
          do i=1,ndetimgx
            k = k + 1
            wght = detimg(k)
            if (abs(wght).gt.0.0) then
              
              totweight = totweight + wght
              
c         a) calculate detx, dety
              call pix2det (float(i),float(j),detx,dety,
     ~            crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2)
              
c         aa) add manual shift
              detx=detx+manualshiftx
              dety=dety+manualshifty
              
              
c         b) convert detx,dety to chipx,chipy,ccdid
              pixsize = max(abs(cdelt1),abs(cdelt2))
              call det_coords_to_chip (detx,dety,pixsize,chipx,chipy,iccd)
              
              ichipx=nint(chipx)
              ichipy=nint(chipy)
              l = fefregion ((ichipx-1)/8+1,(ichipy-1)/8+1,iccd)
              weight(l)=weight(l)+wght
            endif
          enddo
        enddo
        
      endif
      
c Find max weight
      if (totweight.gt.0.0) then
        wmax=0
        do i=1,nreg
          wmax=max(wmax,weight(i))
        enddo
        
        wmin=0.1*wmax*2.0
        wtot=0.0
        do while (wtot.lt.0.9*totweight.and.wmin.gt.1e-10)
          wmin=wmin/2.0
          wtot=0.0
          do i=1,nreg
            if (weight(i).gt.wmin) then
              wtot=wtot+weight(i)
            endif
          enddo
        enddo
      else
        call exiterror ('total weight = 0')
      endif


      if (special low energy treatment) then
        call get_pv_default ('eminrmf',eminthresh,-1000000.0,'e')
      else
        call get_pv_default ('eminrmf',eminthresh,-1000000.0,'e')
      endif
      
c  Read rmfs and add them with the weight
      firstrmf=.true.
      wtot=0.0
      print *,' Weight      Ireg  '
      do k=1,nreg
        if (weight(k).gt.wmin) then
          print '(f6.4,2x,i5)',sngl(weight(k)/totweight),k
          nfef = nfefdata (k)
          if (nfef.gt.nfef2max) call exiterror ('increase nfef2max!')

          if (pha_fef_to_pi_rmf) then
            ix = fefcoords(1,k)
            iy = fefcoords(2,k)
            iccd = fefcoords(3,k)
            igainreg = -1
            do i=1,ngainccdreg(iccd)
              if (ix.ge.gain_chipxmin(iccd,i).and.ix.le.gain_chipxmax(iccd,i)
     ~            .and.
     ~            iy.ge.gain_chipymin(iccd,i).and.iy.le.gain_chipymax(iccd,i))
     ~            then
                igainreg = i
                goto 100
              endif
            enddo
 100        continue
            if (igainreg.lt.0) call exiterror ('gain region not found')
          endif


          do i=1,nfef
            efef(i)=fefdata(1,i,k)
            l = 1
            do icomp = 1,10
              l = l + 1
              g_fwhm(i,icomp)=fefdata(l,i,k)/binfef
              l = l + 1
              g_pos (i,icomp)=fefdata(l,i,k)/binfef
              l = l + 1
              g_ampl(i,icomp)=fefdata(l,i,k)
            enddo
            g_fwhm(i,0)=0
            g_pos(i,0)=npichan
            g_ampl(i,0)=1
            
c --- calculate response at this energy
            do icomp=0,10
              gfwhm(icomp)=g_fwhm(i,icomp)
              gpos(icomp)=g_pos(i,icomp)
              gampl(icomp)=g_ampl(i,icomp)
            enddo
            
            if (pha_fef_to_pi_rmf) then
              gpos(0)=4096
              call fill_in_matr (rspw2,4096,gfwhm,gpos,gampl,
     ~            eminthresh)
              do ich=0,npichan
                rspw(ich)=0
              enddo
              w = 0
              do ich=1,4096
                do ioff=1,3
                  call pha_to_pi (pha(1,iccd,igainreg), energy(1,iccd,igainreg
     ~                ),ngainpoints(iccd,igainreg), ich+phaoff(ioff), ipi)
                  if (ipi.gt.0.and.ipi.le.npichan) then
                    rspw(ipi)=rspw(ipi)+rspw2(ich)
                  endif
                enddo
              enddo
              w = 0
              do ich=1,npichan
                w = w + rspw(ich)
              enddo
              do ich=1,npichan
                rspw(ich)=rspw(ich)/w
              enddo
            else
              call fill_in_matr (rspw,npichan,gfwhm,gpos,gampl,
     ~            eminthresh)
            endif
            
            call resp_to_cresp (rspw(1), npichan, ppoint, nppoints, cresp0(1,i
     ~          ))
            
          enddo
          

          do j=1,nechan
            e = 0.5*(energ_lo(j)+energ_hi(j))
            call find_weights (e,efef,nfef,i1,i2,w1,w2)
            call interpolate_cresp (cresp0(1,i1), cresp0(1,i2), w1,w2, cresp,
     ~          nppoints)
            call cresp_to_resp (ppoint, cresp, nppoints, rspw(1), npichan)
            do i=1,npichan
              resp(i,j)=resp(i,j)+rspw(i)*weight(k)/totweight
            enddo
          enddo
          
          wtot=wtot+weight(k)/totweight
        endif
      enddo
        
      do i=1,npichan
        do j=1,nechan
          resp(i,j)=resp(i,j)/wtot
        enddo
      enddo
      
c   Find grouping
      lo_thresh=1e-6
      maxgrp=100
      call grprmf(chatter, npichanmax, npichan, nechanmax, nechan, 
     &    resp, lo_thresh, maxgrp, 
     &    flchan, ngrp, F_chan, N_chan, status)
      
      
      return
      end
