*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine calc_rmf_weights (
     ~    unitpha, ndetimgx,ndetimgy,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,
     ~    detimg,outfile
     ~    )
      implicit none

      integer unitpha, ndetimgx,ndetimgy
      real crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      real detimg(ndetimgx*ndetimgy)
      character outfile*(*)


c   fitsio variables
      integer nhdu, status, hdutype
      logical anyf
      character*200 phafile

      logical no
      external no


c   Misc
      real wght
      integer i,j,k,l,kreg,icomp
      real detx, dety, chipx,chipy
      real manualshiftx, manualshifty
      real detxmean, detymean, pixsize

      integer iccd,ichipx,ichipy
      character*80 fpsys
      character*200 aimpoint
      real roll_average

      logical defined, yespar
      logical useroll, firstrmf, debug
      character*200 filename


      logical wmap_in_chip
      include 'cfefdata2.inc'
      real weight(nregmax)
      character*200 fefname

      logical firstcall
      data firstcall /.true./
      
      save firstcall, wmap_in_chip

*------------------------------------------------------------------------

      if (firstcall) then

        firstcall = .false.

        wmap_in_chip = yespar ('wmap_in_chip')
        if (wmap_in_chip) then
          call get_parameter_value ('ccd_id',iccd,'j')
        endif

        if (.not.wmap_in_chip) then
          call stf_offsets (manualshiftx, manualshifty)
        endif

        debug = yespar('debug')

        call get_cl_par ('simresp',fefname)
        if (.not.defined(fefname)) 
     ~      call exiterror ('simresp=?')
        call load_cti_scatter (fefname)
        


*------------------------------------------------------------------------
c INI PIXLIB
c  Read aimpoint and fpsys
        useroll=.false.
        call read_pixlib_pars (aimpoint, fpsys, useroll, roll_average)
        call ini_pixlib (aimpoint,fpsys)
      endif

      do i=1,nregmax
        weight(i)=0
      enddo
      

*------------------------------------------------------------------------
c   1) Go to the primary header of the PHA file
      status = 0
      call ftflnm (unitpha,phafile,status)
      call ftghdn (unitpha,nhdu,status)
      call ftmahd (unitpha,1,hdutype,status)
      if (status.ne.0) call exit_fitsio (phafile,status)

*------------------------------------------------------------------------
c   2) Read in detector image
      call ftg2de (unitpha,0,0.0,ndetimgx,ndetimgx,ndetimgy,detimg,anyf,
     ~    status)
      if (status.ne.0) call exit_fitsio (phafile,status)
        
*------------------------------------------------------------------------
c    Go over the image and calculate weights
      k = 0
      do j=1,ndetimgy
        do i=1,ndetimgx
          k = k + 1
          wght = detimg(k)
          if (abs(wght).gt.0.0) then
              
c         a) calculate detx, dety
            call pix2det (float(i),float(j),detx,dety,
     ~          crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2)
              
c         aa) add manual shift
            detx=detx+manualshiftx
            dety=dety+manualshifty
              
              
c         b) convert detx,dety to chipx,chipy,ccdid
            if (wmap_in_chip) then
              chipx = detx
              chipy = dety
            else
              pixsize = max(abs(cdelt1),abs(cdelt2))
              call det_coords_to_chip (detx,dety,pixsize,chipx,chipy,iccd)
            endif

            ichipx=nint(chipx)
            ichipy=nint(chipy)
            l = fefregion ((ichipx-1)/8+1,(ichipy-1)/8+1,iccd)
            weight(l)=weight(l)+wght
          endif
        enddo
      enddo

      call write_fits_image (outfile,weight,nregmax,1,'e','e')
      
      return
      end
