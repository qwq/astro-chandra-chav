      subroutine fill_in_cti_matr (matrix,nmatr,gfwhm,gpos,gampl)
      implicit none
      integer nmatr
      real matrix(-nmatr:nmatr)
      real gfwhm(2),gpos(2),gampl(2)
      real sum, arg
      integer i,j
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      real shift
      save shift
      real exparr (0:25000)
      save exparr
      
      integer ncomp
      real pos(100), ampl(100), sig(100)
      
      
      real chan

      if (firstcall) then
        firstcall = .false.
        call get_pv_default ('rmfshift',shift,0.0,'e')
        do i=0,25000
          exparr(i)=exp(-i/1000.0)
        enddo
      endif

      ncomp = 0
      do j=1,2
        if (gampl(j).ne.0.0) then
          ncomp = ncomp + 1
          pos(ncomp) =  gpos(j)+shift
          ampl(ncomp) = gampl(j)
          sig(ncomp) = gfwhm(j)/2.35482
        endif
      enddo

      do i=-nmatr,nmatr
        chan = i
        sum = 0
        do j=1,ncomp
          arg = 0.5*((chan-pos(j))/sig(j))**2
          if (arg.lt.25.0) then
            sum=sum + ampl(j)*exp(-arg)
          endif
        enddo
        matrix(i)=sum
      enddo


      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine fill_in_cti_matr_king (matrix,nmatr,gfwhm,gpos,gampl)
      implicit none
      integer nmatr
      real matrix(-nmatr:nmatr)
      real gfwhm,gpos,gampl
      integer i
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      real shift
      save shift
      real pos, sig
      
      real chan

      if (firstcall) then
        firstcall = .false.
        call get_pv_default ('rmfshift',shift,0.0,'e')
      endif

      pos = gpos + shift
      sig = gfwhm

      do i=-nmatr,nmatr
        chan = i
        if (chan.le.pos) then
          matrix(i) =
     ~        1/(1+((chan-pos)/sig)**2)**3.70
        else
          matrix(i) =
     ~        1/(1+((chan-pos)/sig)**2)**1.90
        endif
      enddo


      return
      end

