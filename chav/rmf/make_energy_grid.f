      subroutine make_energy_grid (par,nechan,emin,emax,nechanmax)
      implicit none
      character par*(*)
      integer nechan, nechanmax
      real emin(nechanmax), emax(nechanmax)
      
      character*200 str, filename
      integer i,n,lnblnk,status
      double precision e_min,e_max,e_step,ee

      str = par

      n = index(str,':')
      i = index(str,',')
      
      if (n.ne.0.or.i.ne.0) then ! could be numerical argument
        n = lnblnk(str)
        do i=1,n
          if (str(i:i).eq.':'.or.str(i:i).eq.',') str(i:i)=' '
        enddo
        read (str,*,iostat=status) e_min,e_max,e_step
        if (status.eq.0) then
          nechan = 1
          emin(1)=e_min
          emax(1)=e_min+e_step
          ee = e_min + e_step
          do while (ee.le.e_max+e_step*1.0d-5)
            nechan = nechan + 1
            if (nechan.gt.nechanmax) call exiterror ('increase nechanmax')
            emin(nechan)=ee
            emax(nechan)=ee+e_step
            ee = ee + e_step
          enddo
          return
        endif
      endif

c Numerical read was unsuccessful, try reading from a file
      str = par
      i=index(str,',')
      if (i.ne.0) then
        filename = str(1:i-1)
      else
        filename = str
      endif

      i = index(str,'emin=')
      if (i.eq.0) then
        e_min = 0
      else
        read (str(i+5:),*) e_min
      endif
      
      i = index(str,'emax=')
      if (i.eq.0) then
        e_max = 11.0d0
      else
        read (str(i+5:),*) e_max
      endif
      
      call load_energy_grid (filename,e_min,e_max,emin,emax,nechan,nechanmax)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_energy_grid (filename,e_min,e_max,energ_lo,energ_hi
     ~    ,nechan,nechanmax)
      implicit none
      character filename*(*)
      double precision e_min,e_max
      integer nechan,nechanmax 
      real energ_lo(nechanmax), energ_hi(nechanmax)
      double precision ee
      character*80 message

      integer unit, newunit,status

      unit = newunit()
      open (unit,file=filename,status='old')
      status = 0
      nechan = 0
      do while (status.eq.0)
        read (unit,*,iostat=status) ee
        if (status.eq.0) then
          if (ee.ge.e_min.and.ee.le.e_max) then
            nechan = nechan + 1
            if (nechan.gt.nechanmax) call exiterror ('increase nechanmax')
            energ_lo(nechan)=ee
            if (nechan.gt.1) then
              energ_hi(nechan-1)=ee
            endif
          endif
        endif
      enddo
      close(unit)
      nechan = nechan - 1
      if (nechan.le.0) then
        message = filename
        call exiterror ('no energy grid in '//message)
      endif
      
      return
      end

        
            
      
      
        
      
