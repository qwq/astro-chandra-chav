      subroutine find_cont_pos (f,n,level,cpos,nlev)
      implicit none
      integer n, nlev
      real f(n)
      real level(0:nlev), cpos(-nlev:nlev)

      real fmax
      integer imax
      integer i, klev, ilev
      real w1, w2
      

c a) Locate the maximum
      fmax = -1.0e20
      do i=1,n
        if (f(i).gt.fmax) then
          fmax = f(i)
          imax = i
        endif
      enddo

      cpos(0) = imax

c b) One-path linear interpolation on the LEFT side of the peak
      klev = nlev
      i = 2 ! because we might need f(i-1)
      do while (klev.gt.0)
c    find the point which exceeds the given level
        do while (f(i).lt.fmax+level(klev))
          i = i + 1
          if (i.gt.imax) then   ! we reached the maximum - unexpected
            call exiterror ('bad contour levels')
          endif
        enddo
c    we are in the situation when level(klev) is between f(i-1) and f(i)
        w1=f(i)-(fmax+level(klev))
        w2=(fmax+level(klev))-f(i-1)
        cpos(-klev) = (w1*(i-1)+w2*(i))/(w1+w2)
        
c      go to next contour
        klev = klev-1
      enddo

c c) One-path linear interpolation on the RIGHT side of the peak
      klev = 1
      i = imax
      do while (klev.le.nlev)
c    find the point which exceeds the given level
        do while (f(i).gt.fmax+level(klev))
          i = i + 1
          if (i.gt.n) then      ! we reached the end of array
            do ilev = klev,nlev
              cpos(ilev)=n+1+ilev-klev
            enddo
            goto 100
          endif
        enddo
c    we are in the situation when level(klev) is between f(i-1) and f(i)
        w1=f(i)-(fmax+level(klev))
        w2=(fmax+level(klev))-f(i-1)
        cpos(klev) = (w1*(i-1)+w2*(i))/(w1+w2)
        
c      go to next contour
        klev = klev+1
      enddo
 100  continue


      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine interpolate_level_pos (cpos1, cpos2, w1,w2, cpos,nlev)
      implicit none
      integer nlev
      real cpos1(-nlev:nlev), cpos2(-nlev:nlev), cpos(-nlev:nlev)
      real w1,w2
      integer i
      
      do i=-nlev,nlev
        cpos(i)=w1*cpos1(i)+w2*cpos2(i)
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine levels_to_function (cpos,level,nlev,f,n)
      implicit none
      integer nlev, n
      real cpos(-nlev:nlev), level(0:nlev), f(n)

      integer i, ilev, imax
      real w1,w2,c
      
      if (cpos(-nlev).eq.cpos(nlev)) then !
                                ! Pathological case of the delta-function
                                ! response
        do i=1,n
          f(i)=-1e10
        enddo
        imax = cpos(0)
        f(imax)=level(0)
        
      else                      ! normal operation
        
        
        do i=1,min(int(cpos(-nlev))+1,n)
          f(i)=-1e10
        enddo
        
        imax = cpos(0)

        ilev = -nlev+1
        do i=max(int(cpos(-nlev))+2,1), min(n,int(cpos(nlev))+1)
          c = i
          do while (cpos(ilev).lt.c)
            ilev = ilev + 1
            if (ilev.gt.nlev) then
              f(i)=-1e10
              goto 100
            endif
          enddo
                  ! we are between ilev-1 and ilev; linear interpolation
          w2 = c - cpos(ilev-1)
          w1 = cpos(ilev) - c
          f(i) = (w1*level(iabs(ilev-1))+w2*level(iabs(ilev)))/(w1+w2)
        enddo
 100    continue
        do i=max(int(cpos(nlev))+2,1),n
          f(i)=-1e10
        enddo
      endif

      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine scale_level_pos (cpos,nlev,scale)
      implicit none
      integer nlev
      real cpos(-nlev:nlev), scale

      integer i

      do i=-nlev,nlev
        cpos(i)=cpos(0)+scale*(cpos(i)-cpos(0))
      enddo
      
      return
      end
