      subroutine loadgain (gainfile,
     ~    chipxmin, chipxmax, chipymin, chipymax,
     ~    nccdreg, npoints, pha, energy, NREGMAX, NEMAX)
      implicit none
      character*(*) gainfile
      integer NREGMAX, NEMAX
      integer chipxmin (0:9,NREGMAX), chipxmax (0:9,NREGMAX),
     ~    chipymin (0:9,NREGMAX), chipymax (0:9,NREGMAX)
      integer nccdreg (0:9)
      integer npoints (0:9,NREGMAX)
      real pha(NEMAX,0:9,NREGMAX)
      real energy(NEMAX,0:9,NREGMAX)

      integer NREGMAXINT, NEMAXINT
      parameter (NREGMAXINT=10240, NEMAXINT=100)
      integer*2 chxmin(NREGMAXINT), chxmax(NREGMAXINT), chymin(NREGMAXINT),
     ~    chymax(NREGMAXINT),ccdid(NREGMAXINT), np(NREGMAXINT)
      real ph(NEMAXINT,NREGMAXINT), e(NEMAXINT,NREGMAXINT)

      integer unit,status, nrows
      integer ccdidcol, chipxmincol, chipxmaxcol, chipymincol, chipymaxcol,
     ~    npointscol, phacol, energycol
      logical anyf
      integer i,j,iccd,k
      integer lnblnk

      character*200 message

      message = 'Load GAIN file: '//gainfile
      print '(a,$)',message(1:lnblnk(message))

      status = 0

      call FTGIOU (unit,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)
      
      call FTNOPN (unit,gainfile,0,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)

      call FTMNHD (unit, -1, 'AXAF_DETGAIN', 0,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)

      call FTGNRW (unit,nrows,status)
      if (nrows.gt.NREGMAXINT) then
        print*,nrows, NREGMAXINT
        call exiterror ('increase array size in loadgain')
      endif

      call FTGCNO (unit,.false.,'CCD_ID',ccdidcol,status)
      call FTGCNO (unit,.false.,'CHIPX_MIN',chipxmincol,status)
      call FTGCNO (unit,.false.,'CHIPX_MAX',chipxmaxcol,status)
      call FTGCNO (unit,.false.,'CHIPY_MIN',chipymincol,status)
      call FTGCNO (unit,.false.,'CHIPY_MAX',chipymaxcol,status)
      call FTGCNO (unit,.false.,'NPOINTS',npointscol,status)
      call FTGCNO (unit,.false.,'PHA',phacol,status)
      call FTGCNO (unit,.false.,'ENERGY',energycol,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)

      call FTGCVI (unit,ccdidcol,1,1,nrows,0,ccdid,anyf,status)
      call FTGCVI (unit,chipxmincol,1,1,nrows,0,chxmin,anyf,status)
      call FTGCVI (unit,chipxmaxcol,1,1,nrows,0,chxmax,anyf,status)
      call FTGCVI (unit,chipymincol,1,1,nrows,0,chymin,anyf,status)
      call FTGCVI (unit,chipymaxcol,1,1,nrows,0,chymax,anyf,status)
      call FTGCVI (unit,npointscol,1,1,nrows,0,np,anyf,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)

      do i=1,nrows
        k = np(i)
        if (k.gt.NEMAXINT) then
          call exiterror ('increase array-2 size in loadgain')
        endif
        call FTGCVE (unit,phacol,i,1,k,0.0,ph(1,i),anyf,status)
        call FTGCVE (unit,energycol,i,1,k,0.0,e(1,i),anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (gainfile,status)

      call FTCLOS (unit,status)
      call FTFIOU (unit,status)
      if (status.ne.0) call exit_fitsio (gainfile,status)
      
      do iccd=0,9
        nccdreg(iccd)=0
      enddo

      do i=1,nrows
        iccd = ccdid(i)

        nccdreg(iccd)=nccdreg(iccd)+1
        
        if (nccdreg(iccd).gt.NREGMAX)  then
          call exiterror ('too many regions in loadgain')
        endif

        chipxmin(iccd,nccdreg(iccd)) = chxmin(i)
        chipxmax(iccd,nccdreg(iccd)) = chxmax(i)
        chipymin(iccd,nccdreg(iccd)) = chymin(i)
        chipymax(iccd,nccdreg(iccd)) = chymax(i)
        npoints(iccd,nccdreg(iccd))  = np(i)

        if (np(i).gt.NEMAX) then
          call exiterror ('too many energy points in loadgain')
        endif
        
        k = np(i)
        call sort2 (k,ph(1,i),e(1,i))
        do j=1,k
          pha(j,iccd,nccdreg(iccd)) = ph(j,i)
          energy(j,iccd,nccdreg(iccd)) = e(j,i)
        enddo
      enddo

      print*,' OK'
      
      return
      end
