      subroutine pix2det (x,y,detx,dety,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2)
      implicit none
      real x,y,detx,dety,crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2

      real dx,dy, cosr,sinr, temp

      dx = (x-crpix1)*cdelt1
      dy = (y-crpix2)*cdelt2
      
      if (crota2 .ne. 0.0) then
        cosr = cos(crota2*3.1415926536/180)
        sinr = sin(crota2*3.1415926536/180)
        temp = dx * cosr - dy * sinr
        dy = dy * cosr + dx * sinr
        dx = temp
      endif

      detx = crval1 + dx
      dety = crval2 + dy
      return
      end
c The code above is adopted from worldpos.c; the relevant piece is
*C   *** Offset from ref pixel
*        dx = (xpix-xrefpix) * xinc
*        dy = (ypix-yrefpix) * yinc
*C   *** Take out rotation
*        cosr = dcos(rot*cond2r)
*        sinr = dsin(rot*cond2r)
*        if (rot .ne. 0.0) then
*          temp = dx * cosr - dy * sinr
*          dy = dy * cosr + dx * sinr
*          dx = temp
*        end if


      
