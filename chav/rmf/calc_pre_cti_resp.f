      subroutine calc_pre_cti_resp (ee,resp,n,iccd, inode)
      implicit none
      real ee
      integer n
      real resp(n)
      integer iccd, inode
      
      integer iccdold
      data iccdold /-1/
      integer ieold
      data ieold /-1/

      integer npha
      parameter (npha=4096)
      integer nsim2max, ngrpmax
      parameter (nsim2max=600)
      parameter (ngrpmax=600)
      real respfit0 (npha,2)
      real shift(npha), shift_w(npha)
      real eref(nsim2max)
      integer nsim
      integer*2 ibounds(ngrpmax,nsim2max)
      real baverage (ngrpmax,nsim2max)
      integer*2 bshift (ngrpmax,nsim2max)
      integer nbounds(nsim2max)

      real gain0, gainnode(0:9,0:3), shiftnode(0:9,0:3)
      
      character*200 filename
      logical defined, yespar

      save nsim, iccdold, ieold, eref, nbounds, ibounds, baverage, 
     ~    bshift, gain0, gainnode, shiftnode

c FITSIO
      
      integer ie, j, i, i1, i2
      real respbin(npha)
      real s1, s2, w, w1, w2, e

c Global tweak
      logical qtweak
      save qtweak
      character parname*80
      integer ntweak, ntweakmax, ntweak0
      parameter (ntweakmax=10000)
      real etweak(ntweakmax), tweak(ntweakmax),
     ~    tweak0(ntweakmax,0:9), etweak0(ntweakmax)
      save ntweak, etweak, tweak
      save ntweak0, etweak0, tweak0
      
      logical expert_mode
      logical firstcall
      data firstcall /.true./
      save firstcall
      real scale_precti_rmf
      save scale_precti_rmf

      real x



      if (firstcall) then
        call get_cl_par ('simresp',filename)
        call get_parameter_value_default ('scale_precti_rmf',scale_precti_rmf,
     ~      1.0,'e')
        if (.not.defined(filename)) call exiterror ('simresp=?')
        call load_global_tweaks (filename,etweak0,tweak0,ntweak0,ntweakmax)
        firstcall=.false.
      endif
      

      if (iccd.ne.iccdold) then
        iccdold = iccd
        call get_cl_par ('simresp',filename)
        if (.not.defined(filename)) call exiterror ('simresp=?')
        call read_prectiresp (filename,iccd,nsim,eref,nbounds,ibounds,baverage
     ~      ,bshift,ngrpmax,nsim2max, gain0, gainnode,shiftnode)
        ieold = -1

        qtweak = .false.
        ntweak = ntweak0
        do i=1,ntweak
          etweak(i)=etweak0(i)
          tweak(i)=tweak0(i,iccd)
          if (abs(tweak(i)-1.0).gt.0.0001) qtweak = .true.
        enddo
            

        expert_mode = yespar('expert_mode')
        write (parname,'(''global_tweak_'',i1)') iccd
        call get_cl_par (parname,filename)
        if ((.not.expert_mode).and.defined(filename)) then
          write (0,'(a)') 'DON''T USE '//parname
          write (0,'(a)') ' USE NEW acis-resp.fits instead'
          call exit(1)
        elseif (expert_mode) then
          if (defined(filename)) then
            qtweak = .true.
            call read_global_tweak (filename,etweak,ntweak,tweak,ntweakmax)
          else
            qtweak = .false.
          endif
        endif
      endif

      e = ee
      if (qtweak) then
        if (e.le.etweak(1)) then
          ie = 1
          w = 1.0
        else if (e.gt.etweak(ntweak)) then
          ie = ntweak - 1
          w = 0.0
        else
          ie = ntweak/2
          call hunt (etweak,ntweak-1,e,ie)
          if (ie.lt.1.or.ie.gt.nsim-1) then
            call exiterror ('unexpected error in calc_pre_cti_resp')
          endif
          call get_weights (e,etweak(ie),etweak(ie+1),w1,w2)
          w = w1
        endif
        e = e * (tweak(ie)*w+tweak(ie+1)*(1.0-w))
      endif

      if (e.le.eref(1)) then
        ie = 1
        w = 1.0
        
      else if (e.gt.eref(nsim)) then
        ie = nsim - 1
        w = 0.0
      else
        ie = nsim/2
        call hunt (eref,nsim-1,e,ie)
        if (ie.lt.1.or.ie.gt.nsim-1) then
          call exiterror ('unexpected error in calc_pre_cti_resp')
        endif
        call get_weights (e,eref(ie),eref(ie+1),w1,w2)
        w = w1
      endif

      do i=1,npha
        respbin(i)=0
      enddo

      do i=1,ibounds(1,ie)-1
        shift_w(i)=0
      enddo
      do i=ibounds(nbounds(ie),ie)+1,npha
        shift_w(i)=0
      enddo
      do i=1,nbounds(ie)-1
        i1 = ibounds(i,ie)
        i2 = ibounds(i+1,ie)
        s1 = bshift(i,ie)/100.0
        s2 = bshift(i+1,ie)/100.0
        if (i1.eq.i2) then
          shift_w(i1)=s1
        else
          do j=i1,i2
            shift_w(j)=(s1*(i2-j)+s2*(j-i1))/(i2-i1)
          enddo
        endif
      enddo
      
*      do i=1,npha
*        shift(i)=shift_w(i)*gain0/gainnode(iccd,inode)
*      enddo

      do i=1,npha
        j = nint((i-shiftnode(iccd,inode))*gainnode(iccd,inode)/gain0)
        if (j.ge.1.and.j.le.npha) then
          shift(i)=shift_w(j)*gain0/gainnode(iccd,inode)
        else
          shift(i)=0
        endif
      enddo

      if (ie.ne.ieold) then
        ieold = ie
        call fill_multires_smooth (ibounds(1,ie),baverage(1,ie),nbounds(ie),
     ~      respfit0(1,1),npha,gain0,gainnode(iccd,inode),
     ~      shiftnode(iccd,inode))
        call fill_multires_smooth (ibounds(1,ie+1),baverage(1,ie+1),
     ~      nbounds(ie+1),respfit0(1,2),npha,gain0,gainnode(iccd,inode),
     ~      shiftnode(iccd,inode))
      endif

      call interpolate_shift (respfit0(1,1),respfit0(1,2),shift
     ~    ,respbin,npha,w)


      if (e.le.eref(nsim)) then
        do i=1,n
          resp(i)=respbin(i)
        enddo
      else ! extrapolate beyond the simulated energies
        do i=1,n
          x = i*eref(nsim)/e
          i1 = int(x)
          if (i1.eq.0) i1=1
          i2 = i1 + 1
          w1 = i2 - x
          w2 = 1 - w1
          if (i2.le.n) then
            resp(i) = respbin(i1)*w1 + respbin(i2)*w2
          else if (i1.le.n) then
            resp(i) = respbin(i1)*w1
          else
            resp(i) = 0
          endif
          resp(i)=resp(i)*eref(nsim)/e
        enddo
      endif

      if (abs(1-scale_precti_rmf).gt.1e-4) then
        call escale_response (resp,n,scale_precti_rmf)
      endif

      call normalize_response (resp,n)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine interpolate_shift (y1,y2,s,y,n,w)
      implicit none
      integer n
      real y1(n), y2(n), y(n), w
      real s(n)
      integer i

      real totweight, sum
      real shift
      integer i1,i2
      real x1,x2,v


      do i=1,n

        shift = s(i)

        if (abs(shift).lt.0.1) then
          y(i)=w*y1(i)+(1.0-w)*y2(i)
        else
          sum = 0
          totweight = 0
          x1 = i-(1-w)*shift
          x2 = i+w*shift
          if (x1.gt.1.0.and.x1.lt.float(n)) then
            i1 = int(x1)
            i2 = i1+1
            v = y1(i1)*(i2-x1)+y1(i2)*(x1-i1)
            sum = sum + v*w
            totweight = totweight + w
          endif
          if (x2.gt.1.0.and.x2.lt.float(n)) then
            i1 = int(x2)
            i2 = i1+1
            v = y2(i1)*(i2-x2)+y2(i2)*(x2-i1)
            sum = sum + v*(1-w)
            totweight = totweight + (1-w)
          endif
          if (totweight.gt.0.0) sum = sum / totweight
          y(i)=sum
        endif
      enddo
        
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
      subroutine read_prectiresp (file, iccd,
     ~    nsim,
     ~    e,nbounds,ibounds,baverage,ishift,
     ~    ngrpmax,nsimmax, gain0, gainnode, shiftnode)
      implicit none
      character*(*) file
      integer iccd
      integer nsim, ngrpmax, nsimmax
      real e(nsimmax)
      integer nbounds(nsimmax)
      integer*2 ibounds(ngrpmax,nsimmax), ishift(ngrpmax,nsimmax)
      real baverage(ngrpmax,nsimmax)
      real gain0, gainnode(0:9,0:3), shiftnode(0:9,0:3)

      integer ie, i, inode
      integer nchips, ichip(100)
      logical found
      character*80 ccdidstring, comment, extname

c FITSIO
      integer unit, status, hdutype
      integer cole, colgrp, colbnd, colrsp, colshift
      logical anyf
      integer*2 nulli

      logical qadjgain, nopar
      external nopar

      status = 0

      call ftgiou (unit,status)
      call ftnopn (unit,file,0,status)
      call FTMNHD (unit, 2, 'ACIS PRECTI RESP', 0, status)
      if (status.ne.0) call exit_fitsio (file,status)
      
      found = .false.
c     Go to the ACIS PRECTI RESP extension
      do while (.not. found) 
        call ftgkys (unit,'EXTNAME',extname,comment,status)
        if (extname.eq.'ACIS PRECTI RESP') then
          call ftgkys (unit,'CCD_ID',ccdidstring,comment,status)
          if (status.ne.0) call exit_fitsio (file,status)
          call form_chip_list (ccdidstring,nchips,ichip)
          found = .false.
          do i=1,nchips
            if (iccd.eq.ichip(i)) then
              found = .true.
            endif
          enddo
        else
          found = .false.
        endif
        if (.not.found) then
          call FTMRHD(unit,1,hdutype,status)
          if (status.ne.0) call exit_fitsio (file,status)
        endif
      enddo

c     Read the gain information
      call ftgkye (unit,'GAIN0',gain0,comment,status)
      if (status.ne.0) call perror_fitsio('GAIN0',status)

      qadjgain = .not.nopar('adjust_gain')

      do i=1,nchips
        do inode = 0,3
          if (qadjgain) then
            write (ccdidstring,'(''GAIN'',i1,i1)') ichip(i), inode
            call ftgkye (unit,ccdidstring,gainnode(ichip(i),inode),comment
     ~          ,status)
            if (status.ne.0) call perror_fitsio(ccdidstring,status)
            write (ccdidstring,'(''SHIFT'',i1,i1)') ichip(i), inode
            call ftgkye (unit,ccdidstring,shiftnode(ichip(i),inode),comment
     ~          ,status)
            if (status.ne.0) then
              shiftnode(ichip(i),inode) = 0
              status = 0
            endif
          else
            gainnode(ichip(i),inode)=gain0
            shiftnode(ichip(i),inode)=0
          endif
**          print*,gain0,gainnode(ichip(i),inode),shiftnode(ichip(i),inode)
        enddo
      enddo
      if (status.ne.0) call exit_fitsio (file,status)

c     Read the max number of groups and energy points
      call ftgnrw (unit,nsim,status)
      if (status.ne.0) call exit_fitsio (file,status)

      if (nsim.gt.nsimmax) then
        call exiterror ('Increase nsimmax in read_prectiresp')
      endif

c     Get columns
      call ftgcno (unit,.false.,'ENERGY',cole,status)
      call ftgcno (unit,.false.,'NGRP',colgrp,status)
      call ftgcno (unit,.false.,'PHABOUND',colbnd,status)
      call ftgcno (unit,.false.,'AVERESP',colrsp,status)
      call ftgcno (unit,.false.,'SHIFT',colshift,status)
      if (status.ne.0) call exit_fitsio (file,status)


      nulli = 0
      do ie=1,nsim
        call ftgcve (unit,cole,ie,1,1,0.0,e(ie),anyf,status)
        call ftgcvj (unit,colgrp,ie,1,1,0,nbounds(ie),anyf,status)
        if (nbounds(ie).gt.ngrpmax) then
          call exiterror ('Increase ngrpmax in read_prectiresp')
        endif
        call ftgcvi (unit,colbnd,ie,1,nbounds(ie),nulli,ibounds(1,ie),anyf
     ~      ,status)
        call ftgcve (unit,colrsp,ie,1,nbounds(ie),0.0,baverage(1,ie),anyf
     ~      ,status)
        call ftgcvi (unit,colshift,ie,1,nbounds(ie),nulli,ishift(1,ie),anyf
     ~      ,status)
        
        if (status.ne.0) call exit_fitsio (file,status)
      enddo

      call ftclos (unit,status)
      call ftfiou (unit,status)

      if (status.ne.0) call exit_fitsio (file,status)
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine fill_multires_smooth (ibounds,average,nbounds,fit,npha,
     ~    gain0, gain1,shift1)
      implicit none
      integer nbounds
      integer*2 ibounds(nbounds)
      real average(nbounds)
      integer npha
      real fit(npha)
      
      real xfit(10000), yfit(10000)
      double precision sum

      integer i,j,jmin,jmax, jmaxold
      real gain0, gain1, shift1

      do i=1,nbounds-1
        jmin = ibounds(i)
        jmax = ibounds(i+1)-1
        
        yfit(i)=average(i)
        xfit(i)=0.5*(jmin+jmax)*gain0/gain1+shift1
      enddo
      
*      do i=1,ibounds(1)-1
*        fit(i)=0
*      enddo
*      do i=ibounds(nbounds),npha
*        fit(i)=0
*      enddo
      do i=1,npha
        fit(i)=0
      enddo
      

      jmaxold = -1
      do i=1,nbounds-1
        jmin = int(ibounds(i)*gain0/gain1+shift1)
        if (jmaxold.gt.0.and.jmin.eq.jmaxold+2) then
          jmin = jmaxold+1
        endif
        jmax = int((ibounds(i+1)-1)*gain0/gain1+shift1)
        jmaxold = jmax

        if (i.eq.1.or.i.eq.nbounds-1) then
          do j=jmin,jmax
            fit(j)=yfit(i)
          enddo
        else
          if (jmin.eq.jmax) then
            fit(jmin)=yfit(i)
          else
            do j=jmin,jmin+(jmax-jmin)/2
              fit(j) = yfit(i-1) +
     ~            (j-xfit(i-1))*(yfit(i)-yfit(i-1))/(xfit(i)-xfit(i-1))
            enddo
            do j=jmin+(jmax-jmin)/2+1,jmax
              fit(j) = yfit(i) +
     ~            (j-xfit(i))*(yfit(i+1)-yfit(i))/(xfit(i+1)-xfit(i))
            enddo
          endif
        endif
      enddo

      sum = 0
      do i=1,npha
        sum = sum + fit(i)
      enddo
      do i=1,npha
        fit(i) = fit(i)/sum
      enddo

      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_global_tweak (file,etweak,ntweak,tweak,ntweakmax)
      implicit none
      character file*(*)
      integer ntweakmax,ntweak
      real etweak(ntweakmax), tweak(ntweakmax)
      
      integer unit,newunit, status
      external newunit


      write (0,*) 'reading global tweak: ',file
      unit = newunit()
      open (unit,file=file,status='old')
      ntweak = 0
      status = 0
      do while (status.eq.0)
        read (unit,*,iostat=status) etweak(ntweak+1), tweak(ntweak+1)
        if (status.eq.0) then
          ntweak = ntweak + 1
          if (ntweak.ge.ntweakmax-1) then
            call exiterror ('increase ntweakmax for read_global_tweak')
          endif
        endif
      enddo

      close(unit)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      
      subroutine load_global_tweaks (file,etweak,tweak,ntweak,ntweakmax)
      implicit none
      character file*(*)
      integer ntweak,ntweakmax
      real etweak(ntweakmax), tweak(ntweakmax,0:9)

      
c FITSIO
      integer unit, status
      integer cole, coltweak(0:9)
      logical anyf

      integer i,iccd
      character colname*80


      status = 0

      call ftgiou (unit,status)
      call ftnopn (unit,file,0,status)
      if (status.ne.0) call perror_fitsio(file,status)
      
c     Go to the ACIS RESP TWEAK RESP extension
      call FTMNHD (unit, 2, 'ACIS RESP TWEAK', 0, status)
      if (status.ne.0) then ! not global tweak found
        write (0,*) 'WARNING: no global tweaks found in ', file
        write (0,*) ' --- ASSUME YOU''RE AN EXPERT'
        status = 0
        call ftclos(unit,status)
        call ftfiou(unit,status)
        ntweak = 2
        etweak(1)=0
        etweak(2)=10000
        do i=0,9
          tweak(1,i)=1
          tweak(2,i)=1
        enddo
        return
      endif
        
      call ftgnrw (unit,ntweak,status)
      if (status.ne.0) call exit_fitsio (file,status)

      if (ntweak.gt.ntweakmax) then
        call exiterror ('Increase ntweakmax calc_pre_cti_resp')
      endif

c     Get columns
      call ftgcno (unit,.false.,'ENERGY',cole,status)
      do iccd=0,9
        write (colname,'(''GTWEAK'',i1)') iccd
        call ftgcno (unit,.false.,colname,coltweak(iccd),status)
      enddo
      if (status.ne.0) call exit_fitsio (file,status)
      
      call ftgcve (unit,cole,1,1,ntweak,0.0,etweak,anyf,status)
      do iccd=0,9
        call ftgcve (unit,coltweak(iccd),1,1,ntweak,0.0,tweak(1,iccd),
     ~      anyf,status)
      enddo
      if (status.ne.0) call exit_fitsio (file,status)
      call ftclos (unit,status)
      call ftfiou (unit,status)

      if (status.ne.0) call exit_fitsio (file,status)
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine escale_response (y,n,scale)
      implicit none
      integer n
      real y(n), scale

      integer nmax
      parameter (nmax=40000)
      real xpeak (nmax), x(nmax),yscale(nmax), ylog(nmax)
      real ymax, dist

      integer i, npeak, ipeak, ipeak0
      real xscaled

      if (n.gt.nmax) call exiterror ('increase nmax in scale_precti_rmf')
      
      do i=1,n
        x(i)=i
        ylog(i)=log(max(y(i),1e-10))
      enddo

      ymax=0
      do i=1,n
        if (y(i).gt.ymax) then
          ymax = y(i)
        endif
      enddo

      npeak = 0 
      do i=2,n-1
        if (y(i).gt.0.003*ymax) then
          if (y(i).gt.y(i-1).and.y(i).ge.y(i+1)) then
            npeak = npeak + 1
            xpeak(npeak)=i
          endif
        endif
      enddo

      do i=1,n
        dist = 1e10
c 1) find nearest peak
        do ipeak = 1,npeak
          if (abs(xpeak(ipeak)-i).lt.dist) then
            dist = abs(xpeak(ipeak)-i)
            ipeak0 = ipeak
          endif
        enddo

c 2) find scaled location
        xscaled = xpeak(ipeak0) + (i-xpeak(ipeak0))/scale

c 3) find response at the scaled location
        call lin_interpolate (x,ylog,n,xscaled,yscale(i))
        yscale(i)=exp(yscale(i))
        if (yscale(i).lt.1.01e-10) yscale(i)=0
      enddo

      do i=1,n
        y(i)=yscale(i)
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine normalize_response (resp,n)
      implicit none
      integer n
      real resp(n)
      integer i
      double precision sum

      sum = 0
      do i=1,n
        sum = sum + resp(i)
      enddo

      do i=1,n
        resp(i)=resp(i)/sum
      enddo

      return
      end

