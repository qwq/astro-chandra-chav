*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine resp_to_cresp (resp, nch, ppoint, nppoints, cresp)
      implicit none
      integer nch
      real resp(nch)
      integer nppoints
      double precision ppoint (nppoints), cresp(nppoints)
      double precision c(0:10000)
      double precision ch(0:10000)
      integer i, kpp
      double precision w1,w2, sum
c
c I assume that percentage points are sorted from 0 to 1
c (0.0, 0.001, 0.01, 0.02, ... , 0.99, 0.999, 1.0)
      
c 0) invert response
      sum = 0.0d0
      do i=1,nch
        c(i)=resp(i) ! sqrt(max(0.0,resp(i)))
        sum = sum + c(i)
      enddo
      do i=1,nch
        c(i)=c(i)/sum
      enddo
      

c 1) Compute commulative response
      c(0)=0.0d0
      ch(0)=0
      do i=1,nch
        c(i)=c(i-1)+c(i)
        ch(i)=i
      enddo

c 2) One-path linear interpolation
      kpp = 1
      i = 1
      do while (kpp.le.nppoints)
c    find the point which exceeds the given percentage point
        do while (c(i).lt.ppoint(kpp))
          i = i + 1
          if (i.gt.nch) then    ! we reached the end of array, exit
            cresp(kpp)=i
            goto 100
          endif
        enddo
c    we are in the situation when ppoint(kpp) is between c(i-1) and c(i)
        w1=c(i)-ppoint(kpp)
        w2=ppoint(kpp)-c(i-1)
        cresp(kpp) = (w1*(i-1)+w2*(i))/(w1+w2)
        
c      go to next percentage point
        kpp = kpp+1
      enddo
 100  continue
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine cresp_to_resp (ppoint, cresp, nppoints, resp, nch)
      implicit none
      integer nppoints, nch
      double precision ppoint(nppoints), cresp(nppoints)
      real resp(nch)

      double precision c(0:10000)
      
      integer i, kpp, i1, i2, m
      double precision ch, ch1, ch2, p1, p2, w1, w2
      double precision sum
      

      m=int(cresp(1))
      do i=0,m
        c(i)=i/cresp(1)*ppoint(1)
      enddo

      do kpp=1,nppoints-1

        ch1 = cresp(kpp)
        ch2 = cresp(kpp+1)

        p1 = ppoint(kpp)
        p2 = ppoint(kpp+1)

        i1 = int(ch1)+1
        i2 = int(ch2)
        
        i1 = max(i1,1)
        i2 = min(i2,nch)
        do i=i1,i2
          w1 = ch2 - i
          w2 = i - ch1
          c(i) = (p1*w1 + p2*w2)/(w1+w2)
        enddo

      enddo

      m = int(cresp(nppoints))+1
      p1 = ppoint(nppoints)
      p2 = 1.0d0-p1
      ch1 = cresp(nppoints)
      ch2 = nch+0.01
      do i=m,nch
        c(i)=p1 + p2*(i-ch1)/(ch2-ch1)
      enddo
      
      sum = 0
      do i=nch,1,-1
        c(i) = c(i)-c(i-1)
        sum = sum + c(i)
      enddo

      do i=1,nch
        resp(i)=c(i)/sum
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine interpolate_cresp (cresp1, cresp2, w1,w2, cresp,n)
      implicit none
      integer n
      double precision cresp1(n), cresp2(n), cresp(n)
      real w1,w2
      double precision ww1, ww2
      integer i
      
      ww1 = w1
      ww2 = w2
      do i=1,n
        cresp(i)=w1*cresp1(i)+w2*cresp2(i)
      enddo

      return
      end

      
