      subroutine map_pi_to_pha (pha, energy, npoints, mindex, npichan)
      implicit none
      integer npoints, npichan
      real pha(npoints), energy(npoints), mindex(0:npichan)

      real e

      real de_pi
      logical firstcall
      data firstcall /.true./
      save firstcall, de_pi

      integer i

      if (firstcall) then
        firstcall=.false.
        call get_parameter_value_default ('pi_channel_width',de_pi,14.6,'e')
      endif
      
      do i=0, npichan
        e = de_pi*(i-0.5)
        call lin_interpolate (energy,pha,npoints,e,mindex(i))
      enddo
      
      return
      end


      subroutine pha_to_pi (pha, energy, npoints, ph, ipi)
      implicit none
      integer npoints
      real pha(npoints), energy(npoints)
      real ph
      integer ipi
      
      real ch, e
      
      real de_pi
      logical firstcall
      data firstcall /.true./
      save firstcall, de_pi

      if (firstcall) then
        firstcall=.false.
        call get_parameter_value_default ('pi_channel_width',de_pi,14.6,'e')
      endif

      ch = ph
      call lin_interpolate (pha,energy,npoints,ch,e)
      ipi = int(e/de_pi)+1
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
      subroutine pha_to_pi_float (pha, energy, npoints, iph, pi1,pi2,pi3)
      implicit none
      integer npoints
      real pha(npoints), energy(npoints)
      integer iph
      real pi1,pi2,pi3
      
      real ch, e
      
      real de_pi
      logical firstcall
      data firstcall /.true./
      save firstcall, de_pi

      if (firstcall) then
        firstcall=.false.
        call get_parameter_value_default ('pi_channel_width',de_pi,14.6,'e')
      endif

      ch = iph-0.5
      call lin_interpolate (pha,energy,npoints,ch,e)
      pi1 = e/de_pi

      ch = iph
      call lin_interpolate (pha,energy,npoints,ch,e)
      pi2 = e/de_pi

      ch = iph+0.5
      call lin_interpolate (pha,energy,npoints,ch,e)
      pi3 = e/de_pi

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
      subroutine pha_to_pi_float2 (pha, energy, npoints, iph, pi1,pi2)
      implicit none
      integer npoints
      real pha(npoints), energy(npoints)
      integer iph
      real pi1,pi2
      
      real ch, e
      
      real de_pi
      logical firstcall
      data firstcall /.true./
      save firstcall, de_pi

      if (firstcall) then
        firstcall=.false.
        call get_parameter_value_default ('pi_channel_width',de_pi,14.6,'e')
      endif

      ch = iph-0.5
      call lin_interpolate_gain (pha,energy,npoints,ch,e)
      pi1 = e/de_pi

      ch = iph+0.5
      call lin_interpolate_gain (pha,energy,npoints,ch,e)
      pi2 = e/de_pi

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine lin_interpolate_gain (x,y,n,x0,y0)
      implicit none
      integer n
      real x(n),y(n)
      real x0,y0
      
      
      integer ibin
      save ibin
      
      if(x0.le.x(1))then
        ibin=1
      else if (x0.ge.x(n)) then
        ibin=n-1
      else
*        if (ibin.le.1.or.ibin.gt.n-1) then
*          ibin=n/2
*        endif
        call hunt (x,n,x0,ibin)
*        if (ibin.lt.1.or.ibin.gt.n-1) then
*          pause 'unexpected error in lin_interpolate'
*        endif
      endif
      
      if (x(ibin).ne.x(ibin+1)) then
        y0=y(ibin)+(y(ibin+1)-y(ibin))*(x0-x(ibin))/(x(ibin+1)-x(ibin))
      else
        y0=y(ibin)
      endif

      return
      end
