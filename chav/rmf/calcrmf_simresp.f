      subroutine calc_rmf_simresp (
     ~    chantype,npichanmax, npichan, nechanmax, nechan, energ_lo, energ_hi,
     ~    imaxgrp, ngrp, F_chan, N_chan,
     ~    resp, lo_thresh, e_min, e_max,
     ~    unitpha, ndetimgx,ndetimgy,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,
     ~    detimg,
     ~    flchan,
     ~    rmfpos,
     ~    binchan, PICHAN
     ~    )
      implicit none

      character*(*) chantype
      integer npichanmax,nechanmax,npichan,nechan
      real energ_lo(nechanmax), energ_hi(nechanmax)
      integer imaxgrp,ngrp(nechanmax),F_chan(nechanmax,*),N_chan(nechanmax,*)
      real resp(npichan,nechan)
      real lo_thresh
      real e_min(npichanmax),e_max(npichanmax) ! nominal e bounds of PI chans
      integer unitpha, ndetimgx,ndetimgy
      real crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      real detimg(ndetimgx*ndetimgy)
      integer flchan
      character*(*) rmfpos
      integer binchan
      logical PICHAN
      logical show_resp, show_scat_matr


c   fitsio variables
      integer nhdu, status, hdutype
      logical anyf
      character*200 phafile

      logical no
      external no

      character*200 fefname

      include 'cfefdata2.inc'
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall

      real weight(nregmax)

c   Misc
      double precision totweight
      real wght
      integer i,j,k,l,icomp
      real detx, dety, chipx,chipy
      real manualshiftx, manualshifty
      real detxmean, detymean, pixsize

      integer iccd,ichipx,ichipy, i_ccd
      character*80 fpsys
      character*200 aimpoint
      real roll_average

      logical defined, yespar, nopar
      logical useroll, debug, apply_scatter
      character*200 filename

      integer chatter
      integer maxgrp

      double precision wmin,wmax,wtot, wtotgrp, wcurrent

      integer nfefmax, nfef
      parameter (nfefmax=10000)
      real phafit(nfefmax),
     ~    g_fwhm(nfefmax,2), g_pos(nfefmax,2),g_ampl(nfefmax,2)
      real e, gfwhm(2), gpos(2), gampl(2)

c Scatter matrics etc
      integer npha, ndelta
      parameter (npha=3750)
      parameter (ndelta=500)
      real deltaphamatr (-ndelta:ndelta, npha)
      real rspw(-ndelta:ndelta), rspw2(-ndelta:ndelta)
      real rspprecti(10000), rspcti(10000)
      integer ioldmin(10000), ioldmax(10000)
      integer irespmin, irespmax, ie, iold, inew

      integer nfitmax
      parameter (nfitmax=100)
      real deltaphafitmatr (-ndelta:ndelta, nfitmax)

      integer nlev
      parameter (nlev=100)
      real level(0:nlev), cpos(-nlev:nlev), cposfit(-nlev:nlev,nfitmax)
      real sum, w1, w2, pha, normadd, w
      integer ifit1, ifit2, ipha

      integer nchan

      integer inode, knode
      logical ccd_found, set_to_zero
      
      

c Gain map
      
      integer NGAINREGMAX, NGAINEMAX
      parameter (NGAINREGMAX=4094, NGAINEMAX=100)
      integer gain_chipxmin (0:9,NGAINREGMAX), gain_chipxmax (0:9,NGAINREGMAX)
     ~    ,gain_chipymin (0:9,NGAINREGMAX), gain_chipymax (0:9,NGAINREGMAX)
      integer ngainccdreg (0:9)
      integer ngainpoints (0:9,NGAINREGMAX)
      real phagain(NGAINEMAX,0:9,NGAINREGMAX)
      real energy(NGAINEMAX,0:9,NGAINREGMAX)

      save gain_chipxmin, gain_chipxmax, gain_chipymin, gain_chipymax,
     ~    ngainccdreg, ngainpoints, phagain, energy

      integer ix,iy, igainreg
      real pi1,pi2
      integer ipi1,ipi2,ipi

      integer ipi10(4096), ipi20(4096)
      real pi10(4096), pi20(4096)

      integer i1,i2

      real floor

      real scalectimatr
      logical noscatter

      logical wmap_in_chip

      integer rmfshift

      integer iregmax
      logical calcebounds

      logical zero_shift
      real meanpos, flux1,flux2

      real shift_from_z

*------------------------------------------------------------------------

      show_resp = yespar('show_resp')
      show_scat_matr=yespar('show_scat_matr')
      zero_shift = yespar ('zero_cti_shift')
      noscatter = yespar ('noctiscatter')
      apply_scatter = .not.nopar('apply_scatter')
            

      call get_parameter_value_default ('rmfshift',rmfshift,0,'j')
      call get_parameter_value_default ('shift_from_z',shift_from_z,0.0,'e')

      wmap_in_chip = yespar ('wmap_in_chip')
      if (wmap_in_chip) then
        call get_parameter_value ('ccd_id',iccd,'j')
      endif

      call get_parameter_value_default ('scalectimatr',scalectimatr,1.0,'e')

      if (.not.PICHAN) then
        call get_cl_par ('gainfile',filename)
        if (defined(filename)) then
          calcebounds = .true.
        else
          calcebounds = .false.
        endif
      else
      	calcebounds = .false.
      endif

      debug = yespar('debug')

      do i=1,nregmax
        weight(i)=0.0
      enddo
      do i=1,npichan
        do j=1,nechan
          resp(i,j)=0
        enddo
      enddo

      if (firstcall) then

        firstcall = .false.

        
        
*        call get_cl_par ('fptemp',arg)
*        if (.not.defined(arg)) 
*     ~      call exiterror ('fptemp=? (focal plane temperature)')
*        read (arg,*) fpt
        
        call get_cl_par ('simresp',fefname)
        if (.not.defined(fefname)) 
     ~      call exiterror ('simresp=?')
        call load_cti_scatter (fefname)

        if (PICHAN.or.calcebounds) then
          call determine_gain_file (filename)
          call loadgain (filename,
     ~        gain_chipxmin, gain_chipxmax, gain_chipymin,
     ~        gain_chipymax, ngainccdreg, ngainpoints, phagain, energy,
     ~        NGAINREGMAX, NGAINEMAX)
        endif
      endif
      
*------------------------------------------------------------------------
      if (.not.defined(rmfpos)) then


        if (.not.wmap_in_chip) then

          if (.not.yespar('wmap_rmfw')) then
            call stf_offsets (manualshiftx, manualshifty)
          
c INI PIXLIB
c  Read aimpoint and fpsys
            useroll=.false.
            call read_pixlib_pars (aimpoint, fpsys, useroll, roll_average)
            call ini_pixlib (aimpoint,fpsys)
          endif
        else
          manualshiftx=0
          manualshifty=0
        endif

*------------------------------------------------------------------------
c   1) Go to the primary header of the PHA file
        status = 0
        call ftflnm (unitpha,phafile,status)
        call ftghdn (unitpha,nhdu,status)
        call ftmahd (unitpha,1,hdutype,status)
        if (status.ne.0) call exit_fitsio (phafile,status)
        
*------------------------------------------------------------------------
c   2) Read in detector image
        call ftg2de (unitpha,0,0.0,ndetimgx,ndetimgx,ndetimgy,detimg,anyf,
     ~      status)
        if (status.ne.0) call exit_fitsio (phafile,status)

        if (yespar('wmap_rmfw')) then
          if (nregmax.ne.ndetimgx) then
            call exiterror ('not matching structure of simresp and rmfw map')
          endif
          totweight=0
          do i=1,nreg
            weight(i)=detimg(i)
            totweight=totweight+weight(i)
          enddo
          
        else ! normal mode
          
cccc IF the user wants to calculate the matrix just in the region center,
          if (yespar('one_fef_in_reg')) then ! Find the average point in the region
                                ! and calculate matrix in this position
            detxmean =0
            detymean =0
            totweight=0
            k = 0
            do j=1,ndetimgy
              do i=1,ndetimgx
                k = k + 1
                wght = detimg(k)
                if (abs(wght).gt.0.0) then
                  call pix2det (float(i),float(j),detx,dety,
     ~                crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2)
                  detx=detx+manualshiftx
                  dety=dety+manualshifty
                  detxmean = detxmean + detx*wght
                  detymean = detymean + dety*wght
                  totweight = totweight + wght
                endif
              enddo
            enddo
            detx = detxmean/totweight
            dety = detymean/totweight
            pixsize = max(abs(cdelt1),abs(cdelt2))
            if (wmap_in_chip) then
              chipx = detx
              chipy = dety
            else
              call det_coords_to_chip (detx,dety,pixsize,chipx,chipy,iccd)
            endif
            
            
            ichipx=nint(chipx)
            ichipy=nint(chipy)
            
            l = fefregion ((ichipx-1)/8,(ichipy-1)/8,iccd)
            weight(l)=1
            totweight = 1.0
            
          else
            
            
*------------------------------------------------------------------------
c   3) Initialize arrays and variables
            totweight = 0
            do i=1,nreg
              weight(i)=0
            enddo
          
*------------------------------------------------------------------------
c    Go over the image and calculate weights
            k = 0
            do j=1,ndetimgy
              do i=1,ndetimgx
                k = k + 1
                wght = detimg(k)
                if (abs(wght).gt.0.0) then
                  
                  totweight = totweight + wght
                  
c         a) calculate detx, dety
                  call pix2det (float(i),float(j),detx,dety,
     ~                crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2)
                  
c         aa) add manual shift
                  detx=detx+manualshiftx
                  dety=dety+manualshifty
                  
                  
c         b) convert detx,dety to chipx,chipy,ccdid
                  if (wmap_in_chip) then
                    chipx = detx
                    chipy = dety
                  else
                    pixsize = max(abs(cdelt1),abs(cdelt2))
                    call det_coords_to_chip (detx,dety,pixsize,chipx,chipy
     ~                  ,iccd)
                    if (iccd.lt.0) then ! wrong coords:
                      write (0,*)
     ~                    ' ! could not get chip coordinates for detx,dety='
     ~                    ,detx,dety
                      goto 7821
                    endif
                  endif
                  
                  
                  ichipx=nint(chipx)
                  ichipy=nint(chipy)
                  l = fefregion ((ichipx-1)/8+1,(ichipy-1)/8+1,iccd)
                  weight(l)=weight(l)+wght
                  
 7821             continue
                endif
              enddo
            enddo
          endif
        endif
      else
        totweight = 0
        do i=1,nreg
          weight(i)=0
        enddo
        read (rmfpos,*) iccd, ichipx, ichipy
        l = fefregion ((ichipx-1)/8+1,(ichipy-1)/8+1,iccd)
        iregmax = l
        weight(l) = 1
        totweight = 1
      endif

c Find max weight
      if (totweight.gt.0.0) then
        wmax=0
        do i=1,nreg
          if (weight(i).gt.wmax) then
            iregmax=i
            wmax = weight(i)
          endif
        enddo
        
        wmin=0.1*wmax*2.0
        wtot=0.0
        do while (wtot.lt.0.9*totweight.and.wmin.gt.1e-10)
          wmin=wmin/2.0
          wtot=0.0
          do i=1,nreg
            if (weight(i).gt.wmin) then
              wtot=wtot+weight(i)
            endif
          enddo
        enddo
      else
        call exiterror ('total weight = 0')
      endif

c Setup levels for contour interpolation
      do i=0,nlev
        level(i)=-i*15.0/nlev
      enddo

c  Read rmfs and add them with the weight
      wtot=0.0
      wcurrent = 0.0
      do i_ccd=0,9
        do inode = 0,3

*          print*,i_ccd,inode
c In each ccd group, we need its own PRE-CTI response, so initialize all local
c matrices

          wtotgrp = 0.0
          set_to_zero = .false.
          do k=1,nreg
c Determine whether the current reg falls in one of the CCDs in the current
c group
            
            ccd_found = .false.
*            if (weight(k).gt.wmin.and.fefcoords(3,k).eq.i_ccd) then
            if (weight(k).gt.wmin.and.fefcoords(3,k).eq.i_ccd) then
              knode = (fefcoords(1,k)-1)/256
              if (inode.eq.knode) ccd_found = .true.

              if ((.not.set_to_zero).and.ccd_found) then
                do j=1,npha
                  do i=-ndelta,ndelta
                    deltaphamatr(i,j)=0
                  enddo
                enddo
                set_to_zero = .true.
              endif
            endif
            

c Do we need to do this region?
            if (ccd_found) then
              wcurrent = wcurrent + weight(k)
              write (*,
     ~            '("\rcurrently doing region ",i5,1x,"(",i2,"%)",$)')
     ~            k,int(wcurrent*100/totweight)
              call flush(6)
c Copy the PHA scatter matrix parameters into local arrays
              nfef = nfefdata (k)
              do i=1,nfef
                phafit(i)=fefdata(1,i,k)
                l = 1
                do icomp = 1,2
                  l = l + 1
                  g_fwhm(i,icomp)=fefdata(l,i,k)
                  l = l + 1
                  g_pos (i,icomp)=fefdata(l,i,k)
                  l = l + 1
                  g_ampl(i,icomp)=fefdata(l,i,k)
                enddo
              enddo

c Find correct gain if the output matrix will be in the PI channels
              if (PICHAN.or.(calcebounds.and.k.eq.iregmax)) then
                ix = fefcoords(1,k)
                iy = fefcoords(2,k)
                igainreg = -1
                do i=1,ngainccdreg(i_ccd)
                  if (ix.ge.gain_chipxmin(i_ccd,i).and.ix.le.gain_chipxmax(i_ccd
     ~                ,i).and.iy.ge.gain_chipymin(i_ccd,i).and.iy.le
     ~                .gain_chipymax(i_ccd,i))then
                    igainreg = i
                    goto 100
                  endif
                enddo
 100            continue
                if (igainreg.lt.0) call exiterror ('gain region not found')
              endif
              
c Make the PHA scatter matrix in the reference points
              do i=1,nfef
                do icomp = 1,2
                  gfwhm(icomp)=g_fwhm(i,icomp)
                  gampl(icomp)=g_ampl(i,icomp)*
     ~                gfwhm(icomp)/max(gfwhm(icomp),1.5)
                  if (feftype(iccd).ne.2) then
                    gfwhm(icomp)=max(gfwhm(icomp),1.5)
                  endif
                  gpos(icomp)=g_pos(i,icomp)
                enddo
                if (zero_shift) then
                  flux1 = gampl(1)*gfwhm(1)
                  flux2 = gampl(2)*gfwhm(2)
                  meanpos = (gpos(1)*flux1+gpos(2)*flux2)/(flux1+flux2)
                  gpos(1) = gpos(1)-meanpos
                  gpos(2) = gpos(2)-meanpos
                endif
                  
                if (feftype(iccd).eq.2) then
                  call fill_in_cti_matr_king
     ~                (deltaphafitmatr(-ndelta,i),ndelta,gfwhm,gpos,gampl)
                else
                  call fill_in_cti_matr (deltaphafitmatr(-ndelta,i),ndelta,
     ~                gfwhm,gpos,gampl)
                endif
                
              enddo
              
              do i=1,nfef
c   we will need log of the matrix in order to interpolate
                call log_of_cti_matr (deltaphafitmatr(-ndelta,i),2*ndelta+1)
c   calculate level positions for the reference responses
                call find_cont_pos
     ~              (deltaphafitmatr(-ndelta,i), 2*ndelta+1,
     ~              level, cposfit(-nlev,i), nlev)
              enddo

c Make the PHA scatter matrix by contour interpolation.
              ifit1 = nfef/2
              do ipha=1,npha
                pha = ipha
c      find reference points and weights
                if (pha.le.phafit(1)) then
                  ifit1 = 1
                  ifit2 = 2
                  w1 = 1.0
                  w2 = 0.0
                elseif (pha.ge.phafit(nfef)) then
                  ifit1 = nfef-1
                  ifit2 = nfef
                  w1 = 0.00
                  w2 = 1.0
                else
                  call hunt (phafit,nfef,pha,ifit1)
                  if (ifit1.lt.1.or.ifit1.gt.nfef-1) then
                    pause 'unexpected error in hunt'
                  endif
                  ifit2 = ifit1+1
                  w2 = pha-phafit(ifit1)
                  w1 = phafit(ifit2)-pha
                  sum = w1+w2
                  w1 = w1/sum
                  w2 = w2/sum
                endif
                
                if (.not.noscatter) then
c      Contour interpolation
                  call interpolate_level_pos (cposfit(-nlev,ifit1),cposfit(
     ~                -nlev,ifit2),w1,w2,cpos,nlev)
                  if (scalectimatr.ne.1.0) then
                    ! Scale response, keeping the peak location intact
                    call scale_level_pos (cpos,nlev,scalectimatr)
                  endif
                  call levels_to_function (cpos, level, nlev, rspw2, 2*ndelta
     ~                +1)
                  
c       contour interpolation gives the log of the function; the normalization
c       is arbitrary, so we have to reormalize:
                  call exp_of_cti_matr (rspw2,2*ndelta+1,sum)
                  if (sum.gt.0.0) then
                    normadd = weight(k)/sum/totweight
                  else
                    normadd = 0.0
                  endif
                else            ! scatter matrix is delta function
                  do i=-ndelta,ndelta
                    rspw2(i)=0
                  enddo
                  rspw2(0)=1
                  normadd = weight(k)/totweight
                endif
                
c    Recalculate ebounds if necessary
                if (calcebounds.and.iregmax.eq.k) then
                  do j=1,npichan
                    w = (j-1)*binchan+1
                    call lin_interpolate (
     ~                  phagain(1,i_ccd,igainreg), energy(1,i_ccd,igainreg),
     ~                  ngainpoints(i_ccd,igainreg),w,e_min(j))
                    w = w + binchan
                    call lin_interpolate (
     ~                  phagain(1,i_ccd,igainreg), energy(1,i_ccd,igainreg),
     ~                  ngainpoints(i_ccd,igainreg),w,e_max(j))
                    e_min(j)=e_min(j)/1000
                    e_max(j)=e_max(j)/1000
                  enddo
                endif

c    Rebin the scatter matrix into PI space if necessary:
                if (PICHAN) then
                  do i=-ndelta,ndelta
                    rspw(i)=0
                  enddo

                  if (ipha.eq.1) then
c             find channel boundaries in the PI space
                    do i=1,4096
                      call pha_to_pi_float2 (
     ~                    phagain(1,i_ccd,igainreg), energy(1,i_ccd,igainreg
     ~                    ),ngainpoints(i_ccd,igainreg), i, pi1,pi2)
                      pi1=pi1*(1+shift_from_z)-500.0 ! -1.0
                      pi2=pi2*(1+shift_from_z)-500.0 ! -1.0
                      pi10(i)=pi1
                      pi20(i)=pi2
                      ipi10(i)=floor(pi1)+1
                      ipi20(i)=floor(pi2)+1

c$$$                                  ! UNNECESSARY PIECE
c$$$                      pi1=pi10(i) ! strange , but speeds up UltraIIIcu
c$$$                      pi2=pi20(i) ! by 10%
c$$$                      ipi1=ipi10(i) !
c$$$                      ipi2=ipi20(i) !
                    enddo
                  endif
                     

                  do j=-ndelta,ndelta
                    if (rspw2(j).ne.0.0) then
                      i = ipha+j
                      if (i.ge.1.and.i.lt.4096) then
                        ipi1=ipi10(i)
                        ipi2=ipi20(i)
                        pi1=pi10(i)
                        pi2=pi20(i)
c             and add the appropriate fraction of the flux into PI channels
                        if ((ipi1.eq.ipi2)) then
                          if (ipi1.ge.-ndelta.and.ipi1.le.ndelta) then
                            rspw(ipi1)=rspw(ipi1)+rspw2(j)
                          endif
                        else
                          if (ipi1.ge.-ndelta.and.ipi1.le.ndelta) then
                            rspw(ipi1)=rspw(ipi1)+rspw2(j)*(ipi1-pi1)/(pi2
     ~                          -pi1)
                          endif
                          if (ipi2.ge.-ndelta.and.ipi2.le.ndelta) then
                            rspw(ipi2)=rspw(ipi2)+rspw2(j)*(pi2-ipi2+1)/(pi2
     ~                          -pi1)
                          endif
                          do ipi=ipi1+1,ipi2-1
                            if (ipi.ge.-ndelta.and.ipi.le.ndelta) then
                              rspw(ipi)=rspw(ipi)+rspw2(j)
                            endif
                          enddo
                        endif
                      endif
                    endif
                  enddo
                  call add_1d_array (deltaphamatr(-ndelta,ipha), rspw,
     ~                2*ndelta+1,normadd,1,2*ndelta+1)
                  
                else
                  call add_1d_array (deltaphamatr(-ndelta,ipha), rspw2,
     ~                2*ndelta+1,normadd,1,2*ndelta+1)
                endif
                
              enddo
              
              wtotgrp = wtotgrp + weight(k)/totweight
              wtot=wtot+weight(k)/totweight
              
            endif
          enddo
          
          if (wtotgrp.gt.0.0.and.show_scat_matr)
     ~        call saoimage (deltaphamatr,2*ndelta+1,npha,'e')
          
          if (wtotgrp.gt.0.0) then
            
c Convolve the scatter matrix with the PRE-CTI matrix
            
c To save storage, the scatter matrix is computed not as p(pha,phaold), but as
c p(pha-phaold,phaold) or as p(pi-500,phaold), so we need a special treatment
c for the matrix multiplication
            
            nchan = npichan*binchan
            
c   Find response boundaries
            do inew=1,nchan
              ioldmin(inew)=npha
              ioldmax(inew)=1
            enddo
            do iold=1,npha
              do j = -ndelta,ndelta
                if (deltaphamatr(j,iold).gt.0.0) then
                  if (PICHAN) then
                    inew = j + 500
                  else
                    inew = iold + j
                  endif
                  if (inew.ge.1.and.inew.le.npha) then
                    ioldmin(inew)=min(ioldmin(inew),iold)
                    ioldmax(inew)=max(ioldmax(inew),iold)
                  endif
                endif
              enddo
            enddo
            
            
c  Convolution
            do ie = 1, nechan
              e = (energ_lo(ie)+energ_hi(ie))/2.0/(1+shift_from_z)
c ... calculate matrix at this energy
              call calc_pre_cti_resp (e,rspprecti,npha,i_ccd,inode)
              
              if (.not.apply_scatter) then
                
                do i=1,nchan
                  k = (i-1)/binchan + 1
                  resp(k,ie)=resp(k,ie)+rspprecti(i)
                enddo
                
              else
                
                irespmin=npha
                irespmax=1
                do i=1,npha
                  if (rspprecti(i).gt.0.0) then
                    irespmin=min(irespmin,i)
                    irespmax=max(irespmax,i)
                  endif
                enddo
                do inew = 1,nchan
                  i1 = ioldmin(inew)
                  i2 = ioldmax(inew)
                  i1 = max(i1,irespmin)
                  i2 = min(i2,irespmax)
                  sum = 0
                  do iold=i1,i2
                    if (PICHAN) then
                      i = inew-500
                    else
                      i = inew - iold
                    endif
                    if (i.ge.-ndelta.and.i.le.ndelta) then
                      sum = sum + rspprecti(iold)*deltaphamatr(i,iold)
                    endif
                  enddo
                  rspcti(inew)=sum
                enddo
                
                do i=1,nchan
                  k = (i-1+rmfshift)/binchan + 1
                  if (k.ge.1.and.k.le.npichan) then
                    resp(k,ie)=resp(k,ie)+rspcti(i)
                  endif
                enddo
              endif
              
            enddo
            
            if (show_resp) then
              call saoimage (resp,npichan,nechan,'e')
              call mset(resp,npichan*nechan,0.0,'e')
            endif

          endif
          
        enddo
      enddo
      
      do i=1,npichan
        do j=1,nechan
          resp(i,j)=resp(i,j)/wtot
        enddo
      enddo
      
      
c   Find grouping
      call get_pv_default ('lo_thresh',lo_thresh,1e-6,'e')
      maxgrp=100
      call grprmf(chatter, npichan, npichan, nechan, nechan, 
     &    resp, lo_thresh, maxgrp, 
     &    flchan, ngrp, F_chan, N_chan, status)
      
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine add_1d_array (x,y,n,w,i1,i2)
      implicit none
      integer n
      real x(n), y(n), w
      integer i1,i2
      integer i, k1,k2
      real vw,v
      k1 = i1
      k2 = i2
      vw = w
      do i=k1,k2
        v = y(i)
        if (v.ne.0.0) then
          x(i)=x(i)+vw*v
        endif
      enddo
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine log_of_cti_matr (x,n)
      implicit none
      integer n
      real x(n)
      integer j
      real f
      
      
      do j=1,n
        f = x(j)
        if (f.gt.1.0e-10) then
          x(j) = log(f)
        else
          x(j) = -1.0e10
        endif
      enddo

      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine exp_of_cti_matr (x,n,sum)
      implicit none
      integer n
      real x(n), sum
      integer i

      sum = 0
      do i=1,n
        if (x(i).gt.-16.0) then
          x(i) = exp(x(i))
          sum = sum + x(i)
        else
          x(i)=0
        endif
      enddo

      return
      end
