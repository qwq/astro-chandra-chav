c The program is identical to that which loads fefs.
c
c
      subroutine load_cti_scatter (feffile)
      implicit none
      character*(*) feffile
      
      integer iunit,status

      integer nrows,nrowsmax
      parameter (nrowsmax=172032) ! P2_RESP N0008 row count
      integer chipxmin,chipxmax,chipymin,chipymax
      integer*2 ireg(nrowsmax), nulli
      real  nulle
      integer icol, icole
      integer icolchipxmin,icolchipxmax,icolchipymin,icolchipymax,icolccdid
      integer hdutype, nullj
      logical anyf
      integer ne
      include 'cfefdata2.inc'
      real energy(nemax), gpar(nemax,2,3) ! 3 parameters: width, pos, ampl
      integer icolg(2,3)
      integer indx(nemax)
      
      integer *2 wchipxmin (nrowsmax), wchipxmax(nrowsmax),
     ~    wchipymin(nrowsmax), wchipymax(nrowsmax),
     ~    wccdid(nrowsmax)
      real wenergy(nrowsmax)
      real wgpar (nrowsmax,2,3)
      integer windx(nrowsmax)

      character*80 funcid,comment,name
      character*200 sjnk

      integer iccd,imin,imax,jmin,jmax,i,j,l,lw,k,icomp,itype
      integer iregold
      integer lnblnk

      integer iprintreg
      character*80 gov, keyname, keyval
      logical defined

      sjnk = feffile
      write (0,'(''LOAD CTI SCATTER file: '',a,$)') sjnk(1:lnblnk(sjnk))
      status = 0
      call ftgiou (iunit,status)
      if (status.ne.0) call exit_fitsio (feffile,status)
      
      call ftnopn (iunit,feffile,0,status)
      if (status.ne.0) call exit_fitsio (feffile,status)
      
      hdutype = 2 ! binary table
      call ftmnhd(iunit, hdutype, 'ACIS_CTI_SCATTER', 0, status)
      if (status.ne.0) call exit_fitsio (feffile,status)

*      call ftgkys (iunit,'FUNCTION',funcid,comment,status)
*      if (status.ne.0) call exit_fitsio (feffile,status)
*
*      call rmblanks(funcid)
*      call strlow (funcid)
*      
*      if (funcid.ne.'fg1+fg2+fg3+fg4+fg5+fg6+fg7+fg8+fg9+fg10') then
*        call exiterror ('Unknown FEF function: '//funcid)
*      endif

      
      call ftgnrw (iunit,nrows,status)
      if (status.ne.0) call exit_fitsio (feffile,status)

      if (nrows.gt.nrowsmax) call exiterror
     ~    ('NROWSMAX is too small in loadfefdata')
      
      call ftgcno (iunit,.false.,'REGNUM',icol,status)
      if (status.ne.0) call exit_fitsio (feffile,status)
      nulli=0
      call ftgcvi (iunit,icol,1,1,nrows,nulli,ireg,anyf,status)
      do i=1,nrows
        ireg(i)=ireg(i)! +1
      enddo
*c ! check that regions are sorted
*      do i=2,nrows
*        if (ireg(i).lt.ireg(i-1)) then
*          call exiterror ('REGNUM is not sorted in fef file '//sjnk)
*        endif
*      enddo
*      nreg = ireg(nrows)

      call indexxi2 (nrows,ireg,windx) ! windx is an index that sorts REGNUM values
      nreg = ireg(windx(nrows)) ! nreg is the largest value in REGNUM

c fill in the region mask
      do i=1,128
        do j=1,128
          do k=0,9
            fefregion (i,j,k) = -1
          enddo
        enddo
      enddo

      nulli=0
      nullj=0
      nulle=0

      call ftgcno (iunit,.false.,'CCD_ID',icolccdid,status)
      call ftgcvi (iunit,icolccdid,1,1,nrows,nulli,wccdid,anyf,status)

      call ftgcno (iunit,.false.,'CHIPX_LO',icolchipxmin,status)
      call ftgcvi (iunit,icolchipxmin,1,1,nrows,nulli,wchipxmin,anyf,status)

      call ftgcno (iunit,.false.,'CHIPX_HI',icolchipxmax,status)
      call ftgcvi (iunit,icolchipxmax,1,1,nrows,nulli,wchipxmax,anyf,status)

      call ftgcno (iunit,.false.,'CHIPY_LO',icolchipymin,status)
      call ftgcvi (iunit,icolchipymin,1,1,nrows,nulli,wchipymin,anyf,status)

      call ftgcno (iunit,.false.,'CHIPY_HI',icolchipymax,status)
      call ftgcvi (iunit,icolchipymax,1,1,nrows,nulli,wchipymax,anyf,status)

      call ftgcno (iunit,.false.,'PHACHAN',icole,status)
*      call ftgcno (iunit,.false.,'ENERGY',icole,status)
      call ftgcve (iunit,icole,1,1,nrows,nulle,wenergy,anyf,status)

      do icomp=1,2
*      do icomp=1,10
        do itype=1,3
          if (itype.eq.1) then
            write (name,'(''G'',i2,''_FWHM'')') icomp
          else if (itype.eq.2) then
            write (name,'(''G'',i2,''_POS'')') icomp
          else if (itype.eq.3) then
            write (name,'(''G'',i2,''_AMPL'')') icomp
          endif
          call rmblanks (name)
          call ftgcno (iunit,.false.,name,icolg(icomp,itype),status)
          call ftgcve (iunit,icolg(icomp,itype),1,1,nrows,nulle,wgpar(1,icomp
     ~        ,itype),anyf,status) ! read pars into wgpar(1, icomp, itype)
        enddo
      enddo


      if (status.ne.0) call exit_fitsio (feffile,status)

      do iccd=0,9 ! Look for FITFUNC0 through FITFUNC9 in header
        write (keyname,'(''FITFUNC'',i1)') iccd
        call ftgkys (iunit,keyname,keyval,comment,status)
        if (status.ne.0) then
          status = 0
          feftype(iccd)=1 ! gauss
        else ! Determine which function in fill_in_cti_mtr.f is used
          if (keyval.eq.'2king') then
            feftype(iccd)=2 ! FI chips only uses G1_FWHM and G1_POS
          else
            feftype(iccd)=1 ! BI chips uses all 6 params
          endif
        endif
      enddo

      if (status.ne.0) call exit_fitsio (feffile,status)
      call ftclos(iunit,status)
      call ftfiou(iunit,status)
      if (status.ne.0) call exit_fitsio (feffile,status)
      

      iregold=-1
      ne = 0
      do lw=1,nrows
        l = windx(lw)
        if ((ireg(l).ne.iregold.and.l.gt.1).or.lw.eq.nrows) then ! new region
          if (lw.eq.nrows) then ! add the last point
            ne = ne + 1
            energy(ne)=wenergy(l)
            do icomp=1,2
              do itype=1,3
                gpar (ne,icomp,itype) = wgpar(l,icomp,itype)
              enddo
            enddo
          endif

          if (ne.gt.0) then
            nfefdata (iregold)=ne
            ! sort by energies
            call indexx (ne,energy,indx)
            do i=1,ne
              fefdata (1,i,iregold) = energy(indx(i))
              k = 1
              do icomp=1,2
*             do icomp=1,10
                do itype=1,3
                  k = k + 1
                  fefdata (k,i,iregold) = gpar(indx(i),icomp,itype)
                enddo
              enddo
            enddo
          endif

          ne = 0
          iccd = wccdid(l)
          chipxmin = wchipxmin(l)
          chipxmax = wchipxmax(l)
          chipymin = wchipymin(l)
          chipymax = wchipymax(l)
*          call ftgcvj (iunit,icolccdid,L,1,1,nullj,iccd,anyf,status)
*          call ftgcvj (iunit,icolchipxmin,L,1,1,nullj,chipxmin,anyf,status)
*          call ftgcvj (iunit,icolchipxmax,L,1,1,nullj,chipxmax,anyf,status)
*          call ftgcvj (iunit,icolchipymin,L,1,1,nullj,chipymin,anyf,status)
*          call ftgcvj (iunit,icolchipymax,L,1,1,nullj,chipymax,anyf,status)
          imin = (chipxmin-1)/8+1
          imax = (chipxmax-1)/8+1
          jmin = (chipymin-1)/8+1
          jmax = (chipymax-1)/8+1

          iregold = ireg(l)
          if (iregold.gt.nregmax) then
            write (0,*) iregold, nregmax
            call exiterror
     ~          ('too many regions in fef file :'//sjnk)
          endif

          do i=imin,imax
            do j=jmin,jmax
              fefregion (i,j,iccd) = iregold
              fefcoords(1,iregold) = (chipxmin+chipxmax)/2
              fefcoords(2,iregold) = (chipymin+chipymax)/2
              fefcoords(3,iregold) = iccd
            enddo
          enddo
        endif
        ne = ne + 1
        energy(ne)=wenergy(l)
*        call ftgcve (iunit,icole,l,1,1,nulle,energy(ne),anyf,status)

        do icomp=1,2
*       do icomp=1,10
          do itype=1,3
            gpar (ne,icomp,itype) = wgpar(l,icomp,itype)
*            call ftgcve (iunit,icolg(icomp,itype),l,1,1,nulle,
*     ~          gpar(ne,icomp,itype),anyf,status)
          enddo
        enddo

      enddo
      

      write (0,*)
      
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine indexxi2(n,arrin,indx)
c
c
c   indexes an array arrin(n), i.e. outputs an array indx(n)
c
c  such that arr(indx(j)) are in ascending order for j=1,..N
c
c  N and ArriN are bot changed on exit
c
c
c
      implicit none
      integer n,i,j,l,ir,indxt
      integer*2 arrin(n)
      integer indx(n)
      real q

      do j=1,n
        indx(j)=j
      enddo

      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          indxt=indx(l)
          q=arrin(indxt)
        else
          indxt=indx(ir)
          q=arrin(indxt)
          indx(ir)=indx(1)
          ir=ir-1
          if(ir.eq.1)then
            indx(1)=indxt
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
          endif
          if(q.lt.arrin(indx(j)))then
            indx(i)=indx(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        indx(i)=indxt
      goto 10
      end
