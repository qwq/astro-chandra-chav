      subroutine get_rmf_structure ( 
     ~    binchan,PICHAN,chantype,npichan,nechan,
     ~    energ_lo, energ_hi, nechanmax,
     ~    e_min,e_max, npichanmax,
     ~    flchan )
      implicit none
      integer binchan
      logical PICHAN
      integer npichan,nechan, npichanmax, nechanmax
      real energ_lo(nechanmax), energ_hi(nechanmax)
      real e_min(npichanmax),e_max(npichanmax)
      integer flchan
      character chantype*(*)

      logical yespar, defined, defpar
      character refrmf*200, filename*200, matext*80
      character*80 telescop, instrume, detnam, filter, rmfversn, hduclas3
      real areascal
      integer channel(8192)
      integer unitrmf, status, blocksize
      integer chatter
      logical debug
      
      integer i

      debug = defpar('debug')

      if (
     ~    (defpar('engrid').or.defpar('egrid')) .and.
     ~    (defpar('changrid').or.defpar('cgrid'))) then
        
        call get_rmf_structure_pars ( 
     ~      binchan,PICHAN,chantype,npichan,nechan,
     ~      energ_lo, energ_hi, nechanmax,
     ~      e_min,e_max, npichanmax,
     ~      flchan )
        return
      endif

      call get_cl_par ('ref_pi_rmf',refrmf)
      if (defined (refrmf)) then
      
        call get_pv_default ('binchan',binchan,1,'j')
        PICHAN = yespar ('pha_fef_to_pi_rmf')

        status = 0
        call ftgiou (unitrmf,status)
        filename = refrmf
        call ftopen (unitrmf,filename,0,blocksize,status)
        if (status.ne.0) call exit_fitsio (filename,status)
        if (debug) then
          chatter = 10
        else
          chatter = 0
        endif
        
        matext = 'SPECRESP MATRIX'
        call ftmnhd (unitrmf,-1,matext,0,status)
        if (status.ne.0) then
          status = 0
          matext='MATRIX'
          call ftmnhd (unitrmf,-1,matext,0,status)
        endif
        if (status.ne.0) 
     ~      call exit_fitsio ('SPECRESP MATRIX, '//filename,status)
        
        call rdrmf3_info(unitrmf, chatter,matext,
     &      telescop, instrume, detnam, filter, areascal,
     &      chantype, flchan, 
     &      npichan, nechan, energ_lo, energ_hi,
     &      rmfversn,hduclas3,status)
        if (status.ne.0) call exit_fitsio ('MATRIX, '//filename,status)
        
        call ftmnhd (unitrmf,-1,'EBOUNDS',0,status)
        if (status.ne.0) call exit_fitsio ('EBOUNDS, '//filename,status)
        call rdebd3(unitrmf,chatter,npichanmax, 
     &      telescop,instrume,detnam,filter,areascal, 
     &      chantype, flchan,
     &      npichan,channel,e_min,e_max,rmfversn,status)
        
        call ftclos(unitrmf,status)
        call ftfiou(unitrmf,status)
        if (status.ne.0) call exit_fitsio ('EBOUNDS, '//filename,status)

      else ! no parameters: use default
        if (debug) then
          write (0,*) 'You have not specified RMF/ARF structure'
          write (0,*) 'I use egrid=0.1:11.0:0.005  cgrid=pi:1:1024:1'
        endif

        binchan = 1
        PICHAN = .true.
        chantype='PI'
        npichan=1024
        nechan = 2179
        do i=1,nechan
          energ_lo(i)=0.1+(i-1)*0.005
        enddo
        do i=1,nechan-1
          energ_hi(i)=energ_lo(i+1)
        enddo
        energ_hi(nechan)=energ_lo(nechan)+0.005
        do i=1,npichan
          e_min(i)=0.0146*(i-1)
          e_max(i)=0.0146*i
          if (e_min(i).eq.0.0) e_min(i)=0.5*e_max(i)
        enddo
        flchan = 1
      endif
        
      return
      end

      

      subroutine get_rmf_structure_pars  ( 
     ~    binchan,PICHAN,chantype,npichan,nechan,
     ~    energ_lo, energ_hi, nechanmax,
     ~    e_min,e_max, npichanmax,
     ~    flchan )
      implicit none

      integer binchan
      logical PICHAN
      integer npichan,nechan, npichanmax, nechanmax
      real energ_lo(nechanmax), energ_hi(nechanmax)
      real e_min(npichanmax),e_max(npichanmax)
      integer flchan
      character chantype*(*)

      character*200 parval
      logical defined

      character word(30)*80
      integer i, lnblnk, n
      
      integer chanmin, chanmax
      integer status

c CHANNEL GRID
      call get_cl_par ('changrid',parval)
      if (.not.defined(parval)) then
        call get_cl_par ('cgrid',parval)
        if (.not.defined(parval)) call exiterror ('cgrid=?')
      endif
      
      n = lnblnk(parval)
      do i=1,n
        if (parval(i:i).eq.':'.or.parval(i:i).eq.',') parval(i:i)=' '
      enddo

      call splitwords(parval,word,n)
      if (n.ne.4) then
        call exiterror ('cgrid != pi:1:max:bin or pha:1:max:bin')
      endif

      call strlow(word(1))
      if (word(1).eq.'pi') then
        chantype = 'PI'
        PICHAN = .true.
      elseif (word(1).eq.'pha') then
        chantype = 'PHA'
        PICHAN = .false.
      else
        call exiterror ('cgrid != pi:... or pha:...')
      endif
        
      read (word(2),*,iostat=status) chanmin
      if (status.ne.0.or.chanmin.ne.1) then
        call exiterror ('cgrid != pi:1:... or pha:1:...')
      endif
        
      read (word(3),*,iostat=status) chanmax
      if (status.ne.0) then
        call exiterror ('cgrid != pi:1:max:... or pha:1:max:...')
      endif
      if (chanmax.le.chanmin) then
        call exiterror ('chanmax <= chanmin in cgrid')
      endif

      read (word(4),*,iostat=status) binchan
      if (status.ne.0) then
        call exiterror ('cgrid != pi:1:max:bin or pha:1:max:bin')
      endif
      if (chanmax.lt.1) then
        call exiterror ('bin < 1 in cgrid')
      endif

      flchan = 1
      npichan = (chanmax-chanmin+1)/binchan
      if (npichan.gt.npichanmax) then
        call exiterror ('increase npichanmax')
      endif

      if (PICHAN) then
        do i=1,npichan
          chanmin = 1+(i-1)*binchan
          chanmax = i*binchan
          e_min(i)=0.0146*(chanmin-1)
          e_max(i)=0.0146*chanmax
          if (e_min(i).eq.0.0) e_min(i)=0.5*e_max(i)
        enddo
      endif

c ENERGY GRID
      call get_cl_par ('engrid',parval)
      if (.not.defined(parval)) then
        call get_cl_par ('egrid',parval)
        if (.not.defined(parval)) call exiterror ('egrid=?')
      endif

      call make_energy_grid (parval,nechan,energ_lo,energ_hi,nechanmax)

      return
      end
