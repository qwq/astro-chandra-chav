  program asol2aoff
    implicit none
    
    interface realloc1
       subroutine realloc1_r (p,n)
         real (kind=4), pointer, dimension (:) :: p
         integer, intent(in) :: n
       end subroutine realloc1_r
       
       subroutine realloc1_d (p,n)
         real (kind=8), pointer, dimension (:) :: p
         integer, intent(in) :: n
       end subroutine realloc1_d
    end interface realloc1

    interface drealloc1
       subroutine drealloc1_r (p,n)
         real (kind=4), pointer, dimension (:) :: p
         integer, intent(in) :: n
       end subroutine drealloc1_r
       
       subroutine drealloc1_d (p,n)
         real (kind=8), pointer, dimension (:) :: p
         integer, intent(in) :: n
       end subroutine drealloc1_d
    end interface drealloc1

    ! convert [multiple] asol files to one aoff specified by the reference 
    ! aoff file

    
    real (kind=8), dimension(:), pointer :: time_asol => null(), time_aoff => null()
    real (kind=8), dimension(:), pointer :: ra_asol => null(), dec_asol => null(), roll_asol => null()
    real (kind=4), dimension(:), pointer :: dy => null(), dz => null(), dtheta => null()

    real (kind=8), dimension(:), pointer :: dx_aoff => null(), dy_aoff => null(), droll_aoff => null(), &
         stf_y => null(), stf_z => null(), stf_roll => null()
    
    character (len=200) :: outfile, reffile, asolfile

    logical, external :: defined

!   CFITSIO stuff
    integer (kind=4) :: ounit, status, aunit, colnum, eunit
    character (len=200) :: comment
    logical :: anyf

    character (len=30) :: ttype(100), tform(100), tunit(100)
    integer :: tfields


!   POSITIONS
    real (kind=8) :: ra_nom, dec_nom, roll_nom


!   MISC
    integer (kind=4) :: iasolfile, naoff, nasol, nrows, i, j

! -----------------------------------------------------------------

    call get_cl_par ('o',outfile)
    if ( .not. defined(outfile) ) call exiterror ('o=??')

! ini cfitsio
    status = 0
    call ftgiou (ounit,status)

    call get_cl_par ('refaoff',reffile)
    if ( defined(reffile) ) then
! create output file using the input as a template
      call fttplt (ounit,outfile,trim(reffile)//'[ASPOFF][roll_offsets>1e10]',&
           status)
      if (status .ne. 0 ) call exit_fitsio (outfile,status)

      ! move to the ASPOFF extension in the output file
      call ftmnhd (ounit,-1,'ASPOFF',0,status)
      if (status .ne. 0 ) call exit_fitsio (outfile,status)

      ! read in the required information
      !RA_NOM  =  2.3061611800000E+02 / Nominal RA
      !DEC_NOM =  2.7692772000000E+01 / Nominal Dec
      !ROLL_NOM=  2.6167899100000E+02 / Nominal Roll
      !RA_PNT  =  2.3063885685110E+02 / Pointing RA
      !DEC_PNT =  2.7709789320036E+01 / Pointing Dec
      !ROLL_PNT=  1.1962325742716E+01 / Pointing Roll

      call ftgkyd (ounit,'RA_NOM',ra_nom,comment,status)
      call ftgkyd (ounit,'DEC_NOM',dec_nom,comment,status)
      call ftgkyd (ounit,'ROLL_NOM',roll_nom,comment,status)
      if (status .ne. 0 ) call exit_fitsio ('RA_NOM, DEC_NOM, ROLL_NOM',status)

    else ! try to get the info from evtfile
      call get_cl_par ('evtfile',reffile)
      if ( .not. defined(reffile) ) then
        call exiterror ('refaoff/evtfile not given')
      endif

      call ftgiou (eunit,status)
      call ftnopn (eunit,trim(reffile)//'[EVENTS]',0,status)
      if (status .ne. 0 ) call exit_fitsio (trim(reffile)//'[EVENTS]',status)
      
      call ftgkyd (eunit,'RA_NOM',ra_nom,comment,status)
      call ftgkyd (eunit,'DEC_NOM',dec_nom,comment,status)
      call ftgkyd (eunit,'ROLL_NOM',roll_nom,comment,status)
      if (status .ne. 0 ) call exit_fitsio ('RA_NOM, DEC_NOM, ROLL_NOM',status)

      ! create aofffile
      call ftinit (ounit,outfile,0,status)
      if (status .ne. 0 ) call exit_fitsio (outfile,status)

      tfields =8
      ttype(1)='TIME'
      ttype(2)='X_OFFSETS'
      ttype(3)='Y_OFFSETS'
      ttype(4)='ROLL_OFFSETS'
      ttype(5)='STF_Y'
      ttype(6)='STF_Z'
      ttype(7)='STF_ROLL'
      ttype(8)='AOFF_GAP'

      tform(1)='1D'
      tform(2)='1E'
      tform(3)='1E'
      tform(4)='1E'
      tform(5)='1D'
      tform(6)='1D'
      tform(7)='1D'
      tform(8)='1I'

      tunit(1)='s'
      tunit(2)='pixel'
      tunit(3)='pixel'
      tunit(4)='deg'
      tunit(5)='mm'
      tunit(6)='mm'
      tunit(7)='deg'
      tunit(8)=' '
      
      call ftibin (ounit,0,tfields,ttype,tform,tunit,'ASPOFF',0,status)

      call ftukyg (ounit,'RA_NOM',ra_nom,10,'nominal ra',status)
      call ftukyg (ounit,'DEC_NOM',dec_nom,10,'nominal dec',status)
      call ftukyg (ounit,'ROLL_NOM',roll_nom,10,'nominal roll',status)

      if (status .ne. 0 ) call exit_fitsio (outfile,status)

      call ftclos(eunit,status)
      call ftfiou(eunit,status)
      if (status .ne. 0 ) call exit_fitsio (reffile,status)

    endif

! Now go over the input asol files
    naoff = 0
    asolfile = 'gov'
    iasolfile = 0

    nasol = 0

    do
       iasolfile = iasolfile + 1
       call get_numbered_par ('asol',iasolfile,asolfile)
       if (.not.defined(asolfile)) exit
       write (0,*) 'processing ',trim(asolfile)
       !      open ASOL at the right extension
       call ftgiou (aunit,status)
       call ftnopn (aunit,trim(asolfile)//'[ASPSOL]',0,status)
       if (status .ne. 0 ) call exit_fitsio (trim(asolfile)//'[ASPSOL]',status)

       !      get the number of rows and allocate memory
       call ftgnrw (aunit,nrows,status)
       if ( nrows > nasol ) then
          call drealloc1 (time_asol,nrows)
          call drealloc1 (ra_asol,nrows)
          call drealloc1 (dec_asol,nrows)
          call drealloc1 (roll_asol,nrows)
          call drealloc1 (dy,nrows)
          call drealloc1 (dz,nrows)
          call drealloc1 (dtheta,nrows)
          nasol = nrows
       endif
       
       ! read columns
       call ftgcno (aunit,0,'time',colnum,status)
       call ftgcvd (aunit,colnum,1,1,nrows,0.0d0,time_asol,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: time:',status)

       call ftgcno (aunit,0,'ra',colnum,status)
       call ftgcvd (aunit,colnum,1,1,nrows,0.0d0,ra_asol,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: ra:',status)

       call ftgcno (aunit,0,'dec',colnum,status)
       call ftgcvd (aunit,colnum,1,1,nrows,0.0d0,dec_asol,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: dec:',status)
 
       call ftgcno (aunit,0,'roll',colnum,status)
       call ftgcvd (aunit,colnum,1,1,nrows,0.0d0,roll_asol,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: roll:',status)

       call ftgcno (aunit,0,'dy',colnum,status)
       call ftgcve (aunit,colnum,1,1,nrows,0.0,dy,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: dy:',status)

       call ftgcno (aunit,0,'dz',colnum,status)
       call ftgcve (aunit,colnum,1,1,nrows,0.0,dz,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: dz:',status)

       call ftgcno (aunit,0,'dtheta',colnum,status)
       call ftgcve (aunit,colnum,1,1,nrows,0.0,dtheta,anyf,status)
       if (status.ne.0) call exit_fitsio ('asol: dtheta:',status)

       call ftclos (aunit,status)
       call ftfiou (aunit,status)

       ! increase the size of the AOFF array
       call realloc1 (time_aoff,naoff+nrows)
       call realloc1 (dx_aoff,naoff+nrows)
       call realloc1 (dy_aoff,naoff+nrows)
       call realloc1 (droll_aoff,naoff+nrows)
       call realloc1 (stf_y,naoff+nrows)
       call realloc1 (stf_z,naoff+nrows)
       call realloc1 (stf_roll,naoff+nrows)
       
       ! convert asol to aaoff info
       do i=naoff+1,naoff+nrows
          j = i - naoff
          time_aoff(i)=time_asol(j)
          stf_y(i)=dy(j)
          stf_z(i)=dz(j)
          stf_roll(i) = dtheta(j)
          
          droll_aoff(i) = roll_asol(j)-roll_nom
          call normalize_angle (droll_aoff(i))

          call sol2off (ra_nom,dec_nom,ra_asol(j),dec_asol(j), &
               dx_aoff(i),dy_aoff(i))
       enddo
       naoff = naoff + nrows

    enddo

    if (iasolfile.eq.1) then ! this means -asol1 was not found
       call exiterror ('asol1=??')
    endif

! write out the aoff1 info
    call ftgcno (ounit,0,'time',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,time_aoff,status)
    if (status.ne.0) call exit_fitsio ('out: time:',status)

    call ftgcno (ounit,0,'x_offsets',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,dx_aoff,status)
    if (status.ne.0) call exit_fitsio ('out: x_offsets:',status)

    call ftgcno (ounit,0,'y_offsets',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,dy_aoff,status)
    if (status.ne.0) call exit_fitsio ('out: y_offsets:',status)

    call ftgcno (ounit,0,'roll_offsets',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,droll_aoff,status)
    if (status.ne.0) call exit_fitsio ('out: roll_offsets:',status)

    call ftgcno (ounit,0,'stf_y',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,stf_y,status)
    if (status.ne.0) call exit_fitsio ('out: stf_y:',status)

    call ftgcno (ounit,0,'stf_z',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,stf_z,status)
    if (status.ne.0) call exit_fitsio ('out: stf_z:',status)

    call ftgcno (ounit,0,'stf_roll',colnum,status)
    call ftpcld (ounit,colnum,1,1,naoff,stf_roll,status)
    if (status.ne.0) call exit_fitsio ('out: stf_roll:',status)
    

    call ftclos (ounit,status)
    call ftfiou (ounit,status)
    if (status .ne. 0 ) call exit_fitsio (outfile,status)
    


  end program asol2aoff

       
  subroutine realloc1_r (p,n)
    implicit none
    real (kind=4), pointer, dimension (:) :: p
    real (kind=4), pointer, dimension (:) :: temp
    integer, intent(in) :: n
    integer :: nold
    
    nold = size(p)
    print*,'>>>',nold
    if (nold>0) then
       allocate (temp(n))
       temp(1:min(n,nold)) = p(1:min(n,nold))
       deallocate(p)
       p => temp
    else
       allocate(p(n))
    endif
    return
  end subroutine realloc1_r

  subroutine realloc1_d (p,n)
    implicit none
    real (kind=8), pointer, dimension (:) :: p, temp
    integer, intent(in) :: n
    integer :: nold

    if (associated(p)) then
       nold = size(p)
       allocate (temp(n))
       temp(1:min(n,nold)) = p(1:min(n,nold))
       deallocate(p)
       p => temp
    else
       allocate(p(n))
    endif
    return
  end subroutine realloc1_d

  subroutine drealloc1_r (p,n)
    implicit none
    real (kind=4), pointer, dimension (:) :: p, temp
    integer, intent(in) :: n

    if (associated(p)) then
       allocate (temp(n))
       deallocate(p)
       p=>temp
    else
       allocate(p(n))
    endif
    return
  end subroutine drealloc1_r

  subroutine drealloc1_d (p,n)
    implicit none
    real (kind=8), pointer, dimension (:) :: p, temp
    integer, intent(in) :: n
    
    if (associated(p)) then
       allocate (temp(n))
       deallocate(p)
       p=>temp
    else
       allocate(p(n))
    endif
    return
  end subroutine drealloc1_d
  

  subroutine normalize_angle (a)
    implicit none
    real (kind=8) :: a

    do 
       if (a<180.0d0) exit
       a = a - 360.0d0
    enddo

    do
       if (a>-180.0d0) exit
       a = a + 360.0d0
    enddo

    return
  end subroutine normalize_angle
