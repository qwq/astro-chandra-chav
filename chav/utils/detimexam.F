#include <zhtools.h>

c
c Reads in the image in the primary header, finds non-zero rectangular 
c non-zero region and maximum in the image
c
      implicit none
      character*200 data
      integer unit,nx,ny
      ZHDECL(real,pimg)
      
      call getarg(1,data)
      
      call op_fits_img (data,unit,nx,ny)

      ZHMALLOC(pimg,nx*ny,'e')
      call do_imexam (data,unit,nx,ny,ZHVAL(pimg))
      call exit(0)
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_imexam (data,unit,nx,ny,img)
      implicit none
      character*(*) data
      integer unit,nx,ny
      
c image arrays
      real img(nx*ny)

c-----------------------------------------------------------------      
      
      
c read image
      call read_fits_image_unit (unit,data,img,nx,ny,'e')
      
      call do_imexam2 (img,nx,ny)
      call exit(0)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine do_imexam2 (img,nx,ny)
      implicit none
      integer nx,ny
      real img(nx,ny)
      
      integer xmin,xmax
      integer ymin,ymax
      integer immax, i,j

      immax = 0
      xmin = nx
      xmax = 1
      ymin = ny
      ymax = 1

      do i=1,nx
        do j=1,ny
          if (img(i,j).gt.0.5) then
            xmin = min(xmin,i)
            ymin = min(ymin,j)
            xmax = max(xmax,i)
            ymax = max(ymax,j)
            immax = max(immax,nint(img(i,j)))
          endif
        enddo
      enddo


      ! need multiline print so that PERL treats them as separate vars
      print*,xmin
      print*,xmax
      print*,ymin
      print*,ymax
      print*,immax
      return
      end

      
      
