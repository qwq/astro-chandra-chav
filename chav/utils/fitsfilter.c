/* Updated for cfitsio-2.033 */
/*
  When possible, this program uses the output file as a name component of 
  the input file, thus avoiding creating possibly huge in-memory files;
  
  If no filters is specified or the optional output name is already supplied
  with the input name, just call fitscopy from CFITSIO
*/

#include <stdio.h>
#include <string.h>
#include "fitsio.h"

#define N 1024

int fitscopy(int argc, char *argv[]);

int main(int argc, char *argv[])
{
  fitsfile *infptr;
  char infile[FLEN_FILENAME];
  char filetype[N], inputfile[N], outfile[N], extspec[N], filter[N], 
    binspec[N], colspec[N];
  int status;
  int n;

  if (argc != 3)
    {
      printf("Usage: fitsfilter inputfile outputfile\n");
      return(1);
    }
  
  status = 0;
  ffiurl (argv[1], filetype, inputfile, outfile, extspec, filter,
          binspec, colspec, &status);
  if (status != 0) {
    ffrprt (stderr,status);
    exit(1);
  }
  
  if (*outfile || ! ( *filter || *binspec || *colspec )) {
    fprintf (stderr,"Nothing to filter, just copy\n");
    exit(fitscopy (argc,argv));
  }
  
  n = strlen(inputfile);
  if (strcmp(inputfile+(n-3),".gz")==0 || strcmp(inputfile+(n-2),".Z")==0 ) {
    fprintf (stderr,"compressed file, call fitscopy\n");
    exit(fitscopy (argc,argv));
  }
  

  strcpy(infile,filetype);
  strcat(infile,outfile);
  strcat(infile,inputfile);
  strcat(infile,"("); strcat (infile,argv[2]); strcat(infile,")");
  if (*extspec ) {
    strcat (infile,"["); strcat(infile,extspec); strcat (infile,"]");
  }
  if (*colspec) {
    strcat (infile,"["); strcat(infile,colspec); strcat (infile,"]");
  }
  if (*filter) {
    fixspec (filter); /* Change ][ to && */
    strcat (infile,"["); strcat(infile,filter); strcat (infile,"]");
  }
  if (*binspec) {
    strcat (infile,"["); strcat(infile,binspec); strcat (infile,"]");
  }

  if ( fits_open_file(&infptr, infile, READONLY, &status) )
    {
      fits_report_error(stderr, status);
      return(status);
    }
  fits_close_file(infptr,  &status);
  if (status)
    fits_report_error(stderr, status);

  exit(0);
}

int fitscopy(int argc, char *argv[])
{
    fitsfile *infptr, *outfptr;
    int status = 0, hdutype, ii;
    char infile[FLEN_FILENAME],outfile[FLEN_FILENAME]; 

    if (argc != 3)
    {
        printf("Usage: fitscopy inputfile outputfile\n");
        return(1);
    }

    strcpy(infile,  argv[1] );  
    strcpy(outfile, argv[2] );  

    if ( fits_open_file(&infptr, infile, READONLY, &status) )
    {
         fits_report_error(stderr, status);
         return(status);
    }

    if ( fits_create_file(&outfptr, outfile, &status) )
    {
         fits_close_file(infptr, &status);
         fits_report_error(stderr, status);
         return(status);
    }

    /* attempt to move to next HDU, until we get an EOF error */
    for (ii = 1; !(fits_movabs_hdu(infptr, ii, &hdutype, &status) ); ii++) 
    {
        if ( fits_copy_hdu(infptr, outfptr, 0, &status) )
        {
            fits_report_error(stderr, status);
            break;
        }
    }

    if (status == END_OF_FILE)   
        status = 0;              /* got the expected EOF error; reset = 0  */

    fits_close_file(infptr,  &status);
    fits_close_file(outfptr, &status);

    if (status)
        fits_report_error(stderr, status);

    return(0);
}

int fixspec (char *spec)
{
  char *s;
  s = spec;
  while ( ( s = strstr (s,"][") ) != NULL){
    s[0]='&'; s[1]='&';
  }
  return 0;
}
