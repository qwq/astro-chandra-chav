/* #include "readpar.h" */
#include <stdio.h>
#include <string.h>

char *RP_Rem_Lead_Blanks(char *arg);

/* This program can be used in shell scripts to get values of command 
   line parameters */

main(int argc, char **argv)
{
  char arg[2000],buff[1000], *name;
  char *p;
  Ini_ReadPar(argc,argv);
  
  getarg (1, buff);
  name=buff;
  while ( name != NULL ) {
    p = strchr (name,',');
    if ( p != NULL ) *p='\0';
    get_command_line_par (name,arg);
    if ( p != NULL ) 
      name = p + 1;
    else
      name = NULL;

    p = RP_Rem_Lead_Blanks(arg);
    if (*p=='\0') strcpy(arg,"defined");
    printf("%s\n",arg);
  }

}
