! AV modified Paul's cocde to better conform the CHAV system

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C Convert asol RA and Dec entries to the equivalent (x, y)
C offsets in the aoff file.
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C***********************************************************
C
C $n$ is the unit vector for the nominal pointing,
C $n = (\cos\alpha \cos\delta, \sin\alpha \cos\delta, \sin\delta)$
C and $m$ is the unit vector for the offset position,
C $m = (\cos\alpha_2 \cos\delta_2, \sin\alpha_2 \cos\delta_2, \sin\delta_2)$.
C Corresponding to $m$ on the tangent plane, defined by $n \cdot r = 1$,
C we have $r = \lambda m$, so $\lambda = 1 / (n \cdot m)$, and 
C $n \cdot m = \cos\delta \cos\delta_2 \cos (\alpha - \alpha_2)
C              + \sin\delta \sin\delta_2
C = 1 - 2 \cos^2[(\delta + \delta_2)/2] \sin^2[(\alpha - \alpha_2)/2]
C     - 2 \sin^2[(\delta - \delta_2)/2] \cos^2[(\alpha - \alpha_2)/2]$
C (latter form is for compatibility with other expressions).
C Coordinate vectors in the tangent plane are $e_x = - e_\alpha$ and
C $e_y = e_\delta$, so
C $x = e_x \cdot r = \lambda \cos\delta_2 \sin (\alpha - \alpha_2)
C = 2 \lambda (\cos[(\delta + \delta_2)/2] \cos[(\delta - \delta_2)/2]
C              + \sin[(\delta + \delta_2)/2] \sin[(\delta - \delta_2)/2])
C   \sin[(\alpha - \alpha_2)/2] \cos[(\alpha - \alpha_2)/2]$
C and
C $y = \lambda [\cos\delta \sin\delta_2 
C               - \sin\delta \cos\delta_2 \cos (\alpha - \alpha_2)]
C = 2 \lambda \{\sin[(\delta + \delta_2)/2] \cos[(\delta + \delta_2)/2]
C               \sin^2 [(\alpha - \alpha_2)/2]
C               - \sin[(\delta - \delta_2)/2] \cos[(\delta - \delta_2)/2]
C               \cos^2 [(\alpha - \alpha_2)/2]\}$.
C 
C Arguments:
C alpha, delta = nominal RA and Dec for observation (deg)
C alpoff, deltoff = actual RA, Dec  (deg)
C x, y = offsets in pixels, as in aoff file
C
      subroutine sol2off (alpha, delta, alpoff, deltoff, x, y)
      implicit none
      double precision alpha, delta, alpoff, deltoff, x, y
      double precision hdalp, shda, s2hda, chda, c2hda, dav, hdd, lambda
      double precision cdav, sdav, chdd, shdd
C NB: better to define these elsewhere (maybe an include file?)
      double precision pi, degtorad
      parameter (pi = 3.141592653589793238462643d0)
      parameter (degtorad = pi / 180.d0)

      logical firstcall
      data firstcall /.true./
      double precision pixsec, radtopix
      save pixsec, radtopix
      
      if (firstcall) then
        firstcall = .false.
        call get_pv_default ('acis_pix_scale',pixsec,0.492d0,'d')
        radtopix = 3600.0d0/(degtorad*pixsec)
      endif


C     $(\alpha - \alpha_2)/2$
      hdalp = 0.5 * degtorad * (alpha - alpoff)
      shda = sin (hdalp)
C     $\sin^2 [(\alpha - \alpha_2)/2]$
      s2hda = shda**2
      chda = cos (hdalp)
C     $\cos^2 [(\alpha - \alpha_2)/2]$
      c2hda = chda**2
C     $(\delta + \delta_2)/2$
      dav = 0.5 * degtorad * (delta + deltoff)
      cdav = cos (dav)
      sdav = sin (dav)
C     $(\delta - \delta_2)/2$
      hdd = 0.5 * degtorad * (delta - deltoff)
      chdd = cos (hdd)
      shdd = sin (hdd)
      lambda = 1.d0
     &     / (1.d0 - 2.d0 * (cdav**2 * s2hda + shdd**2 * c2hda))
      x = 2.d0 * lambda * (cdav * chdd + sdav * shdd) * shda * chda
     &     * radtopix
      y = 2.d0 * lambda * (sdav * cdav * s2hda - shdd * chdd * c2hda)
     &     * radtopix
      return
      end
