      subroutine sky_to_chip (i,j,chipid,chipx,chipy)
      implicit none
      integer i,j
      integer chipid,chipx,chipy
      
      integer binsize
      save binsize
c     logical firstcall /.true./
      logical firstcall
      data firstcall /.true./
      save firstcall
      
      character*80 arg
      logical defined
      
      real x,y, xrot, yrot, detx,dety, chipxx,chipyy
      integer naxmax,naymax,nrotmax
      parameter (naxmax=256, naymax=256, nrotmax=10)
      integer nax,nay,nrot
      real rollav(nrotmax), rollnom
      real crval1, crpix1, cdelt1, crval2, crpix2, cdelt2
      real asphist(naxmax,naymax,nrotmax)
      integer indx(naxmax*naymax*nrotmax)
      save nax,nay,nrot,rollav,crval1,crpix1,cdelt1,crval2,crpix2,cdelt2
     ~    ,asphist, rollnom
      character*200 asphistfile
      character*80 fpsys
      character*200 aimpoint
      real wjunk
      real aspmax
      integer irot,ix,iy,ibin,k
      real rotangle, sina, cosa, shiftx, shifty
      integer iccd
      integer junk, det2chip
      integer ixmax,iymax,irotmax
      save ixmax,iymax,irotmax
      integer npix,npixmax
      real sumx,sumy,xmean,ymean
      save xmean,ymean

      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('imgbin',arg)
        if (.not.defined(arg)) call exiterror ('imgbin=?')
        read (arg,*) binsize
        call get_cl_par ('asphist',asphistfile)
        if (.not.defined(asphistfile)) call exiterror ('-asphist is undefined'
     ~      )
        call load_aspect (asphistfile,
     ~      asphist,naxmax,naymax,nrotmax,nax,nay,nrot,rollav, rollnom,
     ~      crval1, crpix1, cdelt1, crval2, crpix2, cdelt2)

        npixmax = 0
        do irot=1,nrot
          sumx = 0
          sumy = 0
          npix = 0
          do ix=1,nax
            do iy=1,nay
              if (asphist(ix,iy,irot).gt.aspmax) then
                ixmax=ix
                iymax=iy
                irotmax=irot
                aspmax=asphist(ix,iy,irot)
              endif
              npix = npix + asphist(ix,iy,irot)
              sumx = sumx + ix*asphist(ix,iy,irot)
              sumy = sumy + iy*asphist(ix,iy,irot)
            enddo
          enddo
          if (npix.gt.npixmax) then
            npixmax=npix
            xmean = sumx/npix
            ymean = sumy/npix
          endif
        enddo
        
        call indexx(naxmax*naymax*nrot,asphist,indx)

        call read_pixlib_pars (aimpoint, fpsys, .false., wjunk)
        call ini_pixlib (aimpoint,fpsys)
      endif

      x = (i-0.5)*binsize
      y = (j-0.5)*binsize


      chipid = -1

      irot=irotmax
*      ix=ixmax
*      iy=iymax
      rotangle = (rollav(irot)+rollnom)
      sina = sin(rotangle*3.14159265358979/180.0)
      cosa = cos(rotangle*3.14159265358979/180.0)
      shiftx = crval1+(xmean-crpix1)*cdelt1
      shifty = crval2+(ymean-crpix2)*cdelt2
      shiftx = -shiftx
      shifty = -shifty
      xrot = x + shiftx
      yrot = y + shifty
      detx = 4096.0 + (xrot-4096.0)*cosa - (yrot-4096.0)*sina
      dety = 4096.0 + (xrot-4096.0)*sina + (yrot-4096.0)*cosa
      junk = det2chip (detx,dety,chipxx,chipyy,iccd)
      if (iccd.ge.0) then
        chipid = iccd
        chipx = min(1023,max(nint(chipxx),0))
        chipy = min(1023,max(nint(chipyy),0))
        return
      else
        
        chipid = -1
        chipx = 0
        chipy = 0
        do ibin = naxmax*naymax*nrot,1,-1
          k = indx(ibin)
          irot = k/(naxmax*naymax)+1
          iy   = (k-(irot-1)*naxmax*naymax)/naxmax+1
          ix   = k-(irot-1)*naxmax*naymax-(iy-1)*naxmax
          if (asphist(ix,iy,irot).gt.0.5) then
            rotangle = (rollav(irot)+rollnom)
            sina = sin(rotangle*3.14159265358979/180.0)
            cosa = cos(rotangle*3.14159265358979/180.0)
            shiftx = crval1+(ix-crpix1)*cdelt1
            shifty = crval2+(iy-crpix2)*cdelt2
            shiftx = -shiftx
            shifty = -shifty
            
            xrot = x + shiftx
            yrot = y + shifty
            
            detx = 4096.0 + (xrot-4096.0)*cosa - (yrot-4096.0)*sina
            dety = 4096.0 + (xrot-4096.0)*sina + (yrot-4096.0)*cosa
            
            junk = det2chip (detx,dety,chipxx,chipyy,iccd)
*            print*,i,j,detx,dety,chipxx,chipyy,iccd
*              print*,x,y,xrot,yrot,detx,dety,iccd
            if (iccd.ge.0) then
              goto 100
              chipid = iccd
              chipx = min(1023,max(nint(chipxx),0))
              chipy = min(1023,max(nint(chipyy),0))
            endif
          endif
        enddo
 100    continue
      endif

      return
      end

      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine load_aspect (asphistfile,
     ~    asphist,naxmax,naymax,nrotmax,nax,nay,nrot,rollav,rollnom,
     ~    crval1, crpix1, cdelt1, crval2, crpix2, cdelt2)
      implicit none
      character*(*) asphistfile
      integer naxmax,naymax,nrotmax, nax,nay,nrot
      real asphist(naxmax,naymax,nrotmax)
      real rollav(nrotmax), rollnom
      real crval1, crpix1, cdelt1, crval2, crpix2, cdelt2
      real databuff(512*512)

      logical anyf
      integer status, unitasp, blocksize, naxis, naxes(10), icolroll, icolasp
      character*80 comment, keyname
      integer i,ix,iy,k

      status = 0
      call ftgiou (unitasp,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      call ftopen (unitasp,asphistfile,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)

c    go to the ASPECT_HISTOGRAM extension
      call ftmnhd (unitasp,-1,'ASPECT_HISTOGRAM',0,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
c    read nominal roll
      call ftgkye (unitasp,'ROLL_NOM',rollnom, comment, status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
c    Find ROLLAV and ASPHIST column numbers
      call ftgcno (unitasp,.false.,'ROLLAV',icolroll,status)
      call ftgcno (unitasp,.false.,'ASPHIST',icolasp,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      
c    Find information about ASPHIST
      call ftgtdm (unitasp,icolasp,3,naxis,naxes,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      if (naxis.ne.2) call exiterror ('Not a 2D image in ASPHIST column')
      nax = naxes(1)
      nay = naxes(2)
      if (nax.gt.naxmax.or.nay.gt.naymax) then
        write (0,*) 'aspect hist size is greater than ',naxmax,' by ',naymax
        call exit(0)
      endif

c    Find nrot
      call ftgkyj (unitasp,'NAXIS2',nrot,comment,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      if (nrot.gt.nrotmax) then
        write (0,*) 'aspect hist nrot is greater than ',nrotmax
        call exit(0)
      endif

      write (keyname,'(i3,a)') icolasp,'CRVL1'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crval1,comment,status)

      write (keyname,'(i3,a)') icolasp,'CRVL2'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crval2,comment,status)

      write (keyname,'(i3,a)') icolasp,'CDLT1'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,cdelt1,comment,status)

      write (keyname,'(i3,a)') icolasp,'CDLT2'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,cdelt2,comment,status)

      write (keyname,'(i3,a)') icolasp,'CRPX1'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crpix1,comment,status)

      write (keyname,'(i3,a)') icolasp,'CRPX2'
      call rmblanks(keyname)
      call ftgkye (unitasp,keyname,crpix2,comment,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)

      do i=1,nrot
        call ftgcve (unitasp,icolroll,i,1,1,0.0,rollav,anyf,status)
        if (status.ne.0) call exit_fitsio (asphistfile,status)
        
        call ftgcve (unitasp,icolasp,i,1,nax*nay,0.0,databuff,anyf
     ~      ,status)
        if (status.ne.0) call exit_fitsio (asphistfile,status)
        
        k = 0
        do iy=1,nay
          do ix=1,nax
            k = k + 1
            asphist(ix,iy,i)=databuff(k)
          enddo
        enddo
      enddo

      call ftclos(unitasp,status)
      if (status.ne.0) call exit_fitsio (asphistfile,status)
      call ftfiou(unitasp,status)

      return
      end
