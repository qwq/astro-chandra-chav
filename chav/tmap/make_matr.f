      subroutine make_matr (chipid,chipx,chipy,
     ~    nspchan,pimin,pimax,
     ~    nechan,elow,ehigh,
     ~    matrix, nspchanmax, effarea)
      implicit none
      integer chipid, chipx, chipy
      integer nspchan, pimin(nspchan), pimax(nspchan)
      integer nechan
      real elow(nechan), ehigh(nechan)
      integer nspchanmax
      real matrix(nspchanmax,*)
      real effarea(nechan)
      
      character*80 fefname, feflowname,fefnameold, feflownameold
      character*200 fefdir, fefaffix, filename
      save fefdir,fefaffix
      data fefnameold /'n o n e'/
      data feflownameold /'n o n e'/
      logical firstcall
      data firstcall /.true./
      save fefnameold, firstcall, feflownameold
      character*1 node_code(0:3)
      integer xccdbin(0:9), yccdbin(0:9)
      data xccdbin /4,4,4,4,4,16,4,32,4,4/
      data node_code /'a','b','c','d'/
      save node_code, xccdbin, yccdbin
      integer fpt
      save fpt
      integer iccd,inode,ixccd,iyccd
      integer iccdold,inodeold,ixccdold,iyccdold
      data iccdold,inodeold,ixccdold,iyccdold /-1,-1,-1,-1/
      save iccdold,inodeold,ixccdold,iyccdold

      real eminthresh
      integer nfefmax, nfef, nfef_low
      parameter (nfefmax=10000)
      real efef(nfefmax),
     ~    g_fwhm(nfefmax,0:10), g_pos(nfefmax,0:10), g_ampl(nfefmax,0:10)
      real efef_low(nfefmax), g_fwhm_low(nfefmax,0:10),
     ~    g_pos_low(nfefmax,0:10), g_ampl_low(nfefmax,0:10)
      real e, gfwhm(0:10), gpos(0:10), gampl(0:10)
      real gfwhm_low(0:10), gpos_low(0:10), gampl_low(0:10)
      
      character*80 arg
      logical defined,no
      integer lnblnk
      integer i, j
      logical special low energy treatment
      save special low energy treatment
      
      logical qsinglefef, qreadyrmf
      save qsinglefef, qreadyrmf

      
      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('fixlowermf',arg)
        if (defined(arg).and..not.(no(arg))) then
          special low energy treatment = .true.
        else
          special low energy treatment = .false.
        endif
        if (special low energy treatment) then
          call get_pv_default ('eminrmf',eminthresh,-10000.0,'e')
        else
          call get_pv_default ('eminrmf',eminthresh,0.0,'e')
        endif

        call get_cl_par ('fef',arg)
        if (defined(arg)) then
          qsinglefef = .true.
        else
          qsinglefef = .false.
        endif

        call get_cl_par ('readyrmf',arg)
        if (defined(arg)) then
          qreadyrmf = .true.
        else
          qreadyrmf = .false.
        endif

        if (.not.qsinglefef) then
          call get_cl_par ('fptemp',arg)
          if (.not.defined(arg)) 
     ~        call exiterror ('fptemp=? (focal plane temperature)')
          read (arg,*) fpt
          
          call get_cl_par ('fefdir',fefdir)
          if (.not.defined(fefdir)) 
     ~        call exiterror ('fefdir=?')
          
          call get_cl_par ('fefaffix',fefaffix)
          if (.not.defined(fefaffix)) 
     ~        call exiterror ('fefaffix=?')
          
          if (fpt.eq.-110.or.fpt.eq.-90.or.fpt.eq.-120) then
            do iccd=0,9
              if (iccd.ne.5) then
                yccdbin(iccd)=32
              else
                yccdbin(iccd)=16
              endif
            enddo
          else if (fpt.eq.-100) then
            do iccd=0,9
              if (iccd.eq.5) then
                yccdbin(iccd)=16
              else if (iccd.eq.7) then
                yccdbin(iccd)=32
              else
                yccdbin(iccd)=1
              endif
            enddo
          else
            write (0,*)
     ~          'Only focal plane temperatures -90,-100, -110, and -120 are supported'
            call exit(1)
          endif
        endif
      endif

      if (qreadyrmf) then
        call make_matr_readyrmf (chipid,chipx,chipy,
     ~      nspchan,pimin,pimax,
     ~      nechan,elow,ehigh,
     ~      matrix, nspchanmax, effarea)
        return
      endif

      if (qsinglefef) then
        call make_matr_singlefef (chipid,chipx,chipy,
     ~      nspchan,pimin,pimax,
     ~      nechan,elow,ehigh,
     ~      matrix, nspchanmax, effarea)
        return
      endif

      iccd = chipid
      inode=chipx/256
      ixccd=(chipx-inode*256)/(1024/xccdbin(iccd))
      iyccd=chipy/(1024/yccdbin(iccd))
      if ((iccd.ne.iccdold) .or.
     ~    (inode.ne.inodeold) .or.
     ~    (ixccd.ne.ixccdold) .or.
     ~    (iyccd.ne.iyccdold) ) then
        
        iccdold = iccd
        inodeold = inode
        ixccdold = ixccd
        iyccdold = iyccd
        
        write (fefname,
     ~    '(''acis'',i1,a1,''_x'',i2.2,''_y'',i2.2,''_FP'',i4)')
     ~      iccd,node_code(inode),ixccd,iyccd,fpt
        call rmblanks(fefname)
      
        print*,'calc matrix for ',iccd,inode,iyccd
        
        filename=fefdir
        call strcat(filename,'/')
        call strcat(filename,fefname)
        call strcat(filename,fefaffix)

        i = lnblnk(filename)
        filename (i+1:i+1) = char(0)
        print*,'load fef:',filename
        call loadfef (filename, nfef,
     ~      efef,
     ~      g_fwhm(1,0), g_pos(1,0), g_ampl(1,0),
     ~      g_fwhm(1,1), g_pos(1,1), g_ampl(1,1),
     ~      g_fwhm(1,2), g_pos(1,2), g_ampl(1,2),
     ~      g_fwhm(1,3), g_pos(1,3), g_ampl(1,3),
     ~      g_fwhm(1,4), g_pos(1,4), g_ampl(1,4),
     ~      g_fwhm(1,5), g_pos(1,5), g_ampl(1,5),
     ~      g_fwhm(1,6), g_pos(1,6), g_ampl(1,6),
     ~      g_fwhm(1,7), g_pos(1,7), g_ampl(1,7),
     ~      g_fwhm(1,8), g_pos(1,8), g_ampl(1,8),
     ~      g_fwhm(1,9), g_pos(1,9), g_ampl(1,9),
     ~      g_fwhm(1,10), g_pos(1,10), g_ampl(1,10) )

        if (special low energy treatment) then
          write (feflowname,
     ~        '(''acis'',i1,a1,''_x'',i2.2,''_y'',i2.2,''_FP'',i4)')
     ~        iccd,node_code(inode),ixccd,0,fpt
          call rmblanks(feflowname)
          filename=fefdir
          call strcat(filename,'/')
          call strcat(filename,feflowname)
          call strcat(filename,fefaffix)
          if (filename.ne.feflownameold) then
            feflownameold = filename
            i = lnblnk(filename)
            filename (i+1:i+1) = char(0)
            print*,'load fef:',filename
            call loadfef (filename, nfef_low,
     ~          efef_low,
     ~          g_fwhm_low(1,0), g_pos_low(1,0), g_ampl_low(1,0),
     ~          g_fwhm_low(1,1), g_pos_low(1,1), g_ampl_low(1,1),
     ~          g_fwhm_low(1,2), g_pos_low(1,2), g_ampl_low(1,2),
     ~          g_fwhm_low(1,3), g_pos_low(1,3), g_ampl_low(1,3),
     ~          g_fwhm_low(1,4), g_pos_low(1,4), g_ampl_low(1,4),
     ~          g_fwhm_low(1,5), g_pos_low(1,5), g_ampl_low(1,5),
     ~          g_fwhm_low(1,6), g_pos_low(1,6), g_ampl_low(1,6),
     ~          g_fwhm_low(1,7), g_pos_low(1,7), g_ampl_low(1,7),
     ~          g_fwhm_low(1,8), g_pos_low(1,8), g_ampl_low(1,8),
     ~          g_fwhm_low(1,9), g_pos_low(1,9), g_ampl_low(1,9),
     ~          g_fwhm_low(1,10), g_pos_low(1,10), g_ampl_low(1,10) )
          endif
        endif

        
        do j=1,nechan
          e = 0.5*(elow(j)+ehigh(j))
          call find_coeff (e,efef,nfef,g_fwhm,g_pos,g_ampl,nfefmax,
     ~        gfwhm,gpos,gampl)
          if (special low energy treatment) then
            call find_coeff (e,efef_low,nfef_low,g_fwhm_low,g_pos_low
     ~          ,g_ampl_low,nfefmax,gfwhm_low,gpos_low,gampl_low)
            do i=1,10
              if (gpos(i).lt.0.) then
                gpos(i)=gpos_low(i)
                gampl(i)=gampl_low(i)
                gfwhm(i)=gfwhm_low(i)
              endif
            enddo
          endif
          call fill_in_matr (matrix(1,j),nspchan,pimin,pimax,gfwhm,gpos,gampl,
     ~        eminthresh)
          do i=1,nspchan
            matrix(i,j)=matrix(i,j)*effarea(j)
          enddo
        enddo
        
        

      endif
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      
      subroutine make_matr_singlefef (chipid,chipx,chipy,
     ~    nspchan,pimin,pimax,
     ~    nechan,elow,ehigh,
     ~    matrix, nspchanmax, effarea)
      implicit none
      integer chipid, chipx, chipy
      integer nspchan, pimin(nspchan), pimax(nspchan)
      integer nechan
      real elow(nechan), ehigh(nechan)
      integer nspchanmax
      real matrix(nspchanmax,*)
      real effarea(nechan)
      
      character*200 fefname
      logical firstcall
      data firstcall /.true./
      save fefname
      save firstcall
      
      integer ireg, iregold
      data iregold /-1/
      save iregold

      logical defined
      integer i, nfef, l, icomp,j

      real eminthresh
      integer nfefmax, nfef
      parameter (nfefmax=10000)
      real efef(nfefmax),
     ~    g_fwhm(nfefmax,0:10), g_pos(nfefmax,0:10), g_ampl(nfefmax,0:10)
      real e, gfwhm(0:10), gpos(0:10), gampl(0:10)


      include 'cfefdata.inc'

      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('fef',fefname)
        if (.not.defined(fefname)) call exiterror ('fef=?')
        
        call loadfefdata (fefname)
      endif

      ireg = fefregion ((chipx-1)/8+1,(chipy-1)/8+1,chipid)
      
      if (ireg.ne.iregold) then
        iregold = ireg

        nfef = nfefdata (ireg)
        do i=1,nfef
          efef(i)=fefdata(1,i,ireg)
          l = 1
          do icomp = 1,10
            l = l + 1
            g_fwhm(i,icomp)=fefdata(l,i,ireg)
            l = l + 1
            g_pos (i,icomp)=fefdata(l,i,ireg)
            l = l + 1
            g_ampl(i,icomp)=fefdata(l,i,ireg)
          enddo
          g_fwhm(i,0)=0
          g_pos(i,0)=1024
          g_ampl(i,0)=1
        enddo
        
        do j=1,nechan
          e = 0.5*(elow(j)+ehigh(j))
          call find_coeff (e,efef,nfef,g_fwhm,g_pos,g_ampl,nfefmax,
     ~        gfwhm,gpos,gampl)
          call fill_in_matr (matrix(1,j),nspchan,pimin,pimax,gfwhm,gpos,gampl,
     ~        eminthresh)
          do i=1,nspchan
            matrix(i,j)=matrix(i,j)*effarea(j)
          enddo
        enddo
      endif
      
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      
      subroutine make_matr_readyrmf (chipid,chipx,chipy,
     ~    nspchan,pimin,pimax,
     ~    nechan,elow,ehigh,
     ~    matrix, nspchanmax, effarea)
      implicit none
      integer chipid, chipx, chipy
      integer nspchan, pimin(nspchan), pimax(nspchan)
      integer nechan
      real elow(nechan), ehigh(nechan)
      integer nspchanmax
      real matrix(nspchanmax,*)
      real effarea(nechan)
      
      character*200 rmfname
      logical firstcall
      data firstcall /.true./
      save rmfname
      save firstcall
      
      logical defined
      
      real matrix0(2048*2048)
      integer ne0, nchan0
      save matrix0,ne0,nchan0

      integer i,j,l,i1,i2
      real sum, totnorm
      
      

      if (firstcall) then
        firstcall = .false.
        call get_cl_par ('readyrmf',rmfname)
        if (.not.defined(rmfname)) call exiterror ('readyrmf=?')
c Read ready rmf:
        call get_matrix (rmfname,matrix0,ne0,nchan0)
        if (nechan.ne.ne0)
     ~      call exiterror('not matching readyrmf and refpirmf')

        do j=1,nechan
          totnorm = 0.0
          do i=1,nspchan
            sum = 0.0
            i1=min(max(pimin(i),1),nchan0)
            i2=min(max(pimax(i),1),nchan0)
            do l = i1,i2
              sum = sum + matrix0(l+(j-1)*ne0)
            enddo
            matrix(i,j)=sum
            totnorm = totnorm + sum
          enddo
          
          do i=1,nspchan
            matrix(i,j)=matrix(i,j)*effarea(j)/totnorm
          enddo
        enddo
      endif

      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine fill_in_matr (matrix,nmatr,pimin,pimax,gfwhm,gpos,gampl,
     ~    eminthresh)
      implicit none
      integer nmatr
      real matrix(nmatr)
      integer pimin(nmatr), pimax(nmatr)
      real gfwhm(0:10),gpos(0:10),gampl(0:10)
      real eminthresh
      real nrm
      integer i,j
      double precision normd8
      external normd8
      real totnrm
      double precision xmin,xmax, mean, sig

      integer imin,imax
      imin = gfwhm(0)
      imax = gpos(0)
      nrm = gampl(0)

      do i=1,nmatr
        matrix(i)=0
      enddo
      
      totnrm=0
      do j=1,10
        if (gampl(j).ne.0.0.and.gpos(j).gt.eminthresh) then
          do i=1,nmatr
            xmin = pimin(i)-0.5d0
            xmax = pimax(i)+0.5d0
            mean = gpos(j)
            sig  = gfwhm(j)/2.35482
            matrix(i)=matrix(i)+nrm*gampl(j)*normd8(mean,sig,xmin,xmax)
          enddo
          totnrm   =totnrm   +nrm*gampl(j)*normd8(mean,sig,0.0d0,1024.5d0)
        endif
      enddo

      if (totnrm.gt.0.0) then
        do i=1,nmatr
          matrix(i)=matrix(i)/totnrm
        enddo
      endif
      
      return
      end

      subroutine find_order (chipcoords,nodes,indx,nx,ny)
      implicit none
      integer nx,ny
      integer chipcoords (nx*ny), nodes(nx*ny), indx(nx*ny)
      
      integer i
      integer iccd, inode, chipx, chipy
      integer xccdbin(0:9), yccdbin(0:9)
      data xccdbin /4,4,4,4,4,16,4,32,4,4/
      integer fpt
      save fpt
      character*80 arg
      logical defined
      integer ix,iy

      call get_cl_par ('fptemp',arg)
      if (.not.defined(arg)) 
     ~    call exiterror ('fptemp=? (focal plane temperature)')
      read (arg,*) fpt
        

      if (fpt.eq.-110.or.fpt.eq.-90.or.fpt.eq.-120) then
        do iccd=0,9
          if (iccd.ne.5) then
            yccdbin(iccd)=32
          else
            yccdbin(iccd)=16
          endif
        enddo
      else if (fpt.eq.-100) then
        do iccd=0,9
          if (iccd.eq.5) then
            yccdbin(iccd)=16
          else if (iccd.eq.7) then
            yccdbin(iccd)=32
          else
            yccdbin(iccd)=1
          endif
        enddo
      else
        write (0,*)
     ~      'Only focal plane temperatures -90,-100, -110, and -120 are supported'
        call exit(1)
      endif

      do i=1,nx*ny
        nodes(i)=-1
        call unpack_chip_coords (chipcoords(i),chipx,chipy,iccd)
        iccd = chipcoords(i)/4194304-1
        if (iccd.ge.0) then
          ix = chipx/(1024/xccdbin(iccd))+1
          iy = chipy/(1024/yccdbin(iccd))+1
          nodes(i)= -(iccd*1024*16 + ix + iy*xccdbin(iccd))
        endif
      enddo

      call indexxi4 (nx*ny,nodes,indx)
      return
      end



*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine get_matrix (refrmf,matrix,ne,nchan)
      implicit none
      character*(*) refrmf
      real matrix(*)
      integer ne, nchan
      integer status, unitrmf, colnum, blocksize
      logical anyf
      character*200 wrefrmf

      character*80 keyname, comment
      integer chanmin, chanmax, colnum
      character*80 matext, telescop, instrume, detnam, filter, chantype,
     ~    rmfversn, hduclas3
      real areascal
      integer flchan, ichan, ienerg, energ_lo, energ_hi, imaxgrp
      integer n_chan(2048), f_chan(2048), ngrp(2048)
      real lo_thresh
      
      status = 0
      call ftgiou (unitrmf,status)
      call ftopen (unitrmf,refrmf,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (refrmf,status)

      wrefrmf = refrmf
      call ftmnhd (unitrmf,-1,'SPECRESP MATRIX',0,status)
      if (status.ne.0) then
        status = 0
        call ftmnhd (unitrmf,-1,'MATRIX',0,status)
      endif
      if (status.ne.0) call exit_fitsio ('SPECRESP, '//wrefrmf,status)
      
      call ftgnrw (unitrmf,ne, status)
      if (status.ne.0) call exit_fitsio ('NAXIS2, '//wrefrmf,status)
      
      call ftgcno (unitrmf,.false.,'F_CHAN',colnum,status)
      if (status.ne.0) call exit_fitsio ('F_CHAN, '//wrefrmf,status)
      
      write (keyname,'(''TLMIN'',i2)') colnum
      call rmblanks(keyname)
      call ftgkyj (unitrmf,keyname,chanmin,comment,status)
      if (status.ne.0) call exit_fitsio ('keyname '//wrefrmf,status)

      write (keyname,'(''TLMAX'',i2)') colnum
      call rmblanks(keyname)
      call ftgkyj (unitrmf,keyname,chanmax,comment,status)
      if (status.ne.0) call exit_fitsio ('keyname '//wrefrmf,status)


      chanmin = 1
      nchan = chanmax-chanmin+1
      
      call rdrmf3 (unitrmf,0,matext,
     ~    telescop, instrume, detnam, filter, areascal,
     ~    chantype, flchan,
     ~    ichan, ienerg, energ_lo, energ_hi,
     ~    imaxgrp, ngrp, F_chan, N_chan,
     ~    matrix, lo_thresh, nchan,ne,
     ~    rmfversn,hduclas3,status)
      
      if (status.ne.0) call exit_fitsio (wrefrmf,status)


      call ftclos(unitrmf,status)
      if (status.ne.0) call exit_fitsio (refrmf,status)
      call ftfiou(unitrmf,status)
      return
      end
