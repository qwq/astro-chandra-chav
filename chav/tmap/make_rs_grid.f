      subroutine make_rs_grid (ebin,ne,amode,a,z,rsspec,ntgrid,Tgrid)
      implicit none
      real a,z
      character*(*) amode
      integer ne
      real ebin(0:ne)
      integer ntgrid
      real Tgrid(ntgrid)
      real rsspec(0:ne,-1:ntgrid)
      real testspec(2000)
      double precision norm
      integer i,j
      real e1,e2,w1
      
      character*80 code
      logical usemekal

      call get_cl_par ('plasma_code',code)
      call strlow (code)
      if (code.eq.'mekal') then
        usemekal = .true.
      else
        usemekal = .false.
      endif

      if (usemekal) then
        print*,'Initialize mekal grid...'
      else
        print*,'Initialize Raymond-Smith grid...'
      endif

      do i=1,ntgrid
        rsspec(0,i)=Tgrid(i)
        if (usemekal) then
          call mekal (Tgrid(i),a,z,ne,ebin,rsspec(1,i))
        else
          call raym (Tgrid(i),a,z,ne,ebin,rsspec(1,i))
        endif

        if (i.eq.1) then
          norm=1.0/rsspec(1,i)
        endif
        do j=1,ne
          rsspec(j,i)=norm*rsspec(j,i)
        enddo
      enddo
      
      do j=1,ne
        rsspec(j,-1)=ebin(j-1)
        rsspec(j,0)=ebin(j)
      enddo
      print*,'OK'

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine make_rs_grid_abund (ebin,ne,amode,a,z,rsspec,ntgrid,Tgrid)
      implicit none
      real a,z
      character*(*) amode
      integer ne
      real ebin(0:ne)
      integer ntgrid
      real Tgrid(ntgrid)
      real rsspec(0:ne,-1:ntgrid,2) ! the last index is 1 for continuum and
                                !    2 for lines
      real testspec(2000)
      double precision norm
      integer i,j
      real e1,e2,w1

      character*80 code
      logical usemekal

      call get_cl_par ('plasma_code',code)
      call strlow (code)
      if (code.eq.'mekal') then
        usemekal = .true.
      else
        usemekal = .false.
      endif

      if (usemekal) then
        print*,'Initialize mekal grid with abundances...'
      else
        print*,'Initialize Raymond-Smith grid with abundances...'
      endif
      
      write (*,'(''t='',$)')
      do i=1,ntgrid
        write (*,'(f5.2,'','',$)') Tgrid(i)
        call flush(6)
        rsspec(0,i,1)=Tgrid(i)
        if (usemekal) then
          call mekal (Tgrid(i),1.,z,ne,ebin,rsspec(1,i,2))
          call mekal (Tgrid(i),0.0,z,ne,ebin,rsspec(1,i,1))
        else
          call raym (Tgrid(i),1.,z,ne,ebin,rsspec(1,i,2))
          call raym (Tgrid(i),0.0,z,ne,ebin,rsspec(1,i,1))
        endif
        do j=1,ne
*          print*,ebin(j)
          rsspec(j,i,2)=rsspec(j,i,2)-rsspec(j,i,1)
        enddo
*        norm=0.0d0
*        do j=1,ne
*          if (ebin(j-1).ge.0.5.and.ebin(j).le.2.0) then
*            norm=norm+rsspec(j,i,1)
*          endif
*        enddo
*        norm=1000.0/norm
        if (i.eq.1) then
          norm=1.0/rsspec(1,i,1)
        endif
        do j=1,ne
          rsspec(j,i,1)=norm*rsspec(j,i,1)
          rsspec(j,i,2)=norm*rsspec(j,i,2)
        enddo
      enddo
      
      do j=1,ne
        rsspec(j,-1,1)=ebin(j-1)
        rsspec(j,0,1)=ebin(j)
      enddo
      print*,'OK'

      return
      end

