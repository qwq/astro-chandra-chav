      subroutine pack_chip_coords (chipx,chipy,chipid,pack)
      implicit none
      integer chipx,chipy,chipid,pack

      if (chipid.ge.0) then
        pack = 4194304*(chipid+1)+2048*chipx+chipy
      else
        pack = 0
      endif
      
      return
      end

      subroutine unpack_chip_coords (pack,chipx,chipy,chipid)
      implicit none
      integer chipx,chipy,chipid,pack
      
      chipid = pack/4194304-1
      chipx = (pack-4194304*(chipid+1))/2048
      chipy = pack-4194304*(chipid+1)-chipx*2048
      
      return
      end

