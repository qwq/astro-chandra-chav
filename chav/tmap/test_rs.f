      implicit none
      real ebin (0:1500)
      real spec_cont(1:1500), spec_1(1:1500), spec(1:1500)
      real T, a, z
      integer i, ne
      
      ne=1500

      do i=0,ne
        ebin(i)=0.01+0.01*(i-1)
      enddo


      T = 1.0
      z =-0.001
      a = 0.08

      call raym (T,1.,z,ne,ebin,spec_1)
      call raym (T,0.,z,ne,ebin,spec_cont)
      call raym (T,a,z,ne,ebin,spec)

      do i=1,ne
        spec_1(i)=spec_1(i)-spec_cont(i)
      enddo

      do i=1,ne
        print*,ebin(i),spec_cont(i)+a*spec_1(i),spec(i),
     ~      (spec_cont(i)+a*spec_1(i))/spec(i)
      enddo

      end
      
      
