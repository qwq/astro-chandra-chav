      implicit none
      
      
      integer nechan
      parameter (nechan=1500)
      real ebin(0:nechan)
      real elow(nechan), ehigh(nechan), emean(nechan)
      real e, qe
      real hrma_onaxis_ea
      external hrma_onaxis_ea
      
      integer nspchanmax, nspchan, ichan
      parameter (nspchanmax=10)
      
      real matrix  (nspchanmax,nechan)
      real effarea (nechan), onaxisarea(nechan)
      
      real data(nspchanmax)
      real error(nspchanmax)
      logical usechan(nspchanmax)
      real syserr(nspchanmax)
      real sys
      
      common /cdata/
     ~    ebin, matrix, effarea, data, error, usechan, syserr, nspchan
      
      real tfitmax,tfitmin
      
      integer nx,ny,nxmax,nymax
      parameter (nxmax=512, nymax=512)
      real img(nxmax*nymax*nspchanmax), err(nxmax*nymax*nspchanmax)
      byte mask(nxmax*nymax)
      real results(nxmax*nymax)
      integer chipcoords(nxmax*nymax), indx(nxmax*nymax)
      real sigmasmo
      
      character*80 range
      integer xmin,xmax,ymin,ymax
      
      character*80 parfile
      logical defined, yespar
      character*80 parname,parvalue
      integer lnblnk
      character*80 dataname(100)
      integer ndata
      
      real*8 p(200), pp(200)
      integer pfit(200)
      real*8 punit(200)
      common /cpar/ p,punit,pfit
      
      real*8 ftol,dp
      integer iret,maxit,npar
      
      

      real nh,T,abund,z
      integer fitnh,fitT,fitabund,fitz
      double precision fmin
      real*8 ch2,ch2_T,ch2_nH,ch2_T_err, ch2_nh_err, ch2_a
      external ch2_T,ch2_nH,ch2_T_err, ch2_nh_err, ch2_a
      real chi2thresh
      common /cchi2/ chi2thresh
      real*8 zeroin
      real tpos,tneg,nhpos,nhneg
      
      character*80 amode
      common /camode/ amode, fitabund
      
      real deltaT,deltanh,told,nhold
      double precision tmin,tmax,nhmin,nhmax,stepT,stepnh
      real x,chi2min,ttt
      integer ndof
      
      integer i,j,ipar,k,l
      
      integer nwords
      character*2000 buff
      character*30 word(100)
      integer pimin(nspchanmax), pimax(nspchanmax)

      character*200 refrmf, calfile, arg, smoothmap
      character*80 chipname
      integer chipid, chipx,chipy
      integer ne,nemax
      parameter (nemax=11000)
      real e0(nemax), ea(nemax), work(nemax)
      
      character*80 out
      logical anydone
      character*200 filename
      character*20 affix
      character*1 separator

      
      real ccd_qe
      external ccd_qe

      logical singleresp
      real singlerespchipx, singlerespchipy, singlerespchipx_rmf,
     ~    singlerespchipy_rmf
      integer singlerespccd

      integer nhrmacont
      real ehrmacont(10000), hrmacont(10000)
      character hrmacontfile*200
      logical hrma_cont_cor
      
      integer unit,status,newunit
      real a, ac
   

c Data parameters
      call get_cl_par ('o',out)
      if (.not.defined(out)) call exiterror ('o=?')

      call get_cl_par ('piranges',buff)
      if (.not.defined(buff)) call exiterror ('piranges=?')
      call splitwords_char (buff,',',word,nwords)
      nspchan=nwords
      
      call get_cl_par ('separator',arg)
      if (defined(arg)) then
        separator = arg
      else
        separator = '-'
      endif
        
      do i=1,nspchan
        call replace_characters (word(i),separator,' ')
        read (word(i),*) pimin(i), pimax(i)
      enddo

      call get_cl_par ('xrange',buff)
      if (.not.defined(buff)) call exiterror ('xrange=?')
      read (buff,*) xmin,xmax
      call get_cl_par ('yrange',buff)
      if (.not.defined(buff)) call exiterror ('yrange=?')
      read (buff,*) ymin,ymax
      write (range,'(''['',i4,'':'',i4,'','',i4,'':'',i4,'']'')') xmin,xmax
     ~    ,ymin,ymax
      call rmblanks(range)
      nx=xmax-xmin+1
      ny=ymax-ymin+1
      if (nx*ny.gt.nxmax*nymax) call exiterror ('x,y range is too big')
      

      call get_cl_par ('affix',affix)
      if (.not.defined (affix)) affix = ' '

      call get_cl_par ('dataimages',buff)
      if (.not.defined(buff)) call exiterror ('dataimages=?')
      call splitwords_char (buff,',',word,nwords)
      if (nwords.ne.nspchan) call exiterror
     ~    ('The number of data images .ne. number of spectral bands')
      do i=1,nspchan
        filename = word(i)
        call strcat (filename,'.spec')
        call strcat (filename,affix)
        call strcat (filename,range)
        call read_fits_image (filename,img(nx*ny*(i-1)+1),nx,ny,'e')
        filename = word(i)
        call strcat (filename,'.err')
        call strcat (filename,affix)
        call strcat (filename,range)
        call read_fits_image (filename,err(nx*ny*(i-1)+1),nx,ny,'e')
      enddo

      call get_cl_par ('mask',buff)
      if (.not.defined(buff)) call exiterror ('mask=?')
      call strcat (buff,range)
      call read_fits_image (buff,mask,nx,ny,'b')

      call get_cl_par ('smoothmap',smoothmap)
      if (.not.defined(smoothmap)) call exiterror ('smoothmap=?')
      call strcat (smoothmap,range)
      call read_fits_image (smoothmap,results,nx,ny,'e')
      
c set mask to zero where smoothmap is zero
      do i=1,nx*ny
        if (results(i).le.0.0) then
          mask(i)=0
        endif
      enddo

      
c FIT parameters
      parname = 'nh'
      call get_cl_par (parname,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'Please set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      read (parvalue,*) nh
      
      if (nh.gt.1e10) then      ! it is in absolute units, convert to 1e22
        nh=nh/1.0e22
      endif
      
      parname = 'fitnh'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        fitnh=0
      else
        if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
          fitnh=1
        else
          fitnh=0
        endif
      endif
      
      call get_parameter_value_default ('tmax',tfitmax,14.5,'e')
      call get_parameter_value_default ('tmin',tfitmin,2.5,'e')

      parname = 'T'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'Please set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      read (parvalue,*) T
      
      parname = 'fitT'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        fitT=1
      else
        if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
          fitT=1
        else
          fitT=0
        endif
      endif
      
      parname = 'abund'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'Please set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      read (parvalue,*) abund
      
      parname = 'fitabund'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        fitabund=0
      else
        if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
          fitabund=1
        else
          fitabund=0
        endif
      endif
 
      
      parname = 'z'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        write(0,*)'Please set ',parname(1:lnblnk(parname))
        call exit(1)
      endif
      read (parvalue,*) z

      
      parname = 'fitz'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        fitz=0
      else
        if (parvalue(1:1).eq.'y'.or.parvalue(1:1).eq.'Y') then
          fitz=1
        else
          fitz=0
        endif
      endif
 
      write(0,*) ' Parameter      start value        fit it'
      write(0,'("   Nh            ",f7.2,14x,i1)') nh,fitnh
      write(0,'("   T             ",f7.2,14x,i1)') T,fitT
      write(0,'("   abund         ",f7.2,14x,i1)') abund,fitabund
      write(0,'("   z             ",f7.3,14x,i1)') z,fitz


c Abund mode
      parname = 'amode'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        amode="allen"
      else
        amode=parvalue
      endif
      
c Sys error
      parname = 'syserr'
      call get_command_line_par (parname,1,parvalue)
      if (.not.defined(parvalue)) then
        sys=0.0
      else
        read (parvalue,*) sys
      endif

c Single resp
      singleresp = yespar ('singleresp')
      if (singleresp) then
        call get_parameter_value_default ('singlerespchipx',singlerespchipx
     ~      ,1022.0,'e')
        call get_parameter_value_default ('singlerespchipy',singlerespchipy
     ~      ,1022.0,'e')
        call get_parameter_value_default ('singlerespchipx',singlerespchipx_rmf
     ~      ,singlerespchipx,'e')
        call get_parameter_value_default ('singlerespchipy',singlerespchipy_rmf
     ~      ,singlerespchipx,'e')
        call get_parameter_value_default ('singlerespccd',singlerespccd
     ~      ,3,'j')
      endif



c Calculate arf
      call get_cl_par ('ref_pi_rmf',refrmf)
      if ( .not. defined (refrmf)) call exiterror ('ref_pi_rmf=?')

      call get_ebounds (refrmf,elow,ehigh,ne)
      if (ne.ne.nechan) call exiterror
     ~    ('Nechan in the code does not match that in ref_pi_rmf')
      

      call get_cl_par ('hrmacont',hrmacontfile)
      if (defined(hrmacontfile)) then
        write (0,*) 'HRMA CONTAMINATION CORRECTION:'
     ~      ,hrmacontfile(1:lnblnk(hrmacontfile))
        hrma_cont_cor = .true.
        unit = newunit()
        open (unit,file=hrmacontfile,status='old')
        nhrmacont = 0
        status = 0
        do while (status.eq.0)
          read (unit,*,iostat=status)
     ~        ehrmacont(nhrmacont+1),hrmacont(nhrmacont+1)
          if (status.eq.0) nhrmacont = nhrmacont + 1
        enddo
        close(unit)
      else
        hrma_cont_cor = .false.
      endif


*      call get_cl_par ('ccd_qe',calfile)
*      if (.not.defined(calfile)) then
*        call get_cl_par ('chip',arg)
*        if (.not.defined(arg)) then
*          call exiterror ('Neither ccd_qe nor chip is set')
*        endif
*        call chip_name_to_idname (arg,chipname)
*        read (chipname,*) chipid
*        call chip_id_to_normname (chipid,chipname)
*        call get_cl_par ('ccd_qe_'//chipname,calfile)
*        if (.not.defined(calfile)) then
*          call exiterror('ccd_qe is not set for '//chipname)
*        endif
*      endif
*      call read_ccd_qe (calfile,ne,e0,ea)
*      call spline(e0,ea,ne,1.0e30,1.0e30,work)

      ebin(0)=elow(1)
      do i=1,nechan
        ebin(i)=ehigh(i)
        e = 0.5*(elow(i)+ehigh(i))
        emean(i)=e
*        if (e.lt.e0(1).or.e.gt.e0(ne)) then
*          qe=0
*        else
*          call splint (e0,ea,work,ne,e,qe)
*        endif
        a = hrma_onaxis_ea (e)
        if (hrma_cont_cor) then
          call lin_interpolate (ehrmacont,hrmacont,nhrmacont,e,ac)
          a = a * ac
        endif
        onaxisarea(i) = a
      enddo

      if (singleresp) then      ! assume that exposure maps correct the data
                                ! to the aim point in I3 chip
        do ichan=1,nechan
          effarea(ichan) = onaxisarea(ichan)*ccd_qe(emean,ichan,nechan
     ~        ,singlerespchipx,singlerespchipy,singlerespccd)
        enddo

        do i=1,nx*ny
          indx(i)=i
        enddo
        
      endif

      if (.not.singleresp) then
c find chip coordinates
        call get_cl_par ('chipcoordsmap',arg)
        if (defined(arg)) then
          call read_fits_image (arg,chipcoords,nx,ny,'j')
        else
          print*,'finding chip coordinates...'
          l=0
          do j=1,ny
            do i=1,nx
              l=l+1
              chipcoords(l)=0
              if (mask(l).eq.1) then
                call sky_to_chip (i+xmin-1,j+ymin-1,chipid,chipx,chipy)
                call pack_chip_coords (chipx,chipy,chipid,chipcoords(l))
              endif
*            print*,i,j,chipid,chipx,chipy
            enddo
            write (*,'("\r",i4,$)')j
            call flush(6)
          enddo
          write (*,*)
          call get_cl_par ('savechipcoords',arg)
          if (defined(arg)) then
            call write_fits_image (arg,chipcoords,nx,ny,'j','j')
          endif
        endif
        print*,'find optimum fitting order ...'
        call find_order (chipcoords,results,indx,nx,ny)
        print*,'ok'
      endif

      call read_fits_image (smoothmap,results,nx,ny,'e')
      print*,'smo map read'

c fit it
      punit(1)=1
      p(1)=alog10(nh)
      pfit(1)=fitnh
      
      punit(2)=1
      p(2)=6.0
      pfit(2)=fitT
      
      punit(3)=abund
      p(3)=1.0
      pfit(3)=fitabund
            
      punit(4)=z
      p(4)=1.0
      pfit(4)=fitz
      
      chi2min = ch2()
      

      anydone = .false.

      do l = 1, nx*ny
        k=indx(l)
        j = (k-1)/nx+1
        i = k-(j-1)*nx
*        print*,i,j,mask(k)
        if (mask(k).eq.1) then
          anydone = .true.
          
          if (.not.singleresp) then
            call unpack_chip_coords (chipcoords(k),chipx,chipy,chipid)
          else
            chipid=singlerespccd
            chipx=singlerespchipx_rmf
            chipy=singlerespchipy_rmf
          endif

          if (chipid.ge.0) then
            
            sigmasmo = results(k)
            call adapt_smooth (i,j,img,err,mask,nx,ny,nspchan,sigmasmo,data
     ~          ,error)
            do ichan = 1,nspchan
***              data(ichan)=img(i,j,ichan)
***              error(ichan)=err(i,j,ichan)
              usechan(ichan)=.true.
              syserr(ichan)=0.0
*              print*,ichan,data(ichan),error(ichan)
            enddo
            
            if (.not.singleresp) then
              do ichan=1,nechan
                effarea(ichan) = onaxisarea(ichan)*ccd_qe(emean,ichan,nechan
     ~              ,float(chipx),float(chipy),chipid)
*              print*,emean(ichan),effarea(ichan),onaxisarea(ichan),float(chipx),float(chipy),chipid
              enddo
            endif

            call make_matr (chipid,chipx,chipy,
     ~          nspchan,pimin,pimax,
     ~          nechan,elow,ehigh,
     ~          matrix,nspchanmax, effarea)
! I multiply matrix by effective area to save computing time
            punit(1)=1
            p(1)=alog10(nh)
            pfit(1)=fitnh
            
            punit(2)=1
            p(2)=T
            pfit(2)=fitT
            
            punit(3)=abund
            p(3)=1.0
            pfit(3)=fitabund
            
            punit(4)=z
            p(4)=1.0
            pfit(4)=fitz
            
            dp=0.5
            
*              deltat=1.0
*              deltanh=1.0
              
*              stepT=1.5
*              stepnh=0.5
*              told=t
*              nhold=p(1)
*              tmin=T/stepT
*              tmax=T*stepT
*              nhmin=p(1)-stepnh
*              nhmax=p(1)+stepnh
*              Tmin=max(0.1d0,Tmin)
*              Tmax=min(25.0d0,Tmax)
*              nhmin=max(-1.0d0,nhmin)
*              nhmax=min(2.0d0,nhmax)
              

            ftol=1.0e-6
            npar=4
            maxit=5000
            do ipar=1,npar
              pp(ipar)=p(ipar)
            enddo
*            if (fitt.eq.1.and.fitnh.eq.0) then
*                ! we can do 1-dimensional minimization
*              chi2min = 1e9
*              do x=Tfitmin,Tfitmax,1.0
*                ttt = ch2_T (dble(x)/punit(2))
*                if (ttt.lt.chi2min) then
*                  chi2min = ttt
*                  tmin = x
*                endif
*              enddo
*              if (tmin.eq.Tfitmax) then
*                pp(2)=Tfitmax/punit(2)
*              else
*                ftol = 1.0e-3
*                pp(2)=fmin(
*     ~              max(2.1,tmin-1.0)/punit(2),
*     ~              min(Tfitmax,tmin+1.0)/punit(2),
*     ~              ch2_T,ftol)
*              endif
*            else
              call amoebf8(pp,pfit,dp,npar,ftol,maxit,ch2_a,iret)
*            endif
            do ipar=1,npar
              p(ipar)=pp(ipar)
            enddo
            
            chi2min=ch2_a(pp,npar)
            
            chi2thresh=chi2min+2.706
            
*            if (fitt.eq.1) then
*              p(1)=alog10(nh)/punit(1)
*              p(2)=T/punit(2)
*              Tmin=T
*              Tmax=25.0
*              Tpos=zeroin(Tmin,Tmax,ch2_T_err,1.0d-2)
*              write(0,*),'     Tpos:',Tpos
*              Tmin=0.1
*              Tmax=T
*              Tneg=zeroin(Tmin,Tmax,ch2_T_err,1.0d-2)
*              write(0,*),'     Tneg:',Tneg
*            endif
*            
*            if (fitnh.eq.1) then
*              p(1)=alog10(nh)/punit(1)
*              p(2)=T/punit(2)
*              nhmin=alog10(nh)/punit(2)
*              nhmax=2
*              nhpos=zeroin(nhmin,nhmax,ch2_nh_err,1.0d-2)
*              nhpos=10.0**(nhpos*punit(1))
*              write(0,*),'     Nhpos:',Nhpos
*              nhmin=-1
*              nhmax=alog10(nh)/punit(2)
*              nhneg=zeroin(nhmin,nhmax,ch2_nh_err,1.0d-4)
*              nhneg=10.0**(nhneg*punit(1))
*              write(0,*),'     Nhneg:',Nhneg
*            endif



*              Nh=10.0**(p(1)*punit(1))
*              T=p(2)*punit(2)
              
            results(k)=p(2)*punit(2)
            print
     ~  '(i6,2x,i4,1x,i4,2x,f6.2,3x,f6.3,3x,1pe9.3,3x,0pf6.2,'' :fitres'')'
     ~          ,l,i,j,p(2)*punit(2),p(3)*punit(3),10.0**(p(1)*punit(1))
     ~          ,chi2min
          else
            results(k)=0.0
          endif
        endif
        if (anydone.and.((l/256)*256.eq.l)) then
          anydone = .false.
          call write_fits_image (out,results,nx,ny,'e','e')
        endif
      enddo

      call write_fits_image (out,results,nx,ny,'e','e')

      call exit(0)
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_T_err (x)
      implicit none
      real*8 ch2_T_err,x,ch2
      real*8 p(200)
      real*8 punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      real chi2thresh
      common /cchi2/ chi2thresh
      real*8 ch2_nh,fmin
      external ch2_nh
       
      p(2)=x
      if (pfit(1).eq.1) then
        p(1)=fmin(-1.0d0,2.0d0,ch2_nh,1.0d-5)
      endif
      ch2_T_err=ch2()-chi2thresh
      return
      end
      
      function ch2_nh_err (x)
      implicit none
      real*8 ch2_nh_err,x,ch2
      real*8 p(200)
      real*8 punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      real chi2thresh
      common /cchi2/ chi2thresh
      real*8 ch2_T,fmin
      external ch2_T
       
      p(1)=x
      if (pfit(2).eq.1) then
        p(2)=fmin(0.1d0,25.0d0,ch2_T,1.0d-5)
      endif
      ch2_nh_err=ch2()-chi2thresh
      return
      end
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_T (x)
      implicit none
      real*8 x
      real*8 ch2_T,ch2
      real*8 p(200)
      real*8 punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
       
      p(2)=x
      ch2_T=ch2()
      return
      end
      
      function ch2_nh (x)
      implicit none
      real*8 x
      real*8 ch2_nh,ch2
      real*8 p(200)
      real*8 punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
       
      p(1)=x
      ch2_nh=ch2()
      return
      end
      

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_a (pp,n)
      implicit none
      real*8 ch2_a,pp(*)
      integer n
      real*8 ch2
      real*8 p(200)
      real*8 punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      integer i

      do i=1,n
        p(i)=pp(i)
      enddo
      ch2_a=ch2()
      return
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      function ch2()
      implicit none
      real*8 ch2
      integer npar
      real*8 p(200)
      real*8 punit(200)
      integer pfit(200)
      common /cpar/ p,punit,pfit
      character*80 amode
      integer fitabund
      common /camode/ amode, fitabund

      integer nechan
      parameter (nechan=1500)
      real ebin(0:nechan)
      real elow(nechan), ehigh(nechan)
      real e

      integer nspchanmax, nspchan, ichan
      parameter (nspchanmax=10)
      
      real matrix  (nspchanmax,nechan)
      real effarea (nechan)
      
      real data(nspchanmax)
      real error(nspchanmax)
      logical usechan(nspchanmax)
      real syserr(nspchanmax)
      real sys
      
      common /cdata/
     ~    ebin, matrix, effarea, data, error, usechan, syserr, nspchan
 

      real nH, T, a, z
      real modelspec(nechan)
      real model(nspchanmax)
      real Told,aold,zold,nhold
      real rsmodelspec(nechan)
      data Told/-1.0/, nhold /-1.0/
      real ee,absorp,sigism
      real sum,ch2_1,www
      logical firstcall
      data firstcall /.true./
      logical calc_RS
      save calc_RS
      integer ntgrid
      parameter (ntgrid=100)
      real rsspec(0:nechan,-1:ntgrid), absorb(0:nechan)
      real rsspec_a(0:nechan,-1:ntgrid,2)
      real Tgrid(ntgrid)
      real Tmin,Tmax, Tmin1, Tmax1
      save Tmin,Tmax,Tmin1,Tmax1
      real w1,w2
      integer ibin
      integer i,idata
      integer lnblnk,status

      save firstcall,aold,zold,Told,Tgrid,rsspec,rsspec_a,ibin,rsmodelspec
     ~    ,nhold,absorb

      character*80 rsgridfile
      logical defined, yespar

      integer unit,newunit
      logical saved
      data saved /.false./
      save saved
      
      
      nH=10.0**(p(1)*punit(1))
      T=p(2)*punit(2)
      a=p(3)*punit(3)
      z=p(4)*punit(4)

      if (nH.lt.0.0) then
        ch2=1.0e10
        return
      endif
      
      if (nH.lt.0.0) then
        ch2=1.0e10
        return
      endif
      
      
      if (a.lt.0.0) then
        ch2=1.0e10
        return
      endif

c z can be negative - for M86!        
*      if (z.lt.0.0) then
*        ch2=1.0e10
*        return
*      endif

      if (firstcall) then

        calc_RS = yespar ('calc_rs')

        if (.not.calc_RS) then
          call get_parameter_value_default ('tmax',tmax,14.5,'e')
          call get_parameter_value_default ('tmin',tmin,2.5,'e')

          call get_cl_par ('rsgrid',rsgridfile)
          if (defined(rsgridfile)) then
            if (fitabund.eq.1) then
              call read_fits_image (rsgridfile,rsspec_a,nechan+1,(ntgrid+2)*2
     ~            ,'e')
              do i=1,ntgrid
                Tgrid(i)=rsspec_a(0,i,1)
              enddo
            else
              call read_fits_image (rsgridfile,rsspec,nechan+1,ntgrid+2,'e')
              do i=1,ntgrid
                Tgrid(i)=rsspec(0,i)
              enddo
            endif
          else
            Tmin1=max(Tmin-0.5,0.1)
            Tmax1=Tmax+0.5
            do i=1,ntgrid
              Tgrid(i)=Tmin1+(i-1)*(Tmax1-Tmin1)/ntgrid
            enddo
            if (fitabund.eq.0) then
              call make_rs_grid (ebin,nechan,amode,a,z,rsspec,ntgrid,Tgrid)
              call get_cl_par ('saversgrid',rsgridfile)
              if (defined(rsgridfile)) then
                call write_fits_image (rsgridfile,rsspec,nechan+1,ntgrid+2,
     ~              'e','e')
              endif
            else
              call make_rs_grid_abund (ebin,nechan,amode,a,z,rsspec_a,ntgrid
     ~            ,Tgrid)
              call get_cl_par ('saversgrid',rsgridfile)
              if (defined(rsgridfile)) then
                call write_fits_image (rsgridfile,rsspec_a,nechan+1,(ntgrid+2)
     ~              *2,'e','e')
              endif
            endif
          endif
        endif
          
        firstcall=.false.
        zold=z
      endif

*      print*,'TMAX,TMNIN',Tmax,Tmin,calc_RS
*      pause

      if (.not.calc_RS) then
          
        if ( (z.ne.zold)) then
          call exiterror('only allow fixed z')
        endif
        if ( (T.ne.Told) .or. (a.ne.aold) ) then
          Told=T
          aold=a
          if ((T.lt.Tmin).or.(T.gt.Tmax)) then
            ch2=1.0e10
            return
          endif
          ibin = int(ntgrid*(T-Tmin1)/(Tmax1-Tmin1))+1
          if (ibin.ge.ntgrid) then
            ibin = ntgrid-1
          endif
          w1=(Tgrid(ibin+1)-T)/(Tgrid(ibin+1)-Tgrid(ibin))
          w2=1.0-w1
          if (fitabund.eq.0) then
            call veclinint(w1,w2,rsspec(1,ibin),rsspec(1,ibin+1),rsmodelspec
     ~          ,nechan,rsspec(1,-1),rsspec(1,0))
          else
            call veclinint_a(a,w1,w2,rsspec_a(1,ibin,1),rsspec_a(1,ibin,2),
     ~          rsspec_a(1,ibin+1,1),rsspec_a(1,ibin+1,2),rsmodelspec,nechan
     ~          ,rsspec_a(1,-1,1),rsspec_a(1,0,1))
          endif
        endif
*        print*, T,a,(rsmodelspec(i),i=1,10)
*        pause
      else
        if (T.le.0.0) then
          ch2=1.0e10
          return
        endif
        call raym (T,a,z,nechan,ebin,rsmodelspec)
*        print*, (rsmodelspec(i),i=1,10)
*        pause
      endif

      if ( nh.ne.nhold) then
        nhold = nh
*        print*,'recalc NH'
        do i=1,nechan
          ee=500.0*(ebin(i-1)+ebin(i))
          absorp=sigism(ee)*nH*1.0e22
          if (absorp.gt.-50.0) then
            absorp=exp(-absorp)
          else
            absorp=0.0
          endif
          absorb(i)=absorp
        enddo
      endif
      
      do i=1,nechan
        modelspec(i)=rsmodelspec(i)*absorb(i)
      enddo
      
      if (.not.calc_RS) then
        if (abs(T-6.0).lt.1.0e-4.and.(.not.saved)) then
          saved = .true.
          write (0,*) 'SAVE model spectrum for T=6keV ', T
          unit = newunit()
          open (unit,file='rs.6.0.grid')
          do i=1,nechan
            write (unit,'(1pe10.4,1x,1pe10.4,2x,1pe10.4,2x,2(1x,1pe10.4))')
     ~          ebin(i-1), ebin(i), modelspec(i), rsmodelspec(i), absorb(i)
          enddo
          close(unit)
        endif
      endif

      sum=0

      call conv_matr (model,modelspec,effarea,matrix,nspchanmax,nspchan,nechan
     ~    )
        
      www = ch2_1(model,data,error,syserr,usechan,nspchan)
      sum = sum + www

      
      ch2=sum
*      print*,'??? ',w1,w2,T,Tgrid(ibin),Tgrid(ibin+1),sum

*      print*, nH, T, a, z, ch2
      
*      if (sum.gt.20.0) then
*        print*,T,w1,w2
*        do i=1,nspchan
*          print*,i,data(i),error(i),model(i)
*        enddo
**        do i=1,nechan
**          print*,i,rsmodelspec(i),absorb(i),modelspec(i)
**        enddo
*        pause
*      endif
      
      
      return
      end

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine veclinint(w1,w2,v1,v2,r,n,e1,e2)
      implicit none
      real w1,w2
      integer n
      real v1(n),v2(n),r(n),e1(n),e2(n)
      integer i
      
      do i=1,n
        r(i)=(w1*v1(i)+w2*v2(i)) !!!*(e2(i)-e1(i))
      enddo
      return
      end
       
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine veclinint_a(a,w1,w2,v1,vlines1,v2,vlines2,r,n,e1,e2)
      implicit none
      real a
      real w1,w2
      integer n
      real v1(n),v2(n),r(n),e1(n),e2(n)
      real vlines1(n), vlines2(n)
      integer i
      
      do i=1,n
        r(i)=(
     ~      w1*(v1(i)+a*vlines1(i)) +
     ~      w2*(v2(i)+a*vlines2(i))
     ~      )                   !!!*(e2(i)-e1(i))
      enddo
      return
      end
       

*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      function ch2_1 (model,data,err,syserr,usechan,n)
      implicit none
      real ch2_1
      real model(*),data(*),err(*),syserr(*)
      logical usechan(*)
      integer n
      
      real rr,ss,norm,sum
      integer i
      
      rr=0
      ss=0
      do i=1,n
        if (usechan(i)) then
*          print*,model(i),data(i),err(i),syserr(i)
          ss=ss+model(i)*data(i)/(err(i)**2+(data(i)*syserr(i))**2)
          rr=rr+model(i)**2/(err(i)**2+(data(i)*syserr(i))**2)
        endif
      enddo
      
      norm=ss/rr
      
      sum=0
      do i=1,n
        if (usechan(i)) then
          sum=sum+
     ~        (norm*model(i)-data(i))**2/(err(i)**2+(data(i)*syserr(i))**2)
        endif
        model(i)=norm*model(i)
      enddo
       
      ch2_1=sum
      return
      end


*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine conv_matr (model,modelspec,effarea,matrix,nspchanmax,nspchan
     ~    ,nechan)
      implicit none
      integer nspchan,nechan
      real model(nspchan)
      real modelspec (nechan)
      real effarea(nechan)
      integer nspchanmax
      real matrix(nspchanmax,nechan)
      
      real flux
      integer ich,ie
      real sum

      do ich=1,nspchan
        model(ich)=0.0
      enddo
      
      do ie=1,nechan
        flux=modelspec(ie)      !*effarea(ie) ! I already multiplied matrix
                                ! by effarea in make_matr
        do ich=1,nspchan
*          print*,ie,ich,modelspec(ie),effarea(ie),matrix(ich,ie)
          model(ich)=model(ich)+flux*matrix(ich,ie)
        enddo
      enddo
      return
      end
      




*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine splitwords_char (string,char,words,nwords)
      implicit none
      character*(*) string
      character*1 char
      character*(*) words(*)
      integer nwords
      integer len,i,j,lnblnk
      
      len=lnblnk(string)
      
      i=1
      
      nwords=0
      
      do while (i.le.len)
        do while ((string(i:i).eq.char).and.i.lt.len)
          i=i+1
        enddo
        if (i.eq.len) then
          if (string(i:i).ne.char) then
            nwords=nwords+1
            words(nwords)=string(i:i)
          endif
          goto 100
        endif
        
        nwords=nwords+1
        j=1
        words(nwords)=' '
        do while ((string(i:i).ne.char).and.i.le.len)
          words(nwords)(j:j)=string(i:i)
          i=i+1
          j=j+1
        enddo
      enddo
      
 100  continue
      
      return
      end
      

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine get_ebounds (refrmf,elow,ehigh,ne)
      implicit none
      character*(*) refrmf
      real elow(*), ehigh(*)
      integer ne
      integer status, unitrmf, colnum, blocksize
      logical anyf
      character*200 wrefrmf
      
      status = 0
      call ftgiou (unitrmf,status)
      call ftopen (unitrmf,refrmf,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (refrmf,status)

      wrefrmf = refrmf
      call ftmnhd (unitrmf,-1,'SPECRESP MATRIX',0,status)
      if (status.ne.0) then
        status = 0
        call ftmnhd (unitrmf,-1,'MATRIX',0,status)
      endif
      if (status.ne.0) call exit_fitsio ('SPECRESP, '//wrefrmf,status)
      
      call ftgnrw (unitrmf,ne, status)
      if (status.ne.0) call exit_fitsio ('NAXIS2, '//wrefrmf,status)
      
      call ftgcno (unitrmf,.false.,'ENERG_LO',colnum,status)
      if (status.ne.0) call exit_fitsio ('ENERG_LO, '//wrefrmf,status)
      call ftgcve (unitrmf,colnum,1,1,ne,0.0,elow,anyf,status)
      if (status.ne.0) call exit_fitsio ('ENERG_LO, '//wrefrmf,status)
      
      call ftgcno (unitrmf,.false.,'ENERG_HI',colnum,status)
      if (status.ne.0) call exit_fitsio ('ENERG_HI, '//wrefrmf,status)
      call ftgcve (unitrmf,colnum,1,1,ne,0.0,ehigh,anyf,status)

      call ftclos(unitrmf,status)
      if (status.ne.0) call exit_fitsio (refrmf,status)
      call ftfiou(unitrmf,status)
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine adapt_smooth (i,j,img,err,mask,nx,ny,nchan,sigma,data,error)
      implicit none
      integer nx,ny,nchan
      real img(nx,ny,nchan)
      real err(nx,ny,nchan)
      byte mask (nx,ny)
      real sigma
      real data(nchan), error(nchan)
      
      integer idist(-100:100,-100:100)
      real gaussprof(0:10000)
      logical firstcall
      data firstcall /.true./
      save idist,gaussprof,firstcall
      
      real r,r2,w,weight
      integer ir,i1,i2,j1,j2,ii,jj,rr,i,j,ichan

      if (firstcall) then
        do ii=0,10000
          gaussprof(ii)=exp(-(0.01*ii)/2.0)
        enddo
        do ii=-100,100
          do jj=-100,100
            idist(ii,jj)=ii**2+jj**2
          enddo
        enddo
        firstcall = .false.
      endif

      do ichan=1,nchan
        data(ichan) = 0.0
        error(ichan)= 0.0
      enddo
      weight = 0.0
      r= sigma
      r2=r**2
      ir = int(r)+1
      i1=max(1,i-2*ir)
      j1=max(1,j-2*ir)
      i2=min(nx,i+2*ir)
      j2=min(ny,j+2*ir)
      rr=(2.0*ir)**2
      do ii=i1,i2
        do jj=j1,j2
          if (mask(ii,jj).eq.1) then
            ir = idist(ii-i,jj-j)
            if (ir.le.rr) then
              ir = nint(100.0*ir/r2) ! in unit of 0.1 sigma
              w = gaussprof(ir)
              do ichan=1,nchan
                data(ichan)  = data(ichan)  + w*img(ii,jj,ichan)
                error(ichan) = error(ichan) + (w*err(ii,jj,ichan))**2
              enddo
              weight = weight + w
            endif
          endif
        enddo
      enddo
      if (weight.gt.0) then
        do ichan=1,nchan
          data(ichan)=data(ichan)/weight
          error(ichan)=sqrt(error(ichan))/weight
        enddo
      endif
      
      return
      end
 
