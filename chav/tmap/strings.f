      subroutine remleadblanks (s)
      implicit none
      character*(*) s
      
      integer lnblnk
      integer i,n

      n = lnblnk(s)
      i = 1
      do  while (i.lt.n.and.(s(i:i).eq.' '.or.s(i:i).eq.'\t'))
        i = i + 1
      enddo
      
      s = s(i:)
      return
      end
      


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine strlow (s)
      implicit none
      integer n,lnblnk,i
      character*(*) s
      
      n=lnblnk(s)
      
      do i=1,n
        call tolower(s(i:i))
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine tolower (c)
c Lower case for ASCII chars
      character*1 c
      integer ic
      
      ic=ichar(c)
      if (ic.gt.64.and.ic.lt.90) then
        ic=ic+32
        c=char(ic)
      endif
      return
      end
      

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine strcat (s1,s2)
      implicit none
      character*(*) s1,s2
      integer lnblnk
      s1=s1(1:lnblnk(s1))//s2(1:lnblnk(s2))
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine rmblanks (s)
c Remove blanks from a string
      implicit none
      character*(*) s
      
      integer lnblnk,len,i,n

      len=lnblnk(s)
      n=0
      do i=1,len
        if(s(i:i).ne.' '.and.s(i:i).ne.'\t')then
          n=n+1
          s(n:n)=s(i:i)
        endif
      enddo
      
      do i=n+1,len
        s(i:i)=' '
      enddo
      
      return
      end
      
