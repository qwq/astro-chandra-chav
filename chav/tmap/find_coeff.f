      subroutine find_coeff (e,eref,ne,g_fwhm,g_pos,g_ampl,nmax,
     ~    gfwhm,gpos,gampl)
      implicit none
      real e
      integer ne
      real eref(ne)
      integer nmax
      real g_fwhm(nmax,0:10),g_pos(nmax,0:10),g_ampl(nmax,0:10),
     ~    gfwhm(0:10),gpos(0:10),gampl(0:10)

      real w1,w2
      integer i
      
      
      integer ibin
      if(e.le.eref(1))then
        ibin=1
        w1=1.0
        w2=0.0
      else if (e.ge.eref(ne)) then
        ibin=ne-1
        w1=0.0
        w2=1.0
      else
        ibin=ne/2
        call hunt (eref,ne-1,e,ibin)
        if (ibin.lt.1.or.ibin.gt.ne-1) then
          call exiterror ('unexpected error in find_coeff')
        endif
        call get_weights (e,eref(ibin),eref(ibin+1),w1,w2)
      endif


      do i=0,10
        gfwhm(i)= w1*g_fwhm(ibin,i) + w2*g_fwhm(ibin+1,i)
        gpos(i) = w1*g_pos(ibin,i)  + w2*g_pos(ibin+1,i)
        gampl(i)= w1*g_ampl(ibin,i) + w2*g_ampl(ibin+1,i)
      enddo
      
      return
      end
