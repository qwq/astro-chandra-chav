
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
c Print a error message to stderr and exit(1)
      subroutine exiterror (message)
      character*(*) message
      integer lnblnk

      write (0,*)
      write (0,'(a,a)') 'ERROR: ',message(1:lnblnk(message))
      call exit(1)
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine exit_fitsio (message,status)
      implicit none
      character*(*) message
      integer status

      call perror_fitsio (message,status)
      call exit(1)
      return
      end
       
      
      subroutine perror_fitsio (message,status)
      implicit none
      character*(*) message
      integer lnblnk
      integer status
      character*80 errtext
      
      if (status.ne.0) then
        call FTGERR (status,errtext)
        do while (errtext.ne.'')
          write(0,'(a,a,a)')message(1:lnblnk(message)),': '
     ~        ,errtext(1:lnblnk(errtext))
          call FTGMSG (errtext)
        enddo
      endif
      return
      end
      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
c Print a message to stderr
      subroutine putmessage (message)
      character*(*) message
      integer lnblnk

      write (0,*)
      write (0,'(a)') message(1:lnblnk(message))
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
c Print a warning to stderr
      subroutine putwarning (whowarns,message)
      character*(*) whowarns, message
      integer lnblnk
      character*200 whowrns, msg

      whowrns = whowarns
      msg     = message

      write (0,'(a)') 'Warning from '//whowrns(1:lnblnk(whowrns))//':  '//
     ~    msg(1:lnblnk(msg))
      return
      end

