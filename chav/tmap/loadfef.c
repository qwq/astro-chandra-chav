#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fitsio.h"
#include <math.h>

#define NMAX 10000
float ENERGY[NMAX], 
  B1_XLOW[NMAX], B1_XHI[NMAX], B1_AMPL[NMAX],
  G1_FWHM[NMAX], G1_POS[NMAX], G1_AMPL[NMAX],
  G2_FWHM[NMAX], G2_POS[NMAX], G2_AMPL[NMAX],
  G3_FWHM[NMAX], G3_POS[NMAX], G3_AMPL[NMAX],
  G4_FWHM[NMAX], G4_POS[NMAX], G4_AMPL[NMAX],
  G5_FWHM[NMAX], G5_POS[NMAX], G5_AMPL[NMAX],
  G6_FWHM[NMAX], G6_POS[NMAX], G6_AMPL[NMAX],
  G7_FWHM[NMAX], G7_POS[NMAX], G7_AMPL[NMAX],
  G8_FWHM[NMAX], G8_POS[NMAX], G8_AMPL[NMAX],
  G9_FWHM[NMAX], G9_POS[NMAX], G9_AMPL[NMAX],
  G10_FWHM[NMAX], G10_POS[NMAX], G10_AMPL[NMAX];

int N;
int NGAUSS;


void loadfef_(char *feffile, long *nread,
	      float *energy,
	      float *b1_xlow, float *b1_xhi, float *b1_ampl,
	      float *g1_fwhm, float *g1_pos, float *g1_ampl,
	      float *g2_fwhm, float *g2_pos, float *g2_ampl,
	      float *g3_fwhm, float *g3_pos, float *g3_ampl,
	      float *g4_fwhm, float *g4_pos, float *g4_ampl,
	      float *g5_fwhm, float *g5_pos, float *g5_ampl,
	      float *g6_fwhm, float *g6_pos, float *g6_ampl,
	      float *g7_fwhm, float *g7_pos, float *g7_ampl,
	      float *g8_fwhm, float *g8_pos, float *g8_ampl,
	      float *g9_fwhm, float *g9_pos, float *g9_ampl,
	      float *g10_fwhm, float *g10_pos, float *g10_ampl)
{	      
  extern load_fef ();   /* EXTERNAL WORK FUNCTION IS PASSED TO THE ITERATOR */
  fitsfile *fptr;
  iteratorCol cols[100]; /* STRUCTURE USED BY THE ITERATOR FUNCTION */
  int ncols;
  long rows_per_loop, offset;
  
  char funcid[200], comment[200];
  int status, nkeys, keypos, hdutype, i;
  

  status = 0; 
  
  fits_open_file(&fptr, feffile, READONLY, &status); /* open file */
    
    /* move to the EVENTS binary table extension */
  if (fits_movnam_hdu(fptr, BINARY_TBL, "FUNCTION", 0, &status) ) {
    fits_report_error(stderr, status);    /* print out error messages */
    exit(1);
  }
  
  
  fits_read_key (fptr,TSTRING,"FUNCTION",funcid,comment,&status);
  if (status) {
    fits_report_error(stderr, status);  /* print out error messages */
    fprintf (stderr,"Function definition not found\n");
    exit(1);
  }
  
  if (strcmp(funcid,"(b1*(((((((((g1+g2)+g3)+g4)+g5)+g6)+g7)+g8)+g9)+g10))") 
      == 0 ) {
    NGAUSS = 10;
  } else {
    if (strcmp(funcid,"(b1*((((((g1+g2)+g3)+g4)+g5)+g6)+g7))") 
	== 0 ) {
      NGAUSS = 7;
    }
    else {
      if (strcmp(funcid,"(g1+g2)") == 0 ||
	  strcmp(funcid,"(b1*(g1+g2))") == 0 ) {
	NGAUSS = 2;
      }
      else {
	fprintf (stderr,"ERROR: nnknown function: %s\n",funcid);
	exit(1);
      }
    }
  }
  ncols  = 1+NGAUSS*3;   /* number of columns
		   */
    /* define input column structure members for the iterator function */
  fits_iter_set_by_name(&cols[0], fptr, "energy", TFLOAT,  InputCol);

  /*
    fits_iter_set_by_name(&cols[1], fptr, "b1_xlow",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[2], fptr, "b1_xhi", TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[3], fptr, "b1_ampl",TFLOAT,  InputCol);
  */

  fits_iter_set_by_name(&cols[1], fptr, "g1_fwhm",TFLOAT,  InputCol);
  fits_iter_set_by_name(&cols[2], fptr, "g1_ampl",TFLOAT,  InputCol);
  fits_iter_set_by_name(&cols[3], fptr, "g1_pos", TFLOAT,  InputCol);

  if (NGAUSS>1) {
    fits_iter_set_by_name(&cols[4], fptr, "g2_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[5], fptr, "g2_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[6], fptr, "g2_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>2) {
    fits_iter_set_by_name(&cols[7], fptr, "g3_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[8], fptr, "g3_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[9], fptr, "g3_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>3) {
    fits_iter_set_by_name(&cols[10], fptr, "g4_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[11], fptr, "g4_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[12], fptr, "g4_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>4) {
    fits_iter_set_by_name(&cols[13], fptr, "g5_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[14], fptr, "g5_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[15], fptr, "g5_pos", TFLOAT,  InputCol);
  }
  
  if (NGAUSS>5) {
    fits_iter_set_by_name(&cols[16], fptr, "g6_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[17], fptr, "g6_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[18], fptr, "g6_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>6) {
    fits_iter_set_by_name(&cols[19], fptr, "g7_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[20], fptr, "g7_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[21], fptr, "g7_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>7) {
    fits_iter_set_by_name(&cols[22], fptr, "g8_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[23], fptr, "g8_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[24], fptr, "g8_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>8) {
    fits_iter_set_by_name(&cols[25], fptr, "g9_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[26], fptr, "g9_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[27], fptr, "g9_pos", TFLOAT,  InputCol);
  }

  if (NGAUSS>9) {
    fits_iter_set_by_name(&cols[28], fptr, "g10_fwhm",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[29], fptr, "g10_ampl",TFLOAT,  InputCol);
    fits_iter_set_by_name(&cols[30], fptr, "g10_pos", TFLOAT,  InputCol);
  }

  rows_per_loop = 0;  /* use default optimum number of rows */
  offset = 0;         /* process all the rows */

    /* apply the rate function to each row of the table */
  N = 0;
  fits_iterate_data(ncols, cols, offset, rows_per_loop,
		    load_fef, 0L, &status);

  fits_close_file(fptr, &status);      /* all done */

  if (status)
    fits_report_error(stderr, status);  /* print out error messages */
  
  for (i=0;i<N;i++) {
    energy[i]   = ENERGY[i];

    /*
      b1_xlow[i]  = B1_XLOW[i];
      b1_xhi[i]   = B1_XHI[i];
      b1_ampl[i]  = B1_AMPL[i];
    */
    
    b1_xlow[i]  = 1;
    b1_xhi[i]   = 1024;
    b1_ampl[i]  = 1.0;

                           
    g1_fwhm[i]  = G1_FWHM[i];
    g1_ampl[i]  = G1_AMPL[i];
    g1_pos[i]   = G1_POS[i];
                      
    if (NGAUSS>1) {
      g2_fwhm[i]  = G2_FWHM[i];
      g2_ampl[i]  = G2_AMPL[i];
      g2_pos[i]   = G2_POS[i];
    }
     
    if (NGAUSS>2) {
      g3_fwhm[i]  = G3_FWHM[i];
      g3_ampl[i]  = G3_AMPL[i];
      g3_pos[i]   = G3_POS[i];
    } else
      g3_ampl[i] = 0.0;

    if (NGAUSS>3) {
      g4_fwhm[i]  = G4_FWHM[i];
      g4_ampl[i]  = G4_AMPL[i];
      g4_pos[i]   = G4_POS[i];
    } else
      g4_ampl[i] = 0.0;
    
    if (NGAUSS>4) {
      g5_fwhm[i]  = G5_FWHM[i];
      g5_ampl[i]  = G5_AMPL[i];
      g5_pos[i]   = G5_POS[i];
    } else
      g5_ampl[i] = 0.0;
    
    if (NGAUSS>5) {
      g6_fwhm[i]  = G6_FWHM[i];
      g6_ampl[i]  = G6_AMPL[i];
      g6_pos[i]   = G6_POS[i];
    } else
      g6_ampl[i] = 0.0;
                           
    if (NGAUSS>6) {
      g7_fwhm[i]  = G7_FWHM[i];
      g7_ampl[i]  = G7_AMPL[i];
      g7_pos[i]   = G7_POS[i];
    } else
      g7_ampl[i] = 0.0;
    
    if (NGAUSS>7) {
      g8_fwhm[i]  = G8_FWHM[i];
      g8_ampl[i]  = G8_AMPL[i];
      g8_pos[i]   = G8_POS[i];
    } else
      g8_ampl[i] = 0.0;
    
    if (NGAUSS>8) {
      g9_fwhm[i]  = G9_FWHM[i];
      g9_ampl[i]  = G9_AMPL[i];
      g9_pos[i]   = G9_POS[i];
    } else
      g9_ampl[i] = 0.0;

    if (NGAUSS>9) {
      g10_fwhm[i] = G10_FWHM[i];
      g10_ampl[i] = G10_AMPL[i];
      g10_pos[i]  = G10_POS[i];
    } else
      g10_ampl[i] = 0.0;
  }
  *nread = N;

}
/*--------------------------------------------------------------------------*/
int load_fef (long totalrows, long offset, long firstrow, long nrows,
	      int ncols, iteratorCol *cols, void *user_strct ) 
{
  int ii, status = 0, i;
  
  static float *energy,
    *b1_xlow, *b1_xhi, *b1_ampl,
    *g1_fwhm, *g1_pos, *g1_ampl,
    *g2_fwhm, *g2_pos, *g2_ampl,
    *g3_fwhm, *g3_pos, *g3_ampl,
    *g4_fwhm, *g4_pos, *g4_ampl,
    *g5_fwhm, *g5_pos, *g5_ampl,
    *g6_fwhm, *g6_pos, *g6_ampl,
    *g7_fwhm, *g7_pos, *g7_ampl,
    *g8_fwhm, *g8_pos, *g8_ampl,
    *g9_fwhm, *g9_pos, *g9_ampl,
    *g10_fwhm, *g10_pos, *g10_ampl;
  /* Declare variables static to preserve their values between calls */
  

  /*--------------------------------------------------------*/
  /*  Initialization procedures: execute on the first call  */
  /*--------------------------------------------------------*/
  if (firstrow == 1)
    {
      if (ncols != 1+NGAUSS*3)
	return(-1);  /* number of columns incorrect */
      
      for (i=0;i<ncols;i++)
	if (fits_iter_get_datatype(&cols[0]) != TFLOAT )
	  return(-2);  /* bad data type */
      
      /* assign the input pointers to the appropriate arrays and null ptrs*/
      energy = (float *) fits_iter_get_array(&cols[0]);
  
      /*      b1_xlow = (float *) fits_iter_get_array(&cols[1]);
	      b1_xhi  = (float *) fits_iter_get_array(&cols[2]);
	      b1_ampl = (float *) fits_iter_get_array(&cols[3]);
      */
      
      g1_fwhm = (float *) fits_iter_get_array(&cols[1]);
      g1_ampl = (float *) fits_iter_get_array(&cols[2]);
      g1_pos  = (float *) fits_iter_get_array(&cols[3]);
      
      if (NGAUSS>1) {
	g2_fwhm = (float *) fits_iter_get_array(&cols[4]);
	g2_ampl = (float *) fits_iter_get_array(&cols[5]);
	g2_pos  = (float *) fits_iter_get_array(&cols[6]);
      }

      if (NGAUSS>2) {
	g3_fwhm = (float *) fits_iter_get_array(&cols[7]);
	g3_ampl = (float *) fits_iter_get_array(&cols[8]);
	g3_pos  = (float *) fits_iter_get_array(&cols[9]);
      }
      
      if (NGAUSS>3) {
	g4_fwhm = (float *) fits_iter_get_array(&cols[10]);
	g4_ampl = (float *) fits_iter_get_array(&cols[11]);
	g4_pos  = (float *) fits_iter_get_array(&cols[12]);
      }
      
      if (NGAUSS>4) {
	g5_fwhm = (float *) fits_iter_get_array(&cols[13]);
	g5_ampl = (float *) fits_iter_get_array(&cols[14]);
	g5_pos  = (float *) fits_iter_get_array(&cols[15]);
      }
      
      if (NGAUSS>5) {
	g6_fwhm = (float *) fits_iter_get_array(&cols[16]);
	g6_ampl = (float *) fits_iter_get_array(&cols[17]);
	g6_pos  = (float *) fits_iter_get_array(&cols[18]);
      }
      
      if (NGAUSS>6) {
	g7_fwhm = (float *) fits_iter_get_array(&cols[19]);
	g7_ampl = (float *) fits_iter_get_array(&cols[20]);
	g7_pos  = (float *) fits_iter_get_array(&cols[21]);
      }
      
      if (NGAUSS>7) {
	g8_fwhm = (float *) fits_iter_get_array(&cols[22]);
	g8_ampl = (float *) fits_iter_get_array(&cols[23]);
	g8_pos  = (float *) fits_iter_get_array(&cols[24]);
      }
      
      if (NGAUSS>8) {
	g9_fwhm = (float *) fits_iter_get_array(&cols[25]);
	g9_ampl = (float *) fits_iter_get_array(&cols[26]);
	g9_pos  = (float *) fits_iter_get_array(&cols[27]);
      }
      
      if (NGAUSS>9) {
	g10_fwhm= (float *) fits_iter_get_array(&cols[28]);
	g10_ampl= (float *) fits_iter_get_array(&cols[29]);
	g10_pos = (float *) fits_iter_get_array(&cols[30]);
      }
      
    }
  
  /*--------------------------------------------*/
  /*  Main loop: process all the rows of data */
  /*--------------------------------------------*/

  /*  NOTE: 1st element of array is the null pixel value!  */
  /*  Loop from 1 to nrows, not 0 to nrows - 1.  */

  for (ii = 1; ii <= nrows; ii++)
    {
      ENERGY[N]  = energy[ii];
  		          
      /*      B1_XLOW[N] = b1_xlow[ii]; 
	      B1_XHI[N]  = b1_xhi[ii];  
	      B1_AMPL[N] = b1_ampl[ii]; 
      */
      		          
      G1_FWHM[N] = g1_fwhm[ii]; 
      G1_AMPL[N] = g1_ampl[ii]; 
      G1_POS[N]  = g1_pos[ii]; 
      	
      if (NGAUSS>1) {
	G2_FWHM[N] = g2_fwhm[ii];
	G2_AMPL[N] = g2_ampl[ii];
	G2_POS[N]  = g2_pos[ii];
      }
      		          
      if (NGAUSS>2) {
	G3_FWHM[N] = g3_fwhm[ii]; 
	G3_AMPL[N] = g3_ampl[ii]; 
	G3_POS[N]  = g3_pos[ii];  
      }
      		          
      if (NGAUSS>3) {
	G4_FWHM[N] = g4_fwhm[ii]; 
	G4_AMPL[N] = g4_ampl[ii]; 
	G4_POS[N]  = g4_pos[ii];  
      }
      		          
      if (NGAUSS>4) {
	G5_FWHM[N] = g5_fwhm[ii]; 
	G5_AMPL[N] = g5_ampl[ii]; 
	G5_POS[N]  = g5_pos[ii];  
      }
      		          
      if (NGAUSS>5) {
	G6_FWHM[N] = g6_fwhm[ii]; 
	G6_AMPL[N] = g6_ampl[ii]; 
	G6_POS[N]  = g6_pos[ii];  
      }
      		          
      if (NGAUSS>6) {
	G7_FWHM[N] = g7_fwhm[ii]; 
	G7_AMPL[N] = g7_ampl[ii]; 
	G7_POS[N]  = g7_pos[ii];  
      }
      		          
      if (NGAUSS>7) {
	G8_FWHM[N]= g8_fwhm[ii]; 
	G8_AMPL[N] = g8_ampl[ii]; 
	G8_POS[N]  = g8_pos[ii];  
      }
      		          
      if (NGAUSS>8) {
	G9_FWHM[N] = g9_fwhm[ii]; 
	G9_AMPL[N] = g9_ampl[ii]; 
	G9_POS[N]  = g9_pos[ii];  
      }
      		          
      if (NGAUSS>9) {
	G10_FWHM[N]= g10_fwhm[ii];
	G10_AMPL[N]= g10_ampl[ii];
	G10_POS[N] = g10_pos[ii];
      }

      N++;

    }
  
  return(0);  /* return successful status */
}









