      subroutine spline(x,y,n,yp1,ypn,y2)
c
c Given arrays x(n) and y(n) containing tab. function with x1<x2<..<xn
c and given first deriv. of of the interpolating function yp1 and ypn at
c points x1 and xn respectively this routine returns an array y2(n). If yp1 
c and/or ypn are equal to 1e30 or larger, the routie is signaled to set corr. 
c boundary condition to natural spline, i.e. with 0 second deriv. on that
c boundary 
c
      implicit undefined (a-z)
      integer n,nmax
      parameter (nmax=1000)
      real x(n),y(n),y2(n),u(nmax)
      real yp1,ypn
      integer i,k
      real p,qn,sig,un
      
      if (yp1.gt.0.99e30) then
        y2(1)=0.
        u(1)=0.
      else
        y2(1)=-0.5
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.
        y2(i)=(sig-1.)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     ~      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
      enddo
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5
        un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
      do k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
      enddo
      return
      end
      
      
      subroutine spline8(x,y,n,yp1,ypn,y2)
c
c Given arrays x(n) and y(n) containing tab. function with x1<x2<..<xn
c and given first deriv. of of the interpolating function yp1 and ypn at
c points x1 and xn respectively this routine returns an array y2(n). If yp1 
c and/or ypn are equal to 1e30 or larger, the routie is signaled to set corr. 
c boundary condition to natural spline, i.e. with 0 second deriv. on that
c boundary 
c
      implicit undefined (a-z)
      integer n,nmax
      parameter (nmax=1000)
      real*8 x(n),y(n),y2(n),u(nmax)
      real*8 yp1,ypn
      integer i,k
      real*8 p,qn,sig,un
      
      if (yp1.gt.0.99d30) then
        y2(1)=0.
        u(1)=0.
      else
        y2(1)=-0.5
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.
        y2(i)=(sig-1.)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     ~      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
      enddo
      if (ypn.gt..99d30) then
        qn=0.
        un=0.
      else
        qn=0.5
        un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
      do k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
      enddo
      return
      end
