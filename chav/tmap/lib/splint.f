      subroutine splint(xa,ya,y2a,n,x,y)
c
c Given the arrays xa(n) and ya(n) of length n which tabulate a function and
c given y2a(n) which is teh output from SPLINE returns cub. spline
c interpolated value of y in point x
c
c---------------------------
      implicit undefined (a-z)
      integer n
      real xa(n),ya(n),y2a(n),x,y
      integer klo,khi,k
      real h,a,b
      
      klo=1
      khi=n
      do while(khi-klo.gt.1)
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      enddo
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input. in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     ~    ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
      return
      end
      
      
      
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine splint8(xa,ya,y2a,n,x,y)
c
c Given the arrays xa(n) and ya(n) of length n which tabulate a function and
c given y2a(n) which is teh output from SPLINE returns cub. spline
c interpolated value of y in point x
c
c---------------------------
      implicit undefined (a-z)
      integer n
      real*8 xa(n),ya(n),y2a(n),x,y
      integer klo,khi,k
      real*8 h,a,b
      
      klo=1
      khi=n
      do while(khi-klo.gt.1)
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      enddo
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input. in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+
     ~    ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
      return
      end
