      subroutine read_pixlib_pars (aimpoint, fpsys, useroll, roll_average)
      implicit none
      character*(*) aimpoint, fpsys
      logical useroll
      real roll_average
      
      integer subarray_firstrow, subarray_nrows
      call read_pixlib_pars_work (aimpoint, fpsys, useroll,
     ~    roll_average, .false., subarray_firstrow, subarray_nrows) 
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      subroutine read_pixlib_subarray_pars (aimpoint, fpsys, useroll,
     ~    roll_average, subarray_firstrow, subarray_nrows)
      implicit none
      character*(*) aimpoint, fpsys
      logical useroll
      real roll_average
      integer subarray_firstrow, subarray_nrows

      call read_pixlib_pars_work (aimpoint, fpsys, useroll,
     ~    roll_average, .true., subarray_firstrow, subarray_nrows) 
      
      return
      end
      

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_pixlib_pars_work (aimpoint, fpsys, useroll,
     ~    roll_average, usesubarray, subarray_firstrow, subarray_nrows) 
c
c Reads command line parameters that set aimpoint and fpsys; 
c
      implicit none
      character*(*) aimpoint, fpsys
      logical useroll
      real roll_average
      logical usesubarray
      integer subarray_firstrow, subarray_nrows

c FITSIO vars
      integer unitevt, status
      character*200 eventfile
      character*80 comment, sim_z, sim_x, sim_y

      logical defined


c Read aim point and focal plane system for pixlib
      unitevt = 0
      call get_cl_par ('aimpoint',aimpoint)
      if (.not.defined(aimpoint)) then
c       Try to open events file and read it from there
        if (unitevt.le.0) then
          call get_cl_par ('evtfile',eventfile)
          if (.not.defined(eventfile)) then
            call exiterror ('Neither -aimpoint nor -evtfile is set')
          endif
c         Open events file and move to events extension
          call read_pixlib_pars_move_to_evt (eventfile,unitevt)
        endif
c         Read keyword
        status = 0
        call ftgkey (unitevt,'SIM_Z',sim_z,comment,status)
        if (status.ne.0) then
          write (0,*) 'SIM_Z keyword is not found in the events file'
          write (0,*) 'please set amipoint manually'
          call exit(1)
        endif
        call ftgkey (unitevt,'SIM_X',sim_x,comment,status)
        call ftgkey (unitevt,'SIM_Y',sim_y,comment,status)

        aimpoint = sim_x
        call strcat (aimpoint,',')
        call strcat (aimpoint,sim_y)
        call strcat (aimpoint,',')
        call strcat (aimpoint,sim_z)
        
      endif

      call get_cl_par ('fpsys',fpsys)
      if (.not.defined(fpsys)) then
c       Try to open events file and read it from there
        if (unitevt.le.0) then
          call get_cl_par ('evtfile',eventfile)
          if (.not.defined(eventfile)) then
            call exiterror ('Neither -fpsys nor -evtfile is set')
          endif
c         Open events file and move to events extension
          call read_pixlib_pars_move_to_evt (eventfile,unitevt)
        endif
c         Read keyword
        status = 0
        call ftgkys (unitevt,'ACSYS3',fpsys,comment,status)
        if (status.ne.0) then
          write (0,*) 'ACSYS3 keyword is not found in the events file'
          write (0,*) 'please set FP system manually'
          call exit(1)
        endif
        if (fpsys(1:8).eq.'DET:ASC-') fpsys = fpsys (9:)
      endif
      
c Roll angle, if needed
      if (useroll) then
        if (unitevt.le.0) then
          call get_cl_par ('evtfile',eventfile)
          if (.not.defined(eventfile)) then
            call exiterror ('Roll required but -evtfile is uneset')
          endif
c         Open events file and move to events extension
          call read_pixlib_pars_move_to_evt (eventfile,unitevt)
        endif
        call ftgkye (unitevt,'ROLL_NOM',roll_average,comment,status)
        if (status.ne.0) then
          call exit_fitsio ('ROLL_NOM: '//eventfile,status)
        endif
      endif

c Subarray info, if needed
      if (usesubarray) then
        if (unitevt.le.0) then
          call get_cl_par ('evtfile',eventfile)
          if (.not.defined(eventfile)) then
            call exiterror ('SUBARRAYS required but -evtfile is uneset')
          endif
c         Open events file and move to events extension
          call read_pixlib_pars_move_to_evt (eventfile,unitevt)
        endif
        call ftgkyj (unitevt,'FIRSTROW',subarray_firstrow,comment,status)
        call ftgkyj (unitevt,'NROWS',subarray_nrows,comment,status)
        if (status.ne.0) then
          write (0,*) 'WARNING: subarray info not found: assume no subarray'
          subarray_firstrow = 1
          subarray_nrows = 1023
          status = 0
        endif
      endif

      if (unitevt.gt.0) then
        status = 0
        call ftclos (unitevt,status)
        call ftfiou (unitevt,status)
      endif
      
      call strlow(fpsys)
      call strlow(aimpoint)
      return
      end
        

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_pixlib_pars_move_to_evt (eventfile,unitevt)
      implicit none
      character*(*) eventfile
      integer unitevt
      integer status
      character*80 eventsname
      logical defined
      integer blocksize
      
      status = 0
      call ftgiou (unitevt,status)
      if (status.ne.0) call exit_fitsio (eventfile,status)
      call ftopen (unitevt,eventfile,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (eventfile,status)
c          read par that specs the events ext. name; if not set, use EVENTS
      call get_cl_par ('eventsname',eventsname)
      if (.not.defined(eventsname)) eventsname = 'EVENTS'
c          go to EVENTS extension
      call ftmnhd (unitevt,-1,eventsname,0,status)
      if (status.ne.0) call exit_fitsio (eventfile,status)
      
      return
      end
