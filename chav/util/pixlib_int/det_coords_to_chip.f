      subroutine det_coords_to_chip (detx,dety,pixsize,chipx,chipy,iccd)
      implicit none
      real detx,dety,chipx,chipy
      integer iccd
      real pixsize
      integer ncorstep, icorstep
      parameter (ncorstep=20)
      real corx(ncorstep)
      real cory(ncorstep)
      data corx /0.25, 0.0, -0.25,  0.0, 0.5, 0.0, -0.5,  0.0, 0.5, 0.5,-0.5,
     ~    -0.5, -1.126, 0.0, 1.126, 0.0, -1.126, 1.126,  1.126, -1.126/
      data cory /0.0, 0.25,  0.0, -0.25, 0.0, 0.5,  0.0, -0.5, 0.5,-0.5, 0.5,
     ~    -0.5,  0.0, 1.126, 0.0, -1.126, 1.126, 1.126, -1.126, -1.126/
      save corx,cory

      call det2chip (detx,dety,chipx,chipy,iccd)

      if (iccd.lt.0) then
        icorstep=0
        do while (iccd.lt.0.and.icorstep.lt.ncorstep)
          icorstep=icorstep+1
*          print*, detx+pixsize*corx(icorstep), dety+pixsize*cory(icorstep)
          call det2chip (
     ~        detx+pixsize*corx(icorstep),
     ~        dety+pixsize*cory(icorstep),
     ~        chipx,chipy,iccd)
***          print*,detx+pixsize*corx(icorstep),dety+pixsize*cory(icorstep),chipx,chipy,iccd
        enddo
      endif
      if (iccd.lt.0) then
        write (0,*) ' ! could not calculate chip coordinates for detx,dety=',
     ~      detx,dety
*        call exit(1)
      endif

      if (nint(chipx).lt.1.or.nint(chipx).gt.1024.or.
     ~     nint(chipy).lt.1.or.nint(chipx).gt.1024) then
        write (0,'(a,1x,f6.2,1x,f6.2,1x,'' => '',i1,1x,f6.2,1x,f6.2)')
     ~       'WARNING: chip coords outside the bounds for: ',
     ~       detx,dety,iccd,chipx,chipy
      endif

      chipx = max(1.0,min(chipx,1024.0))
      chipy = max(1.0,min(chipy,1024.0))

      
      
      return
      end
