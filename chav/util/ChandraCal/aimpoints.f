      subroutine aimpoint_pixlib_name (aim,pixlibname)
      implicit none
      character*(*) aim, pixlibname
      character*200 aimpoint

      aimpoint = aim
      call strlow (aimpoint)
      
      if (aimpoint(1:5).eq.'acis-') aimpoint = aimpoint(6:)

      if (aimpoint.eq.'i1') then
        pixlibname = 'AI1'
      else if (aimpoint.eq.'c0') then
        pixlibname = 'A2C0'
      else if (aimpoint.eq.'i3') then
        pixlibname = 'AI2'
      else if (aimpoint.eq.'s3') then
        pixlibname = 'AS1'
      else if (aimpoint.eq.'hrc-i') then
        pixlibname = 'HI1'
      else if (aimpoint.eq.'hrc-s2') then
        pixlibname = 'HS1'
      else
        pixlibname = aim
      endif

      return
      end
