      subroutine read_hrma_onaxis_ea (ne,e0,ea)
      implicit none
c
c Reads calibration data for onaxis effective area
c
c O ne      integer    - the number of grid points
c O e0(ne)  real       - energy grid [keV]
c O ea(ne)  real       - onaxis effective area [cm**2]
c
      integer ne
      real e0(*), ea(*)

      integer unit,newunit,status
      integer i,k
      real ww(300)
      integer nw, icole,icola
      character*200 calfile, string
      character*30 word(100)
      integer nwords
      logical defined
      logical read_hrma_onaxis_ea_fits
      external read_hrma_onaxis_ea_fits
      integer qexpand

      include 'calcomments.inc'

c
c Read hrma_ea_raytrace_scaled_orbit.rdb from
c http://hea-www.harvard.edu/MST/mirror/www/orbit/hrma_ea.html
c
c
c energy  ea_1    err_1   ea_3    err_3   ea_4    err_4   ea_6    err_6   ea_hrma err_hrma
c  9N      7N      6N      7N      5N      7N      5N      7N      5N      7N      8N
c 0.010000        395.306 18.173  251.689 6.597   197.119 6.618   108.491 3.463   952.266 22.782

      call rp_q_expand_vars (qexpand)
      call rp_set_expand_vars (1)
      call get_cl_par ('hrma_onaxis_area',calfile)
      call rp_set_expand_vars (qexpand)
      NCALCOM = NCALCOM + 1
      CALCOMNAME(NCALCOM) = 'HRMA_A'
      CALCOMVALUE(NCALCOM) = calfile
      CALCOMCOM(NCALCOM) = 'HRMA onaxis area'

      if (.not.defined(calfile)) then
        call exiterror ('(read_hrma_onaxis_ea:) hrma_onaxis_area=?')
      endif
      
      call putmessage ('Onaxis HRMA EA: '//calfile)
      
      if (read_hrma_onaxis_ea_fits(calfile,ne,e0,ea)) then
        return
      endif
! ELSE, READ IN RDB format
      unit=newunit()
      open(unit,file=calfile,status='old')

      read (unit,'(a)') string
c find enery and hrma area columns
      call splitwords (string,word,nwords)
      icole = 0
      icola = 0
      do i=1,nwords
        if (word(i).eq.'energy') then
          icole=i
        else if (word(i).eq.'ea_hrma') then
          icola=i
        endif
      enddo
      if (icole.eq.0) then
        call exiterror ('Column ''energy'' is not found in RDB file '
     ~      //calfile)
      endif
      if (icola.eq.0) then
        call exiterror ('Column ''ea_hrma'' is not found in RDB file '
     ~      //calfile)
      endif
      nw = max(icole,icola)

      read (unit,*)             ! skip format line
      status=0
      i = 0
      do while (status.eq.0)
        read (unit,*,iostat=status) (ww(k),k=1,nw)
        if (status.eq.0) then
          i = i + 1
          e0(i)=ww(icole)
          ea(i)=ww(icola)
        endif
      enddo
      close(unit)

      ne = i

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      logical function read_hrma_onaxis_ea_fits (calfile,ne,e0,ea)
      implicit none
      character*(*) calfile
      integer ne
      real e0(*), ea(*)
      real e1(100000)
      

      integer unit
      integer status,blocksize,datacode,width,colnum,i
      logical anyf

      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call ftopen (unit,calfile,0,blocksize,status)
      if (status.ne.0) then     ! not a fits file
        status=0
        call ftfiou (unit,status)
        read_hrma_onaxis_ea_fits = .false.
        return
      endif

      call FTMNHD (unit, -1, 'AXAF_AXEFFA', 0,status)

      call FTGCNO (unit,.false.,'ENERG_LO',colnum,status)
      call FTGTCL (unit,colnum, datacode,ne,width,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,e0,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'ENERG_HI',colnum,status)
      call FTGTCL (unit,colnum, datacode,ne,width,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,e1,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'EFFAREA',colnum,status)
      call FTGTCL (unit,colnum, datacode,ne,width,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,ea,anyf,status)

      call ftclos (unit,status)
      call ftfiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      
      do i=1,ne
        e0(i)=0.5*(e0(i)+e1(i))
      enddo
      
      read_hrma_onaxis_ea_fits = .true.
      return

      end
