      real function hrma_onaxis_ea (e)
      implicit none
c 
c HRMA on-axis effective area.
c I e   real    -- photon energy [keV]
c
c Interpolates over cal data points. Since one typically calculates the onaxis
c area array only once, we can afford using cubic spline interpolation
c
c USES  spline from NR
c

      real e

c
      integer nemax, ne
      parameter (nemax=11000)
      real e0(nemax), ea(nemax)
c     real work(nemax)
      save ne, e0, ea
c     save work
      
      logical firstcall
      data firstcall /.true./
      save firstcall

      real a
      
      if (firstcall) then
c        1) Read cal. data
        call read_hrma_onaxis_ea (ne,e0,ea)
        

c spline interpolation is bad because there may be strong discontinuities
c in the cal data
c
*c        2) Ini spline
*        call spline(e0,ea,ne,2.0e30,2.0e30,work)
        
        firstcall = .false.
      endif

      call lin_interpolate (e0,ea,ne,e,a)

      hrma_onaxis_ea = a
      
      return
      end
