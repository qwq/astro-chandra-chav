      subroutine read_ccd_qe (calfile,ne,e0,qe,iccd)
      implicit none
c
c Reads calibration data for ccd qe
c
c O ne      integer    - the number of grid points
c O e0(ne)  real       - energy grid [keV]
c O qe(ne)  real       - quantum efficiency
c i iccd    integer    - ccd; it is ignored when reading the ASCII cal files
c
      character*(*) calfile
      integer ne
      real e0(*), qe(*)
      integer iccd

      integer unit,newunit,status
      integer i,k
      real ww(300)
      integer nw, icole,icola
      character*200 string
      character*30 word(100)
      integer nwords
      integer lnblnk
      character*200 calfilelocal
      logical read_ccd_qe_fits
      external read_ccd_qe_fits
      
      calfilelocal = calfile
      call putmessage ('CCD+filter QE: '//calfilelocal)
      
      if (.not. read_ccd_qe_fits (calfile,ne,e0,qe,iccd)) then
        
        unit=newunit()
        open(unit,file=calfile,status='old')

        read (unit,'(a)') string
c find enery and hrma area columns
        call splitwords (string,word,nwords)
        icole = 0
        icola = 0
        do i=1,nwords
          if (word(i).eq.'Energy(keV)') then
            icole=i
          else if (word(i).eq.'QE*Filt_trans') then
            icola=i
          endif
        enddo
        if (icole.eq.0) then
          call exiterror ('Column ''Energy(keV)'' is not found in QDP file '
     ~        //calfilelocal)
        endif
        if (icola.eq.0) then
          call exiterror ('Column ''QE*Filt_trans'' is not found in QDP file '
     ~        //calfilelocal)
        endif
        nw = max(icole,icola)
        
        status=0
        i = 0
        do while (status.eq.0)
          read (unit,*,iostat=status) (ww(k),k=1,nw)
          if (status.eq.0) then
            i = i + 1
            e0(i)=ww(icole)
            qe(i)=ww(icola)
            if (i.gt.1) then
              if (e0(i).lt.e0(i-1)) then
                write (0,*) 'warning: Energies are not sorted in'// 
     ~              calfilelocal(1:lnblnk(calfilelocal))
                write (0,*) e0(i),e0(i-1)
              endif
            endif
          endif
        enddo
        ne = i
        
        close(unit)
      endif
      
      call sort2 (ne,e0,qe)
      do i=2,ne
        if (e0(i).eq.e0(i-1)) then
          write (0,*) ' warning: duplicate energy ',e0(i)
          e0(i)=e0(i)+0.0005
        endif
      enddo
      
      
      return
      end

      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      logical function read_ccd_qe_fits (calfile,ne,e0,qe,iccd)
      implicit none
c
c Reads calibration data for ccd qe
c
c O ne      integer    - the number of grid points
c O e0(ne)  real       - energy grid [keV]
c O qe(ne)  real       - quantum efficiency
c i iccd    integer    - ccd
c
      character*(*) calfile
      integer ne
      real e0(*), qe(*)
      integer iccd
      
      integer unit
      integer status,blocksize,colnum,hdutype
      logical anyf
      character*80 extname,comment
      integer chipid

      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call ftopen (unit,calfile,0,blocksize,status)
      if (status.ne.0) then     ! not a fits file
        status=0
        call ftfiou (unit,status)
        read_ccd_qe_fits = .false.
        return
      endif
      
      chipid = -1
      do while (chipid.ne.iccd)
        call FTMRHD(unit,1,hdutype,status)
        if (status.ne.0) then   ! this means that we're at the end of the file
                                ! but have not found the QE table
          write (0,*) 'ERROR calibration data for ccd ',iccd
     ~        ,' was not found in ',calfile
          call exit(1)
        endif

        if (hdutype.eq.2) then  ! binary table
          call ftgkys (unit,'EXTNAME',extname,comment,status)
          if (status.ne.0) call exit_fitsio (calfile,status)
          if (extname.eq.'AXAF_QE') then
            call ftgkyj (unit,'CCD_ID',chipid,comment,status)
            if (status.ne.0) call exit_fitsio (calfile,status)
          endif
        endif
      enddo

c we are at the required CCD extension
      call ftgnrw (unit,ne,status)

      call FTGCNO (unit,.false.,'ENERGY',colnum,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,e0,anyf,status)

      call FTGCNO (unit,.false.,'QE',colnum,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,qe,anyf,status)

      if (status.ne.0) call exit_fitsio (calfile,status)
      
      call ftclos(unit,status)
      status = 0
      call ftfiou(unit,status)
      status = 0
      
      read_ccd_qe_fits = .true.
      return
      end
      
        

        










