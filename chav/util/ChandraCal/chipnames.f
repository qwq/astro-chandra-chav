      subroutine form_chip_list (string,nchips,ichip)
      implicit none
      character*(*) string
      integer nchips
      integer ichip(*)
      character*80 word(100),word2(100)
      integer nwords,nwords2
      character*80 chipname1,chipname2, chipname
      integer chipid,chipid1,chipid2
      integer i, status, k
      integer index
      character*80 stringlocal
      
      stringlocal = string
      nchips=0

      call splitwords_char (string,word,nwords,',')
      
      if (nwords.eq.0) then
        nchips = 0
        return
      endif

      do i=1,nwords
        call remleadblanks(word(i))
        call splitwords_char (word(i),word2,nwords2,':')
        if (nwords2.gt.2) then
          call exiterror ('Cannot parse chip names in '//stringlocal)
        endif
        if (nwords2.eq.1) then  ! single chip
          call chip_name_to_idname (word2(1),chipname)
          if (index(chipname,':').gt.0) then
            k = index(chipname,':')
            chipname(k:k) = ' '
            read (chipname,*,iostat=status) chipid1, chipid2
            if (status.ne.0) 
     ~          call exiterror ('Cannot parse chip name '//word2(1))
            do chipid=chipid1,chipid2
              nchips = nchips+1
              ichip(nchips)=chipid
            enddo
          else
            read (chipname,*,iostat=status) chipid
            if (status.ne.0) 
     ~          call exiterror ('Cannot parse chip name '//word2(1))
            nchips = nchips+1
            ichip(nchips)=chipid
          endif
        else
          call chip_name_to_idname (word2(1),chipname1)
          call chip_name_to_idname (word2(2),chipname2)
          if (index(chipname1,':').ne.0.or.index(chipname2,':').ne.0) then
            call exiterror 
     ~          ('You cannot use composit chip as a part of the range')
          endif
          read (chipname1,*,iostat=status) chipid1
          if (status.ne.0) call exiterror ('Cannot parse chip name '//word2(1))
          read (chipname2,*,iostat=status) chipid2
          if (status.ne.0) call exiterror ('Cannot parse chip name '//word2(2))
          do chipid = chipid1,chipid2
            nchips = nchips+1
            ichip(nchips)=chipid
          enddo
        endif
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine chip_name_to_idname (string,chipname)
      implicit none
      character*(*) string,chipname
      character*20 name

      name = string
      if (name(1:5).eq.'ACIS-'.or.name(1:5).eq.'acis-') then
        name = name(6:)
      endif
      
      chipname = name
      if (name.eq.'I0'.or.name.eq.'i0') then
        chipname = '0'
      else if (name.eq.'I1'.or.name.eq.'i1') then
        chipname = '1'
      else if (name.eq.'I2'.or.name.eq.'i2') then
        chipname = '2'
      else if (name.eq.'I3'.or.name.eq.'i3') then
        chipname = '3'
      else if (name.eq.'S0'.or.name.eq.'s0') then
        chipname = '4'
      else if (name.eq.'S1'.or.name.eq.'s1') then
        chipname = '5'
      else if (name.eq.'S2'.or.name.eq.'s2') then
        chipname = '6'
      else if (name.eq.'S3'.or.name.eq.'s3') then
        chipname = '7'
      else if (name.eq.'S4'.or.name.eq.'s4') then
        chipname = '8'
      else if (name.eq.'S5'.or.name.eq.'s5') then
        chipname = '9'
      else if (name.eq.'I'.or.name.eq.'i'
     ~      .or.name.eq.'acisi'.or.name.eq.'ACISI') then
        chipname = '0:3'
      endif

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine chip_id_to_normname (ich,chipname)
      implicit none
      integer ich
      character*(*) chipname
      character*2 name0(0:9)
      data name0 /'I0', 'I1', 'I2', 'I3', 'S0', 'S1', 'S2', 'S3', 'S4', 'S5'/
      
      if (ich.ge.0.and.ich.le.9) then
        chipname = name0(ich)
      else
        write (0,*)' ERROR: (chip_id_to_normname): wrong chipid: ',ich
        call exit(1)
      endif

      return
      end
