      subroutine detpix2pol_acis (detx,dety,theta,phi)
      implicit none 
c
c Detector coordinates (detx,dety) -> theta, phi
c returns theta in arcmin and phi in degrees
c
c Reads command line pars optaxis_detx, optaxis_dety, detpixscale
c
c
      real detx,dety,theta,phi

      real optaxis_detx, optaxis_dety, pixscale
c Default parameters are from ASC coordinates memo for AXAF-FP-1.1
      real optaxis_detx0, optaxis_dety0, pixscale0
      parameter (
     ~    optaxis_detx0=4096.5, ! see ASC coordinates memo p.46
     ~    optaxis_dety0=4096.5, ! see ASC coordinates memo p.46
     ~    pixscale0=0.4912      ! arcsec, see ASC coordinates memo, p.46
     ~    )

      real tx,ty
      parameter (
     ~    tx=1.0,              ! verified against dmcoords
     ~    ty=1.0               ! 
     ~    )

      real pi
      parameter (pi=3.1415926536)
      real x,y

      logical firstcall
      data firstcall /.true./
      save firstcall, optaxis_detx, optaxis_dety, pixscale

      if (firstcall) then
        call get_pv_default ('optaxis_detx',optaxis_detx,optaxis_detx0,'e')
        call get_pv_default ('optaxis_dety',optaxis_dety,optaxis_dety0,'e')
        call get_pv_default ('detpixscale',pixscale,pixscale0,'e')
        write (0,*) ' detpix2pol_acis: using optical axis center at ',
     ~      optaxis_detx, optaxis_dety
        write (0,*) '                  and detx,y scale of ',pixscale,
     ~      ' arcsec per pixel'
        firstcall = .false.
      endif
      
      x = tx*(detx-optaxis_detx)
      y = ty*(dety-optaxis_dety)
      
      theta = sqrt(x**2+y**2)*pixscale/60.0
      if (theta.lt.0.001) then
        phi = 90.0
      else
        phi = atan2(y,x)*180.0/pi
        if (phi.lt.0.0) phi = phi + 360.0
      endif
      
*      call det2mirror (detx,dety, wt,wphi)
*      print*,detx,dety,theta,phi,wt,wphi

      return
      end
        


c This is the relevant code from pixlib
*
*/***********************************************************************
* *
* * Converts 2-D Focal Plane Coordinates (FPC) of current FP system 
* * to 3-D Mirror Nodal direction (cosine) coordinates (unit).
* *
* ***********************************************************************/
*short pix_fpc_to_mnc_dir
*       (
*        VEC2_DBLE fpc,     
*        VEC3_DBLE mnc_dir /* 3-D direction vector along  MNC axies */
*        )
*{
*  double theta, azi, x_n;
*  VEC3_DBLE  mnc;
*
*  pix_fpc_to_mnc(fpc, mnc);
*  if( fabs(mnc[0]) < DBL_EPSILON ) {
*    pix_put_status(DIVIDE_BY_ZERO);
*    print_err_1d(DIVIDE_BY_ZERO, "%8.2f", mnc[0]);
*    return DIVIDE_BY_ZERO ;
*  }
*  x_n = mnc[0]/fabs(mnc[0]);
*
*  theta = mnc[1]/mnc[0];
*  azi   = mnc[2]/mnc[0];
*
*
*  mnc_dir[0] = x_n * cos(theta) * cos(azi);
*  mnc_dir[2] = x_n * cos(theta) * sin(azi);
*  mnc_dir[1] = x_n * sin(theta);
*
*  return PIX_GOOD;
* }
*
*      
*/***********************************************************************
* *
* * Converts 2-D Focal Plane pixel Coordinates (FPC) of current FP syytem
*   to 3-D Mirror Nodal coordinates (MNC). The output of MNC is normalized.
* *
* ***********************************************************************/
*short pix_fpc_to_mnc
*       (
*        VEC2_DBLE fpc,       
*        VEC3_DBLE mnc        
*        )
*{
*  double f     = fabs(*(s2t_consts->focal_length));
*  double fpx0  = s2t_consts->pix_ima_cntr[0];
*  double fpy0  = s2t_consts->pix_ima_cntr[1];
*  short  tx    = s2t_consts->flip_factor[0];
*  short  ty    = s2t_consts->flip_factor[1];
*  double delta = s2t_consts->arcsec_per_pix;
*  double theta, azi, x_n;
*  
*  /* bounds checking */
*  if(fpc[0] < 0.0 || fpc[0] > 2*(fpx0-0.5) || 
*     fpc[1] < 0.0 || fpc[1] > 2*(fpy0-0.5) ) {
*    pix_put_status(OUT_FPC_BOUNDS);
*    pix_prt_errmsg(); /* print out the newest error message in text */
*    print_err_2d("", "(%8.2f %8.2f)", fpc);
*    /* return OUT_FPC_BOUNDS; */
*    /* 11/18/98
*       only print out warnning message and continue calculation followed.
*       Any fpc values off the bounds are valid in extended chip coordinates
*       */ 
*   }
*
*  delta *= (1.0/3600.0) * (DEG_TO_RAD); 
*  theta  =  (double)tx * delta * (fpc[0] - fpx0);
*  azi    =  (double)ty * delta * (fpc[1] - fpy0);
*
*  /* s2t_consts->fpc2 retains the info of mnc[0] previously stored */
*  mnc[0] = f*(delta * s2t_consts->fpc2 - 1); 
*
*  x_n = fabs(mnc[0]);
*  mnc[1] = x_n * theta;
*  mnc[2] = x_n * azi;
*
*  return PIX_GOOD;
* }

c And from MIT's libemap:
*void emap_mnc_to_theta_phi (double *mnc, double *theta, double *phi)
*{
*   double x, y, z;
*
*   x = mnc[0];
*   y = mnc[1];
*   z = mnc[2];
*
*   if (x < 0)
*     {
*        x = -x;
*        y = -y;
*        z = -z;
*     }
*
*   
*   /* allow for roundoff errors */
*   if (x > 1.0) x = 1.0;
*   *theta = acos (x);
*
*   if (y == 0.0)
*     {
*        if (z >= 0) *phi = PI/2.0;
*        else *phi = -PI/2.0;
*
*        return;
*     }
*
*   
*   x = atan2 (z, y);
*   /* atan2 returns value in [-PI,PI] */
*   if (x < 0) x += 2*PI;
*
*   *phi = x;
*}
*
