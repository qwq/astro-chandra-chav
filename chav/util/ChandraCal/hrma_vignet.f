      real function hrma_vignetting (e,theta,phi)
c
c The ratio of off-axis and on-axis areas as a function of energy, theta, phi
c
c  e      [keV]     real    - energy
c  theta  [arcmin]  real    - offaxis angle
c  phi    [deg]     real    - rotation angle
c
      implicit none
      real e, theta, phi

      integer ne,ntheta,nphi
      integer nemax, nthetamax, nphimax
      parameter (nemax=500, nthetamax=40, nphimax=20)
      real e0(nemax), theta0(nthetamax), phi0(nphimax)
      real x0(nemax,nthetamax), thetahalf0(nemax)
      real vignetting(nthetamax*nphimax*nemax)
      logical firstcall
      data firstcall /.true./
      save e0, theta0, phi0, vignetting, firstcall, ne, ntheta, nphi,
     ~    x0, thetahalf0

*      integer ie(2),itheta(2),iphi(2)
*      real we(2),wtheta(2),wphi(2)
      integer ie,it
      integer ie1,ie2,it1,it2,ip1,ip2
      real we1,we2,wt1,wt2,wp1,wp2
      real eold, thetaold, phiold
      save
*     ~    ie,itheta,iphi,
*     ~    we,wtheta,wphi,
     ~    eold, thetaold, phiold,
     ~    ie1,ie2,it1,it2,ip1,ip2,
     ~    we1,we2,wt1,wt2,wp1,wp2

      real tol
      parameter (tol=1.0e-4)
      
      real x1,x2,x,thetahalf
      real vign_int_adddata

      logical recalcwt
      logical yespar

      if (firstcall) then
                                ! 1) Read cal data
        call read_hrma_vignetting (vignetting,e0,theta0,phi0,ntheta,nphi,ne)
        if (yespar('debug')) then
          print*,'theta=',theta0(1),theta0(2),theta0(3)
          call show_vignetting(vignetting,ntheta,nphi,ne,1)
          call show_vignetting(vignetting,ntheta,nphi,ne,ne)
        endif

        ! 2) Initialize 'old' variables and set the firstcall flag
        eold=-1
        thetaold=-1
        phiold=-1
        
        ! 3) Calculate abscissas for theta interpolation
        do ie=1,ne
          thetahalf = 16.0*(1-0.5*(e0(ie)/8.5)**4)
          thetahalf = max(thetahalf,3.5)
          thetahalf0(ie)=thetahalf
          do it = 1,ntheta
            x0(ie,it) = 1.0/(1.0+(theta0(it)/thetahalf)**2)
          enddo
        enddo

        firstcall = .false.
       
      endif

      recalcwt = .false.

c  A) Find energy bin
      if (e.ne.eold) then
          ! When the energy grid in cal data is settled, we can speed up
          ! by direct calculation of ie
        call vign_int_findbin (e,eold,e0,ne,ie1,ie2)
        we1 = (e0(ie2)-e)/(e0(ie2)-e0(ie1))
        we2 = 1.0-we1
*        call vign_int_weights (e,e0(ie1),e0(ie2),we1,we2)
        eold=e
      endif

c  B) Find theta bin
      if (theta.ne.thetaold) then
          ! When off-axis grid in cal data is settled, we can speed up
          ! by direct calculation of itheta

        call vign_int_findbin 
     ~      (theta,thetaold,theta0,ntheta,it1,it2)
c We recalculate theta weights for each energy
*        call vign_int_weights
*     ~      (theta,theta0(it1),theta0(it2),wt1,wt2)
        thetaold=theta
      endif

c  C) Find phi bin
      if (phi.ne.phiold) then
          ! When off-axis grid in cal data is settled, we can speed up
          ! by direct calculation of iphi

        call vign_int_findbin (phi,phiold,phi0,nphi,ip1,ip2)
        wp1 = (phi0(ip2)-phi)/(phi0(ip2)-phi0(ip1))
        wp2 = 1.0-wp1
        phiold=phi
      endif

c% The off-axis dependence of vignetting is roughly 
c% $v=1/(1+\theta^2/\theta_0^2)$ where $\theta_0$ is a function of energy.
c% Therefore, it is better to perform linear interpolation in $x=1/(1+\theta^2
c% /\theta_0^2)$ coordinates, because $v(x)$ is almost linear:
c%
c% \centerline{\includegraphics[width=0.4\linewidth]{vign.ps}\hfill%
c%             \includegraphics[width=0.4\linewidth]{vign1.ps}}
c%
c% (this plot is derived using raytracing simulations taken from \\
c% http://hea-www.harvard.edu/MST/simul/orbit/eff-area/offaxis/index.html)
c% 
c% Dependence of $\theta_0$ on energy can be described as $\theta_0\approx16'
c% \times(1-0.5(E/8.5)^4)$:
c% 
c% \centerline{\includegraphics[width=0.4\linewidth]{vighalf.ps}}
c%
c% So, we recalculate theta weights for interpolation over $x=1/(1+\theta^2
c% /\theta_0^2)$ grid. Note that we do not use an analytic approximation of 
c% vignetting, but simply interpolate in coordinates where the function is
c% closer to linear.
c%

      x1 = x0(ie1,it1)
      x2 = x0(ie1,it2)
      x  = 1.0/(1.0+(theta/thetahalf0(ie1))**2)

      wt1 = (x2-x)/(x2-x1)
      wt2 = 1.0-wt1

      hrma_vignetting = vign_int_adddata (
     ~    we1,we2,wt1,wt2,wp1,wp2,ie1,ie2,it1,it2,ip1,ip2,
     ~    ne,ntheta,nphi,vignetting)

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      real function vign_int_adddata
     ~    (we1,we2,wt1,wt2,wp1,wp2,ie1,ie2,it1,it2,ip1,ip2,ne,ntheta,nphi,v)
***     ~    (we,wtheta,wphi,ie,itheta,iphi,ne,ntheta,nphi,v)
      implicit none
c
c Needed to access vignetting as if it is a 3-D array
c
*      real we(2), wtheta(2), wphi(2)
*      integer ie(2), itheta(2), iphi(2)
      integer ne,ntheta,nphi
      real v (ntheta,nphi,ne)
      integer ie1,ie2,ip1,ip2,it1,it2
      real we1,we2,wp1,wp2,wt1,wt2

*      c = 0
*      do i=1,2
*        c1 = 0
*        ii = ie(i)
*        do j=1,2
*          jj = itheta(j)
*          c2 = 0
*          do k=1,2
*            c2 = c2 + wphi(k)*v(jj,iphi(k),ii)
*          enddo
*          c1 = c1 + c2*wtheta(j)
*        enddo
*        c = c + c1*we(i)
*      enddo

*      we1 = we(1)
*      we2 = we(2)
*      ie1 = ie(1)
*      ie2 = ie(2)
*
*      wp1 = wphi(1)
*      wp2 = wphi(2)
*      ip1 = iphi(1)
*      ip2 = iphi(2)
*      
*      wt1 = wtheta(1)
*      wt2 = wtheta(2)
*      it1 = itheta(1)
*      it2 = itheta(2)

      vign_int_adddata = 
     ~    we1*(wp1*(wt1*v(it1,ip1,ie1)+wt2*v(it2,ip1,ie1))
     ~        +wp2*(wt1*v(it1,ip2,ie1)+wt2*v(it2,ip2,ie1)))
     ~    +
     ~    we2*(wp1*(wt1*v(it1,ip1,ie2)+wt2*v(it2,ip1,ie2))
     ~        +wp2*(wt1*v(it1,ip2,ie2)+wt2*v(it2,ip2,ie2)))
      

      return
      end


*
**xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*
*      subroutine vign_int_weights (x,x1,x2,w1,w2)
*      implicit none
*      real x,x1,x2,w1,w2
*      real s
*
*      w1=(x2-x)/(x2-x1)
*      w2 = 1.0-w1
*
**      s  = w1+w2
**      w1 = w1 / s
**      w2 = w2 / s
*      
*      return
*      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine vign_int_findbin (x,xold,x0,nx,i1,i2)
      implicit none
c
c Find which two points in x0 to use for linear interpolation at x
c xold < 0 signals that no searches were performed with this array
c The values of i1 and i2 must be saved from the previous search
c
c Uses locate from NR
c
      real x,xold
      integer nx
      real x0(nx)
      integer i1,i2
      
c If xold < 0 no searches were perfored. Otherwise, check whether the point
c is in the old interval

      if ((xold.lt.0.0).or.(.not.(x.ge.x0(i1).and.x.le.x0(i2)))) then
        if (x.le.x0(1)) then
          i1=1
          i2=2
        else if (x.gt.x0(nx)) then
          i1=nx-1
          i2=nx
        else
          call locate (x0,nx,x,i1)
          i2=i1+1
        endif
      endif
      return
      end

      
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine show_vignetting (v,nt,nph,ne,ie)
      implicit none
      integer nt,nph,ne,ie
      real v(nt,nph,ne)

      print *,v(1,1,ie),v(1,nph/4,ie),v(1,nph/2,ie),v(1,nph*3/4,ie)
      print *,v(3,1,ie),v(3,nph/4,ie),v(3,nph/2,ie),v(3,nph*3/4,ie)
      return
      end
