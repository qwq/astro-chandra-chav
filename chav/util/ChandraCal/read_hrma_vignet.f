      subroutine read_hrma_vignetting 
     ~    (vignetting,e0,theta0,phi0,ntheta,nphi,ne)
c
c  Reads the grid of vignetting calibration data
c
c O  ntheta                      integer - the number of off-axis grid points
c O  nphi                        integer - ...           position angle ...
c O  ne                          integer - ...           energy angle ...
c O vignetting(ntheta,nphi,ne)   real    - vignetting data (theta,phi,e)
c O e0(ne)                       real    - energy grid points
c O theta0(ne)                   real    - offaxis grid points
c O phi0(ne)                     real    - position angle grid points
c
c If ntheta,ne,nphi do not match those in cal. data file, programs complains
c and interrupts itself
c
      implicit none

      integer ne,ntheta,nphi
      real e0(*), theta0(*), phi0(*)
      real vignetting(*)
      integer i,k
      integer unit, newunit
      character*200 calfile
      character*100 string
      logical defined
      character*80 word(100)
      integer nwords, nw
      real ee, tt, aa, pp, ttw
      integer icole, icolt, icola, icolp
      integer ie,it,ip
      integer status, statuse, statusa, statust, statusp
      integer qexpand

      logical read_hrma_vignetting_fits
      external read_hrma_vignetting_fits

      include 'calcomments.inc'

      call rp_q_expand_vars (qexpand)
      call rp_set_expand_vars (1)
      call get_cl_par ('hrma_vignetting',calfile)
      call rp_set_expand_vars (qexpand)
  
      NCALCOM = NCALCOM + 1
      CALCOMNAME(NCALCOM) = 'HRMAVIGN'
      CALCOMVALUE(NCALCOM) = calfile
      CALCOMCOM(NCALCOM) = 'HRMA vignetting'

      if (.not.defined(calfile)) then
        call exiterror ('(read_hrma_vignetting:) hrma_vignetting=?')
      endif
      
      call putmessage ('Vignetting: '//calfile)

      if ( read_hrma_vignetting_fits
     ~    (calfile,vignetting,e0,theta0,phi0,ntheta,nphi,ne) ) then
        return
      endif

      unit = newunit()
      open(unit,file=calfile,status='old')
      read (unit,'(a)') string
      icole = 0
      icolt = 0
      icolp = 0
      icola = 0
      call splitwords (string,word,nwords)
      do i=1,nwords
        if (word(i).eq.'energy') then
          icole=i
        else if (word(i).eq.'aeff') then
          icola=i
        else if (word(i).eq.'theta') then
          icolt=i
        else if (word(i).eq.'phi') then
          icolp=i
        endif
      enddo

      if (icole.eq.0) then
        call exiterror ('Column ''energy'' is not found in RDB file '
     ~      //calfile)
      endif
      if (icola.eq.0) then
        call exiterror ('Column ''aeff'' is not found in RDB file '
     ~      //calfile)
      endif
      if (icolt.eq.0) then
        call exiterror ('Column ''theta'' is not found in RDB file '
     ~      //calfile)
      endif
      if (icolp.eq.0) then
        call exiterror ('Column ''phi'' is not found in RDB file '
     ~      //calfile)
      endif


c
c  I assume below that EA is tabulated at energies     E = 0.1, 0.2 ....
c                                         off-axis theta = 0', 2', 5', ... 25'
c                                         azimuth    phi = 0, 30, ... 360
c



c Initialize theta and phi
      ntheta = 7                ! 0, 2, 5, 10, 15, 20, 25
      nphi = 13                 !0,30,60,90,120,150,180,210,240,270,300,330,360
      
      theta0(1)=0
      theta0(2)=2
      do k=3,ntheta
        theta0(k)=5+5*(k-3)
      enddo
      
      do k=1,nphi
        phi0(k)=30.0*(k-1)
      enddo

c
      read (unit,*)             ! skip format line
      status=0
      nw = max(icole,max(icola,max(icolt,icolp)))
      ne = 0
      do while (status.eq.0)
        read (unit,'(a)',iostat=status) string
        if (status.eq.0) then
          call splitwords (string,word,nwords)
          if (nwords.lt.nw) then
            status=1
          else
            read (word(icole),*,iostat=statuse) ee
            read (word(icola),*,iostat=statusa) aa
            read (word(icolp),*,iostat=statusp) pp
            read (word(icolt),*,iostat=statust) tt
            if (statuse.eq.0.and.statusa.eq.0.and.statusp.eq.0.and.statust.eq
     ~          .0.and. tt.lt. 25.001 ) then
              ie = nint(ee*10.0)
              if (abs(ie/10.0-ee).gt.0.001) then
                call exiterror
     ~              ('(read_hrma_vignetting:) expect E=0.1*i keV in'//calfile)
              endif
              ip = nint(pp/30.0)+1
              if (abs(pp-(ip-1)*30.0).gt.0.01) then
                call exiterror
     ~              ('(read_hrma_vignetting:) expect Phi=30*i deg in'//calfile
     ~              )
              endif
              
              if (tt.lt.4.99) then
                it = nint(tt/2.0)+1
                ttw = (it-1)*2.0
              else
                it = nint(tt/5.0)+2
                ttw = (it-2)*5.0
              endif

              if (abs(tt-ttw).gt.0.01) then
                call exiterror (
     ~              '(read_hrma_vignetting:) expect theta=0,2,5,10.. in'
     ~              //calfile)
              endif
              
              ne = max(ne,ie)

              call read_hrma_vignetting_puta 
     ~            (vignetting,ntheta,nphi,it,ip,ie,aa)
            endif
          endif
        endif
      enddo


      do i=1,ne
        e0(i)=0.1*i
      enddo

      close(unit)
      
      call  read_hrma_vignetting_normalize (vignetting,ntheta,nphi,ne)

      write (0,*) ('Reading mirror vignetting file done')
     

*c Write a test file
*      open(unit,file='govea')
*      write (unit,*) ne,ntheta,nphi,' --- ne, ntheta, nphi'
*      write (unit,*) 'E:'
*      write (unit,'(<ne>(1x,f5.1))') (e0(i),i=1,ne)
*      write (unit,*) 'theta:'
*      write (unit,'(<ntheta>(f3.0,1x))') (theta0(i),i=1,ntheta)
*      write (unit,*) 'phi:'
*      write (unit,'(<nphi>(f4.0,1x))') (phi0(i),i=1,nphi)
*      write (unit,*) 'ie itheta iphi correction:'
*      do ie=1,ne
*        do it=1,ntheta
*          do ip=1,nphi
*            call read_hrma_vignetting_access 
*     ~          (vignetting,ntheta,nphi,it,ip,ie,aa)
*            if (ip.eq.nphi.and.it.gt.1) aa = 0
*            write (unit,'(i3,1x,i2,1x,i2,1x,f9.7,2x,f4.1,1x,f4.0,1x,f4.0)') 
*     ~          ie, it, ip, aa,
*     ~          e0(ie), theta0(it), phi0(ip)
*          enddo
*        enddo
*      enddo
*      close(unit)


      
      return
      end



*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_hrma_vignetting_puta 
     ~    (vignetting,ntheta,nphi,it,ip,ie,aa)
c
c Use this function to access vignetting as a 3-d array with known dimensions
c
      implicit none
      integer ntheta,nphi
      integer it,ie,ip
      real aa
      real vignetting(ntheta,nphi,*)


      vignetting (it,ip,ie) = aa
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_hrma_vignetting_access
     ~    (vignetting,ntheta,nphi,it,ip,ie,aa)
c
c Use this function to access vignetting as a 3-d array with known dimensions
c
      implicit none
      integer ntheta,nphi
      integer it,ie,ip
      real aa
      real vignetting(ntheta,nphi,*)


      aa = vignetting (it,ip,ie)
      
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_hrma_vignetting_normalize (vignetting,ntheta,nphi,ne)
      implicit none
      integer ntheta,nphi,ne
      real vignetting(ntheta,nphi,ne)
      integer it,ie,ip
      
c Normilize by the onaxis area
      do it=2,ntheta
        do ip=1,nphi-1
          do ie=1,ne
            if (vignetting(1,1,ie).eq.0.0) then
              write (0,*)'(read_hrma_vignetting:) zero onaxis for ie=',ie
              call exit(1)
            endif
            vignetting(it,ip,ie) = vignetting(it,ip,ie) / vignetting(1,1,ie)
          enddo
        enddo
      enddo

c theta=0 vignetting is 1 for all phi's
      do ie=1,ne
        do ip=1,nphi
          vignetting(1,ip,ie)=1
        enddo
      enddo
      
c  phi=0 data is copied to phi=360
      do ie=1,ne
        do it=1,ntheta
          vignetting(it,nphi,ie) = vignetting(it,1,ie)
        enddo
      enddo

c  Check
      do it=1,ntheta
        do ip=1,nphi
          do ie=1,ne
            if (vignetting(it,ip,ie).lt.0.001) then
              write (0,*) 
     ~            '(read_hrma_vignetting:) vignetting=0 for it,ip,ie=',it,ip
     ~            ,ie, vignetting(it,ip,ie)
              call exit(1)
            endif
          enddo
        enddo
      enddo
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      logical function read_hrma_vignetting_fits
     ~    (calfile,vignetting,e0,theta0,phi0,ntheta,nphi,ne)
      implicit none
      character*(*) calfile
      integer ne,ntheta,nphi
      real e0(*), theta0(*), phi0(*)
      real vignetting(*)
      real e1(100000)

      integer unit
      integer status,blocksize,datacode,width,colnum,hdutype,i,k
      logical anyf
      character*30 CBD40001
      character*80 comment,extname

      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      
      call ftopen (unit,calfile,0,blocksize,status)
      if (status.ne.0) then     ! not a fits file
        status=0
        call ftfiou (unit,status)
        read_hrma_vignetting_fits = .false.
        return
      endif

      CBD40001 = 'gov'
*      do while (CBD40001.ne.'SHELL(1111)') ! this was good prior to caldb4
      do while (CBD40001.ne.'1111')
        call FTMRHD(unit,1,hdutype,status)
        if (status.ne.0) then   ! this means that we're at the end of the file
                                ! but have not found the all-shell vignetting
          write (0,*) 'ERROR vignetting for SHELL(1111) was not found in '
     ~        ,calfile
          call exit(1)
        endif
        if (hdutype.eq.2) then  ! binary table
          call ftgkys (unit,'EXTNAME',extname,comment,status)
          if (status.ne.0) call exit_fitsio (calfile,status)
          if (extname.eq.'AXAF_VIGNET') then
*            call ftgkys (unit,'CBD40001',CBD40001,comment,status)
            call ftgkys (unit,'SHELL',CBD40001,comment,status)
            if (status.ne.0) call exit_fitsio (calfile,status)
          endif
        endif
      enddo

      call FTGCNO (unit,.false.,'ENERG_LO',colnum,status)
      call FTGTCL (unit,colnum, datacode,ne,width,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,e0,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call FTGCNO (unit,.false.,'ENERG_HI',colnum,status)
      call FTGTCL (unit,colnum, datacode,ne,width,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,e1,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      do i=1,ne
        e0(i)=0.5*(e0(i)+e1(i))
      enddo

      call FTGCNO (unit,.false.,'THETA',colnum,status)
      call FTGTCL (unit,colnum, datacode,ntheta,width,status)
      call ftgcve (unit,colnum,1,1,ntheta,0.0,theta0,anyf,status)
      
      call FTGCNO (unit,.false.,'PHI',colnum,status)
      call FTGTCL (unit,colnum, datacode,nphi,width,status)
      call ftgcve (unit,colnum,1,1,nphi,0.0,phi0,anyf,status)
      
      call FTGCNO (unit,.false.,'VIGNET',colnum,status)
      call FTGTCL (unit,colnum, datacode,k,width,status)
      call ftgcve (unit,colnum,1,1,k,0.0,vignetting,anyf,status)

      if (status.ne.0) call exit_fitsio (calfile,status)
      
      if (phi0(nphi).ne.360.0) then
        nphi=nphi+1
        phi0(nphi)=360.0
      endif

      call read_hrma_vignetting_rearrange (vignetting,ne,ntheta,nphi)
      
      call ftclos (unit,status)
      call ftfiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      
      
      read_hrma_vignetting_fits = .true.
      return

      end

c---------------------------------------------------------------------------
c Vignetting in the data file is (E,THETA,PHI) array, while the code wants
c (theta,phi,e). So:

      subroutine read_hrma_vignetting_rearrange (vignetting,ne,ntheta,nphi)
      implicit none
      integer ne,ntheta,nphi
      real vignetting(*)
      integer nmax
      parameter (nmax=200*20*24)
      real buff(nmax)
      integer i
      
      if (ne*ntheta*nphi.gt.nmax) then
        call exiterror
     ~      ('Too big a vignetting array in read_hrma_vignetting_rearrange')
      endif
      
      do i=1,ne*ntheta*nphi
        buff(i)=vignetting(i)
      enddo

      call read_hrma_vignetting_rearrange_1 (buff,vignetting,ne,ntheta,nphi)
      return
      end

      subroutine read_hrma_vignetting_rearrange_1 
     ~    (buff,vignetting,ne,ntheta,nphi)
      implicit none
      integer ne,ntheta,nphi
      real buff(ne,ntheta,nphi)
      real vignetting(ntheta,nphi,ne)

      integer iphi,ie,itheta

      do iphi=1,nphi-1
        do itheta=1,ntheta
          do ie=1,ne
            vignetting(itheta,iphi,ie)=buff(ie,itheta,iphi)
          enddo
        enddo
      enddo

      do itheta=1,ntheta
        do ie=1,ne
          vignetting(itheta,nphi,ie)=buff(ie,itheta,1)
        enddo
      enddo

      return
      end
