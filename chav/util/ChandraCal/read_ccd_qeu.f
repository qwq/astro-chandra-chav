      subroutine read_ccd_qeu (calfile,
     ~    ne,nchipx,nchipy,
     ~    crpix,crval,cdelt,
     ~    ndatamax,
     ~    e0,qeu,
     ~    iccd)
      implicit none
c
c Reads calibration data for ccd qeu
c
c O ne       integer    - the number of enery grid points
c O nchipx   integer    - the number of chipx grid points
c O nchipy   integer    - the number of chipy grid points
c O crpix(3) real       
c O crval(3) real       arrays to specify the transformation from the image
c O cdelt(3) real       to the data values
c I ndatamax integer    - maximum size of the QEU map
c O e0(ne)   real       - energy grid for QE
c O qeu(ne*nchipx*nchipy)  real       - QEU map
c i iccd    integer    - ccd; it is ignored when reading the ASCII cal files
c
      character*(*) calfile
      integer ne, nchipx, nchipy, ndatamax
      real qeu(ndatamax),e0(*)
      real crpix(3), crval(3), cdelt(3)
      integer iccd

      integer unit,status
      character*200 calfilelocal

      integer chipid, ndat
      integer blocksize, hdutype, colnum, datacode, width
      character*80 extname, comment, keyname
      logical anyf
      integer naxis,naxes(10)
      
      
      calfilelocal = calfile
      call putmessage ('CCD QEU: '//calfilelocal)
      
      status = 0
      call ftgiou (unit,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call ftopen (unit,calfile,0,blocksize,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      
      chipid = -1
      do while (chipid.ne.iccd)
        call FTMRHD(unit,1,hdutype,status)
        if (status.ne.0) then   ! this means that we're at the end of the file
                                ! but have not found the QE table
          write (0,*) 'ERROR calibration data for ccd ',iccd
     ~        ,' was not found in ',calfile
          call exit(1)
        endif
        
        if (hdutype.eq.2) then  ! binary table
          call ftgkys (unit,'EXTNAME',extname,comment,status)
          if (status.ne.0) call exit_fitsio (calfile,status)
          if (extname.eq.'AXAF_QEU') then
            call ftgkyj (unit,'CCD_ID',chipid,comment,status)
            if (status.ne.0) call exit_fitsio (calfile,status)
          endif
        endif
      enddo
      
c we are at the required CCD extension
      call FTGCNO (unit,.false.,'ENERGY',colnum,status)
      call FTGTCL (unit,colnum, datacode,ne,width,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      call ftgcve (unit,colnum,1,1,ne,0.0,e0,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)


      call FTGCNO (unit,.false.,'QEU',colnum,status)
      call FTGTCL (unit,colnum, datacode,ndat,width,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      if (ndat.gt.ndatamax) then
        write (0,*) 'QEU array is too small to load data for CCD ',iccd
        call exit(1)
      endif
      call ftgcve (unit,colnum,1,1,ndat,0.0,qeu,anyf,status)
      if (status.ne.0) call exit_fitsio (calfile,status)

      call ftgtdm (unit,colnum,10,naxis,naxes,status)
      if (status.ne.0) call exit_fitsio (calfile,status)
      if (naxis.ne.3) then
        call exiterror ('Not a 3D image in the QEU column')
      endif
      if (naxes(1).ne.ne) then
        call exiterror ('first dim of the QEU image != ne')
      endif
      nchipx = naxes(2)
      nchipy = naxes(3)

      write (keyname,'(''2CRVL'',i2)') colnum
      call rmblanks(keyname)
      call ftgkye (unit,keyname,crval(2),comment,status)
      write (keyname,'(''2CRPX'',i2)') colnum
      call rmblanks(keyname)
      call ftgkye (unit,keyname,crpix(2),comment,status)
      write (keyname,'(''2CDLT'',i2)') colnum
      call rmblanks(keyname)
      call ftgkye (unit,keyname,cdelt(2),comment,status)

      write (keyname,'(''3CRVL'',i2)') colnum
      call rmblanks(keyname)
      call ftgkye (unit,keyname,crval(3),comment,status)
      write (keyname,'(''3CRPX'',i2)') colnum
      call rmblanks(keyname)
      call ftgkye (unit,keyname,crpix(3),comment,status)
      write (keyname,'(''3CDLT'',i2)') colnum
      call rmblanks(keyname)
      call ftgkye (unit,keyname,cdelt(3),comment,status)


      if (status.ne.0) call exit_fitsio (calfile,status)
      
      call ftclos(unit,status)
      status = 0
      call ftfiou(unit,status)
      status = 0
      
      return
      end
      
        

        










