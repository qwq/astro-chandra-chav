#ifndef ZHINT
#define ZHINT int
#endif

void ALLOCNAME (ZHINT *pointer, int *size)
{
  void *p;
  
  p = (void *)malloc(*size);
  if( p==NULL ){
    *pointer = 0;
//    return 0;
  }
  else{
    *pointer = (ZHINT)p;
//    return 1;
  }
}

void REALLOCNAME (ZHINT *oldpointer, ZHINT *pointer, int *size)
{
  void *p;
  
  p = (void *)realloc(oldpointer,*size);
  if( p==NULL ){
    *pointer = 0;
  }
  else {
    *pointer = (ZHINT)p;
  }
}
