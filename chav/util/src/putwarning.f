
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
c Print a warning to stderr
      subroutine putwarning (whowarns,message)
      character*(*) whowarns, message
      integer lnblnk
      character*200 whowrns, msg

      whowrns = whowarns
      msg     = message

      write (0,'(a)') 'Warning from '//whowrns(1:lnblnk(whowrns))//':  '//
     ~    msg(1:lnblnk(msg))
      return
      end
