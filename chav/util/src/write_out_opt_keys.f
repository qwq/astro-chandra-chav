      subroutine write_out_optional_keys (unit,file)
      implicit none
      integer unit
      character file*(*)

      include 'calcomments.inc'
      integer i,status
      real v
      

      if (NCALCOM.le.0) return



      status = 0

      call FTPLSW(unit,status)
      do i=1,NCALCOM
        if (CALCOMNAME(i).eq.'COMMENT') then
          call ftpcom (unit,CALCOMVALUE(i),status)
        elseif (CALCOMNAME(i).eq.'HISTORY') then
          call ftphis (unit,CALCOMVALUE(i),status)
        else
          if (CALCOMCOM(i)(1:2).eq.'%f') then
            CALCOMCOM(i) = CALCOMCOM(i)(3:)
            read (CALCOMVALUE(i),*) v
            call FTPKYF (unit,CALCOMNAME(i),v,8,CALCOMCOM(i)
     ~          ,status)
          elseif (CALCOMCOM(i)(1:2).eq.'%e') then
            CALCOMCOM(i) = CALCOMCOM(i)(3:)
            read (CALCOMVALUE(i),*) v
            call FTPKYE (unit,CALCOMNAME(i),v,6,CALCOMCOM(i)
     ~          ,status)
          else
            call ftpkls (unit,CALCOMNAME(i),CALCOMVALUE(i),CALCOMCOM(i)
     ~          ,status)
          endif
        endif
      enddo

      
      if (status.ne.0) call exit_fitsio (file,status)
      return
      end

        
