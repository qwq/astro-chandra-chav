      subroutine fitsio_print_error (status)
      implicit none
      integer status
      character*50 errtext
      integer lnblnk
      
      call ftgerr (status,errtext)
      write(0,*) 'FITSIO:',status,'  (',errtext(1:lnblnk(errtext)),')'
      
      return
      end
