      subroutine min_(x,n,xmin)
      implicit none
      integer n
      real x(n),xmin
      
      integer i
      xmin=1.0e35
      
      do i=1,n
        if(x(i).lt.xmin)xmin=x(i)
      enddo
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine max_(x,n,xmax)
      implicit none
      integer n
      real x(n),xmax
      
      integer i
      xmax=-1.0e35
      
      do i=1,n
        if(x(i).gt.xmax)xmax=x(i)
      enddo
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine min_max(x,n,xmin,xmax)
      implicit none
      integer n
      real x(n),xmax,xmin
      
      integer i
      xmax=-1.0e35
      xmin=1.0e35
      
      do i=1,n
        if(x(i).gt.xmax)xmax=x(i)
        if(x(i).lt.xmin)xmin=x(i)
      enddo
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine min_8(x,n,xmin)
      implicit none
      integer n
      real*8 x(n),xmin
      
      integer i
      xmin=1.0d40
      
      do i=1,n
        if(x(i).lt.xmin)xmin=x(i)
      enddo
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine max_8(x,n,xmax)
      implicit none
      integer n
      real*8 x(n),xmax
      
      integer i
      xmax=-1.0d40
      
      do i=1,n
        if(x(i).gt.xmax)xmax=x(i)
      enddo
      
      return
      end
      
cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
      subroutine min_max8(x,n,xmin,xmax)
      implicit none
      integer n
      real*8 x(n),xmax,xmin
      
      integer i
      xmax=-1.0d40
      xmin=1.0d40
      
      do i=1,n
        if(x(i).gt.xmax)xmax=x(i)
        if(x(i).lt.xmin)xmin=x(i)
      enddo
      
      return
      end
      
      
