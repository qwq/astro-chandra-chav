c
c Provided for compatibility with old programs. Call get_pv_default
c from the ReadPar library
c
c

      subroutine get_parameter_value_default (parname,parvalue,default,type)
      implicit none
      character parname*(*)
      integer parvalue,default
      character type*(*)
      
      call get_pv_default (parname,parvalue,default,type)
      
      return
      end
