       
      
      subroutine perror_fitsio (message,status)
      implicit none
      character*(*) message
      integer lnblnk
      integer status
      character*80 errtext
      
      if (status.ne.0) then
        call FTGERR (status,errtext)
        do while (errtext.ne.'')
          write(0,'(a,a,a)')message(1:lnblnk(message)),': '
     ~        ,errtext(1:lnblnk(errtext))
          call FTGMSG (errtext)
        enddo
      endif
      return
      end
