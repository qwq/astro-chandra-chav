      subroutine lin_interpolate (x,y,n,x0,y0)
      implicit none
      integer n
      real x(n),y(n)
      real x0,y0
      
      
      integer ibin
      save ibin
      
      if(x0.le.x(1))then
        ibin=1
      else if (x0.ge.x(n)) then
        ibin=n-1
      else
        if (ibin.le.1.or.ibin.gt.n-1) then
          ibin=n/2
        endif
        call hunt (x,n-1,x0,ibin)
        if (ibin.lt.1.or.ibin.gt.n-1) then
          pause 'unexpected error in lin_interpolate'
        endif
      endif
      
      if (x(ibin+1).ne.x(ibin)) then
        y0=y(ibin)+(y(ibin+1)-y(ibin))*(x0-x(ibin))/(x(ibin+1)-x(ibin))
      else
        y0=y(ibin)
      endif

      return
      end
      
      subroutine lin_interpolate8 (x,y,n,x0,y0)
      implicit none
      integer n
      double precision x(n),y(n)
      double precision x0,y0
      
      
      integer ibin
      
      if(x0.le.x(1))then
        ibin=1
      else if (x0.ge.x(n)) then
        ibin=n-1
      else
        if (ibin.le.1.or.ibin.gt.n-1) then
          ibin=n/2
        endif
        call hunt8 (x,n-1,x0,ibin)
        if (ibin.lt.1.or.ibin.gt.n-1) then
          pause 'unexpected error in lin_interpolate'
        endif
      endif
      
      
      if (x(ibin+1).ne.x(ibin)) then
        y0=y(ibin)+(y(ibin+1)-y(ibin))*(x0-x(ibin))/(x(ibin+1)-x(ibin))
      else
        y0=y(ibin)
      endif
      
      return
      end
      

