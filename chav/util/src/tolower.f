
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine tolower (c)
c Lower case for ASCII chars
      character*1 c
      integer ic
      
      ic=ichar(c)
      if (ic.gt.64.and.ic.lt.90) then
        ic=ic+32
        c=char(ic)
      endif
      return
      end
