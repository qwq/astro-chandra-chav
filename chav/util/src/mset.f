      subroutine mset (a,n,value,type)
      implicit none
      integer a(*)
      integer n
      integer value
      character*1 type

      
      if (type.eq.'e'.or.type.eq.'E') then
        call mset_e(a,n,value)
      else if (type.eq.'i'.or.type.eq.'I') then
        call mset_i(a,n,value)
      else if (type.eq.'j'.or.type.eq.'J')then
        call mset_j(a,n,value)
      else if (type.eq.'d'.or.type.eq.'D') then
        call mset_d(a,n,value)
      else if (type.eq.'l'.or.type.eq.'L') then
        call mset_l(a,n,value)
      else
        write(0,*)'Wrong data type in mset: ',type
      endif
      
      return
      end
      
*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      subroutine mset_e(a,n,value)
      implicit none
      real a(*)
      integer n
      real value
      integer i
      
      do i=1,n
        a(i)=value
      enddo
      
      return
      end
      
      subroutine mset_c(a,n,value)
      implicit none
      complex a(*)
      integer n
      complex value
      integer i

      do i=1,n
        a(i)=value
      enddo

      return
      end
      
      subroutine mset_i(a,n,value)
      implicit none
      integer*2 a(*)
      integer n
      integer*2 value
      integer i
      
      do i=1,n
        a(i)=value
      enddo
      
      return
      end
      
      
      subroutine mset_j(a,n,value)
      implicit none
      integer a(*)
      integer n
      integer value
      integer i
      
      do i=1,n
        a(i)=value
      enddo
      
      return
      end
      
      

      subroutine mset_d(a,n,value)
      implicit none
      real*8 a(*)
      integer n
      real*8 value
      integer i
      
      do i=1,n
        a(i)=value
      enddo
      
      return
      end
      
      
      subroutine mset_l(a,n,value)
      implicit none
      logical a(*)
      integer n
      logical value
      integer i
      
      do i=1,n
        a(i)=value
      enddo
      
      return
      end
      
      
