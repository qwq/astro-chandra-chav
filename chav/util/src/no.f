      logical function no (argument)
      character*(*) argument
      
      no=(argument.eq.'n').or.(argument.eq.'no').or.(argument.eq.'N').or.
     ~    (argument.eq.'NO').or.(argument.eq.'No')
      return
      end
      
*
*      logical function nopar (parname)
*      character*(*) parname
*      character*200 parvalue
*      logical no
*      
*      call get_cl_par (parname,parvalue)
*      nopar = no(parvalue)
*      return
*      end
*      
