      
      
      
      subroutine fitsio_write_error (status,file)
      implicit none
      integer status,file
      character*50 errtext
      integer lnblnk
      
      call ftgerr (status,errtext)
      write(file,*)'FITSIO:',status,'  (',errtext(1:lnblnk(errtext)),')'
      
      return
      end
