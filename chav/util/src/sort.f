      subroutine sort(n,ra)
c
c
c  Quick Sort of ra(n) into array of non-descending order 
c
c
c
c
c
c
      implicit none
      integer n
      real ra

      dimension ra(n)

      integer l,ir,i,j
      real rra


      l=n/2+1
      ir=n
 10   continue
      if(l.gt.1)then
        l=l-1
        rra=ra(l)
      else
        rra=ra(ir)
        ra(ir)=ra(1)
        ir=ir-1
        if(ir.eq.1)then
          ra(1)=rra
          return
        endif
      endif
      i=l
      j=l+l
 20   if(j.le.ir)then
        if(j.lt.ir)then
          if(ra(j).lt.ra(j+1))j=j+1
        endif
        if(rra.lt.ra(j))then
          ra(i)=ra(j)
          i=j
          j=j+j
        else
          j=ir+1
        endif
        goto 20
      endif
      ra(i)=rra
      goto 10
      END
      
      

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine sort2(n,ra,rb)
c
c
c
c QuiickSort of ra(n), same rearrangements in rb(n)
c
c
c
c
c
      implicit none
      integer n
      real ra,rb
      integer l,ir,i,j
      dimension ra(n),rb(n)
      real rra,rrb

      l=n/2+1
      ir=n
 10   continue
        if(l.gt.1)then
          l=l-1
          rra=ra(l)
          rrb=rb(l)
        else
          rra=ra(ir)
          rrb=rb(ir)
          ra(ir)=ra(1)
          rb(ir)=rb(1)
          ir=ir-1
          if(ir.eq.1)then
            ra(1)=rra
            rb(1)=rrb
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(ra(j).lt.ra(j+1))j=j+1
          endif
          if(rra.lt.ra(j))then
            ra(i)=ra(j)
            rb(i)=rb(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        ra(I)=rra
        rb(I)=rrb
      goto 10
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine sort3__ (n,ra,rb,rc)
c
c
c
c QuiickSort of ra(n), same rearrangements in rb(n)
c
c
c
c
c
      implicit none
      integer n
      real ra,rb, rc
      integer l,ir,i,j
      dimension ra(n),rb(n), rc(n)
      real rra,rrb,rrc

      l=n/2+1
      ir=n
 10   continue
        if(l.gt.1)then
          l=l-1
          rra=ra(l)
          rrb=rb(l)
          rrc=rc(l)
        else
          rra=ra(ir)
          rrb=rb(ir)
          rrc=rc(ir)
          ra(ir)=ra(1)
          rb(ir)=rb(1)
          rc(ir)=rc(1)
          ir=ir-1
          if(ir.eq.1)then
            ra(1)=rra
            rb(1)=rrb
            rc(1)=rrc
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(ra(j).lt.ra(j+1))j=j+1
          endif
          if(rra.lt.ra(j))then
            ra(i)=ra(j)
            rb(i)=rb(j)
            rc(i)=rc(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        ra(I)=rra
        rb(I)=rrb
        rc(I)=rrc
      goto 10
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine locate(xx,n,x,j)
c      
c Given an array xx(n) and given value of x returns j such as x 
c between xx(j) and xx(j+1). xx(n) must be monotonic, either increasing 
c or decreasing. j=0 or j=n return to indicate that x out of range. 
c
c--------
      implicit none
      integer n,j
      real xx(n),x
      integer jl, ju, jm
      
      jl=0
      ju=n+1
      do while (ju-jl.gt.1)
        jm=(ju+jl)/2
        if((xx(n).gt.xx(1)).eqv.(x.gt.xx(jm)))then
          jl=jm
        else
          ju=jm
        endif
      enddo
      j=jl
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine indexx(n,arrin,indx)
c
c
c   indexes an array arrin(n), i.e. outputs an array indx(n)
c
c  such that arr(indx(j)) are in ascending order for j=1,..N
c
c  N and ArriN are bot changed on exit
c
c
c
      implicit none
      integer n,i,j,l,ir,indxt,indx
      real arrin
      dimension arrin(n),indx(n)
      real q

      do j=1,n
        indx(j)=j
      enddo

      l=n/2+1
      ir=n
10    continue
        if(l.gt.1)then
          l=l-1
          indxt=indx(l)
          q=arrin(indxt)
        else
          indxt=indx(ir)
          q=arrin(indxt)
          indx(ir)=indx(1)
          ir=ir-1
          if(ir.eq.1)then
            indx(1)=indxt
            return
          endif
        endif
        i=l
        j=l+l
20      if(j.le.ir)then
          if(j.lt.ir)then
            if(arrin(indx(j)).lt.arrin(indx(j+1)))j=j+1
          endif
          if(q.lt.arrin(indx(j)))then
            indx(i)=indx(j)
            i=j
            j=j+j
          else
            j=ir+1
          endif
        goto 20
        endif
        indx(i)=indxt
      goto 10
      end
