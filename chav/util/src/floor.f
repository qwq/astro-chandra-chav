      real function floor(x)
      real x

      if (x.lt.0.0) then
        floor = int(x)-1
      else
        floor = int(x)
      endif

      return
      end
      
