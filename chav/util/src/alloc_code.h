void ALLOCNAME ( 
	       integer *pointer,
	       integer *size)
{
  ALLOCARRAYTYPE *p;
  
  p = (ALLOCARRAYTYPE *) malloc (*size);
  if (p==NULL) {
    *pointer = 0;
  }
  else {
    *pointer = (int) p;
  }
    
}


void REALLOCNAME (
		 integer *oldpointer,
		 integer *pointer,
		 integer *size)
{
  ALLOCARRAYTYPE *p;
  
  p = (ALLOCARRAYTYPE *) realloc (oldpointer,*size);
  if (p==NULL) {
    *pointer = 0;
  }
  else {
    *pointer = (int) p;
  }
    
}



