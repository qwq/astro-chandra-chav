#include <zhtools.h>

      program makespec
      implicit none
      
      character*200 regwcsimage, regfile, arg
      integer unitregwcs, unitreg
      parameter ( 
     ~    unitregwcs = 11,
     ~    unitreg    = 12
     ~    )
      integer status


      integer nxregwcs, nyregwcs,nxreg, nyreg
      double precision rcrval1,rcrval2,rcrpix1,rcrpix2,rcdelt1,rcdelt2,rcrota2
      character*80 rtype

      ZHDECL(integer,preg)

      logical defined
      integer lnblnk
      
c A) Open regwcs file
      call get_cl_par ('regwcs',regwcsimage)
      if (.not.defined (regwcsimage)) then
        write (0,*) 'regwcs=?'
        call exit(1)
      endif

      status = 0
      call ftnopn (unitregwcs,regwcsimage,0,status)
      if (status.eq.0) then     ! this is fits image
        call FTGICS (unitregwcs,rcrval1,rcrval2,rcrpix1,rcrpix2,rcdelt1
     ~      ,rcdelt2,rcrota2,rtype,status)
        if (status.eq.506) status = 0
        if (status.ne.0) call exit_fitsio (regwcsimage,status)
        call get_image_size (regwcsimage,unitregwcs,nxregwcs,nyregwcs)
        call ftclos(unitregwcs,status)
        if (status.ne.0) call exit_fitsio (regwcsimage,status)
        
      else                      ! this is par file
        call read_par_wcs_keys (regwcsimage,
     ~      rcrval1,rcrval2,rcrpix1,rcrpix2,rcdelt1,rcdelt2,rcrota2,rtype)
        nxregwcs = -1
        nyregwcs = -1
      endif

c B) Read/create region file
      call get_cl_par ('reg',regfile)
      if (.not.defined(regfile)) then
        write (0,*) 'reg=?'
        call exit(1)
      endif
      
      status = 0
      call ftnopn (unitreg,regfile,0,status)
      if (status.eq.0) then     ! this is fits image
        call get_image_size (regfile,unitreg,nxreg,nyreg)
        if ((nxreg.ne.nxregwcs.or.nyreg.ne.nyregwcs) .and.
     ~      (nxregwcs.gt.0.and.nyregwcs.gt.0)) then
          write (0,*) 'Warning: "reg" and "regwcs" image size is different: ',
     ~      nxreg,nyreg, nxregwcs,nyregwcs
        endif
        ZHMALLOC(preg,nxreg*nyreg,'i')

        call read_fits_image_unit (unitreg,regfile,ZHVAL(preg),nxreg,nyreg,'i')
        call ftclos (unitreg,status)
        if (status.ne.0) call exit_fitsio (regfile,status)
      
      else                      ! This is either region file or a single region
c        by defult the region image size will be taken from the regwcs; but
c        if it was a parameter file, we need to know the size
          
        call get_pv_default ('nxreg',nxreg,nxregwcs,'j')
        call get_pv_default ('nyreg',nyreg,nyregwcs,'j')
        if (nxreg.le.0) then
          write (0,*) 'nxreg=? (or use FITS regwcs)'
          call exit(1)
        endif
        if (nyreg.le.0) then
          write (0,*) 'nyreg=? (or use FITS regwcs)'
          call exit(1)
        endif
        ZHMALLOC(preg,nxreg*nyreg,'i')
        arg = regfile
        call make_region_image (ZHVAL(preg),nxreg,nyreg,regfile,lnblnk(regfile)
     ~      ,0)
      endif
      
c ) Region is made, call the rest of the program
      call makespec_1 (ZHVAL(preg),nxreg,nyreg,rcrval1,rcrval2,rcrpix1,rcrpix2
     ~    ,rcdelt1,rcdelt2,rcrota2,rtype)

      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine makespec_1 (reg,nxreg,nyreg,rcrval1,rcrval2,rcrpix1,rcrpix2
     ~    ,rcdelt1,rcdelt2,rcrota2,rtype)
      implicit none
      integer nxreg,nyreg
      integer*2 reg(nxreg,nyreg)
      double precision rcrval1,rcrval2,rcrpix1,rcrpix2,rcdelt1,rcdelt2,rcrota2
      character*(*) rtype
      integer unitevt

c Spectral data:
      character*30 picolname
      integer picol, echanmin, echanmax, specbin, nchan, echanmaxdata,
     ~    echanmindata
      ZHDECL(real,pspec)
      ZHDECL(integer,perror)
C     We are using a realloc function (needed by f90):
      ZHREALLOC_DEFS

c Events file:
      character evtfile*200, eventsname*30, xdetcolname*30, ydetcolname*30,
     ~    xcolname*30, ycolname*30, ccdidcolname*30, energycolname*30,
     ~    gtifile*200, specweightcolname*30
      integer bindetmap
      integer xdetcol, ydetcol, xcol, ycol, ccdidcol, energycol, specweightcol
      integer detxmin,detxmax,detymin,detymax, ndetx, ndety, ccd_id_min,
     ~    ccd_id_max

      ZHDECL(integer,pdetx)
      ZHDECL(integer,pdety)
      ZHDECL(integer,pphotreg)
      ZHDECL(integer,pdetimg)

      integer ndetphot, ndetphotmax
      logical useccd
      double precision dcrval1,dcrval2,dcrpix1,dcrpix2,dcdelt1,dcdelt2,dcrota2
      character*30 dtype

      integer nreadmax
      parameter (nreadmax=16384)
      integer ccdid(nreadmax), detx(nreadmax), dety(nreadmax), x(nreadmax),
     ~    y(nreadmax), pi(nreadmax)
      real energy(nreadmax), specweight(nreadmax)
      real edetmapmin, edetmapmax
      logical qspecweight
      

      integer ngti, ngtimax
      parameter (ngtimax=10000)
      double precision tstart(ngtimax), tstop(ngtimax)
      integer tstopcol, tstartcol, unitgti
      
      

c Output
      character*80 outkey, outchipname,outname
      integer unitpha
      integer bitpix,naxes(10),naxis,pcount,gcount
      logical extend, simple

      integer nchanmax
      parameter (nchanmax=16384)
      integer channel(nchanmax),qualty(nchanmax),grping(nchanmax)
      real counts(nchanmax), serr(nchanmax), syserr(nchanmax)

      character*150 respfil, backfil, ancrfil
      logical qqual,qsys,qgroup,qerror
      integer specdtype
      character*80 hist(30), comm(30)
      real texpos, areascal, backscal
      

c Misc
      logical defined
      integer type, status, nread, nreadthistime, nrows
      integer i,j,k,irow, lreg,ir, jr, idx,idy, ichan
      integer ireg(64000), kreg(64000), nreg
      character*80 arg, chipname
      integer xypix,worldpos
      external xypix,worldpos
      double precision xpix,ypix,ra,dec
      logical useit, anyf
      integer imin,imax,jmin,jmax
      character*30 phaaffix
      integer lnblnk
      real w


      call get_cl_par ('o',outkey)
      if (.not.defined(outkey)) then
        write (0,*) 'o=?'
        call exit(1)
      endif

      call get_cl_par ('phaaffix',phaaffix)
      if (.not.defined(phaaffix)) then
        phaaffix = 'pha'
      endif

c Find and open evtfile
      call get_cl_par ('evtfile',evtfile)
      if (.not.defined (evtfile)) then
        write (0,'(''evtfile=?'')')
        call exit(1)
      endif



      status = 0
      call ftgiou (unitevt,status)
      if (status.ne.0) call exit_fitsio ('I/O unit',status)
      
      call ftnopn (unitevt,evtfile,0,status)
c   if we are at the primary header, we will have to move to the events
c   extension
      call FTGHDT (unitevt,type,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      if (type.eq.0) then       ! this is a primary header
        call get_cl_par ('eventsname',eventsname)
        if (.not.defined(eventsname)) eventsname = 'EVENTS'
        call FTMNHD (unitevt, -1, eventsname,0,status)
        if (status.ne.0) call exit_fitsio (eventsname,status)
      endif

c  Find the number of regions in REG
c
c ireg:  1 . . . . 2    3. . . . . . 
c                 (14) (15)
c
c kreg:  1,14,15,....
c
c
      do i=1,64000
        ireg(i)=0
        kreg(i)=0
      enddo
      do i=1,nxreg
        do j=1,nyreg
          k = reg(i,j)
          if (k.gt.0) then
            ireg(k)=1
          endif
        enddo
      enddo
      k = 0
      do i=1,64000
        if (ireg(i).gt.0) then
          k = k + 1
          ireg (i) = k
          kreg (k) = i
        endif
      enddo
      nreg = k
      write (0,*) ' ---',nreg,' different regions'
      
      if (k.gt.2100) then
        write (0,*) 'I allow maximum 2100 regions'
        call exit(1)
      endif

c  Find the energy column and its info
      call get_cl_par ('ecol',picolname)
      if (.not.defined(picolname)) picolname = 'PI'
      write (0,*) ' --- I will extract spectra in ',picolname(1:lnblnk(picolname))
     ~    ,' channels'
      
      call ftgcno (unitevt,.false.,picolname,picol,status)
      if (status.ne.0) call exit_fitsio (picolname,status)

      call get_col_range (unitevt,picol,echanmin,echanmax,0,1024)
      echanmaxdata = echanmax
      echanmindata = echanmin

      call get_pv_default ('specbin',specbin,1,'j')

      call get_pv_default ('maxspecchans',k,100000,'j')
      k = k * specbin + echanmin
      echanmax = min (echanmax,k)
      nchan = (echanmax-echanmin+1)/specbin

      write (0,*) ' --- Energy channel range is ',echanmin,' -',echanmax
     ~    ,'  binned by',specbin

c Allocate memory for spectra
      ZHMALLOC(pspec,nreg*nchan,'e')

c Find out which columns to use for detmap, and how to bin this map
      call get_cl_par ('xdetcol',xdetcolname)
      call get_cl_par ('ydetcol',ydetcolname)
      if (.not.defined(xdetcolname)) xdetcolname = 'DETX'
      if (.not.defined(ydetcolname)) ydetcolname = 'DETY'
      
      call get_pv_default ('bindetmap',bindetmap,8,'j')
      
      write (0,*) ' --- detector map will be made from columns '
     ~    ,xdetcolname(1:lnblnk(xdetcolname)),','
     ~    ,ydetcolname(1:lnblnk(ydetcolname)),'; binning factor',bindetmap
      
      call ftgcno (unitevt,.false.,xdetcolname,xdetcol,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      call ftgcno (unitevt,.false.,ydetcolname,ydetcol,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      
      call get_col_range (unitevt,xdetcol,detxmin,detxmax,1,8192)
      call get_col_range (unitevt,ydetcol,detymin,detymax,1,8192)
      ndetx = (detxmax-detxmin+1)/bindetmap
      ndety = (detymax-detymin+1)/bindetmap
      
      ndetphotmax = 16384*8
      ZHMALLOC(pdetx,ndetphotmax,'i')
      ZHMALLOC(pdety,ndetphotmax,'i')
      ZHMALLOC(pphotreg,ndetphotmax,'i')

c What is specweight?
      call get_cl_par ('specweight',specweightcolname)
      if (defined(specweightcolname)) then
        qspecweight = .true.
        write (0,*) ' --- ',
     ~      specweightcolname(1:lnblnk(specweightcolname)),
     ~      ' column will be used for spectral weighting'
        call ftgcno (unitevt,.false.,specweightcolname,specweightcol,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
      else
        qspecweight = .false.
      endif

      if (qspecweight) then
         ZHMALLOC(perror,nreg*nchan,'e')
      endif


c Find out which columns to use for region filter
      call get_cl_par ('xcol',xcolname)
      call get_cl_par ('ycol',ycolname)
      if (.not.defined(xcolname)) xcolname = 'X'
      if (.not.defined(ycolname)) ycolname = 'Y'
      write (0,*) ' --- '
     ~    ,xcolname(1:lnblnk(xcolname)),','
     ~    ,ycolname(1:lnblnk(ycolname))
     ~    ,' columns will be used for image filtering'
      
      call ftgcno (unitevt,.false.,xcolname,xcol,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      call ftgcno (unitevt,.false.,ycolname,ycol,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)

      call FTGTCS(unitevt,xcol,ycol, 
     ~    dcrval1,dcrval2,dcrpix1,dcrpix2,dcdelt1,dcdelt2,dcrota2,dtype
     ~    ,status)
      if (status.eq.506) status = 0 
      if (status.ne.0) call exit_fitsio (evtfile,status)


c Should we use CCD_ID?
      call get_cl_par ('chips',arg)
      if (.not.defined(arg)) call get_cl_par ('chip',arg)
      if (defined(arg)) then
        outchipname = arg
        call chip_name_to_idname (arg,chipname)
        k = index(chipname,':')
        if (k.ne.0) then
          chipname (k:k) = ' '
          read (chipname,*) ccd_id_min, ccd_id_max
        else
          read (chipname,*) ccd_id_min
          ccd_id_max = ccd_id_min
        endif
        useccd = .true.
      else
        useccd = .false.
      endif

      if ( useccd ) then
        write (0,*) ' --- spectrum will be extracted for CCDs ',ccd_id_min,' -'
     ~      ,ccd_id_max
      endif
      
      if (useccd) then
        call get_cl_par ('ccdidcol',ccdidcolname)
        if (.not.defined(ccdidcolname)) ccdidcolname = 'CCD_ID'
        write (0,*) ' --- '
     ~      ,ccdidcolname(1:lnblnk(ccdidcolname))
     ~      ,' column will be used for CCD_ID filtering'
        
        call ftgcno (unitevt,.false.,ccdidcolname,ccdidcol,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
      endif


c 
      call get_pv_default ('edetmapmin',edetmapmin,500.0,'e')
      call get_pv_default ('edetmapmax',edetmapmax,2000.0,'e')
      call get_cl_par ('energycol',energycolname)
      if (.not.defined(energycolname)) energycolname = 'ENERGY'
      write (0,*) ' --- '
     ~    ,energycolname(1:lnblnk(energycolname))
     ~    ,' column will be used for energy filtering in detmap'
        
      call ftgcno (unitevt,.false.,energycolname,energycol,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)


c Set spectra and detimage to zero
      call set_to_zero (ZHVAL(pspec),nchan,nreg)
      if (qspecweight) call set_to_zero (ZHVAL(perror),nchan,nreg)
      
c Get the number of photons
      call ftgnrw (unitevt,nrows,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      call FTGRSZ(unitevt,nread,status)
      if (status.ne.0) call exit_fitsio (evtfile,status)
      write (0,*) ' --- optimum number of photons to read at one time is '
     ~    ,nread
      nread = min (nread,nreadmax)

      ndetphot=0
      do irow = 1,nrows,nread
        nreadthistime = min (nrows-irow+1,nread)
        if (useccd) then
          call ftgcvj(unitevt,ccdidcol,irow,1,nreadthistime,0,ccdid,anyf
     ~        ,status)
        endif
        call ftgcvj(unitevt,xdetcol,irow,1,nreadthistime,0,detx,anyf,status)
        call ftgcvj(unitevt,ydetcol,irow,1,nreadthistime,0,dety,anyf,status)
        call ftgcvj(unitevt,xcol,irow,1,nreadthistime,0,x,anyf,status)
        call ftgcvj(unitevt,ycol,irow,1,nreadthistime,0,y,anyf,status)
        call ftgcvj(unitevt,picol,irow,1,nreadthistime,0,pi,anyf,status)
        call ftgcve(unitevt,energycol,irow,1,nreadthistime,0.0,energy,anyf
     ~      ,status)
        if (qspecweight) then
          call ftgcve(unitevt,specweightcol,irow,1,nreadthistime,0.0
     ~        ,specweight,anyf,status)
        else
          do i=1,nreadthistime
            specweight(i)=1.0
          enddo
        endif

        if (status.ne.0) call exit_fitsio (evtfile,status)
        
        do i=1,nreadthistime
c           1) is this photon in the right CCD?
          useit = .true.
          if (useccd) then
            useit = ccdid(i).ge.ccd_id_max .and. ccdid(i).le.ccd_id_min
          endif

          if (useit) then
            
c           calculate its coordinates in the region space
            xpix = x(i)
            ypix = y(i)
            k = worldpos (xpix,ypix,dcrval1,dcrval2,dcrpix1,dcrpix2,dcdelt1
     ~          ,dcdelt2,dcrota2,dtype,ra,dec)
            k = xypix (ra,dec,rcrval1,rcrval2,rcrpix1,rcrpix2,rcdelt1
     ~          ,rcdelt2,rcrota2,rtype,xpix,ypix)

            ir = nint(xpix)
            jr = nint(ypix)

            if (ir.ge.1.and.ir.le.nxreg.and.jr.ge.1.and.jr.le.nyreg) then
              k = reg(ir,jr)
              if (k.gt.0) then
                useit = .true.
                lreg = k
              else
                useit = .false.
              endif
            else
              useit = .false.
            endif

          endif

          if (useit) then
            ichan = (pi(i)-echanmin)/specbin+1
            if (ichan.gt.nchan.or.ichan.lt.1) useit = .false.
          endif

          if (useit) then
            idx = (detx(i)-detxmin)/bindetmap+1
            idy = (dety(i)-detymin)/bindetmap+1
            if (idx.lt.1.or.idx.gt.ndetx.or.idy.lt.1.or.idy.gt.ndety) then
              useit = .false.
            endif
          endif

          if (useit) then
            call increment_data (ZHVAL(pspec),nchan,nreg,ichan,ireg(lreg)
     ~          ,specweight(i))
            if (qspecweight) then
              call increment_data (ZHVAL(perror),nchan,nreg,ichan,ireg(lreg)
     ~            ,specweight(i)**2)
            endif
            if (energy(i).ge.edetmapmin.and.energy(i).le.edetmapmax) then
              ndetphot = ndetphot + 1
              if (ndetphot.gt.ndetphotmax) then
                ndetphotmax = ndetphotmax + 16384*8
                ZHREALLOC(ZHVAL(pdetx),ZHVAL(pdetx),ndetphotmax,'i')
                ZHREALLOC(ZHVAL(pdety),ZHVAL(pdety),ndetphotmax,'i')
                ZHREALLOC(ZHVAL(pphotreg),ZHVAL(pphotreg),ndetphotmax,'i')
              endif
              call store_detphot_data (idx,idy,ireg(lreg),
     ~            ZHVAL(pdetx),ZHVAL(pdety),ZHVAL(pphotreg),ndetphot)
            endif
          endif
            
        enddo
      enddo
      if (qspecweight) then
        call sqrt_data (ZHVAL(perror),nchan,nreg)
      endif

c Read GTI information
      call get_cl_par ('gtifile',gtifile)
      if (defined(gtifile)) then
        status = 0
        call ftgiou (unitgti,status)
        call ftnopn (unitgti,gtifile,0,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        call FTMNHD (unitgti, -1,'GTI',0,status)
        if (status.ne.0) then
          status = 0
          call FTMNHD (unitgti, -1,'STDGTI',0,status)
        endif
        call FTGCNO (unitgti,.false.,'*START*',tstartcol,status)
        call FTGCNO (unitgti,.false.,'*STOP*',tstopcol,status)
        call ftgnrw (unitgti,ngti,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
        if (ngti.gt.ngtimax) call exiterror ('Ngti too large')
        call ftgcvd (unitgti,tstartcol,1,1,ngti,0.0d0,tstart,anyf,status)
        call ftgcvd (unitgti,tstopcol, 1,1,ngti,0.0d0,tstop, anyf,status)
        call ftclos(unitgti,status)
        call ftfiou(unitgti,status)
        if (status.ne.0) call exit_fitsio (gtifile,status)
      else                      
! GTI file not set in the command line; try to find the GTI extension in the
! events file
        call FTMNHD (unitevt, -1,'GTI',0,status)
        if (status.ne.0) then
          status = 0
          call FTMNHD (unitevt, -1,'STDGTI',0,status)
        endif
        if (status.ne.0) then
          write (0,*)
     ~        'GTI file not set and GTI extension not found in the events file'
          call exit_fitsio (evtfile,status)
        endif

        call FTGCNO (unitevt,.false.,'*START*',tstartcol,status)
        call FTGCNO (unitevt,.false.,'*STOP*',tstopcol,status)
        call ftgnrw (unitevt,ngti,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
        if (ngti.gt.ngtimax) call exiterror ('Ngti too large')
        call ftgcvd (unitevt,tstartcol,1,1,ngti,0.0d0,tstart,anyf,status)
        call ftgcvd (unitevt,tstopcol, 1,1,ngti,0.0d0,tstop, anyf,status)
        if (status.ne.0) call exit_fitsio (evtfile,status)
      endif

      texpos=0
      do i=1,ngti
        texpos=texpos+tstop(i)-tstart(i)
      enddo

      call get_cl_par ('bg_renorm',arg)
      if (defined (arg)) then
        read (arg,*) w
        write (0,*) ' --- will rescale exposure by ',w
        texpos = texpos / w
      endif

c Save the data
      backscal = 1
      areascal = 1

      do lreg = 1,nreg
        write (0,*) '--- Saving ',lreg
        call find_reg_boundaries (ZHVAL(pdetx),ZHVAL(pdety),ZHVAL(pphotreg),
     ~      ndetphot,lreg,imin,imax,jmin,jmax)
        if (imin.gt.imax.or.jmin.gt.jmax) then
! This can be only if no photons were detected in this region
          write (0,*) ' *** Region',kreg(lreg),' has no photons'
          goto 100
        endif
        ZHMALLOC(pdetimg,(imax-imin+1)*(jmax-jmin+1),'j')
        call make_specreg_image (ZHVAL(pdetx),ZHVAL(pdety),ZHVAL(pphotreg)
     ~      ,ndetphot,lreg,ZHVAL(pdetimg),(imax-imin+1),(jmax-jmin+1),
     ~      imin,jmin)
        

c       
        outname = outkey
        call strcat (outname,'_')
        write (arg,*) kreg(lreg)
        call rmblanks(arg)
        call strcat (outname,arg)
        if (useccd) then
          call strcat (outname,'_')
          call strcat (outname,outchipname)
        endif
        call strcat (outname,'.')
        call strcat (outname,phaaffix)

        call unlink (outname)
        status = 0
        call ftgiou (unitpha,status)
        if (status.ne.0) call exit_fitsio ('I/O unit',status)
        call ftinit (unitpha,outname,0,status)
        if (status.ne.0) call exit_fitsio (outname,status)
        
        naxis=2
        pcount=0
        gcount=1
        naxes(1)=(imax-imin+1)
        naxes(2)=(jmax-jmin+1)
        simple=.true.
        extend=.true.
        bitpix=32
        call ftphpr(unitpha,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~      ,status)
        call ftpdef(unitpha,bitpix,naxis,naxes,pcount,gcount,status)
        if (status.ne.0) call exit_fitsio (outname,status)
        
        call ftp2dj(unitpha,0,naxes(1),naxes(1),naxes(2),ZHVAL(pdetimg),status)
        if (status.ne.0) call exit_fitsio (outname,status)
        
        call ftpkyj(unitpha,'CRPIX1',1,' ',status)
        call ftpkyf(unitpha,'CRVAL1',(1.0+bindetmap)/2.0+(imin-1)*bindetmap,5,' ',status)
        call ftpkyj(unitpha,'CDELT1',bindetmap,' ',status)
        call ftpkys(unitpha,'CTYPE1','DETX-LIN',' ',status)
        call ftpkys(unitpha,'CUNIT1','detpix',' ',status)
        call ftpkyj(unitpha,'CRPIX2',1,' ',status)
        call ftpkyf(unitpha,'CRVAL2',(1.0+bindetmap)/2.0+(jmin-1)*bindetmap,5,' ',status)
        call ftpkyj(unitpha,'CDELT2',bindetmap,' ',status)
        call ftpkys(unitpha,'CTYPE2','DETY-LIN',' ',status)
        call ftpkys(unitpha,'CUNIT2','detpix',' ',status)

        call ftpkyf(unitpha,'CROTA2',0.0,5,' ',status)
        if (status.ne.0) call exit_fitsio (outname,status)


        specdtype = 1           ! counts, not count rates
        qerror = qspecweight    ! stat err. not passed unless specweight used 
        qsys = .false.          ! sys errors not passed
        qqual = .false.         ! quality not passed
        qgroup = .false.        ! no grouping passed


        if (qspecweight) then
          call extract_spec_arrays (ZHVAL(perror),nchan,nreg,lreg,channel
     ~        ,serr,echanmin)
        endif
        
        call extract_spec_arrays (ZHVAL(pspec),nchan,nreg,lreg,channel,counts
     ~      ,echanmin)
        

        backfil = 'NONE'
        respfil = 'NONE'
        ancrfil = 'NONE'


        call wtpha1 (unitpha,
     ~      0,0,hist,0,comm,'CHANDRA','ACIS',' ','NONE',
     &      '1.1.0','TOTAL',echanmindata,
     &      texpos,areascal,backfil,backscal,'NONE', 
     &      0.0,respfil,ancrfil,echanmaxdata-echanmindata+1,'PI',
     &      channel,counts,specdtype,qerror,serr,qsys,
     &      syserr,qqual,qualty,qgroup,grping,nchan,
     &      status)
          
        call ftclos(unitpha,status)
        call ftfiou(unitpha,status)

        ZHFREE(ZHVAL(pdetimg))
 100    continue
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine read_par_wcs_keys (wcsparfile,
     ~    crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2,type)
      implicit none
      character*(*) wcsparfile
      double precision crval1,crval2,crpix1,crpix2,cdelt1,cdelt2,crota2
      character*(*) type
      character*30 name
      character*80 value
      logical defined
      integer lnblnk
      character wcspfile*200


      wcspfile = wcsparfile

c a) Try to read crval1
      name = 'crval1'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) crval1
      else
        write (0,*) name(1:6),' not found in ',
     ~      wcspfile(1:lnblnk(wcspfile))
        call exit(1)
      endif

      name = 'crval2'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) crval2
      else
        write (0,*) name(1:6),' not found in ',
     ~      wcspfile(1:lnblnk(wcspfile))
        call exit(1)
      endif

      name = 'crpix1'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) crpix1
      else
        write (0,*) name(1:6),' not found in ',
     ~      wcspfile(1:lnblnk(wcspfile))
        call exit(1)
      endif
      
      name = 'crpix2'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) crpix2
      else
        write (0,*) name(1:6),' not found in ',
     ~      wcspfile(1:lnblnk(wcspfile))
        call exit(1)
      endif

      name = 'cdelt1'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) cdelt1
      else
        write (0,*) name(1:6),' not found in ',
     ~      wcspfile(1:lnblnk(wcspfile))
        call exit(1)
      endif
       
      name = 'cdelt2'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) cdelt2
      else
        write (0,*) name(1:6),' not found in ',
     ~      wcspfile(1:lnblnk(wcspfile))
        call exit(1)
      endif
      
      name = 'crota2'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        read (value,*) crota2
      else
        crota2=0
      endif
      
      name = 'ctype1'
      call get_pf_par (wcsparfile,name,value)
      if (defined(value)) then
        type = value(5:)
      else
        type = '-TAN'
      endif


      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine get_image_size (file,unit,nx,ny)
      implicit none
      integer unit
      integer nx,ny
      character*(*) file
      integer status,bitpix,naxis,pcount,gcount,naxes(10)
      logical simple,extend
      

      status = 0
      call ftghpr(unit,2,simple,bitpix,naxis,naxes,pcount,gcount,extend
     ~    ,status)
      if (status.ne.0) call exit_fitsio (file,status)

      nx=naxes(1)
      ny=naxes(2)
      
      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine get_col_range (unit,col,colmin,colmax,colmindef,colmaxdef)
      implicit none
      integer unit, col, colmin,colmax,colmindef, colmaxdef
      character buff*30, comment*80
      integer status

      
      write (buff,'(''TLMIN'',i4)') col
      call rmblanks(buff)
      status = 0
      call ftgkyj (unit,buff,colmin,comment,status)
      if (status.ne.0) then
        colmin = colmindef
        status = 0
      endif

      write (buff,'(''TLMAX'',i4)') col
      call rmblanks(buff)
      call ftgkyj (unit,buff,colmax,comment,status)
      if (status.ne.0) then
        colmax = 1024
        status = 0
      endif

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine set_to_zero (data,nx,ny)
      implicit none
      integer nx,ny
      real data (nx*ny)
      integer i

      do i=1,nx*ny
        data (i)=0.0
      enddo
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine increment_data (data,nx,ny,i,j,incr)
      implicit none
      integer nx,ny
      real data (nx,ny)
      integer i,j
      real incr
      
      data(i,j)=data(i,j)+incr
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine sqrt_data (data,nx,ny)
      implicit none
      integer nx,ny
      real data (nx,ny)
      integer i,j
      
      do i=1,nx
        do j=1,ny
          data(i,j)=sqrt(max(data(i,j),0.0))
        enddo
      enddo

      return
      end


*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine store_detphot_data (ix,iy,ireg,x,y,reg,i)
      implicit none
      integer ix,iy,ireg
      integer i
      integer*2 x(*), y(*), reg(*)

      x(i)=ix
      y(i)=iy
      reg(i)=ireg
      
      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine make_specreg_image (x,y,reg,nphot,ireg,img,nx,ny,imin,jmin)
      implicit none
      integer nx,ny,nphot,ireg
      integer*2 x(nphot), y(nphot),reg(nphot)
      integer img(nx,ny)
      integer imin,jmin

      integer k,i,j

      do i=1,nx
        do j=1,ny
          img(i,j)=0
        enddo
      enddo

      do k=1,nphot
        if (reg(k).eq.ireg) then
          i=x(k)-imin+1
          j=y(k)-jmin+1
          if (i.ge.1.and.i.le.nx.and.j.ge.1.and.j.le.ny) then
            img(i,j)=img(i,j)+1
          endif
        endif
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine find_reg_boundaries (x,y,reg,nphot,ireg,imin,imax,jmin,jmax)
      implicit none
      integer nphot
      integer*2 x(nphot), y(nphot), reg(nphot)
      integer ireg, imin,imax,jmin,jmax
      integer i,j,k
      
      imin= 1 000 000 000
      imax=-1 000 000 000
      jmin= 1 000 000 000
      jmax=-1 000 000 000

      do k=1,nphot
        if (reg(k).eq.ireg) then
          i = x(k)
          j = y(k)
          imin = min(imin,i)
          jmin = min(jmin,j)
          imax = max(imax,i)
          jmax = max(jmax,j)
        endif
      enddo

      return
      end

*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

      subroutine extract_spec_arrays (spec,nchan,nreg,ireg,channel,counts
     ~    ,minchan)
      implicit none
      integer nchan,nreg
      real spec(nchan,nreg)
      integer ireg
      integer channel(nchan),minchan
      real counts(nchan)

      integer i

      do i=1,nchan
        counts(i)=spec(i,ireg)
        channel(i)=(i-1)+minchan
      enddo
      return
      end


