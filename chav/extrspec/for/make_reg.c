#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

long lnblnk_ (char*);
int nint (float x);

int make_sector (short *img, int nx, int ny, 
		  float xx, float yy, float r1, float r2,
		  float a1, float a2, int value);

void make_region_image_ (short *img, long *nx0, long *ny0,char *region,
			 long *ns,int *val0)
{
  int i,j,ii,jj,n;
  int rmfile;
  int x,y,r,r1,r2,x1,x2,y1,y2,rr1,rr2,rr;
  FILE *bkgd, *src;
  char filename[100];
  char type[100];
  int dist;
  float a1,a2,a;
  int quse;
  int nx,ny;
  int value;
  
  region[*ns]='\0';


  nx=*nx0;
  ny=*ny0;
  value=*val0;
  
  n=nx*ny;
  
  for(i=0;i<n;i++)
    img[i]=value;
  
  /* determine if src is described in a file */  
  if (strchr(region,' ') == NULL && strchr(region,'(') == NULL)
    {
      rmfile=0;
      if (strncmp (region,"file:",5) == 0 ) {
	strcpy(filename,region+5); }
      else {
	strcpy(filename,region);
      }
    }
  else
    /* write the string to a temporary file */
    {
      sprintf(filename,"/tmp/.region.%d\0",getpid());
      rmfile=1;
      src=fopen(filename,"w");
      if (src==NULL)
	{
	  fprintf(stderr,"Failed to open for writing: %s\n",filename);
	  exit(1);
	}
      fprintf(src,"%s",region);
      fclose(src);
    }
  src=fopen(filename,"r");
  if (src==NULL)
    {
      fprintf(stderr,"Failed to open file: %s\n",filename);
      exit(1);
    }
      
  make_region_image_0 (img,nx,ny,src);
  fclose(src);
  if (rmfile==1)
    unlink(filename);
} 
  
int make_region_image_0 (short *img, int nx, int ny, FILE *src)
{
  int i,j,ii,jj,n;
  float xx,yy;
  float r1,r2;
  int x,y,x1,x2,y1,y2,rr1,rr2,rr;
  float r;
  char type[100];
  int dist;
  float a1,a2,a;
  int quse;
  float junk;
  int value;
  int xc,yc;
  float ax,ay,angle,cosa,sina,dx,dy,deltax,deltay;
  float sidex,sidey;
  char buff[2000];
  int nread;
  int qsaoimage;
  int exclude, firsttype;
  
  n=nx*ny;


  while (fgets(buff,1000,src)!=NULL)
    {
      if (buff[0]!='#')
	{
	  qsaoimage=saoimage2reg(buff);
	  if (sscanf(buff,"%s",type)==1)
	    {
	      if ( type[0] == '+' || type[0] == '-' ) {
		if ( type[0]=='-' )
		  exclude = 1;
		else
		  exclude = 0;
		firsttype = 1;
	      }
	      else {
		exclude = 0;
		firsttype = 0;
	      }
		  
	      switch (type[firsttype])
		{
		  
		case 'p': /* single pixel */
		  {
		    nread=sscanf(buff,"%s %f %f %d",type,&xx,&yy,&value);
		    if (nread<3||nread>4)
		      {
			fprintf(stderr,
			   "wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    else if (nread==3)
		      {
			value=1;
		      }
		    if (exclude) value = 0;
		    x=nint(xx);
		    y=nint(yy);
		    i=x+(nx)*(y-1)-1;
		    if ( x >=1 && x <= nx && y >=1 && y <=ny ) 
		      img[i]=value;
		    continue;
		  }

		case 'c': /* circle */
		  {
		    nread=sscanf(buff,"%s %f %f %f %d",type,&xx,&yy,&r,&value);
		    if (nread<4||nread>5)
		      {
			fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    else if (nread==4)
		      {
			value=1;
		      }
		    if (exclude) value = 0;
		    rr=r*r;
		    x=nint(xx);
		    y=nint(yy);
		    for (i=-r;i<=r;i++)
		      for (j=-r;j<=r;j++)
			{
			  dist=i*i+j*j;
			  if (dist<=rr)
			    {
			      if (x+i>=1&&x+i<=nx&&y+j>=1&&y+j<=ny)
				{
				  ii=(x+i)+(nx)*(y+j-1)-1;
				  img[ii]=value;
				}
			    }
			}
		    continue;
		  }
		  
		case 'b': /* box */
		  {
		    if (sscanf(buff,"%s %f %f %e %e %e %d",
			       type,&xx,&yy,&sidex,&sidey,&angle,&value)!=7)
		      {
			if (sscanf(buff,"%s %f %f %e %e %e",
				   type,&xx,&yy,&sidex,&sidey,&junk)==6)
			  {
			    if (qsaoimage) {value=1; angle=junk;}
			    else           {value=junk;angle=0;}
			  }
			else
			  {
			    fprintf(stderr,
				    "wrong structure of region descriptor\n");
			    exit(2);
			  }
		      }
		    if (exclude) value = 0;
		    xc=nint(xx);
		    yc=nint(yy);
		    rr=sqrt(sidex*sidex/4+sidey*sidey/4)+1;
		    sina=sin(angle*3.1415926536/180.0);
		    cosa=cos(angle*3.1415926536/180.0);
		    for (i=xc-rr;i<=xc+rr;i++)
		      for (j=yc-rr;j<=yc+rr;j++)
			{
			  dx=i-xc;
			  dy=j-yc;
			  deltax=dx*cosa+dy*sina;
			  deltay=-dx*sina+dy*cosa;
			  if (deltax<0) deltax=-deltax;
			  if (deltay<0) deltay=-deltay;
			  
			  if (deltax<=sidex/2&&deltay<=sidey/2)
			    {
			      if (i>=1&&i<=nx&&j>=1&&j<=ny)
				{
				  ii=i+(nx)*(j-1)-1;
				  img[ii]=value;
				}
			    }
			}
		    
		    continue;
		  }

		case 'e': /* ellipse */
		  {
		    if (sscanf(buff,
			       "%s %f %f %e %e %e %d",
			       type,&xx,&yy,&ax,&ay,&angle,&value)!=7)
		      {
			if (sscanf(buff,"%s %f %f %e %e %e",
				   type,&xx,&yy,&sidex,&sidey,&junk)==6)
			  {
			    if (qsaoimage) {value=1; angle=junk;}
			    else           {value=junk;angle=0;}
			  }
			else
			  {
			    fprintf(stderr,
				    "wrong structure of region descriptor %s\n",type);
			    exit(2);
			  }
		      }
		    if (exclude) value = 0;
		    xc=nint(xx);
		    yc=nint(yy);
		    rr=ay;
		    if(ax>ay)rr=ax;
		    sina=sin(angle*3.1415926536/180.0);
		    cosa=cos(angle*3.1415926536/180.0);
		    for (i=xc-rr;i<=xc+rr;i++)
		      for (j=yc-rr;j<=yc+rr;j++)
			{
			  dx=i-xc;
			  dy=j-yc;
			  deltax=dx*cosa+dy*sina;
			  deltay=-dx*sina+dy*cosa;
			  if ((deltax/ax)*(deltax/ax)
			      +(deltay/ay)*(deltay/ay)<1)
			    {
			      if (i>=1&&i<=nx&&j>=1&&j<=ny)
				{
				  ii=i+(nx)*(j-1)-1;
				  img[ii]=value;
				}
			    }
			}
		    continue;
		  }

		case 'a': /*annulus*/
		  {
		    if (sscanf(buff,"%s %f %f %f %f %d",
			       type,&xx,&yy,&r1,&r2,&value)!=6)
		      {
			fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    if (exclude) value = 0;
		    x=nint(xx);
		    y=nint(yy);
		    rr1=r1*r1;
		    rr2=r2*r2;
		    for (i=-r2;i<=r2;i++)
		      for (j=-r2;j<=r2;j++)
			{
			  dist=i*i+j*j;
			  if (dist<=rr2 && dist > rr1)
			    {
			      if (x+i>=1&&x+i<=nx&&y+j>=1&&y+j<=ny)
				{
				  ii=(x+i)+(nx)*(y+j-1)-1;
				  img[ii]=value;
				}
			    }
			}
		    continue;
		  }
		  
		case 's': /*sector*/
		  {
		    if (sscanf(buff,"%s %f %f %f %f %e %e %d",
			       type,&xx,&yy,&r1,&r2,&a1,&a2,&value)!=8)
		      {
			fprintf(stderr,
			"wrong structure of region descriptor %s\n",type);
			exit(2);
		      }
		    
		    if (exclude) value = 0;

		    while (a2>360.0)
		      a2-=360.0;
		    while (a1>360.0)
		      a1-=360.0;

		    if (a2<a1)
		      {
			/* the user wants to go through 0 */
			make_sector (img,nx,ny,xx,yy,r1,r2,a1,360.0,value);
			make_sector (img,nx,ny,xx,yy,r1,r2,0.0,a2,value);
		      }
		    else
		      {
			make_sector (img,nx,ny,xx,yy,r1,r2,a1,a2,value);
		      }
		    continue;
		  }
	  
		default:
		  fprintf(stderr,"unkown region type: %s\n",type),exit(2);
		}
	    }
	}
    }
}

int make_sector (short *img, int nx, int ny, 
		 float xx, float yy, float r1, float r2,
		  float a1, float a2, int value)
{
  int rr1,rr2,x,y;
  int i,j,ii,dist;
  int quse;
  float a;

  rr1=nint(r1*r1);
  rr2=nint(r2*r2);
  x=nint(xx);
  y=nint(yy);
  for (i=-r2;i<=r2;i++)
    for (j=-r2;j<=r2;j++)
      {
	dist=i*i+j*j;
	if (dist<=rr2 && dist > rr1)
	  {
	    quse=0;
	    if (dist == 0)
	      {
		quse=1;
	      }
	    else
	      {
		a=acos((i+0.0)/sqrt(dist));
		if (j<0)
		  {
		    a=2.0*3.1415926536-a;
		  }
		a=a*180.0/3.1415926536;
		if (a2==360.0)
		  {
		    if ((a>a1 && a<=a2)||a==0)
		      {
			quse=1;
		      }
		  }
		else
		  if (a>a1 && a<=a2)
		    {
		      quse=1;
		    }
		
	      }
	    if (quse==1)
	      {
		if (x+i>=1&&x+i<=nx&&y+j>=1&&y+j<=ny)
		  {
		    ii=(x+i)+(nx)*(y+j-1)-1;
		    img[ii]=value;
		  }
	      }
	  }
      }
}
  


int saoimage2reg (char *string)
{

  int n,i;
  if (strchr(string,'(')==NULL)
    return 0;
  
  n=strlen(string);
  for (i=0;i<n;i++)
    {
      switch (string[i])
	{
	case '(':
	  {
	    string[i]=' ';
	    break;
	  }
	case ')':
	  {
	    string[i]=' ';
	    break;
	  }
	case ',':
	  {
	    string[i]=' ';
	    break;
	  }
	case '+':
	  {
	    string[i]=' ';
	    break;
	  }
	default: string[i]=tolower(string[i]);
	}
    }
  strcat (string,"  1");
  return 1;
}

int nint (float x)
{
  register int i;
  i = floor(x);
  if (x-i>0.5) 
    return i+1;
  else
    return i;
}
