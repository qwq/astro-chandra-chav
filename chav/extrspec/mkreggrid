#!/usr/bin/perl

use Term::Complete;

if ( @ARGV ) {
  $griddescr = join (' ',@ARGV);
}
else {
  $griddescr = Complete ("Grid description (box or sectors)? ","box","sectors");
#  $griddescr = <STDIN>; chop $griddescr;
}

if ( $griddescr =~ /^\s*[\+-]*box/i ) {
  &make_box_grid;
}
else {
  if ( $griddescr =~ /^\s*[\+-]*sector/i ) {
    & make_sectors_grid;
  }
  else {
    die "I can do only sectors and boxes";
  }
}


########################################################################

sub make_box_grid {
  if ( $griddescr =~ /^\s*[\+-]*box\(.+\)/i ) {
    $reg = $griddescr;
  }
  else {
    print STDERR "Enter SAOimage box region: ";
    $reg = <STDIN>; chop $reg;
  }
  $reg =~ s/^.*BOX/box/i;
  $reg =~ s/\).*/\)/;
  $reg =~ s/[\(\),]/ /g;
  $reg =~ s/^box/ /g;
  $reg =~ s/^\s*//;
  ($x,$y,$w,$h,$angle) = split (/\s+/,$reg);
  if ( ! $angle ) { $angle = 0; }
  print STDERR "Center $x,$y; width $w; height $h; angle $angle\n";

  if ( $griddescr =~ /\s\d+x\d+\s*$/ ) {
    $grid = $griddescr;
    $grid =~ s/.*\s(\d+x\d+)\s*$/$1/;
  }
  else {
    print STDERR "Enter grid size (e.g. 10x5): ";
    $grid = <STDIN>; chop $grid;
  }
  $grid =~ s/x/ /i;
  $grid =~ s/^\s+//;
  ($ngridx,$ngridy) = split (/\s+/,$grid);
  print STDERR "$ngridx by $ngridy grid\n";

  $w1 = $w/$ngridx;
  $h1 = $h/$ngridy;

  $ireg = 0;

  $cosa = cos($angle*3.14159265358979/180.0);
  $sina = sin($angle*3.14159265358979/180.0);

  foreach $i ( 1 .. $ngridx ) {
    foreach $j ( 1 .. $ngridy ) {
      $cx = $x-$w/2+$w1/2+($i-1)*$w1;
      $cy = $y-$h/2+$h1/2+($j-1)*$h1;
      
      $dx = $cx - $x;
      $dy = $cy - $y;
      
      $cx = $x + $dx*$cosa - $dy*$sina;
      $cy = $y + $dx*$sina + $dy*$cosa;
      
      $ireg ++;

      printf "box(%g,%g,%g,%g,%g)\t%d\n", $cx, $cy, $w1, $h1, $angle, $ireg;
    }
  }
}

########################################################################

sub make_sectors_grid {
  $center = $griddescr;
  $center =~ s/^\s*sectors?\s*//;
  if ( ! ( $center =~ /\d+\.?\d*\s+\d+\.?\d*/ ) ) {
    print STDERR "Enter center x and y: ";
    $center = <STDIN>; chop $center; $center =~ s/^\s+//;
  }
  ($x,$y) = split (/\s+/,$center); 
  print STDERR "Center $x,$y\n";

  $radii = $griddescr;
  $radii =~ s/.*r=\s*(\d+\.?\d*\s*:\d+\.?\d*\s*:\d+\.?\d*)\.*/$1/;
  if ( ! $radii ) {
    print STDERR "Enter radii (rmin:rmax:rstep): ";
    $radii = <STDIN>; chop $radii; $radii =~ s/^\s+//;
  }
  $radii =~ s/:/ /g;
  ($rmin, $rmax, $rstep) = split (/\s+/,$radii);
  print STDERR "rmin=$rmin rmax=$rmax rstep=$rstep\n";

  $angles = $griddescr;
  $angles =~ s/.*a(ngle(s)?)?=\s*(\d+\.?\d*\s*:\d+\.?\d*\s*:\d+\.?\d*)\.*/$3/;
  if ( ! $angles ) {
    print STDERR "Enter angles (amin:amax:astep): ";
    $angles = <STDIN>; chop $angles; $angles =~ s/^\s+//;
  }
  $angles =~ s/:/ /g;
  ($amin, $amax, $astep) = split (/\s+/,$angles);
  print STDERR "amin=$amin amax=$amax astep=$astep\n";

  $r1 = $rmin;
  $ireg = 0;
  while ( $r1 < $rmax ) {
    $r2 = $r1 + $rstep; if ( $r2 > $rmax ) {$r2 = $rmax;}
   
    $a1 =$amin;
    while ( $a1 < $amax ) {
      $a2 = $a1 + $astep; if ( $a2 > $amax ) {$a2 = $amax;}
      $ireg ++;
      printf "sector(%g,%g,%g,%g,%g,%g)\t%d\n",
      $x,$y,$r1,$r2,$a1,$a2,$ireg;
      
      $a1 += $astep;
    }

    $r1 += $rstep;
  }

}
